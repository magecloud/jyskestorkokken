/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'mage/translate',
    'gls'
], function ($, quote, $t, gls) {
    'use strict';
    return {
        validate: function () {
            let shippingMethod = quote.shippingMethod();

            //check if exist shippingmethod, for virtual products is null
            if ( shippingMethod != null ) {
                if (shippingMethod.carrier_code === 'gls') {
                    let method = [shippingMethod.carrier_code, shippingMethod.method_code].join('_');
                    let params = {};
                    if (method.indexOf('gls_business') === 0) {
                        if ($('#gls-business-comment').length) {
                            let comment = $('#gls-business-comment').val();
                            if (comment) {
                                params['comment'] = comment;
                            }
                        }
                    } else if (method.indexOf('gls_parcelshop') === 0) {
                        let parcelShopSelectorId = '#gls-parcelshop-pickup-id';
                        if ($(parcelShopSelectorId).length) {
                            let pickupId = $(parcelShopSelectorId).val();
                            if (pickupId) {
                                params['pickup_id'] = pickupId;
                                $.each($(parcelShopSelectorId + ' option[value=' + pickupId + ']').data(), function (key, value) {
                                    if (key !== 'bind') {
                                        params['pickup_' + key] = value;
                                    }
                                });
                            } else {
                                alert($t('Please choose a Parcelshop'));
                                return false;
                            }
                        }
                        let glsParcelshopPickupname = $('#gls-parcelshop-pickupname');
                        if (glsParcelshopPickupname.length) {
                            let pickupName = glsParcelshopPickupname.val();
                            if (pickupName) {
                                params['pickup_name'] = pickupName;
                            } else {
                                alert($t('Please enter name of person who will pick up the package'));
                                return false;
                            }
                        }
                        let glsParcelshopNotification = $('#gls-parcelshop-notification');
                        if (glsParcelshopNotification.length) {
                            let notification = glsParcelshopNotification.val();
                            if (notification) {
                                params['notification'] = notification;
                            } else {
                                alert($t('Please enter mobile number for notification about the package'));
                                return false;
                            }
                        }
                    } else if (method.indexOf('gls_private') === 0) {
                        let glsPrivateNotification = $('#gls-private-notification');
                        if (glsPrivateNotification.length) {
                            let notification = glsPrivateNotification.val();
                            if (notification) {
                                params['notification'] = notification;
                            } else {
                                alert($t('Please enter mobile number for notification about the package'));
                                return false;
                            }
                        }
                        let glsPrivateComment = $('#gls-private-comment');
                        if (glsPrivateComment.length && glsPrivateComment.is(':visible')) {
                            let comment = glsPrivateComment.val();
                            if (comment) {
                                params['comment'] = comment;
                            } else {
                                alert($t('Please enter where the package can be placed, in the case no ones there'));
                                return false;
                            }
                        }
                    }
                    if (Object.keys(params).length) {
                        gls.update(params);
                    } else {
                        gls.clear();
                    }
                } else {
                    gls.clear();
                }
                return true;
            }

        }
    }
});
