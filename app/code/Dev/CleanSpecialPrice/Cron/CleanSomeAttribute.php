<?php

namespace Dev\CleanSpecialPrice\Cron;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Catalog\Api\ProductRepositoryInterface;

class CleanSomeAttribute
{
    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;


    /**
     * ProcessSpecialProducts constructor.
     *
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param DateTime $date
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepository,
        DateTime $date
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->date = $date;
    }

    public function execute()
    {
        $currentDate = $this->date->date("Y-m-d H:i:s", mktime(0, 0, 0));
        $expiredProductsCollection = $this->productCollectionFactory->create();
        $expiredProductsCollection
            ->addFieldToFilter(
                'special_to_date',
                [
                    'notnull' => 1,
                    'lteq' => $currentDate
                ]
            )
            ->addFieldToFilter(
                'special_price',
                [
                    'notnull' => 1
                ]
            );

        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($expiredProductsCollection as $product) {
            try {
                $product = $this->productRepository->get($product->getSku(), true);
                if ( $product->getSpecialToDate() < $currentDate  ) {
                    $product->setSpecialToDate(null);
                    $product->setSpecialPrice(null);
                    $product->setSpecialFromDate(null);
                    }
                $this->productRepository->save($product);
            } catch (\Exception $e) {
            }
        }
    }
}
