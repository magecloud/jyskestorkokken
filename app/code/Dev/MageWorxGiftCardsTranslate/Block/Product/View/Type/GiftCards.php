<?php

namespace Dev\MageWorxGiftCardsTranslate\Block\Product\View\Type;

class GiftCards extends \MageWorx\GiftCards\Block\Product\View\Type\GiftCards
{

    /**
     * @return string
     */
    public function getGiftCardPrice()
    {
        $from = '';
        $to   = '';

        $min = $this->priceCurrency->convertAndRound($this->getMinPrice());
        $max = $this->priceCurrency->convertAndRound($this->getMaxPrice());

        if ($min == $max) {
            return $this->checkoutHelper->formatPrice($min);
        }

        if ($min !== null) {
            $from = __('Fra ');
        }

        $product = $this->getProduct();

        if ($product->getMageworxGcAllowOpenAmount() && $product->getMageworxGcOpenAmountMax() <= 0) {
            return $from . $this->checkoutHelper->formatPrice($min);
        }

        if ($max) {
            $to = $from ? __(' Til ') : __('To ');
        }

        return $from . $this->checkoutHelper->formatPrice($min) .
            $to . $this->checkoutHelper->formatPrice($max);
    }

    /**
     * @return string
     * changes dateformat from m/d/y to d/m/y
     */
    public function getDateFormat()
    {
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        /** Escape RTL characters which are present in some locales and corrupt formatting */
        $dateFormat = preg_replace('/[^MmDdYy\/\.\-]/', '', $dateFormat);
        $dateFormat = 'd/M/yy';
        return $dateFormat;
    }

}
