<?php

namespace Dev\ProductPdfPriceBlockOverride\Model;

class Products extends \Magedelight\Productpdf\Model\Products
{

    protected function _getPriceBlock($product, &$data)
    {
        $_taxHelper = $this->_taxHelper;
        //$_coreHelper = $this->_coreHelper;
        $currentCurrency = $this->storeManager->getStore()->getCurrentCurrency();
        $data['price'] = array();
        if ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE || $product->getTypeId() == \Magento\GroupedProduct\Model\Product\Type\Grouped::TYPE_CODE) {
            $product = $this->productCollectionFactory->create()->setStore($product->getStore())
                ->addIdFilter(array($product->getId()))
                ->addAttributeToSelect($this->productConfig->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->load()
                ->getFirstItem();
        }
        $_simplePricesTax = ($_taxHelper->displayPriceIncludingTax() || $_taxHelper->displayBothPrices());

        $_minimalPriceValue = $product->getMinimalPrice();

        //$_price = $this->_catalogHelper->getTaxPrice($product, $product->getPrice());
        //$_price = $this->_catalogHelper->getTaxPrice($product, $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue());
        $_price = $this->_catalogHelper->getTaxPrice($product, $product->getFinalPrice());

        //$_regularPrice = $this->_catalogHelper->getTaxPrice($product, $product->getPrice(), $_simplePricesTax);
        $_regularPrice = $this->_catalogHelper->getTaxPrice($product, $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue(), $_simplePricesTax);


        //$_finalPrice = $this->_catalogHelper->getTaxPrice($product, $product->getFinalPrice());
        $_finalPrice = $this->_catalogHelper->getTaxPrice($product, $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue());


        //$_finalPriceInclTax = $this->_catalogHelper->getTaxPrice($product, $product->getFinalPrice(), true);
        //$_finalPriceInclTax = $this->_catalogHelper->getTaxPrice($product, $product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue(), true);
        $_finalPriceInclTax = $_finalPrice;

        if ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::DEFAULT_TYPE || $product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE || $product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL || $product->getTypeId() == \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE) {
            if ($_taxHelper->displayBothPrices()) {
                if ($_finalPrice >= $_price) {

                    if ($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                        $data['price']['excl_tax'] = $currentCurrency->format($product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue('tax'), false);
                    } else {
                        $data['price']['excl_tax'] = $currentCurrency->format($_price, false);
                    }

                    $data['price']['incl_tax'] = $currentCurrency->format($_finalPriceInclTax, false);
                } else {
                    $data['price']['regular'] = $currentCurrency->format($_regularPrice, false);
                    $data['price']['special']['excl_tax'] = $currentCurrency->format($_finalPrice, false);
                    $data['price']['special']['incl_tax'] = $currentCurrency->format($_finalPriceInclTax, false);
                }
            } elseif ($_taxHelper->displayPriceIncludingTax()) {
                if ($_finalPrice >= $_price) {
                    $data['price'] = $currentCurrency->format($_finalPriceInclTax, false);
                } else {
                    $data['price']['regular'] = $currentCurrency->format($_regularPrice, false);
                    $data['price']['special'] = $currentCurrency->format($_finalPriceInclTax, false);
                }
            } else {
                if ($_finalPrice >= $_price) {
                    $data['price'] = $currentCurrency->format($_price, false);
                } else {
                    $data['price']['regular'] = $currentCurrency->format($_regularPrice, false);
                    $data['price']['special'] = $currentCurrency->format($_finalPrice, false);
                }
            }
        } elseif ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
            $_priceModel = $product->getPriceModel();

            list($_minimalPriceTax, $_maximalPriceTax) = $_priceModel->getTotalPrices($product);
            list($_minimalPriceInclTax, $_maximalPriceInclTax) = $_priceModel->getTotalPrices($product, null, true);
            if ($_taxHelper->displayBothPrices()) {
                $data['price']['from']['excl_tax'] = $currentCurrency->format($_minimalPriceTax, false);
                $data['price']['from']['incl_tax'] = $currentCurrency->format($_minimalPriceInclTax, false);
                $data['price']['to']['excl_tax'] = $currentCurrency->format($_maximalPriceTax, false);
                $data['price']['to']['incl_tax'] = $currentCurrency->format($_maximalPriceInclTax, false);
            } elseif ($_taxHelper->displayPriceIncludingTax()) {
                $data['price']['from'] = $currentCurrency->format($_minimalPriceInclTax, false);
                $data['price']['to'] = $currentCurrency->format($_maximalPriceInclTax, false);
            } else {
                $data['price']['from'] = $currentCurrency->format($_minimalPriceTax, false);
                $data['price']['to'] = $currentCurrency->format($_maximalPriceTax, false);
            }
        } elseif ($product->getTypeId() == 'grouped') {
            $_exclTax = $this->_catalogHelper->getTaxPrice($product, $_minimalPriceValue);
            $_inclTax = $this->_catalogHelper->getTaxPrice($product, $_minimalPriceValue, true);
            if ($_taxHelper->displayBothPrices()) {
                $data['price']['starting']['excl_tax'] = $currentCurrency->format($_exclTax, false);
                $data['price']['starting']['incl_tax'] = $currentCurrency->format($_inclTax, false);
            } elseif ($_taxHelper->displayPriceIncludingTax()) {
                $data['price']['starting'] = $currentCurrency->format($_inclTax, false);
            } else {
                $data['price']['starting'] = $currentCurrency->format($_exclTax, false);
            }
        }
    }

}
