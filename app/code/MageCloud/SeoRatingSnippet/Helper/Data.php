<?php

namespace MageCloud\SeoRatingSnippet\Helper;

use Amasty\ShopbyBase\Helper\OptionSetting;
use Magento\Catalog\Model\Layer;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManager;

class Data extends AbstractHelper
{
    /**
     * @var  Layer\Resolver
     */
    private $layerResolver;

    /**
     * @var  OptionSetting
     */
    private $optionHelper;

    /**
     * @var  StoreManager
     */
    private $storeManager;

    /**
     * @var \Amasty\ShopbyBrand\Helper\Data
     */
    private $helper;

   public function __construct(
       Context $context,
       \Amasty\ShopbyBrand\Helper\Data $helper
   ) {
       parent::__construct($context);
       $this->helper = $helper;
   }

    public function getBrandAttributeValue()
    {
        return $this->_request->getParam($this->helper->getBrandAttributeCode());
    }

}
