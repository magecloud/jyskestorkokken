<?php
/**
 * Copyright (c) 2020. Volodymyr Hryvinskyi.  All rights reserved.
 * @author: <mailto:volodymyr@hryvinskyi.com>
 * @github: <https://github.com/hryvinskyi>
 */

declare(strict_types=1);

namespace MageCloud\SeoRatingSnippet\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 */
class Config
{
    /**
     *  Configuration paths
     */
    const XML_PATH_GENERAL_ENABLED_MOULE = 'magecloud_seo_rich_snippet/general/enabled';
    const XML_PATH_GENERAL_VALUE = 'magecloud_seo_rich_snippet/general/value';
    const XML_PATH_GENERAL_COUNT = 'magecloud_seo_rich_snippet/general/count';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Config constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_ENABLED_MOULE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return string
     */
    public function getValue($store = null): string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_VALUE,
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? '0';
    }

    /**
     * @param null $store
     * @return string
     */
    public function getCount($store = null): string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_GENERAL_COUNT,
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? '0';
    }
}