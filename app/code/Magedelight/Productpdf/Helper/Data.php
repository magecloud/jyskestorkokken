<?php

namespace Magedelight\Productpdf\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_storeManager;
    
    protected $_customerSession;
    
    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Store\Model\StoreManager $storeManager, \Magento\Customer\Model\Session $customerSession) {
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }
    
    public function isEnabled()
    {
        $currentUrl = $this->_storeManager->getStore()->getBaseUrl();
        $domain = $this->getDomainName($currentUrl);
        $selectedWebsites = $this->getConfig('md_productpdf/general/select_website');
        $websites = explode(',',$selectedWebsites);
        if(in_array($domain, $websites) && $this->getConfig('md_productpdf/general/enabled') && $this->getConfig('md_productpdf/license/data'))
        {
          return true;
        }else{
          return false;
        }
    }

    public function getDomainName($domain){
        $string = '';
        
        $withTrim = str_replace(array("www.","http://","https://"),'',$domain);
        
        /* finding the first position of the slash  */
        $string = $withTrim;
        
        $slashPos = strpos($withTrim,"/",0);
        
        if($slashPos != false){
            $parts = explode("/",$withTrim);
            $string = $parts[0];
        }
        return $string;
    }

    public function getWebsites()
    {
        $websites = $this->_storeManager->getWebsites();
        $websiteUrls = array();
        foreach($websites as $website)
        {
            foreach($website->getStores() as $store){
                $wedsiteId = $website->getId();
                $storeObj = $this->_storeManager->getStore($store);
                $storeId = $storeObj->getId();
                $url = $storeObj->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                $parsedUrl = parse_url($url);
                $websiteUrls[] = str_replace(array('www.', 'http://', 'https://'), '', $parsedUrl['host']);
            }
        }

        return $websiteUrls;
    }

    public function getConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
    public function isAllowGroups(){
		$config = null;
        $isAllowed = false;
        $value = array();
		if($this->scopeConfig->getValue("md_productpdf/general/enabled"))
		{	
			$customerGroupId = $this->_customerSession->getCustomerGroupId();
        
			if(strlen($this->scopeConfig->getValue("md_productpdf/general/enable_customergroups"))){
				$config = (string)$this->scopeConfig->getValue("md_productpdf/general/enable_customergroups");
                $value = explode(",",$config);
			}
        	if(strlen($config) <= 0){
				$isAllowed = true;
			}elseif(count($value) < 0){
				$isAllowed = true;
			}elseif(in_array($customerGroupId,$value)){
				$isAllowed = true;
			}
        }
        return $isAllowed;
    }
    
    public function getHeaderLogo(){
        $path = null;
        $mediaPath = $this->_storeManager->getStore()->getBaseMediaDir().'/theme/productpdf/header_logo/';
        $file = $this->scopeConfig->getValue('md_productpdf/header_footer/pdf_header_image');
        if(is_string($file) && strlen($file) > 0){
            $path = $mediaPath.''.$file;
        }
        return $path;
    }
}