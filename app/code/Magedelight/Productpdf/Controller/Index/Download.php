<?php
namespace Magedelight\Productpdf\Controller\Index;

class Download extends \Magento\Framework\App\Action\Action
{
    protected $sessionStorage;
    protected $_coreRegistry;
    protected $storeManager;
    protected $fileFactory;
    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Store\Model\StoreManager $storeManager, \Magento\Framework\Session\Storage $sessionStorage, \Magento\Framework\App\Response\Http\FileFactory $fileFactory) {
        $this->storeManager = $storeManager;
        $this->sessionStorage = $sessionStorage;
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $file = $this->getRequest()->getParam('file');
        if(isset($file) && !empty($file)) {
            $pdf = \Zend_Pdf::load($this->storeManager->getStore()->getBaseMediaDir().'/md/product-print/'. $file);
            $this->fileFactory->create(
            $file,
            str_replace('/Annot /Subtype /Link', '/Annot /Subtype /Link /Border[0 0 0]', $pdf->render()),
            \Magento\Framework\App\Filesystem\DirectoryList::SYS_TMP,
            'application/pdf'
        );
        }
    }
}

