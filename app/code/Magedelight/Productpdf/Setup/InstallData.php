<?php
namespace Magedelight\Productpdf\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Module\Dir;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    protected $_storeManager;
    protected $moduleReader;
    public function __construct(
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        EavSetupFactory $eavSetupFactory)
    {
        $this->_storeManager = $storeManager;
        $this->moduleReader = $moduleReader;
        $this->eavSetupFactory = $eavSetupFactory;
    }
    
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $destPath =$this->_storeManager->getStore()->getBaseMediaDir().DIRECTORY_SEPARATOR.'md_product_print'.DIRECTORY_SEPARATOR .'fonts';
        if(!is_dir($destPath))
        {
            @mkdir($destPath, 0777,true);
        }
        $src = $this->moduleReader->getModuleDir(Dir::MODULE_VIEW_DIR,'Magedelight_Productpdf') . DIRECTORY_SEPARATOR . 'adminhtml' . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR .'fonts'.DIRECTORY_SEPARATOR;
        $dst = $destPath . DIRECTORY_SEPARATOR ;
        $dir = opendir($src);
        @mkdir($dst, 0777,true);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
        $this->addRatingImages();
    }
    
    public function addRatingImages(){
        $destPath = $this->_storeManager->getStore()->getBaseMediaDir().DIRECTORY_SEPARATOR.'md_product_print'.DIRECTORY_SEPARATOR;
        if(!is_dir($destPath))
        {
            mkdir($destPath, 0777,true);
        }
        $src = $this->moduleReader->getModuleDir(Dir::MODULE_VIEW_DIR,'Magedelight_Productpdf') . DIRECTORY_SEPARATOR . 'adminhtml' . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR .'images'.DIRECTORY_SEPARATOR;
        $dst = $destPath . DIRECTORY_SEPARATOR ;
        $dir = opendir($src);
        @mkdir($dst, 0777,true);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}