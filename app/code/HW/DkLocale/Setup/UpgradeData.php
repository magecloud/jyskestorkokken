<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 15.06.2018
 * Time: 12:06
 */


namespace HW\DkLocale\Setup;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
	const THEME_NAME = 'frontend/hw/default';

	/**
	 * @var \Magento\Theme\Model\Data\Design\ConfigFactory
	 */
	private $_configFactory;

	/**
	 * @var \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory
	 */
	private $_collectionFactory;

	/**
	 * @var \Magento\Theme\Api\DesignConfigRepositoryInterface
	 */
	private $_designConfigRepository;

	/**
	 *  @var \Magento\Framework\App\Config\Storage\WriterInterface
	 */
	protected $_configWriter;


	/**
	 * UpgradeData constructor.
	 *
	 * @param \Magento\Theme\Api\DesignConfigRepositoryInterface         $designConfigRepository
	 * @param \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $collectionFactory
	 * @param \Magento\Theme\Model\Data\Design\ConfigFactory             $configFactory
	 * @param \Magento\Framework\App\Config\Storage\WriterInterface      $configWriter
	 */
	public function __construct(
		\Magento\Theme\Api\DesignConfigRepositoryInterface $designConfigRepository,
		\Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $collectionFactory,
		\Magento\Theme\Model\Data\Design\ConfigFactory $configFactory,
		\Magento\Framework\App\Config\Storage\WriterInterface $configWriter
	) {
		$this->_designConfigRepository  = $designConfigRepository;
		$this->_collectionFactory       = $collectionFactory;
		$this->_configFactory           = $configFactory;
		$this->_configWriter            = $configWriter;
	}

	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function upgrade(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$setup->startSetup();

		 if (version_compare($context->getVersion(), '0.1.1', '<')) {
			 $this->assignTheme();
		 }

		 if (version_compare($context->getVersion(), '0.1.2', '<')) {
			 $this->_updateVersion012();
		 }

		 if (version_compare($context->getVersion(), '0.1.3', '<')) {
			$this->_updateVersion013();
		 }

		 if (version_compare($context->getVersion(), '0.1.4', '<')) {
			$this->_updateVersion014();
		 }

		$setup->endSetup();
	}

	/**
	 * Assign Theme
	 *
	 * @return void
	 */
	protected function assignTheme()
	{
		$themeCollection = $this->_collectionFactory->create();
		$theme = $themeCollection->getThemeByFullPath(self::THEME_NAME);
		$themeId = $theme->getId();

		$designConfigData = $this->_configFactory->create('default', 0, ['theme_theme_id' => $themeId]);

		$this->_designConfigRepository->save($designConfigData);
	}

	protected function _updateVersion012() {

		$this->_configWriter->save('customer/address/street_lines',1);
	}

	protected function _updateVersion013() {

		$this->_configWriter->save('admin/security/password_is_forced','0');
		$this->_configWriter->save('admin/security/password_lifetime','180');
	}

	protected function _updateVersion014() {

		$this->_configWriter->save('catalog/custom_options/date_fields_order','d,m,y');
		$this->_configWriter->save('catalog/custom_options/time_format','24h');
		$this->_configWriter->save('catalog/custom_options/year_range','1950,2028');
		$this->_configWriter->save('catalog/custom_options/use_calendar',1);
	}


}