<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 15.06.2018
 * Time: 10:25
 */
namespace HW\DkLocale\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface {

	/**
	 *  @var \Magento\Framework\App\Config\Storage\WriterInterface
	 */
	protected $_configWriter;

	/**
	 * InstallData constructor.n9
	 *
	 * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
	 */
	public function __construct(\Magento\Framework\App\Config\Storage\WriterInterface $configWriter) {

		$this->_configWriter = $configWriter;
	}

	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$setup->startSetup();

		/*admin session*/
		$this->_configWriter->save('admin/security/session_lifetime',86400);

		/*Country General options*/
		$this->_configWriter->save('general/country/default','DK');
		$this->_configWriter->save('general/country/destinations','DK');
		$this->_configWriter->save('general/region/display_all',0);

		/*Locale*/
		$this->_configWriter->save('general/locale/timezone','Europe/Copenhagen');
		$this->_configWriter->save('general/locale/code','da_DK');
		$this->_configWriter->save('general/locale/weight_unit','kgs');

		/*Store Information*/
		$this->_configWriter->save('general/store_information/name','Demo Store');
		$this->_configWriter->save('general/store_information/phone','+555 5555');
		$this->_configWriter->save('general/store_information/hours','10-18');
		$this->_configWriter->save('general/store_information/country_id','DK');
		$this->_configWriter->save('general/store_information/postcode','3700');
		$this->_configWriter->save('general/store_information/city','City');
		$this->_configWriter->save('general/store_information/street_line1','Street Line');
		$this->_configWriter->save('general/store_information/merchant_vat_number','1234567');


		/*Web*/
		$this->_configWriter->save('web/seo/use_rewrites',1);

		/*Currency*/
		$this->_configWriter->save('currency/options/base','DKK');
		$this->_configWriter->save('currency/options/default','DKK');


		/*Sales Tax*/
		$this->_configWriter->save('tax/defaults/country','DK');

		/*Sales Shipping*/
		$this->_configWriter->save('shipping/origin/country_id','DK');
		$this->_configWriter->save('shipping/origin/region_id','');
		$this->_configWriter->save('shipping/origin/postcode','');

		$setup->endSetup();
	}
}