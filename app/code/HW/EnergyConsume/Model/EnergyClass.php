<?php

namespace HW\EnergyConsume\Model;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\DB\Ddl\Table;

class EnergyClass extends \Magento\Framework\DataObject implements OptionSourceInterface
{

	protected $_options;
	protected $_attribute;
	protected $_eavEntityAttribute;
	
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavEntityAttribute,
        array $data = []
    ) {
        $this->_eavEntityAttribute = $eavEntityAttribute;
        parent::__construct($data);
    }
	
    public static function getOptionArray()
    {
        return [
			''=>'No class',
			'aaaa'=>'A+++',
			'aaa'=>'A++',
			'aa'=>'A+',
			'a'=>'A',
			'b'=>'B',
			'c'=>'C',
			'd'=>'D',
			'e'=>'E',
			'f'=>'F',
			'g'=>'G'
        ];
    }
	
    public static function getAllOption()
    {
        $options = self::getOptionArray();
        return $options;
    }
	
    public static function getAllOptions()
    {
        $res = [];
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    public static function getOptionText($optionId)
    {
        $options = self::getOptionArray();
        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
	
    public function getFlatColumns()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();

        return [
            $attributeCode => [
                'default' => null,
                'extra' => null,
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Energy Consume Class column',
            ],
        ];
    }
	
    public function getFlatIndexes()
    {
        return [];
    }
	
    public function getFlatUpdateSelect($store)
    {
        return $this->_eavEntityAttribute->getFlatUpdateSelect($this->getAttribute(), $store);
    }
	
    public function setAttribute($attribute)
    {
        $this->_attribute = $attribute;
        return $this;
    }
	
    public function getAttribute()
    {
        return $this->_attribute;
    }
	
    public function addValueSortToCollection($collection, $dir = 'asc')
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $attributeId = $this->getAttribute()->getId();
        $attributeTable = $this->getAttribute()->getBackend()->getTable();
        $linkField = $this->getAttribute()->getEntity()->getLinkField();

        if ($this->getAttribute()->isScopeGlobal()) {
            $tableName = $attributeCode . '_t';
            $collection->getSelect()->joinLeft(
                [$tableName => $attributeTable],
                "e.{$linkField}={$tableName}.{$linkField}" .
                " AND {$tableName}.attribute_id='{$attributeId}'" .
                " AND {$tableName}.store_id='0'",
                []
            );
            $valueExpr = $tableName . '.value';
        } else {
            $valueTable1 = $attributeCode . '_t1';
            $valueTable2 = $attributeCode . '_t2';
            $collection->getSelect()->joinLeft(
                [$valueTable1 => $attributeTable],
                "e.{$linkField}={$valueTable1}.{$linkField}" .
                " AND {$valueTable1}.attribute_id='{$attributeId}'" .
                " AND {$valueTable1}.store_id='0'",
                []
            )->joinLeft(
                [$valueTable2 => $attributeTable],
                "e.{$linkField}={$valueTable2}.{$linkField}" .
                " AND {$valueTable2}.attribute_id='{$attributeId}'" .
                " AND {$valueTable2}.store_id='{$collection->getStoreId()}'",
                []
            );
            $valueExpr = $collection->getConnection()->getCheckSql(
                $valueTable2 . '.value_id > 0',
                $valueTable2 . '.value',
                $valueTable1 . '.value'
            );
        }

        $collection->getSelect()->order($valueExpr . ' ' . $dir);
        return $this;
    }
	
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
	
}
