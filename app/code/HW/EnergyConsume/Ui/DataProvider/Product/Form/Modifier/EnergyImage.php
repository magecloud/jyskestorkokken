<?php

namespace HW\EnergyConsume\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Filesystem;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;

class EnergyImage extends AbstractModifier
{

    protected $locator;
	private $fileInfo;
	private $_storeManager;
	
    public function __construct(
		LocatorInterface $locator,
		\Magento\Store\Model\StoreManagerInterface $storeManager
	)
    {
        $this->locator = $locator;
		$this->_storeManager=$storeManager;
    }

    public function modifyMeta(array $meta)
    {
        //unset($meta[self::CODE_IMAGE_MANAGEMENT_GROUP]);

        return $meta;
    } 

    public function modifyData(array $data)
    {
        $product = $this->locator->getProduct();
        $modelId = $product->getId();

        if ($product->getData('energy_image'))
        {
			$fileName = $product->getData('energy_image');
            $fileInfo = $this->getFileInfo();
			$mime = $fileInfo->getMimeType($fileName);
			$stat = $fileInfo->getStat($fileName);
			
            $data[$modelId][self::DATA_SOURCE_DEFAULT]['energy_image']=array(array(
				'name'=>basename($fileName),
				'url'=>$this->getImageUrl($fileName),
				'size'=>isset($stat) ? $stat['size'] : 0,
				'type'=>$mime
			));
        }

        return $data;
    }
	
    public function getImageUrl($image)
    {
        $url = false;        
        if ($image) {
            if (is_string($image)) {
                $store = $this->_storeManager->getStore();

                $isRelativeUrl = substr($image, 0, 1) === '/';

                $mediaBaseUrl = $store->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                );

                if ($isRelativeUrl) {
                    $url = $image;
                } else {
                    $url = $mediaBaseUrl
                        . ltrim(FileInfo::ENTITY_MEDIA_PATH, '/')
                        . '/'
                        . $image;
                }
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
	
    private function getFileInfo()
    {
        if ($this->fileInfo === null) {
            $this->fileInfo = ObjectManager::getInstance()->get(FileInfo::class);
        }
        return $this->fileInfo;
    }
	
}
