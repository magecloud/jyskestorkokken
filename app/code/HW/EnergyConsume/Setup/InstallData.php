<?php


namespace HW\EnergyConsume\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
	
    public function __construct(
		EavSetupFactory $eavSetupFactory
		)
    {
        $this->eavSetupFactory = $eavSetupFactory;
		
    }
	
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
		
		
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$eavSetup->addAttribute(
			Product::ENTITY,
			'energy_type',
			[
				'group' 					=> 'Energy Consume Data',
				'type'                    	=> 'varchar',
				'label'                   	=> 'Energy Consume Class',
				'input'                   	=> 'select',
				'source' 					=> \HW\EnergyConsume\Model\EnergyClass::class,
				'required' 					=> false,
                'visible' 					=> true,
                'sort_order' 				=> 20,
                'searchable' 				=> false,
                'filterable' 				=> false,
                'comparable' 				=> false,
				'global'                  	=> \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
				'used_in_product_listing' 	=> true,
				'visible_on_front' 			=> false,
				'is_used_in_grid' 			=> false,
                'is_visible_in_grid' 		=> false,
                'is_filterable_in_grid' 	=> false,
			]);
			
		$eavSetup->addAttribute(
			Product::ENTITY,
			'energy_image',
			[
				'group' 					=> 'Energy Consume Data',
				'type'                    	=> 'varchar',
				'label'                   	=> 'Energy Consume Image',
				'input'                   	=> 'image',
				'backend' 					=> \HW\EnergyConsume\Model\Backend\Image::class,
				'required' 					=> false,
                'visible' 					=> false,
                'sort_order' 				=> 30,
				'global'                  	=> \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
				'used_in_product_listing' 	=> true,
				'visible_on_front' 			=> false,
				'is_used_in_grid' 			=> false,
                'is_visible_in_grid' 		=> false,
                'is_filterable_in_grid' 	=> false,
			]);


    }
	
}
