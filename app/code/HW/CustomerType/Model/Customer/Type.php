<?php

namespace HW\CustomerType\Model\Customer;

use Magento\Framework\Data\OptionSourceInterface;

class Type extends \Magento\Framework\DataObject implements OptionSourceInterface
{

	protected $_options;
	protected $_attribute;
	protected $_eavEntityAttribute;
	
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavEntityAttribute,
        array $data = []
    ) {
        $this->_eavEntityAttribute = $eavEntityAttribute;
        parent::__construct($data);
    }
	
    public static function getOptionArray()
    {
        return [
			'0'=>'Private',
			'1'=>'Business'
        ];
    }
	
    public static function getAllOption()
    {
        $options = self::getOptionArray();
        array_unshift($options, ['value' => '', 'label' => '']);
        return $options;
    }
	
    public static function getAllOptions()
    {
        $res = [];
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    public static function getOptionText($optionId)
    {
        $options = self::getOptionArray();
        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
	
	
    public function setAttribute($attribute)
    {
        $this->_attribute = $attribute;
        return $this;
    }
	
    public function getAttribute()
    {
        return $this->_attribute;
    }
	
	
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
	
} 