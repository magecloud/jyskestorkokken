<?php


namespace HW\CustomerType\Model\Config\Source;

use Magento\Shipping\Model\Config\Source\Allmethods;


class ShippingMethods extends Allmethods
{


    public function toOptionArray($isActiveOnlyFlag = false)
    {
        $options = parent::toOptionArray(true);
		
        $options[0]['label'] = ' ';

        foreach ($options as &$option) {
            if (is_array($option['value'])) {
                foreach ($option['value'] as &$method) {
                    $method['label'] = preg_replace('#^\[.+?\]\s#', '', $method['label']);
					if(!$method['label']){$method['label']=$method['value'];}
                }
            }
        }
		

        return $options;
    }
}
