<?php
namespace HW\CustomerType\Observer;


use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class PaymentRestriction implements ObserverInterface
{
	protected $_scopeConfig;
	
    public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
		)
    {
        $this->_scopeConfig = $scopeConfig;
    }
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $result          = $observer->getEvent()->getResult();
        $method_instance = $observer->getEvent()->getMethodInstance();
        $quote           = $observer->getEvent()->getQuote();
       
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/payment.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);

$logger->info($method_instance->getCode());

        if (null !== $quote ) { 
			$customerType=(int) $quote->getCustomerType();
			 
$logger->info($customerType);
		
		
			$configPath="checkout/options/".($customerType==1?"business_payments":"privat_payments");
			$enabled=explode(",",$this->_scopeConfig->getValue($configPath, "websites"));
		
$logger->info(print_r($enabled,true));
			 
            if (!in_array($method_instance->getCode(),$enabled)) {
                $result->setData('is_available', false);
            }
        }

    }
}