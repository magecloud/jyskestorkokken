<?php

namespace HW\CustomerType\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class QuoteToOrder implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        $quote = $observer->getData('quote');
        $order = $observer->getData('order');
        $order->setData('customer_type', $quote->getData('customer_type'));
    }
}