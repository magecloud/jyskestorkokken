<?php


namespace HW\CustomerType\Setup;


use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $customerSetupFactory;    
    private $attributeSetFactory;
	
	
    public function __construct(
		CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }
	
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
		$setup->startSetup();
     
				
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
                
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        
        $customerSetup->addAttribute(Customer::ENTITY, 'customer_type', [
            'type' 		=> 'int',
            'label' 	=> 'Customer Type',
            'input'		=> 'select',
			'source'	=> \HW\CustomerType\Model\Customer\Type::class,
            'required' 	=> false,
            'visible' 	=> true,
            'user_defined' => true,
            'position' 	=>999,
            'system' 	=> 0,
        ]);

		
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'customer_type')
        ->addData([
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => ['adminhtml_customer','checkout_register', 'customer_account_create','customer_account_edit','adminhtml_checkout']
        ]);
        
        $attribute->save();

		$connection = $setup->getConnection();
		
        $connection->addColumn(
            $setup->getTable('quote'),
            'customer_type',
            [
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'length'    => 255,
                'comment'   =>'Customer Type'
            ]
        );
        $connection->addColumn(
            $setup->getTable('sales_order'),
            'customer_type',
            [
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'length'    => 255,
                'comment'   =>'Customer Type'
            ]
        );

		$setup->endSetup();

    }
	
}
