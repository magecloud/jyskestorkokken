<?php

namespace HW\CustomerType\Plugin;
 
class Append
{
 
	protected $_scopeConfig;
	protected $_session;

    public function __construct(
		\Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Backend\Model\Session\Quote $backendQuoteSession,
        \Magento\Framework\App\State $state,\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
		if ($state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            $this->_session = null;
        } else {
            $this->_session = $checkoutSession;
        }
    }

    public function beforeAppend($subject, $result)
    {
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/shipping.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);

 		if (!$result instanceof \Magento\Quote\Model\Quote\Address\RateResult\Method) {
$logger->info('no instanceof');
            return [$result];
        }
		
		if($this->_session){
$logger->info('with sessions');
			$quote = $this->_session->getQuote();
			$customerType=(int) $quote->getCustomerType();
			$configPath="checkout/options/".($customerType==1?"business_shippings":"privat_shippings");
			$enabled=explode(",",$this->_scopeConfig->getValue($configPath, "websites"));
			
$logger->info(print_r($enabled,true));
$methodCode = $result->getCarrier() . '_' . $result->getMethod();
$logger->info('='.$methodCode);
			$enableMethod=false;
			foreach($enabled as $enabledMethod){
				if(strpos(trim($enabledMethod),'_')>0 &&  strpos($methodCode,trim($enabledMethod))!==false){
					$enableMethod=true;
				}
			}
			$result->setIsDisabled(!$enableMethod);
 
		}else{
$logger->info('no sessions');
		}

        return [$result];
    }
}