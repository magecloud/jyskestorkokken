<?php

namespace HW\CustomerType\Plugin;
 
class GetAllRates
{
    public function afterGetAllRates($subject, $result)
    {
        foreach ($result as $key => $rate) {
            if ($rate->getIsDisabled()) {
                unset($result[$key]);
            }
        }
 
        return $result;
    }
	
}