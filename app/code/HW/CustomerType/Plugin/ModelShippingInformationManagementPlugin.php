<?php


declare(strict_types=1);

namespace HW\CustomerType\Plugin;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteRepository;

class ModelShippingInformationManagementPlugin
{
    /** @var QuoteRepository $quoteRepository */
    private $quoteRepository;

    /**
     * @param QuoteRepository $quoteRepository
     */
    public function __construct(QuoteRepository $quoteRepository)
    {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param ShippingInformationManagement $subject
     * @param                               $cartId
     * @param ShippingInformationInterface  $addressInformation
     *
     * @throws NoSuchEntityException
     *
     * @return array
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        $quote = $this->quoteRepository->getActive($cartId);
        $shippingAddress = $addressInformation->getShippingAddress();
        $quote->setData('customer_type', $shippingAddress->getExtensionAttributes()->getCustomerType());
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/shippingAddr.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);
$logger->info(print_r($result,true));
        return [$cartId, $addressInformation];
    }
}
