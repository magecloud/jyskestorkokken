
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
/*             var shippingAddress = quote.shippingAddress();

            if (shippingAddress.customAttributes === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
			
			console.log(shippingAddress.customAttributes['customer_type']);			
            shippingAddress['extension_attributes']['customer_type'] = shippingAddress.customAttributes['customer_type'];
                        
             */
            return originalAction();
        });
    };
});
