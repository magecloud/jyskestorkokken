

define([
    'jquery',
    'uiComponent',
	'Magento_Checkout/js/model/quote',
	'HW_CustomerType/js/model/resource-url-manager', 
    'Magento_Ui/js/lib/view/utils/dom-observer'
], function ($, Component, quote, resourceUrlManager, rateRegistry,  domObserver) {
    'use strict';

    return Component.extend({
        /**
         * Initialize view.
         *
         * @returns {Component} Chainable.
         */
        initialize: function () {
            this._super();
            this.getElements();
			
			$('.amcheckout-shipping-methods').hide();
			$('.amcheckout-payment-methods').hide();
			
			$(document).ajaxComplete(()=>{this.change();});
			$(document).ajaxStart(()=>{this.hide();});	
			
            return this;
        },
		hide: function() {
			$('.amcheckout-shipping-methods').hide();
			$('.amcheckout-payment-methods').hide();
		},
		
        getElements: function () {
            // Wait until elements are available.
            var self = this;
            var elements = 'input[name="billing[type]"], div[name="shippingAddress.company"], div[name="shippingAddress.vat_id"]';
            var elementCount = elements.split(',').length;
            var elementsLoaded = 0;
			
			
			
        },
        click: function(data, event) {
            this.change(event.target.value);
            
            return true;
        },
        change: function(value) {
			value=$("input[name='customer_type']:checked").val();
			
			$('.amcheckout-shipping-methods').show();
			$('.amcheckout-payment-methods').show();
			
            $('div[name="shippingAddress.company"]').hide();
            $('div[name="shippingAddress.vat_id"]').hide();
			$('.amcheckout-shipping-methods .amcheckout-method').hide();
			$('.amcheckout-payment-methods .payment-method').hide();
			
			$("input[name='customer_type']").parents('.switch').removeClass('selected_type');
			$("input[name='customer_type']:checked").parents('.switch').addClass('selected_type');
			
			value=(value==1?1:0);
			
            if (value == 1) {
				$('div[name="shippingAddress.company"]').show();
				$('div[name="shippingAddress.vat_id"]').show();
            } else {
				$('div[name="shippingAddress.company"]').hide();
				$('div[name="shippingAddress.vat_id"]').hide();
            }			
			
			var arrayLength = this.source.customer_type.payments[value].length;
			for (var i = 0; i < arrayLength; i++) {
				$('.amcheckout-payment-methods #'+this.source.customer_type.payments[value][i]).parents('.payment-method').show();
			}
			
			var arrayLength = this.source.customer_type.shippings[value].length;
			for (var i = 0; i < arrayLength; i++) {
				$(".amcheckout-shipping-methods [id^='s_method_"+this.source.customer_type.shippings[value][i]+"']").parents('.amcheckout-method').show();
			}
			
			
/*			var shippingAddress = quote.shippingAddress();
			if (shippingAddress['extension_attributes'] === undefined) {
				shippingAddress['extension_attributes'] = {};
			}
			
			
			
 			this.source.trigger('amcheckout.additional:save');
            
			
			shippingAddress['extension_attributes']['customer_type']=value?1:0;
			
			var cacheKey = shippingAddress.getCacheKey().concat(JSON.stringify(shippingAddress.extensionAttributes)),
				cacheData = rateRegistry.get(cacheKey);

			rateRegistry.set(shippingAddress.getKey(), null);
			rateRegistry.set(shippingAddress.getCacheKey(), null);
			rateRegistry.set(cacheKey, null);

			quote.shippingAddress(shippingAddress); 

			rateRegistry.set(cacheKey, cacheData);*/
			
			
        }

    });
});
