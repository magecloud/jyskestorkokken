<?php

namespace HW\CustomerType\Block\Checkout;


class CustomerTypeDataProcessor implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{

    protected $_scopeConfig;


    public function __construct(        
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
		$this->_scopeConfig = $scopeConfig;
        
    }


    public function process($jsLayout)
    {

        if (!isset($jsLayout['components']['checkoutProvider']['customer_type'])) {
			$customerTypes=array(0,1);
			$methods=array('shippings','payments');
			$data=array(
				'shippings'=>array(0=>array(),1=>array()),
				'payments'=>array(0=>array(),1=>array())
			);
			
			foreach($methods as $method){
				foreach($customerTypes as $customerType){
					$configPath="checkout/options/".($customerType==1?"business_":"privat_").$method;
					$enabled=explode(",",$this->_scopeConfig->getValue($configPath, "websites"));
					foreach($enabled as $item){
						if(strlen(trim($item))>0){
							$data[$method][$customerType][]=$item;
						}
					}
				}
			}		
            $jsLayout['components']['checkoutProvider']['customer_type'] = $data;
        }

        return $jsLayout;
    }
    
}
