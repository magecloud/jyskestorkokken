<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 27.09.2018
 * Time: 9:57
 */
namespace HW\WebsiteSwitcher\Block;

class Link extends \Magento\Framework\View\Element\Html\Link implements \Magento\Customer\Block\Account\SortLinkInterface {

	/**
	 * @var \Magento\Tax\Model\System\Config\Source\Tax\Display\Type
	 */
	protected $_taxModeSource;

	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\View\Element\Template\Context         $context
	 * @param \Magento\Tax\Model\System\Config\Source\Tax\Display\Type $taxType
	 * @param array                                                    $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Tax\Model\System\Config\Source\Tax\Display\Type $taxType,
		array $data = [])
	{
		$this->_taxModeSource = $taxType;

		parent::__construct($context, $data);
	}

	/**
	 * {@inheritdoc}
	 * @since 100.2.0
	 */
	public function getSortOrder()
	{
		return $this->getData(self::SORT_ORDER);
	}

	/**
	 * @return string
	 */
	public function getHref()
	{
		return '#';
	}

	/**
	 * Render block HTML
	 *
	 * @return string
	 */
	protected function _toHtml()
	{
		$this->setData('currentWebsiteName',$this->getCurrentWebsiteName());


		if (false != $this->getTemplate()) {
			return parent::_toHtml();
		}

		return '<li data-bind="scope: \'hw_websiteswitcher_popup\'"><a ' . $this->getLinkAttributes() . ' data-bind="click: openPopup" >'.$this->getCurrentWebsiteName(). ' - ' . $this->escapeHtml($this->getLabel()) . '</a></li>';
	}

	/**
	 * @return string
	 */
	public function getTaxMode(){

		$taxTypesArray  = $this->_taxModeSource->toOptionArray();
		$currentTaxTypeId = $this->_scopeConfig->getValue('tax/display/type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

		foreach ($taxTypesArray as $_taxType){
			if($_taxType['value'] == $currentTaxTypeId){
				return $_taxType['label'];
			}
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getCurrentWebsiteName() {

		return $this->_storeManager->getWebsite()->getName();
	}

	/**
	 * @return string
	 */
	public function getAnotherWebsiteName(){

		$websites = $this->_storeManager->getWebsites();

		foreach ($websites as $website){

			if($website->getId() != $this->_storeManager->getWebsite()->getId()){
				return $website->getName();
			}
		}

		return '';
	}

}