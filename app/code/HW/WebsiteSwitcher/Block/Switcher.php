<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 25.09.2018
 * Time: 20:42
 */

namespace HW\WebsiteSwitcher\Block;

class Switcher extends \Magento\Framework\View\Element\Template {

	const XML_PATH              = 'web/websiteswitcher/';
	const CODE_SESSION_FLAG     = 'hw_websiteswitcher_shown';

	/**
	 * @var \Magento\Customer\Model\Session
	 */
	protected $_customerSession;
	/**
	 * @var \Magento\Framework\Session\SessionManagerInterface
	 */
	protected $_sessionManager;

	/**
	 * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
	 */
	protected $_cookieMetadataFactory;

	/**
	 * @var \Magento\Framework\Stdlib\CookieManagerInterface
	 */
	protected $_cookieManager;

	/**
	 * @var \Magento\Framework\App\Request\Http
	 */
	protected $_httpRequest;


	/**
	 * Switcher constructor.
	 *
	 * @param \Magento\Framework\View\Element\Template\Context       $context
	 * @param \Magento\Customer\Model\Session                        $customerSession
	 * @param \Magento\Framework\Stdlib\CookieManagerInterface       $cookieManager
	 * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
	 * @param \Magento\Framework\Session\SessionManagerInterface     $sessionManager
	 * @param \Magento\Framework\App\Request\Http                    $httpRequest
	 * @param array                                                  $data
	 *
	 * @internal param \Magento\Store\Model\StoreManagerInterface $storeManager
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
		\Magento\Framework\Session\SessionManagerInterface $sessionManager,
		\Magento\Framework\App\Request\Http $httpRequest,
		array $data = []
	)
	{

		$this->_customerSession = $customerSession;
		$this->_cookieManager = $cookieManager;
		$this->_cookieMetadataFactory = $cookieMetadataFactory;
		$this->_sessionManager = $sessionManager;
		$this->_httpRequest = $httpRequest;
		parent::__construct($context, $data);
	}

	/**
	 * @return \Magento\Store\Api\Data\WebsiteInterface[]
	 */
	public function getWebsites() {

		return $this->_storeManager->getWebsites();
	}

	/**
	 * @return array|bool
	 */
	public function getWebsitesData() {

		$_websites = $this->getWebsites();

		if(empty($_websites)) {
			return false;
		}

		$requestString = ltrim($this->_httpRequest->getRequestString(), '/');

		$data = array();

		foreach ($_websites as $_website) {

			$defaultStore = $_website->getDefaultStore();

			if($defaultStore->isActive()) {
				$data[$_website->getId()]['name']   = $_website->getName();
				$data[$_website->getId()]['url']    = $defaultStore->getBaseUrl().$requestString;
			}
		}

		return $data;
	}

	/**
	 * @param string    $field
	 * @param null      $store
	 *
	 * @return mixed
	 */
	public function getConfig($field,$store = null){

		return $this->_scopeConfig->getValue(self::XML_PATH.$field, \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$store);
	}

	/**
	 * @deprecated now we use jQuery cookie managment
	 * @return boolean
	 */
	public function setSessionFlag() {

		return $this->_customerSession->setData(self::CODE_SESSION_FLAG, true);
	}

	/**
	 * @deprecated now we use jQuery cookie managment
	 * @param bool $remove
	 *
	 * @return boolean
	 */
	public function getSessionFlag($remove = false) {

		return $this->_customerSession->getData(self::CODE_SESSION_FLAG, $remove);
	}

	/**
	 * @deprecated now we use jQuery cookie managment
	 * Get cookie
	 *
	 * @return string
	 */
	public function getCookieFlag()
	{
		return $this->_cookieManager->getCookie(self::CODE_SESSION_FLAG);
	}

	/**
	 * @deprecated now we use jQuery cookie managment
	 * @param string $value
	 * @param int $duration
	 * @return void
	 */
	public function setCookieFlag($duration = 60)
	{
		$metadata = $this->_cookieMetadataFactory
			->createPublicCookieMetadata()
			->setDuration($duration)
			->setPath($this->_sessionManager->getCookiePath())
			->setDomain($this->_sessionManager->getCookieDomain());

		$this->_cookieManager->setPublicCookie(
			self::CODE_SESSION_FLAG,
			true,
			$metadata
		);
	}

	/**
	 *@deprecated now we use jQuery cookie managment
	 */
	public function deleteCookieFlag()
	{
		$this->_cookieManager->deleteCookie(
			self::CODE_SESSION_FLAG,
			$this->_cookieMetadataFactory
				->createPublicCookieMetadata()
				->setPath($this->_sessionManager->getCookiePath())
				->setDomain($this->_sessionManager->getCookieDomain())
			);
	}

	/**
	 * @param string $html
	 *
	 * @return string
	 */
	public function _afterToHtml($html)
	{
//		$this->setSessionFlag();
//		$this->setCookieFlag($this->getConfig('cookie_lifetime')); // by default 259200 - 30 days
		return parent::_afterToHtml($html);
	}


}