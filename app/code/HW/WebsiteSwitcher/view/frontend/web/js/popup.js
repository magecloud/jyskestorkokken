define([
    'ko',
    'uiComponent',
    'jquery',
    'Magento_Ui/js/modal/modal',
    'jquery/jquery.cookie'
], function (ko, Component, $, modal) {
    'use strict';

    return Component.extend({
        initialize: function () {
            //initialize parent Component
            this._super();

            var options = {
                type: 'popup',
                modalClass: this.modalClass,
                responsive: true,
                innerScroll: true,
                title: this.title,
                modalLeftMargin: 45,
                buttons: []
            };
            var cookieFlag;
            var popup = modal(options, $(this.elementId));

            cookieFlag = $.cookie('hw_websiteswitcher_shown');
            //console.log(cookieFlag);
            if(!cookieFlag) {
                this.openPopup();
            }
        },

        openPopup: function() {
            $(this.elementId).modal("openModal");
            $.cookie('hw_websiteswitcher_shown', 1, { expires : 3 });
        }

    });
});