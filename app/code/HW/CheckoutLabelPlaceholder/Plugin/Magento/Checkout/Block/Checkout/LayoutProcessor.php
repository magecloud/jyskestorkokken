<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 05.10.2018
 * Time: 10:07
 */

namespace HW\CheckoutLabelPlaceholder\Plugin\Magento\Checkout\Block\Checkout;

class LayoutProcessor {

	/**
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $_logger;

	/**
	 * @var \Magento\Framework\Stdlib\ArrayManager
	 */
	private $_arrayManager;

	/**
	 * LayoutProcessor constructor.
	 *
	 * @param \Psr\Log\LoggerInterface               $logger
	 * @param \Magento\Framework\Stdlib\ArrayManager $arrayManager
	 */
	public function __construct
	(
		\Psr\Log\LoggerInterface                $logger,
		\Magento\Framework\Stdlib\ArrayManager  $arrayManager) {

		$this->_logger = $logger;
		$this->_arrayManager = $arrayManager;
		return $this;
	}

	/**
	 * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
	 * @param array $jsLayout
	 * @return array
	 */
	public function afterProcess(
		\Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
		array  $jsLayout
	) {
		$pathToShippingAddressFieldset = $this->_arrayManager->findPath('shipping-address-fieldset', $jsLayout).\Magento\Framework\Stdlib\ArrayManager::DEFAULT_PATH_DELIMITER.'children';
		$shippingAddressFieldset = $this->_arrayManager->get($pathToShippingAddressFieldset, $jsLayout);

		foreach ($shippingAddressFieldset as $code => $item) {
			if(isset($item['config']['elementTmpl']) && $item['config']['elementTmpl'] == 'ui/form/element/input') {
				$shippingAddressFieldset[$code]['config']['elementTmpl'] = 'HW_CheckoutLabelPlaceholder/form/element/input';
			}
		}

		$shippingAddressFieldset['street']['children']['0']['label'] = __("Street Address");
		$shippingAddressFieldset['street']['children']['0']['config']['elementTmpl'] = 'HW_CheckoutLabelPlaceholder/form/element/input';

		$jsLayout = $this->_arrayManager->set($pathToShippingAddressFieldset, $jsLayout, $shippingAddressFieldset);


		$pathToPaymentList = $this->_arrayManager->findPath('payments-list', $jsLayout).\Magento\Framework\Stdlib\ArrayManager::DEFAULT_PATH_DELIMITER.'children';
		$paymentMethods = $this->_arrayManager->get($pathToPaymentList, $jsLayout);

		foreach ($paymentMethods as $code => $_method) {
			if($_method['component'] == 'Magento_Checkout/js/view/billing-address'){
				$paymentMethods[$code]['children']['form-fields']['children']['street']['children']['0']['label'] = __("Street Address");
				$paymentMethods[$code]['children']['form-fields']['children']['street']['children']['0']['config']['elementTmpl'] = 'HW_CheckoutLabelPlaceholder/form/element/input';

				foreach ($_method['children']['form-fields']['children'] as $fieldCode => $_field ) {
					if(isset($_field['config']['elementTmpl']) && $_field['config']['elementTmpl'] == 'ui/form/element/input') {
						$paymentMethods[$code]['children']['form-fields']['children'][$fieldCode]['config']['elementTmpl'] = 'HW_CheckoutLabelPlaceholder/form/element/input';
					}
				}
			}
		}
		$jsLayout = $this->_arrayManager->set($pathToPaymentList, $jsLayout, $paymentMethods);

/*
		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['firstname']['sortOrder'] = 10;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['lastname']['sortOrder'] = 20;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['company']['sortOrder'] = 30;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['telephone']['sortOrder'] = 40;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['fax']['sortOrder'] = 50;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['vat_id']['sortOrder'] = 60;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['street']['sortOrder'] = 70;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['postcode']['sortOrder'] = 80;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['city']['sortOrder'] = 90;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['region_id']['sortOrder'] = 100;

		$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
		['children']['country_id']['sortOrder'] = 110;
*/

//		$this->_logger->debug(print_r($jsLayout['components']['checkout']['children']['steps']['children'], true) );

		return $jsLayout;
	}
}