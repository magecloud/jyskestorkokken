<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 16.04.2018
 * Time: 17:01
 */
namespace HW\Gallery\Model\ResourceModel\Gallery\Image;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

	/**
	 * @var string
	 */
	protected $_idFieldName = 'image_id';


	/**
	 * @var \Magento\Framework\Serialize\Serializer\Serialize
	 */
	protected $_serializer;


	/**
	 * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
	 * @param \Psr\Log\LoggerInterface $logger
	 * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
	 * @param \Magento\Framework\Event\ManagerInterface $eventManager
	 * @param \Magento\Framework\DB\Adapter\AdapterInterface $connection
	 * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource
	 */
	public function __construct(
		\Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
		\Psr\Log\LoggerInterface $logger,
		\Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
		\Magento\Framework\Event\ManagerInterface $eventManager,
		\Magento\Framework\Serialize\Serializer\Serialize $serializer,
		\Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
		\Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
	) {

		parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
		$this->_serializer = $serializer;
	}

	protected function _construct()
	{
		$this->_init('HW\Gallery\Model\Gallery\Image', 'HW\Gallery\Model\ResourceModel\Gallery\Image');
	}

	/**
	 * Filter collection by gallery belonging
	 *
	 * @param $galleryId int
	 *
	 * @return $this
	 */

	public function addGalleryFilter($galleryId)
	{
		$this->getSelect()->where("gallery_id IN(?) ", $galleryId);

		return $this;
	}


	/**
	 * @return $this
	 */
	public function addVisibleFilter(){

		$this->getSelect()->where('is_active = (?)', 1);

		return $this;
	}

	/**
	 * @param $store
	 *
	 * @return $this
	 */
	public function addStoresFilter($store) {

		return $this->addStoreFilter($store);
	}

	/**
	 * @param      $storeId
	 * @param bool $withAdmin
	 *
	 * @return $this
	 */
	public function addStoreFilter($storeId, $withAdmin = true) {
		$this->getSelect()->join(
			array('store_table' => $this->getTable('hw_gallery_image_store')),
			'main_table.image_id = store_table.image_id',
			array()
		)
			->where('store_table.store_id in (?)', ($withAdmin ? array(0, $storeId) : $storeId))
			->group('main_table.image_id');

		return $this;
	}


	/**
	 * Add stores data
	 *
	 * @return Collection
	 */
	public function addStoreData() {
		$imageIds = $this->getColumnValues('image_id');
		$storesToImage = array();

		if (count($imageIds) > 0) {
			$select = $this->getConnection()->select()
				->from($this->getTable('hw_gallery_image_store'))
				->where('image_id IN(?)', $imageIds);
			$result = $this->getConnection()->fetchAll($select);

			foreach ($result as $row) {
				if (!isset($storesToImage[$row['image_id']])) {
					$storesToImage[$row['image_id']] = array();
				}
				$storesToImage[$row['image_id']][] = $row['store_id'];
			}
		}

		foreach ($this as $item) {
			if(isset($storesToImage[$item->getId()])) {
				$item->setStores($storesToImage[$item->getId()]);
			} else {
				$item->setStores(array());
			}
		}

		return $this;
	}

	/**
	 * @return $this
	 */

	protected function _afterLoad() {

		foreach ($this as $item) {
			$_parameters = $this->_serializer->unserialize($item->getParameters());

			if(is_array($_parameters)){
				foreach($_parameters as $key=>$value){
					if($key == 'spot'){
						$_spotsArray = array();
						$_spots = $this->_serializer->unserialize($value);
						if(is_array($_spots) && !empty($_spots)) {
							foreach ($_spots as $_spotKey => $_spot) {
								foreach ($_spot as $_spotProperty => $_spotPropertyValue) {
									$item->setData( 'spot_'.$_spotKey.'_'.$_spotProperty,$_spotPropertyValue);
									$_spotsArray[$_spotKey][$_spotProperty] = $_spotPropertyValue;
								}
							}
							$item->setData('spot',$_spotsArray);
						}
					} else {
						$item->setData($key,$value);
					}

				}
			}
		}

		return parent::_afterLoad();
	}

}