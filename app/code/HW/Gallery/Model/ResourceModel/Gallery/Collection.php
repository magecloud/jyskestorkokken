<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 28.03.2018
 * Time: 13:22
 */
namespace HW\Gallery\Model\ResourceModel\Gallery;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

	/**
	 * @var string
	 */
	protected $_idFieldName = 'gallery_id';


	protected function _construct()
	{
		$this->_init('HW\Gallery\Model\Gallery', 'HW\Gallery\Model\ResourceModel\Gallery');
	}

}