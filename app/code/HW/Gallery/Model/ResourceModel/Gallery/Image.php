<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 16.04.2018
 * Time: 17:01
 */
namespace HW\Gallery\Model\ResourceModel\Gallery;

class Image extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

	/**
	 * @var \Magento\Framework\Serialize\Serializer\Serialize
	 */
	protected $_serializer;

	/**
	 * @var \HW\Gallery\Helper\Data
	 */
	protected $_helper;

	/**
	 *
	 */
	protected function _construct() {

		$this->_init('hw_gallery_image', 'image_id');
	}

	/**
	 * Image constructor.
	 *
	 * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
	 * @param \Magento\Framework\Serialize\Serializer\Serialize $serializer
	 * @param \HW\Gallery\Helper\Data                           $helper
	 * @param null                                              $connectionName
	 */

	public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context,
	                            \Magento\Framework\Serialize\Serializer\Serialize $serializer,
	                            \HW\Gallery\Helper\Data $helper,
	                            $connectionName = null)
	{
		parent::__construct($context, $connectionName);
		$this->_serializer = $serializer;
		$this->_helper     = $helper;
	}

	/**
	 * Perform actions after object save
	 *
	 * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
	 * @return $this
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	protected function _afterSave(\Magento\Framework\Model\AbstractModel $object) {
		$deleteWhere = $this->getConnection()->quoteInto('image_id = ?', $object->getId());
		$this->getConnection()->delete($this->getTable('hw_gallery_image_store'), $deleteWhere);
		foreach ($object->getStores() as $storeId) {
			$imageStoreData = array(
				'image_id'   => $object->getId(),
				'store_id'  => $storeId
			);
			$this->getConnection()->insert($this->getTable('hw_gallery_image_store'), $imageStoreData);
		}
	}

	/**
	 * Perform actions after object load
	 *
	 * @param \Magento\Framework\Model\AbstractModel $object
	 *
	 * @return $this
	 */

	protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object){

		$_parameters = $this->_serializer->unserialize($object->getParameters());

		if(is_array($_parameters)){
			foreach($_parameters as $key=>$value){
				if($key == 'spot'){
					$_spotsArray = array();
					$_spots = $this->_serializer->unserialize($value);
					if(is_array($_spots) && !empty($_spots)) {
						if($this->_helper->useFauxSpots()){
							foreach ($_spots as $_spotKey => $_spot) {
								foreach ($_spot as $_spotProperty => $_spotPropertyValue) {
									$object->setData( 'spot_'.$_spotKey.'_'.$_spotProperty,$_spotPropertyValue);
									$_spotsArray[$_spotKey][$_spotProperty] = $_spotPropertyValue;
								}
							}
							$object->setData('spot',$_spotsArray);
						} else {
							$object->setData('spot',$_spots);
						}

					}
				} else {
					$object->setData($key,$value);
				}

			}
		}

		/** load data about stores */
		if ($object->getId()) {
			$stores = $this->lookupStoreIds($object->getId());
			$object->setData('stores', $stores);
		}

		return parent::_afterLoad($object);
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function lookupStoreIds($id) {
		return $this->getConnection()->fetchCol($this->getConnection()->select()
			->from($this->getTable('hw_gallery_image_store'), 'store_id')
			->where("{$this->getIdFieldName()} = ?", $id)
		);
	}

}