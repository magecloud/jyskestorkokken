<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 28.03.2018
 * Time: 13:22
 */
namespace HW\Gallery\Model\ResourceModel;

class Gallery extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

	/**
	 * @var \Magento\Framework\Serialize\Serializer\Serialize
	 */
	protected $_serializer;

	protected function _construct()
	{
		$this->_init('hw_gallery', 'gallery_id');
	}

	/**
	 * Gallery constructor.
	 *
	 * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
	 * @param \Magento\Framework\Serialize\Serializer\Serialize $serializer
	 * @param null                                              $connectionName
	 */

	public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context,
	                            \Magento\Framework\Serialize\Serializer\Serialize $serializer,
	                            $connectionName = null)
	{
		parent::__construct($context, $connectionName);
		$this->_serializer = $serializer;
	}

	/**
	 * Perform actions after object save
	 *
	 * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
	 * @return $this
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
	{
		return $this;
	}

	/**
	 * Perform actions after object load
	 *
	 * @param \Magento\Framework\Model\AbstractModel $object
	 *
	 * @return $this
	 */

	protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
	{
		if(is_string($object->getParameters())) {
			$_parameters = $this->_serializer->unserialize($object->getParameters());

			if(is_array($_parameters)){
				foreach($_parameters as $key=>$value){
					$object->setData($key,$value);
				}
			}
		}

		return parent::_afterLoad($object);
	}

}