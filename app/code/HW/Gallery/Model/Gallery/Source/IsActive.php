<?php

/**
 *
 */

namespace HW\Gallery\Model\Gallery\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface {

	/**
	 * @var \HW\Gallery\Model\Gallery
	 */
	protected $_gallery;

	/**
	 * Type constructor.
	 *
	 * @param \HW\Gallery\Model\Gallery $gallery
	 */
	public function __construct(\HW\Gallery\Model\Gallery $gallery) {

		$this->_gallery = $gallery;
	}

	/**
	 * Get options
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$availableOptions = $this->_gallery->getAvailableStatuses();
		$options = [];
		foreach ($availableOptions as $key => $value) {
			$options[] = [
				'label' => $value,
				'value' => $key,
			];
		}
		return $options;
	}

}