<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 11.07.2018
 * Time: 14:36
 */
namespace HW\Gallery\Model\Gallery\Source\Options;

class Colcount implements \Magento\Framework\Option\ArrayInterface {

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		return [
			['value' => 1, 'label' => __('1')],
			['value' => 2, 'label' => __('2')],
			['value' => 3, 'label' => __('3')],
			['value' => 4, 'label' => __('4')],
			['value' => 5, 'label' => __('5')],
			['value' => 6, 'label' => __('6')],
			['value' => 8, 'label' => __('8')],
			['value' => 10, 'label' => __('10')],
			['value' => 12, 'label' => __('12')]
		];
	}


	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		return [
			1 => __("1"),
			2 => __("2"),
			3 => __("3"),
			4 => __("4"),
			5 => __("5"),
			6 => __("6"),
			8 => __("8"),
			10 => __("10"),
			12 => __("12")
		];
	}
}