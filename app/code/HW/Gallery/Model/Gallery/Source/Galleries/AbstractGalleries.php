<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 04.07.2018
 * Time: 16:33
 */

namespace HW\Gallery\Model\Gallery\Source\Galleries;

abstract class AbstractGalleries implements \Magento\Framework\Option\ArrayInterface {

	/**
	 * @var \HW\Gallery\Model\GalleryFactory
	 */
	protected $_galleryFactory;

	/**
	 * Banners constructor.
	 *
	 * @param \HW\Gallery\Model\GalleryFactory $galleryFactory
	 */
	public function __construct(
		\HW\Gallery\Model\GalleryFactory $galleryFactory
	) {
		$this->_galleryFactory = $galleryFactory;
	}

	public function getGalleries() {

		$galleryModel = $this->_galleryFactory->create();
		return $galleryModel->getCollection()->addFieldToFilter('type', array('eq' => $this->_type_code))->getData();
	}

	/**
	 * @return array
	 */
	public function toOptionArray()
	{
		$galleries = [];
		foreach ($this->getGalleries() as $_gallery) {
			array_push($galleries,[
				'value' => $_gallery['gallery_id'],
				'label' => $_gallery['title']." (".$_gallery['identifier'].")"
			]);
		}
		return $galleries;
	}
}