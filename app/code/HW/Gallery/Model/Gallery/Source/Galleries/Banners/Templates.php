<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 11.07.2018
 * Time: 14:53
 */
namespace HW\Gallery\Model\Gallery\Source\Galleries\Banners;

class Templates implements \Magento\Framework\Option\ArrayInterface {

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		return [
			['value' => 'default'   , 'label' => __("Static grid")],
			['value' => 'template_1', 'label' => __("Owl carousel")]
		];
	}


	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		return [
			'default'       => __("Static grid"),
			'template_1'    => __("Owl carousel")
		];
	}
}