<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 11.07.2018
 * Time: 14:53
 */
namespace HW\Gallery\Model\Gallery\Source\Galleries\Sliders;

class Templates implements \Magento\Framework\Option\ArrayInterface {

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		return [
			['value' => 'default'   , 'label' => __("Cycle slider")]
		];
	}


	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		return [
			'default'       => __("Cycle slider")
		];
	}
}