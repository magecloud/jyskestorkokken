<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 12.07.2018
 * Time: 14:06
 */
namespace HW\Gallery\Model\Gallery\Source\Galleries\Sliders;

class Effects implements \Magento\Framework\Option\ArrayInterface {

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		return [
			['value' => 'fade', 'label' => __("Fade")],
			['value' => 'fadeout', 'label' => __("Fade Out")],
			['value' => 'scrollHorz', 'label' => __("ScrollHorz")]
		];
	}


	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		return [
			'fade'          => __("Fade"),
			'fadeout'       => __("Fade Out"),
			'scrollHorz'    => __("ScrollHorz")
		];
	}
}