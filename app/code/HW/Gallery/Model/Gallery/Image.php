<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 16.04.2018
 * Time: 17:01
 */
namespace HW\Gallery\Model\Gallery;

/**
 * @method \HW\Gallery\Model\ResourceModel\Gallery\Image getResource()
 * @method \HW\Gallery\Model\ResourceModel\Gallery\Image\Collection getCollection()
 */
class Image extends \Magento\Framework\Model\AbstractModel implements \HW\Gallery\Api\Data\Gallery\ImageInterface, \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'hw_gallery_image';

	const PATCH_TO_SOURCE = "hw/gallery/";

	/**#@+
	 * Image's Statuses
	 */
	const STATUS_ENABLED     = 1;
	const STATUS_DISABLED    = 0;
	/**#@-*/

	protected $_cacheTag = 'hw_gallery_image';
	protected $_eventPrefix = 'hw_gallery_image';

	protected function _construct()
	{
		$this->_init('HW\Gallery\Model\ResourceModel\Gallery\Image');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	/**
	 * Prepare image statuses.
	 *
	 * @return array
	 */
	public function getAvailableStatuses() {
		return [self::STATUS_DISABLED => __('Disabled'), self::STATUS_ENABLED => __('Enabled')];
	}

	public function getSpotCount() {

		if(!$this->getData('spot_count')) {
			$this->setData('spot_count', count($this->getData('spot'))?count($this->getData('spot')):1);
		}

		return $this->getData('spot_count');
	}
}