<?php

/**
 *
 */

namespace HW\Gallery\Model\Gallery\Image\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface {

	/**
	 * @var \HW\Gallery\Model\Gallery\Image
	 */
	protected $_image;

	/**
	 * Type constructor.
	 *
	 * @param \HW\Gallery\Model\Gallery $gallery
	 */
	public function __construct(\HW\Gallery\Model\Gallery $image) {

		$this->_image = $image;
	}

	/**
	 * Get options
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$availableOptions = $this->_image->getAvailableStatuses();
		$options = [];
		foreach ($availableOptions as $key => $value) {
			$options[] = [
				'label' => $value,
				'value' => $key,
			];
		}
		return $options;
	}

}