<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 09.07.2018
 * Time: 17:28
 */

namespace HW\Gallery\Model\Gallery\Image\Source\Spot;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class Type implements OptionSourceInterface {

	const TYPE_HTML     = 1;
	const TYPE_SIMPLE   = 0;


	/**
	 * Get options
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$availableOptions = [self::TYPE_HTML => __('HTML'), self::TYPE_SIMPLE => __('Simple')];
		$options = [];
		foreach ($availableOptions as $key => $value) {
			$options[] = [
				'label' => $value,
				'value' => $key,
			];
		}
		return $options;
	}

}