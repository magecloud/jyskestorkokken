<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 28.03.2018
 * Time: 13:22
 */
namespace HW\Gallery\Model;

/**
 * @method \HW\Gallery\Model\ResourceModel\Gallery getResource()
 * @method \HW\Gallery\Model\ResourceModel\Gallery\Collection getCollection()
 */
class Gallery extends \Magento\Framework\Model\AbstractModel implements \HW\Gallery\Api\Data\GalleryInterface, \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG             = 'hw_gallery_gallery';

	/**#@+
	 * Gallery's Types
	 */
	const TYPE_BANNERS_GRID     = 1;
	const TYPE_SLIDER           = 2;
	/**#@-*/

	/**#@+
	 * Gallery's Statuses
	 */
	const STATUS_ENABLED     = 1;
	const STATUS_DISABLED    = 0;
	/**#@-*/

	protected $_cacheTag        = 'hw_gallery_gallery';
	protected $_eventPrefix     = 'hw_gallery_gallery';

	/**
	 * @var ResourceModel\Gallery\Image\CollectionFactory
	 */
	protected $_imageResourceCollectionFactory;

	/**
	 * @var ResourceModel\Gallery\Image\Collection
	 */
	protected $_imageCollectionFrontend = null;

	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager;




	/**
	 * Gallery constructor.
	 *
	 * @param \Magento\Framework\Model\Context                             $context
	 * @param \Magento\Framework\Registry                                  $registry
	 * @param ResourceModel\Gallery\Image\CollectionFactory                $imageResourceCollectionFactory
	 * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
	 * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
	 * @param \Magento\Framework\Data\Collection\AbstractDb|null           $resourceCollection
	 * @param array                                                        $data
	 */
	public function __construct(
		\Magento\Framework\Model\Context                                    $context,
		\Magento\Framework\Registry                                         $registry,
		\HW\Gallery\Model\ResourceModel\Gallery\Image\CollectionFactory     $imageResourceCollectionFactory,
		\Magento\Store\Model\StoreManagerInterface                          $storeManager,
		\Magento\Framework\Model\ResourceModel\AbstractResource             $resource = null,
		\Magento\Framework\Data\Collection\AbstractDb                       $resourceCollection = null,
		array $data = []
	){
		$this->_imageResourceCollectionFactory = $imageResourceCollectionFactory;
		$this->_storeManager = $storeManager;
		parent::__construct(
			$context,
			$registry,
			$resource,
			$resourceCollection,
			$data
		);
	}

	protected function _construct()
	{
		$this->_init('HW\Gallery\Model\ResourceModel\Gallery');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	/**
	 * Prepare gallery's types.
	 *
	 * @return array
	 */
	public function getAvailableTypes()
	{
		return [self::TYPE_BANNERS_GRID => __('Banners'), self::TYPE_SLIDER => __('Slider')];
	}

	/**
	 * Prepare gallery's statuses.
	 *
	 * @return array
	 */
	public function getAvailableStatuses() {
		return [self::STATUS_DISABLED => __('Disabled'), self::STATUS_ENABLED => __('Enabled')];
	}

	/**
	 * @param string $order
	 *
	 * @return ResourceModel\Gallery\Image\Collection
	 */
	public function getImages($order='position') {

		if($this->_imageCollectionFrontend === null) {

			$this->_imageCollectionFrontend = $this->_imageResourceCollectionFactory->create()
				->addGalleryFilter($this->getId())
				->addStoresFilter($this->_storeManager->getStore()->getId())
				->addVisibleFilter()
				->setOrder($order,'asc')
			;
		}

		return $this->_imageCollectionFrontend;
	}

	/**
	 * @param string $order
	 *
	 * @return $this
	 */
	public function getImageCollection($order='') {

		$imageCollection = $this->_imageResourceCollectionFactory->create();
		$imageCollection->addGalleryFilter($this->getId())
			->addStoreData();

		if($order){
			$imageCollection->getSelect()->order($order);
		}

		return $imageCollection;
	}


}