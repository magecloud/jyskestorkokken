<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 17.04.2018
 * Time: 11:28
 */
namespace HW\Gallery\Controller\Adminhtml\Images;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends Action
{
	/**
	 * @var \Magento\MediaStorage\Model\File\UploaderFactory
	 */
	protected $_uploaderFactory;

	/**
	 * @var \Magento\Framework\Image\AdapterFactory
	 */
	protected $_adapterFactory;

	/**
     * @var \HW\Gallery\Model\Gallery\Image
     */
    protected $_model;

	/**
	 * @var \Magento\Framework\Serialize\Serializer\Serialize
	 */
	protected $_serializer;

	/**
	 * Save constructor.
	 *
	 * @param Action\Context                                    $context
	 * @param \HW\Gallery\Model\Gallery\Image                   $model
	 * @param \Magento\Framework\Serialize\Serializer\Serialize $serializer
	 */
    public function __construct(
        Action\Context $context,
        \HW\Gallery\Model\Gallery\Image $model,
        \Magento\Framework\Serialize\Serializer\Serialize $serializer,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Image\AdapterFactory $adapterFactory
    ) {
        parent::__construct($context);
        $this->_model = $model;
	    $this->_serializer = $serializer;
	    //for uploading images
	    $this->_uploaderFactory = $uploaderFactory;
	    $this->_adapterFactory = $adapterFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Backend::content');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

	    $galleryId = $this->getRequest()->getParam('gallery_id');

        if ($data) {
            /** @var \HW\Gallery\Model\Gallery\Image $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('image_id')?$this->getRequest()->getParam('image_id'):$this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

	        //save parameters
	        $parameter =  isset($data['parameters'])?$data['parameters']:[];

	        if(isset($data['spot']) && is_array($data['spot'])){
	        	foreach ($data['spot'] as $_spotKey => $_spotData) {

	        		if(isset($_spotData['remove'])) {
	        			unset($data['spot'][$_spotKey]);
			        }
		        }
		        $parameter['spot']  = $this->_serializer->serialize($data['spot']);
	        }

	        $data['parameters'] = $this->_serializer->serialize($parameter);

			//Save Image
	        $imageRequest = $this->getRequest()->getFiles('file');
	        if ($imageRequest) {
		        if (isset($imageRequest['name'])) {
			        $fileName = $imageRequest['name'];
		        } else {
			        $fileName = '';
		        }
	        } else {
		        $fileName = '';
	        }

	        if ($imageRequest && strlen($fileName)) {
		        /*
				 * Save image upload
				 */
		        try {
			        $uploader = $this->_uploaderFactory->create(['fileId' => 'file']);

			        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

			        /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
			        $imageAdapter = $this->_adapterFactory->create();

			        $uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
			        $uploader->setAllowRenameFiles(true);
			        $uploader->setFilesDispersion(true);

			        /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
			        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
				        ->getDirectoryRead(DirectoryList::MEDIA);
			        $result = $uploader->save(
				        $mediaDirectory->getAbsolutePath(\HW\Gallery\Model\Gallery\Image::PATCH_TO_SOURCE)
			        );
			        $data['file'] = $result['file'];
		        } catch (\Exception $e) {
			        if ($e->getCode() == 0) {
				        $this->messageManager->addError($e->getMessage());
			        }
		        }
	        } else {
		        if (isset($data['file']) && isset($data['file']['value'])) {
			        if (isset($data['file']['delete'])) {
				        $data['file'] = null;
				        $data['delete_image'] = true;
			        } elseif (isset($data['file']['value'])) {
				        $data['file'] = $data['file']['value'];
			        } else {
				        $data['file'] = null;
			        }
		        }
	        }

            $model->setData($data);

	        $this->_eventManager->dispatch(
		        'hw_gallery_image_prepare_save',
		        ['image' => $model, 'request' => $this->getRequest()]
	        );


            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('Image saved'));
                $this->_getSession()->setFormData(false);
	            if($model->getGalleryId()){
		            $galleryId = $model->getGalleryId();
	            }
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/gallery/edit',['id' => $galleryId,'active_tab' => 'images_section']);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the '));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/gallery/edit',['id' => $galleryId,'active_tab' => 'images_section']);
    }
}