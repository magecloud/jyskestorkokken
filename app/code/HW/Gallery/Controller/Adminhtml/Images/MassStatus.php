<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 28.03.2018
 * Time: 14:26
 */
namespace HW\Gallery\Controller\Adminhtml\Images;


class MassStatus extends \Magento\Backend\App\Action {


    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
	    $galleryId  = $this->getRequest()->getParam('id');
	    $imageIds   = $this->getRequest()->getParam('image_ids');
	    $status   = $this->getRequest()->getParam('images_is_active');

	    if (!is_array($imageIds) || empty($imageIds)) {
		    $this->messageManager->addErrorMessage(__('Please select image(s).'));
	    } else {
		    try {
			    foreach ($imageIds as $imageId) {
				    $image = $this->_objectManager->create('HW\Gallery\Model\Gallery\Image')
					    ->load($imageId);
				    $image->setData('is_active',$status);
				    $image->save();
			    }
			    $this->messageManager->addSuccessMessage(
				    __('A total of %1 record(s) have been changed status.', count($imageIds))
			    );
		    } catch (\Exception $e) {
			    $this->messageManager->addErrorMessage($e->getMessage());
		    }
	    }

	    $resultRedirect = $this->resultRedirectFactory->create();
	    return $resultRedirect->setPath('*/gallery/edit',array('id' => $galleryId,'active_tab' => 'images_section'));

    }
}