<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 17.04.2018
 * Time: 11:28
 */
namespace HW\Gallery\Controller\Adminhtml\Images;

use Magento\Backend\App\Action;

class Delete extends Action
{
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \HW\Gallery\Model\Gallery\Image $model
     */
    public function __construct(
        Action\Context $context,
        \HW\Gallery\Model\Gallery\Image $model
    ) {
        parent::__construct($context);
        $this->_model = $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Backend::content');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $image_id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($image_id) {
            try {
                $model      = $this->_model;
                $model->load($image_id);
	            $galleryId  = $model->getGalleryId();
	            $imageTitle = $model->getTitle();
                $model->delete();
                $this->messageManager->addSuccessMessage(__('Image "%1" (%2) was deleted', $imageTitle,$image_id));
                return $resultRedirect->setPath('*/gallery/edit',array('id' => $galleryId,'active_tab' => 'images_section'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $image_id]);
            }
        }

	    $this->messageManager->addErrorMessage(__('Image does not exist'));

	    if($galleryId = $this->getRequest()->getParam('gallery_id')){
	        return $resultRedirect->setPath('*/gallery/edit',array('id' => $galleryId,'active_tab' => 'images_section'));
        }
        return $resultRedirect->setPath('*/gallery/index');
    }
}