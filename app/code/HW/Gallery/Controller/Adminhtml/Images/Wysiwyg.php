<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 13.07.2018
 * Time: 14:42
 */

namespace HW\Gallery\Controller\Adminhtml\Images;

class Wysiwyg extends \Magento\Backend\App\Action {

	/**
	 * @var \Magento\Framework\Controller\Result\RawFactory
	 */
	protected $resultRawFactory;

	/**
	 * @var \Magento\Framework\View\LayoutFactory
	 */
	protected $layoutFactory;

	/**
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
	 * @param \Magento\Framework\View\LayoutFactory $layoutFactory
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
		\Magento\Framework\View\LayoutFactory $layoutFactory
	) {
		parent::__construct($context);
		$this->resultRawFactory = $resultRawFactory;
		$this->layoutFactory = $layoutFactory;
	}


	/**
	 * WYSIWYG editor action for ajax request
	 *
	 * @return \Magento\Framework\Controller\Result\Raw
	 */
	public function execute()
	{
		$elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
		$storeId = $this->getRequest()->getParam('store_id', 0);
		$storeMediaUrl = $this->_objectManager->get(\Magento\Store\Model\StoreManagerInterface::class)
			->getStore($storeId)
			->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

		$content = $this->layoutFactory->create()
			->createBlock(
				\HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Spots\Wysiwyg::class,
				'',
				[
					'data' => [
						'editor_element_id' => $elementId,
						'store_id' => $storeId,
						'store_media_url' => $storeMediaUrl,
					]
				]
			);

		/** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
		$resultRaw = $this->resultRawFactory->create();
		return $resultRaw->setContents($content->toHtml());
	}

}