<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 17.04.2018
 * Time: 11:28
 */
namespace HW\Gallery\Controller\Adminhtml\Images;

use Magento\Backend\App\Action;

class Edit extends Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \HW\Gallery\Model\Gallery\Image
     */
    protected $_model;

	/**
	 * @var \HW\Gallery\Model\Gallery
	 */
	protected $_modelGallery;

	/**
	 * Edit constructor.
	 *
	 * @param Action\Context                             $context
	 * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
	 * @param \Magento\Framework\Registry                $registry
	 * @param \HW\Gallery\Model\Gallery\Image            $model
	 * @param \HW\Gallery\Model\Gallery                  $modelGallery
	 */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \HW\Gallery\Model\Gallery\Image $model,
        \HW\Gallery\Model\Gallery $modelGallery

    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->_model = $model;
        $this->_modelGallery = $modelGallery;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Backend::content');
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();

	    $resultPage->setActiveMenu('HW_Gallery::gallery');
	    $resultPage->addBreadcrumb(__('CMS'), __('CMS'));
	    $resultPage->addBreadcrumb(__('Gallery manager'), __('Gallery manager'));
	    $resultPage->addBreadcrumb(__('Gallery'), __('Gallery'));

        return $resultPage;
    }

    /**
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id             = $this->getRequest()->getParam('image_id');
        $galleryId      = $this->getRequest()->getParam('gallery_id');
        $spot_count     = $this->getRequest()->getParam('spot_count');

	    if(empty($id)) {
	        $id         = $this->getRequest()->getParam('id');
        }

        $model          = $this->_model;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This  not exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        } else {
        	$model->setData('gallery_id',$galleryId);

        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

		if(isset($spot_count) && !empty($spot_count)){
			$model->setData('spot_count',$spot_count);
		}

        $this->_coreRegistry->register('current_model', $model);

	    $galleryId = $galleryId?$galleryId:$model->getGalleryId();
	    if($galleryId) {
		    $this->_modelGallery->load($galleryId);
		    if($this->_modelGallery->getId()){
			    $this->_coreRegistry->register('gallery_model', $this->_modelGallery);
		    }
	    }


        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();

        $resultPage->addBreadcrumb(
            $id ? __('Edit ') : __('Add '),
            $id ? __('Edit ') : __('Add ')
        );
//        $resultPage->getConfig()->getTitle()->prepend(__('Image'));
	    $resultPage->getConfig()->getTitle()
		    ->prepend($model->getId() ? __('Edit Image: ') . __('%1  (ID: %2)',$model->getTitle(),$model->getId())  : __('Add '));

	    if($this->_modelGallery->getId()){
		    $resultPage->getConfig()->getTitle()
			    ->prepend($model->getId() ? __('Edit Image: ') . __('%1  (ID: %2)',$model->getTitle(),$model->getId()). ' / ' .__('Edit Gallery: ') . __('%1  (ID: %2)',$this->_modelGallery->getTitle(),$galleryId)  : __('Add '));
	    }

        return $resultPage;
    }
}