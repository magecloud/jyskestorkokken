<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 28.03.2018
 * Time: 14:26
 */
namespace HW\Gallery\Controller\Adminhtml\Gallery;

use Magento\Backend\App\Action;

class Index extends Action
{

	/**
	 * A factory that knows how to create a "page" result
	 * Requires an instance of controller action in order to impose page type,
	 * which is by convention is determined from the controller action class.
	 *
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
    protected $resultPageFactory;


    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
	    $resultPage->setActiveMenu('HW_Gallery::gallery');
	    $resultPage->addBreadcrumb(__('CMS'), __('CMS'));
	    $resultPage->addBreadcrumb(__('Gallery manager'), __('Gallery manager'));
	    $resultPage->getConfig()->getTitle()->prepend(__('Gallery manager'));

        return $resultPage;
    }

}