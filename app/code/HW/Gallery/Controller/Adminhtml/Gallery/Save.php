<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 30.03.2018
 * Time: 14:54
 */
namespace HW\Gallery\Controller\Adminhtml\Gallery;

use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var \HW\Gallery\Model\Gallery
     */
    protected $_model;

	/**
	 * @var \Magento\Framework\Serialize\Serializer\Serialize
	 */
	protected $_serializer;

	/**
	 * Save constructor.
	 *
	 * @param Action\Context                                    $context
	 * @param \HW\Gallery\Model\Gallery                         $model
	 * @param \Magento\Framework\Serialize\Serializer\Serialize $serializer
	 */
    public function __construct(
        Action\Context $context,
        \HW\Gallery\Model\Gallery $model,
        \Magento\Framework\Serialize\Serializer\Serialize $serializer
    ) {
        parent::__construct($context);
        $this->_model = $model;
	    $this->_serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Backend::content');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \HW\Gallery\Model\Gallery $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            //save parameters
            $parameter = $this->getRequest()->getParam('parameters');
	        $data['parameters'] = $this->_serializer->serialize($parameter);

            $model->setData($data);

             $this->_eventManager->dispatch(
                 'hw_gallery_prepare_save',
                ['gallery' => $model, 'request' => $this->getRequest()]
             );

            try {
	            $position=$this->getRequest()->getParam('image_position',array());

	            foreach($position as $imageId => $newPosition){
		            $image = $this->_objectManager->create('HW\Gallery\Model\Gallery\Image')
			            ->load($imageId);
		            if($image->getPosition() != $newPosition){
			            $image->setPosition($newPosition)->save();
		            }
	            }

                $model->save();
                $this->messageManager->addSuccess(__('Gallery saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Gallery'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}