<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 30.03.2018
 * Time: 14:54
 */
namespace HW\Gallery\Controller\Adminhtml\Gallery;

use Magento\Backend\App\Action;

class Edit extends Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

	/**
	 * A factory that knows how to create a "page" result
	 * Requires an instance of controller action in order to impose page type,
	 * which is by convention is determined from the controller action class.
	 *
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
    protected $_resultPageFactory;

    /**
     * @var \HW\Gallery\Model\Gallery
     */
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     * @param \HW\Gallery\Model\Gallery $model
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \HW\Gallery\Model\Gallery $model
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->_model = $model;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Backend::content');
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
	    $resultPage->setActiveMenu('HW_Gallery::gallery');
	    $resultPage->addBreadcrumb(__('CMS'), __('CMS'));
	    $resultPage->addBreadcrumb(__('Gallery manager'), __('Gallery manager'));
        return $resultPage;
    }

    /**
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_model;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This Gallery not exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('current_model', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();



        $resultPage->addBreadcrumb(
            $id ? __('Edit Gallery') : __('New Gallery'),
            $id ? __('Edit Gallery') : __('New Gallery')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Gallery'));
        $resultPage->getConfig()->getTitle()
           // ->prepend($model->getId() ? __('Edit Gallery ID: ') . $model->getId() : __('New Gallery'));
            ->prepend($model->getId() ? __('Edit Gallery: ') . __('%1  (ID: %2)',$model->getTitle(),$model->getId())  : __('New Gallery'));

        return $resultPage;
    }
}