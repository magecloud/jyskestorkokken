<?php

namespace HW\Gallery\Controller\Adminhtml\Gallery;

use Magento\Backend\App\Action;

class ImageGrid extends Action {
//class ImageGrid extends Magento\Backend\App\Action {

	/**
	* @var \Magento\Framework\View\Result\LayoutFactory
	*/
	protected $_resultLayoutFactory;

	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $_coreRegistry = null;

	/**
	 * @var \HW\Gallery\Model\Gallery
	 */
	protected $_model;

	public function __construct(
		Action\Context $context,
		\Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
		\Magento\Framework\Registry $registry,
		\HW\Gallery\Model\Gallery $model) {

		$this->_resultLayoutFactory = $resultLayoutFactory;
		$this->_coreRegistry = $registry;
		$this->_model = $model;

		parent::__construct($context);
	}

	public function execute()
	{
		$model = $this->_model;
		$id = $this->getRequest()->getParam('gallery_id');
		if(!$id) {
			$id = $this->getRequest()->getParam('id');
		}


		if ($id) {
			$model->load($id);
			if (!$model->getId()) {
				$this->messageManager->addError(__('This Gallery not exists.'));
				/** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
				$resultRedirect = $this->resultRedirectFactory->create();

				return $resultRedirect->setPath('*/gallery/index');
			}
		}

		$data = $this->_getSession()->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		$this->_coreRegistry->register('current_model', $model);

		$resultLayout = $this->_resultLayoutFactory->create();
		$resultLayout->getLayout()->getBlock('hwgallery.gallery.images');

		return $resultLayout;



	}
}