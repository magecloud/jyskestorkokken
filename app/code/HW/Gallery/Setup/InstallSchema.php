<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 21.03.2018
 * Time: 13:29
 */
namespace HW\Gallery\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
	/**
	 *
	 *
	 * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
	 * @param \Magento\Framework\Setup\ModuleContextInterface $context
	 *
	 * @return void
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		$tableNameGallery = 'hw_gallery';

		if(!$installer->tableExists($tableNameGallery)){
			$tableGallery = $installer->getConnection()->newTable(
				$installer->getTable($tableNameGallery)
			)
				->addColumn(
					'gallery_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'GALLERY_ID'
				)
				->addColumn(
					'identifier',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable' => false],
					'Gallery String Identifier'
				)
				->addColumn(
					'title',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable' => false],
					'Gallery Title'
				)
				->addColumn(
					'is_active',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					['nullable' => false, 'default' => '1'],
					'Is Gallery Active'
				)
				->addColumn(
					'type',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					['nullable' => false, 'default' => '0'],
					'Gallery Type'
				)
				->addColumn(
					'parameters',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'2M',
					[],
					'Gallery parameters'
				)
				->addIndex(
					$installer->getIdxName(
						$installer->getTable($tableNameGallery),
						['identifier','title']),
						['identifier','title']
				)
				->setComment(
					'Gallery Table'
				);
			$installer->getConnection()->createTable($tableGallery);
		}
		/*
		$tableName = ':: EDIT ME ::';
		if (!$installer->tableExists($tableName)) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable($tableName)
			)
			->addColumn(
				$tableName.'_id',
				\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
				null,
				[
					'identity' => true,
					'nullable' => false,
					'primary'  => true,
					'unsigned' => true,
				],
				'ID'
			)
			->addColumn(
				'text_examle',
				\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
				255,
				['nullable => false'],
				'Comment Here'
			)
			->addColumn(
				'created_at',
				\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
				null,
				[],
				'Created At'
			)
			->addIndex(
				$installer->getIdxName($tableName, [COLUMN_NAME]),
				[COLUMN_NAME]
			)
			->addForeignKey(
				$installer->getFkName($tableName, COLUMN_NAME, $tableNameFkName, COLUMNFK_NAME),
				COLUMN_NAME,
				$installer->getTable($tableNameFkName),
				COLUMNFK_NAME,
				Table::ACTION_CASCADE
			)
			->setComment('Optional Comment');

			$installer->getConnection()->createTable($table);

			// example to add index
			$installer->getConnection()->addIndex(
				$installer->getTable($tableName),
				$installer->getIdxName(
					$installer->getTable($tableName),
					[ 'column1', 'column2', ... ],
					\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
				),
				[ 'column1', 'column2', ... ],
				\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
			);
		}

		*/

		$installer->endSetup();
	}
}