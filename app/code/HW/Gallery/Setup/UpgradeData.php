<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 21.03.2018
 * Time: 16:56
 */
namespace HW\Gallery\Setup;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function upgrade(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		if (version_compare($context->getVersion(), '0.1.0', '<=')) {

			$defaultImages =[
				[
					'image_id'      => '1',
					'gallery_id'    => '1',
					'identifier'    => 'slide1',
					'title'         => 'Slide 1',
					'is_active'     => 1,
					'parameters'    => 'a:1:{s:4:"spot";s:339:"a:1:{i:1;a:10:{s:4:"kind";s:1:"1";s:5:"label";s:0:"";s:4:"link";s:1:"#";s:4:"html";s:95:"<h2><a href="http://mag2.headwayit.com/kategori1.html" target="_self">Læs mere her...</a></h2>";s:3:"top";s:3:"645";s:4:"left";s:3:"165";s:6:"height";s:2:"67";s:5:"width";s:3:"364";s:20:"css_predefined_class";s:8:"mb-trans";s:9:"css-class";s:0:"";}}";}',
					'position'      => 1,
					'file'          => '/s/l/slide1.jpg',
				],
				[
					'image_id'      => '2',
					'gallery_id'    => '1',
					'identifier'    => 'slide2',
					'title'         => 'Slide 2',
					'is_active'     => 1,
					'parameters'    => 'a:1:{s:4:"spot";s:411:"a:1:{i:1;a:10:{s:4:"kind";s:1:"1";s:5:"label";s:0:"";s:4:"link";s:0:"";s:4:"html";s:167:"<h2><span style="font-family: arial, helvetica, sans-serif; font-size: xx-large; background-color: #ffffff; color: #333333;">Lokale og Økologiske råvarer</span></h2>";s:3:"top";s:3:"712";s:4:"left";s:3:"527";s:6:"height";s:2:"49";s:5:"width";s:3:"486";s:20:"css_predefined_class";s:8:"mb-trans";s:9:"css-class";s:0:"";}}";}',
					'position'      => 2,
					'file'          => '/s/l/slide2.jpg',
				],
				[
					'image_id'      => '3',
					'gallery_id'    => '2',
					'identifier'    => 'banner1',
					'title'         => 'Brunchbitch',
					'is_active'     => 1,
					'parameters'    => 'a:3:{s:3:"url";s:25:"collections/yoga-new.html";s:15:"link_attributes";s:0:"";s:4:"html";s:0:"";}',
					'position'      => 1,
					'file'          => '/b/a/banner1.jpg',
				],
				[
					'image_id'      => '4',
					'gallery_id'    => '2',
					'identifier'    => 'banner2',
					'title'         => 'Sandwiches',
					'is_active'     => 1,
					'parameters'    => 'a:3:{s:3:"url";s:1:"#";s:15:"link_attributes";s:0:"";s:4:"html";s:19:"Frokosttilbud: 49,-";}',
					'position'      => 2,
					'file'          => '/b/a/banner2.jpg',
				],
				[
					'image_id'      => '5',
					'gallery_id'    => '2',
					'identifier'    => 'banner3',
					'title'         => 'Månedens menu',
					'is_active'     => 1,
					'parameters'    => 'a:3:{s:3:"url";s:1:"#";s:15:"link_attributes";s:0:"";s:4:"html";s:38:"Bestil bord nu til Marts måneds menu!";}',
					'position'      => 3,
					'file'          => '/b/a/banner3.jpg',
				]
			];

			$defaultImagesStore = [
				[
					'image_id' => 1,
					'store_id' => 0
				],
				[
					'image_id' => 2,
					'store_id' => 0
				],
				[
					'image_id' => 3,
					'store_id' => 0
				],
				[
					'image_id' => 4,
					'store_id' => 0
				],
				[
					'image_id' => 5,
					'store_id' => 0
				],
			];

			$installer->getConnection()->insertMultiple($setup->getTable('hw_gallery_image'), $defaultImages);
			$installer->getConnection()->insertMultiple($setup->getTable('hw_gallery_image_store'), $defaultImagesStore);
		}

		// if (version_compare($context->getVersion(), '1.1.0', '<=')) {
		//     $setup->getConnection()->query( SOME QUERY);
		// }

		$installer->endSetup();
	}
}