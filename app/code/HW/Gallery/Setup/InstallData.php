<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 21.03.2018
 * Time: 13:29
 */
namespace HW\Gallery\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$setup->startSetup();

		//     $setup->getConnection()->query( SOME QUERY);

		$defaultGalleries = [
			[
				'gallery_id'    => 1,
				'identifier'    => 'hw-home-page-slider',
				'title'         => 'Home page Slider',
				'is_active'     => 0,
				'type'          => 2,
				'parameters'    => 'a:9:{s:6:"design";s:7:"default";s:6:"effect";s:4:"fade";s:9:"frequency";s:4:"4000";s:8:"duration";s:3:"600";s:10:"show_title";s:1:"0";s:10:"show_pager";s:1:"0";s:15:"show_navigation";s:1:"1";s:9:"css_class";s:0:"";s:9:"css_rules";s:0:"";}'
			],
			[
				'gallery_id'    => 2,
				'identifier'    => 'hw_home-page-banners-grid',
				'title'         => 'Home page banners grid',
				'is_active'     => 0,
				'type'          => 1,
				'parameters'    => 'a:6:{s:6:"design";s:7:"default";s:10:"show_title";s:1:"0";s:15:"show_navigation";s:1:"1";s:9:"grid_cols";s:1:"3";s:9:"css_class";s:0:"";s:9:"css_rules";s:0:"";}'
			]
		];

		$setup->getConnection()->insertMultiple($setup->getTable('hw_gallery'), $defaultGalleries);

		$setup->endSetup();
	}
}