<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 21.03.2018
 * Time: 16:56
 */
namespace HW\Gallery\Setup;

use phpDocumentor\Reflection\Types\Null_;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();

		if (version_compare($context->getVersion(), '0.1.0', '<=')) {

			/**
			 * Create table 'hw_gallery_image'
			 */

			$tableNameImage = 'hw_gallery_image';

			if(!$installer->tableExists($tableNameImage)){
				$tableImage = $installer->getConnection()->newTable(
					$installer->getTable($tableNameImage)
				)
					->addColumn(
						'image_id',
						\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
						null,
						[
							'identity' => true,
							'nullable' => false,
							'primary'  => true,
							'unsigned' => true,
						],
						'IMAGE_ID'
					)
					->addColumn(
						'gallery_id',
						\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
						null,
						['unsigned' => true, 'nullable' => false, 'primary' => true],
						'Gallery ID'
					)
					->addColumn(
						'identifier',
						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						255,
						['nullable' => false],
						'Image String Identifier'
					)
					->addColumn(
						'title',
						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						255,
						['nullable' => false],
						'Image Title'
					)
					->addColumn(
						'is_active',
						\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
						null,
						['nullable' => false, 'default' => '1'],
						'Is Image Active'
					)
					->addColumn(
						'parameters',
						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'2M',
						[],
						'Image parameters'
					)
					->addColumn(
						'position',
						\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
						null,
						['nullable' => false, 'default' => '1'],
						'Image Position'
					)
					->addColumn(
						'file',
						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						255,
						['nullable' => true, 'default' => null],
						'Image file'
					)
					->addIndex(
						$installer->getIdxName(
							$installer->getTable($tableNameImage),
							['identifier','title']),
						['identifier','title']
					)
					->addIndex(
						$installer->getIdxName(
							$installer->getTable($tableNameImage),
							['gallery_id']),
						['gallery_id']
					)
					->addForeignKey(
						$installer->getFkName($installer->getTable($tableNameImage), 'gallery_id', $installer->getTable('hw_gallery'), 'gallery_id'),
						'gallery_id',
						$installer->getTable('hw_gallery'),
						'gallery_id',
						\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
					)
					->setComment(
						'Gallery Image Table'
					);
				$installer->getConnection()->createTable($tableImage);
			}


			/**
			 * Create table 'hw_gallery_image_store'
			 */
			$tableNameImageStore = 'hw_gallery_image_store';

			if(!$installer->tableExists($tableNameImageStore)){

				$table = $installer->getConnection()->newTable(
					$installer->getTable($tableNameImageStore)
				)->addColumn(
					'image_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					['nullable' => false, 'primary' => true, 'unsigned' => true],
					'Image ID'
				)->addColumn(
					'store_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					['unsigned' => true, 'nullable' => false, 'primary' => true],
					'Store ID'
				)->addIndex(
					$installer->getIdxName($installer->getTable($tableNameImageStore), ['store_id']),
					['store_id']
				)->addForeignKey(
					$installer->getFkName($installer->getTable($tableNameImageStore), 'image_id', $installer->getTable($tableNameImage), 'image_id'),
					'image_id',
					$installer->getTable($tableNameImage),
					'image_id',
					\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
				)->addForeignKey(
					$installer->getFkName($installer->getTable($tableNameImageStore), 'store_id', 'store', 'store_id'),
					'store_id',
					$installer->getTable('store'),
					'store_id',
					\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
				)->setComment(
					'Gallery image To Store Linkage Table'
				);
				$installer->getConnection()->createTable($table);
			}

		}

		$installer->endSetup();
	}
}