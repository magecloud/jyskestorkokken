var config = {
   
    paths: {
         "hw_gallery/cycle2"            : "HW_Gallery/js/lib/jquery.cycle2.min",
         "hw_gallery/cycle2_swipe"      : "HW_Gallery/js/lib/jquery.cycle2.swipe.min",
         "hw_gallery/owl_carousel"      : "HW_Gallery/js/lib/owl.carousel"
    },
     shim: {
        'hw_gallery/cycle2': {
            deps: ['jquery']
        },
         'hw_gallery/cycle2_swipe': {
            deps: ['jquery']
        },
         'hw_gallery/owl_carousel': {
            deps: ['jquery']
        },
     }
};





