var config = {
   
    paths: {
        "hw_gallery/prototype"         : "HW_Gallery/js/prototype/prototype",
        "hw_gallery/dragdrop"          : "HW_Gallery/js/dragdrop",
        "hw_gallery/prototype_effects" : "HW_Gallery/js/prototype/effects",
    },
     shim: {
        'hw_gallery/dragdrop': {
            deps: ['hw_gallery/prototype']
        },
        'hw_gallery/prototype_effects': {
            deps: ['hw_gallery/prototype']
        }
     }
};





