<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 13.07.2018
 * Time: 10:15
 */

namespace HW\Gallery\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

	/**
	 * @var \Magento\Cms\Model\Template\FilterProvider
	 */
	protected $_filterProvider;

	/**
	 * Store manager
	 *
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager;

	/**
	 * Data constructor.
	 *
	 * @param \Magento\Framework\App\Helper\Context      $context
	 * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 */
	public function __construct(
			\Magento\Framework\App\Helper\Context $context,
	        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
            \Magento\Store\Model\StoreManagerInterface $storeManager
	) {

		parent::__construct($context);
		$this->_filterProvider = $filterProvider;
		$this->_storeManager = $storeManager;
	}

	/**
	 * @param string $html
	 *
	 * @return string
	 */
	public function processedHtml($html){

		$storeId = $this->_storeManager->getStore()->getId();
		return $this->_filterProvider->getBlockFilter()->setStoreId($storeId)->filter($html);
	}

	/**
	 * @param string $file
	 *
	 * @return string
	 */
	public function getImageUrl($file) {

		return $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . \HW\Gallery\Model\Gallery\Image::PATCH_TO_SOURCE . $file;
	}

	public function useFauxSpots() {
//		return true;
		return false;
	}
}
