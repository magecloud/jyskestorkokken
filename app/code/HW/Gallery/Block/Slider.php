<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 03.07.2018
 * Time: 17:14
 */

namespace HW\Gallery\Block;

class Slider extends AbstractBlock {

	/**
	 * banner  template
	 * @var string
	 */
	protected $_template    = 'HW_Gallery::slider/default.phtml';

	protected $_type_code   = \HW\Gallery\Model\Gallery::TYPE_SLIDER;

	public function getTemplatePath($template){

		return 'HW_Gallery::slider/'.$template.'.phtml';
	}
}