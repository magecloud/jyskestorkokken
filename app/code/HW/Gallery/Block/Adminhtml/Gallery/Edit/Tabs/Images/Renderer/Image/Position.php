<?php

namespace HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs\Images\Renderer\Image;

class Position extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

	/**
	 * Render action.
	 *
	 * @param \Magento\Framework\DataObject $row
	 *
	 * @return string
	 */
	public function render(\Magento\Framework\DataObject $row)
	{
		$html = '';
//		$html = (0+$row->getData($this->getColumn()->getIndex())).'<br />';
		$html .='<input type="text" ';
		$html .= 'name="' . $this->getColumn()->getId() . '['.$row->getId().']" ';
		$html .= 'value="' . $row->getData($this->getColumn()->getIndex()) . '"';
		$html .= 'class="input-text ' . $this->getColumn()->getInlineCss() . '"/>';
		return $html;
	}
}