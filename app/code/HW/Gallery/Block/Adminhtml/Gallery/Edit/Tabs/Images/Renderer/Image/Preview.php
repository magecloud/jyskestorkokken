<?php

namespace HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs\Images\Renderer\Image;

class Preview extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

	/**
	 * Store manager.
	 *
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager;

	/**
	 * Preview constructor.
	 *
	 * @param \Magento\Backend\Block\Context             $context
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param array                                      $data
	 */
	public function __construct(
		\Magento\Backend\Block\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->_storeManager = $storeManager;
	}

	/**
	 * Render action.
	 *
	 * @param \Magento\Framework\DataObject $row
	 *
	 * @return string
	 */
	public function render(\Magento\Framework\DataObject $row)
	{
		$srcImage = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) .  \HW\Gallery\Model\Gallery\Image::PATCH_TO_SOURCE . $row->getFile();

		$html = '<image width="150" height="auto" src ="'.$srcImage.'" alt="'.$row->getFile().'" >';

		return $html;
	}
}