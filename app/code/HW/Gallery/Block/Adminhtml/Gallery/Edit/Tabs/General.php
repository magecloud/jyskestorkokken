<?php

namespace HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class General extends Generic implements TabInterface {

	/**
	 * @var \Magento\Config\Model\Config\Source\Yesno
	 */
	protected $_yesnoSourceModel;

	/**
	 * @var \HW\Gallery\Model\Gallery\Source\Options\Colcount
	 */
	protected $_colCountSourceModel;

	/**
	 * @var \HW\Gallery\Model\Gallery\Source\Galleries\Banners\Templates
	 */
	protected $_templatesBannersSourceModel;

	/**
	 * @var \HW\Gallery\Model\Gallery\Source\Galleries\Sliders\Templates
	 */
	protected $_templatesSlidersSourceModel;

	/**
	 * @var \HW\Gallery\Model\Gallery\Source\Galleries\Sliders\Effects
	 */
	protected $_effectsSlidersSourceModel;

	/**
	 * General constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context                      $context
	 * @param \Magento\Framework\Registry                                  $registry
	 * @param \Magento\Framework\Data\FormFactory                          $formFactory
	 * @param \Magento\Config\Model\Config\Source\Yesno                    $yesnoSourceModel
	 * @param \HW\Gallery\Model\Gallery\Source\Options\Colcount            $colCountSourceModel
	 * @param \HW\Gallery\Model\Gallery\Source\Galleries\Banners\Templates $templatesBannersSourceModel
	 * @param \HW\Gallery\Model\Gallery\Source\Galleries\Sliders\Templates $templatesSlidersSourceModel
	 * @param \HW\Gallery\Model\Gallery\Source\Galleries\Sliders\Effects   $effectsSlidersSourceModel
	 * @param array                                                        $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Data\FormFactory $formFactory,
		\Magento\Config\Model\Config\Source\Yesno $yesnoSourceModel,
		\HW\Gallery\Model\Gallery\Source\Options\Colcount $colCountSourceModel,
		\HW\Gallery\Model\Gallery\Source\Galleries\Banners\Templates $templatesBannersSourceModel,
		\HW\Gallery\Model\Gallery\Source\Galleries\Sliders\Templates $templatesSlidersSourceModel,
		\HW\Gallery\Model\Gallery\Source\Galleries\Sliders\Effects $effectsSlidersSourceModel,
        array $data = []
	) {
		$this->_yesnoSourceModel = $yesnoSourceModel;
		$this->_colCountSourceModel = $colCountSourceModel;
		$this->_templatesBannersSourceModel = $templatesBannersSourceModel;
		$this->_templatesSlidersSourceModel = $templatesSlidersSourceModel;
		$this->_effectsSlidersSourceModel = $effectsSlidersSourceModel;
		parent::__construct($context, $registry, $formFactory, $data);
	}

	/**
	 * @return $this
	 */
	protected function _prepareForm() {

		/** @var \HW\Gallery\Model\Gallery $model */
		$model = $this->_coreRegistry->registry('current_model');

		/** @var \Magento\Framework\Data\Form $form */
		$form = $this->_formFactory->create();


		$fieldset = $form->addFieldset(
			'base_fieldset',
			['legend' => __('General Information'), 'class' => 'fieldset-wide']
		);

		if ($model->getId()) {
			$fieldset->addField('gallery_id', 'hidden', ['name' => 'gallery_id']);
		}


		$fieldset->addField(
			'title',
			'text',
			['name' => 'title',
			 'label' => __('Gallery Title'),
			 'title' => __('Gallery Title'),
			 'required' => true
			]
		);

		$fieldset->addField(
			'identifier',
			'text',
			['name' => 'identifier',
			 'label' => __('Identifier'),
			 'title' => __('Identifier'),
			 'required' => true
			]
		);

		$fieldset->addField(
			'is_active',
			'select',
			['name' => 'is_active',
			 'label' => __('Status'),
			 'title' => __('Status'),
			 'required' => true,
			 'values' => $model->getAvailableStatuses()
			]
		);

		$_typeSwitcherActiveFlag = false;
		$noteOfType = null;
		if($model->getId() > 0 && $model->getType() > 0) {
			$_typeSwitcherActiveFlag = true;
		} else {
			$noteOfType = '<strong>'.__('You have to select gallery type and save gallery to see more parameters').'</strong>';
		}



		$fieldset->addField(
			'type',
			'select',
			['name'         => 'type',
			 'label'        => __('Type'),
			 'title'        => __('Type'),
			 'required'     => true,
			 'disabled'     => $_typeSwitcherActiveFlag,
			 'values'       => $model->getAvailableTypes(),
			 'note'         => $noteOfType,
			]
		);


		if($_typeSwitcherActiveFlag) {

			if($model->getType() == \HW\Gallery\Model\Gallery::TYPE_BANNERS_GRID){
				$fieldset->addField(
					'design',
					'select',
					['name'         => 'parameters[design]',
					 'label'        => __('Design'),
					 'required'     => true,
                     'values'       => $this->_templatesBannersSourceModel->toArray(),
					]
				);
			}

			if($model->getType() == \HW\Gallery\Model\Gallery::TYPE_SLIDER){
				$fieldset->addField(
					'design',
					'select',
					['name'         => 'parameters[design]',
					 'label'        => __('Design'),
					 'required'     => true,
                     'values'       => $this->_templatesSlidersSourceModel->toArray(),
					]
				);
			}


			$fieldsetAdditional = $form->addFieldset(
				'fieldset_additional',
				['legend' => __('Additional Parameters'), 'class' => 'fieldset-wide']
			);

			if($model->getType() == \HW\Gallery\Model\Gallery::TYPE_SLIDER){

				$fieldsetAdditional->addField(
					'effect',
					'select',
					['name'         => 'parameters[effect]',
					 'label'        => __('Effect'),
					 'required'     => false,
					 'default'      => 'fade',
					 'values'       => $this->_effectsSlidersSourceModel->toArray(),
					]
				);

				$fieldsetAdditional->addField(
					'frequency',
					'text',
					['name'         => 'parameters[frequency]',
					 'label'        => __('Frequency'),
					 'required'     => false,
					 'default'      => '1000',
					 'class'        => 'validate-number',
					 'note'         => __('The duration of a full jump, actual for "Slider".').' '.__('Default 4000')
					]
				);

				$fieldsetAdditional->addField(
					'duration',
					'text',
					['name'         => 'parameters[duration]',
					 'label'        => __('Duration'),
					 'required'     => false,
					 'default'      => '3000',
					 'class'        => 'validate-number',
					 'note'         => __('How long slides stays put before the next jump, actual for "Slider".').' '.__('Default 600')
					]
				);
			}

			$fieldsetAdditional->addField(
				'show_title',
				'select',
				['name' => 'parameters[show_title]',
				 'label' => __('Show title'),
				 'required' => false,
				 'default' => 0,
				 'values' => $this->_yesnoSourceModel->toArray(),
//				 'values' => [0 => __("No"), 1 => __("Yes")]
				]
			);

			if($model->getType() == \HW\Gallery\Model\Gallery::TYPE_SLIDER){
				$fieldsetAdditional->addField(
					'show_pager',
					'select',
					['name' => 'parameters[show_pager]',
					 'label' => __('Show pager'),
					 'required' => false,
					 'default' => 0,
					 'values' => $this->_yesnoSourceModel->toArray(),
					 'note'     => 'Only if script is used.'
					]
				);
			}

			$fieldsetAdditional->addField(
				'show_navigation',
				'select',
				['name' => 'parameters[show_navigation]',
				 'label' => __('Show navigation'),
				 'required' => false,
				 'default' => 0,
				 'values' => $this->_yesnoSourceModel->toArray(),
				 'note'     => 'Only if script is used.'
				]
			);

			if($model->getType() == \HW\Gallery\Model\Gallery::TYPE_BANNERS_GRID){
				$fieldsetAdditional->addField(
					'grid_cols',
					'select',
					['name' => 'parameters[grid_cols]',
					 'label' => __('Count of columns'),
					 'required' => false,
					 'default' => 3,
					 'values' => $this->_colCountSourceModel->toArray(),
					]
				);
			}

			$fieldsetAdditional->addField(
				'css_class',
				'text',
				['name' => 'parameters[css_class]',
				 'label' => __('CSS class for wrapping DIV'),
				 'required' => false
				]
			);

			$fieldsetAdditional->addField(
				'css_rules',
				'text',
				['name' => 'parameters[css_rules]',
				 'label' => __('CSS rules for wrapping DIV'),
				 'required' => false
				]
			);
		}

		/*
		$fieldset->addField(
			'name',
			'text',
			['name' => 'name', 'label' => __('SOME OTHER FIELD'), 'title' => __('SOME OTHER FIELD'), 'required' => true]
		);
		*/

		$form->setValues($model->getData());
		$this->setForm($form);
		return parent::_prepareForm();
	}

	/**
	 * Return Tab label
	 *
	 * @return string
	 * @api
	 */
	public function getTabLabel()
	{
		return __('General');
	}

	/**
	 * Return Tab title
	 *
	 * @return string
	 * @api
	 */
	public function getTabTitle()
	{
		return __('General');
	}

	/**
	 * Can show tab in tabs
	 *
	 * @return boolean
	 * @api
	 */
	public function canShowTab()
	{
		return true;
	}

	/**
	 * Tab is hidden
	 *
	 * @return boolean
	 * @api
	 */
	public function isHidden()
	{
		return false;
	}
}