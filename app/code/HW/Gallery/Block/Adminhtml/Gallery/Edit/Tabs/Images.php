<?php

namespace HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs;

class Images extends \Magento\Backend\Block\Widget\Grid\Extended {

	/**
	 * @var \HW\Gallery\Model\Gallery\Image
	 */
	protected $_galleryImage;

	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $_coreRegistry;

	/**
	 * @var \HW\Gallery\Model\Gallery
	 */
	protected $_gallery;

	/**
	 * Images constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Magento\Backend\Helper\Data            $backendHelper
	 * @param \HW\Gallery\Model\Gallery\Image         $galleryImage
	 * @param \Magento\Framework\Registry             $registry
	 * @param array                                   $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Backend\Helper\Data $backendHelper,
		\HW\Gallery\Model\Gallery\Image $galleryImage,
		\Magento\Framework\Registry $registry,
		array $data = []
	) {
//		$this->_backendHelper = $backendHelper;
//		$this->_backendSession = $context->getBackendSession();
		$this->_galleryImage = $galleryImage;
		$this->_coreRegistry = $registry;
		parent::__construct($context, $backendHelper, $data);
	}

	/**
	 * _construct
	 * @return void
	 */
	protected function _construct()
	{
		parent::_construct();
		$this->setId('imagesGrid');
		$this->setDefaultSort('image_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setUseAjax(true);

		$this->_gallery = $this->_coreRegistry->registry('current_model');
	}

	/**
	 * @return $this
	 */
	protected function _prepareLayout()
	{
		$this->setChild(
			'add_image_button',
			$this->getLayout()->createBlock(\Magento\Backend\Block\Widget\Button::class)
				->setData(array(
					'label'     => __('Add Image'),
					'onclick'   => 'location.replace(\''.$this->getUrl('*/images/create', array('gallery_id' => $this->_gallery->getId())).'\')',
					'class'   => 'action-secondary'
				))
		);

		return parent::_prepareLayout();
	}

	/**
	 * @param \Magento\Framework\Data\Collection $collection
	 *
	 * @return $this
	 */
	public function setCollection($collection)
	{
		parent::setCollection($collection);
		return $this;
	}

	/**
	 * @return $this
	 */
	protected function _prepareCollection() {

		$collection = $this->_gallery->getImageCollection();
		$this->setCollection($collection);
		parent::_prepareCollection();
		$this->_collection->addStoreData();
		return $this;
	}

	/**
	 * @return $this
	 */
	protected function _prepareColumns() {

		$this->addColumn(
			'image_id',
			[
				'header' => __('Image Id'),
				'type' => 'number',
				'index' => 'image_id',
				'header_css_class' => 'col-id',
				'column_css_class' => 'col-id',
			]
		);

		$this->addColumn(
			'image_identifier',
			[
				'header' => __('Identifier'),
				'index' => 'identifier',
			]
		);

		$this->addColumn(
			'image_title',
			[
				'header'       => __('Title'),
				'index'        => 'title',
				'width' => '50px',
			]
		);

		$this->addColumn(
			'image_image',
			[
				'header' => __('Image'),
				'filter' => false,
				'header_css_class' => 'width-150',
//				'index' => 'file',
				'renderer' => 'HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs\Images\Renderer\Image\Preview',
			]
		);

		$this->addColumn(
			'image_position',
			[
				'header'    => __('Order'),
//				'type'      => 'input',
				'index'     => 'position',
				'align'     =>'right',
				'filter'    => false,
				'inline_css'=> 'validation-number',
				'renderer' => 'HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs\Images\Renderer\Image\Position',
			]
		);

		/**
		 * Check is single store mode
		 */
		if (!$this->_storeManager->isSingleStoreMode()) {
			$this->addColumn(
				'image_stores',
				[
					'header' => __('Store View'),
					'index' => 'stores',
					'type' => 'store',
					'store_all' => true,
					'store_view' => true,
					'sortable' => false,
					'filter_condition_callback' => [$this, '_filterStoreCondition']
				]
			);
		}

		$this->addColumn('image_is_active', array(
			'header'    => __('Status'),
			'index'     => 'is_active',
			'type'      => 'options',
			'options'   => $this->_galleryImage->getAvailableStatuses(),
		));

		$this->addColumn(
			'image_action',
			[
				'header'    => __('Action'),
				'filter'    => false,
				'type'      => 'action',
				'sortable'  => false,
				'index'     => 'image_action',
				'getter'    => 'getId',
				'header_css_class' => 'col-action',
				'column_css_class' => 'col-action',
				'actions'   => array(
					array(
						'caption'   => __('Edit'),
						'url'       => array('base' => 'hwgallery/images/edit', 'params' => array('gallery_id' => $this->_gallery->getId()) ),
						'field'     => 'id'
					),
					array(
						'caption'   => __('Delete'),
						'url'       => array('base' => 'hwgallery/images/delete', 'params' => array('gallery_id' => $this->_gallery->getId()) ),
						'field'     => 'id',
						'confirm'   => __('Are you sure?'),
					)
				),
			]
		);

		return parent::_prepareColumns();
	}

	/**
	 * @return string
	 */
	public function getMainButtonsHtml()
	{
		return $this->getAddImageButtonHtml().parent::getMainButtonsHtml();
	}

	/**
	 * @return string
	 */
	public function getAddImageButtonHtml()
	{
		return $this->getChildHtml('add_image_button');
	}

	/**
	 * @return $this
	 */
	protected function _prepareMassaction() {
		$this->setMassactionIdField('image_id');
		$this->getMassactionBlock()->setFormFieldName('image_ids');
		$statuses = $this->_galleryImage->getAvailableStatuses();

//		array_unshift($statuses, array('label'=>'', 'value'=>''));

		$this->getMassactionBlock()->setTemplate('HW_Gallery::widget/grid/massaction_extended.phtml');

		$this->getMassactionBlock()->addItem('images_is_active', array(
			'label'=> __('Change status'),
			'url'  => $this->getUrl('*/images/massStatus', array('_current'=>true)),
			'additional' => array(
				'visibility' => array(
					'name' => 'images_is_active',
					'type' => 'select',
					'label' => __('Status'),
					'values' => $statuses
				)
			)
		));
		$this->getMassactionBlock()->addItem('images_delete', array(
			'label'=> __('Delete'),
			'url'  => $this->getUrl('*/images/massDelete', array('_current'=>true)),
			'confirm' => __('Are you sure?'),
		));
		return parent::_prepareMassaction();
	}

	/**
	 * @return string
	 */
	public function getGridUrl()
	{
		return $this->getUrl('*/gallery/imagegrid', ['_current' => true]);
	}

	/**
	 * get row url
	 * @param  object $row
	 * @return string
	 */
//	public function getRowUrl($row)
//	{
//		return '';
//	}

	/**
	 * Filter store condition
	 *
	 * @param \Magento\Framework\Data\Collection $collection
	 * @param \Magento\Framework\DataObject $column
	 * @return void
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	protected function _filterStoreCondition($collection, \Magento\Framework\DataObject $column)
	{
		if (!($value = $column->getFilter()->getValue())) {
			return;
		}

		$this->getCollection()->addStoreFilter($value);
	}


	/**
	 * Return Tab label
	 *
	 * @return string
	 * @api
	 */
	public function getTabLabel()
	{
		return __('Images');
	}

	/**
	 * Return Tab title
	 *
	 * @return string
	 * @api
	 */
	public function getTabTitle()
	{
		return __('Images');
	}


	/**
	 * Can show tab in tabs
	 *
	 * @return boolean
	 * @api
	 */
	public function canShowTab() {

		return true;
	}

	/**
	 * Tab is hidden
	 *
	 * @return boolean
	 * @api
	 */
	public function isHidden()
	{
		return false;
	}

}