<?php

namespace HW\Gallery\Block\Adminhtml\Gallery\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs {

	/**
	 * Tabs constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context  $context
	 * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
	 * @param \Magento\Backend\Model\Auth\Session      $authSession
	 * @param \Magento\Framework\Registry              $registry
	 * @param array                                    $data
	 */
	public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $data = []
    ) {

		parent::__construct($context, $jsonEncoder, $authSession, $data);
    }

	/**
	 * Init
	 *
	 * @return void
	 */
	protected function _construct() {

		parent::_construct();
		$this->setId('hwgallery_edit_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(__('Gallery Information'));
	}

	/**
	 * @return $this
	 */
	protected function _beforeToHtml() {

		$this->addTab(
			'general_section',
			[
				'label'     => __('General'),
				'title'     => __('General'),
				'content'   => $this->getLayout()->createBlock('HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs\General')->toHtml(),
				'active'    => ( $this->getRequest()->getParam('active_tab') == 'general_section' ) ? true : false,
			]
		);

		$this->addTab(
			'images_section',
			[
				'label'     => __('Images'),
				'title'     => __('Images'),
				'content'   => $this->getLayout()->createBlock('HW\Gallery\Block\Adminhtml\Gallery\Edit\Tabs\Images')->toHtml(),
				'active'    => ( $this->getRequest()->getParam('active_tab') == 'images_section' ) ? true : false,
			]
		);

		return parent::_beforeToHtml();
	}
}
