<?php

namespace HW\Gallery\Block\Adminhtml\Images\Edit\Tabs;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class General extends Generic implements TabInterface {

	/**
	 * Core system store model
	 *
	 * @var \Magento\Store\Model\System\Store
	 */
	protected $_systemStore;

	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Data\FormFactory $formFactory,
		\Magento\Store\Model\System\Store $systemStore,
        array $data = []
	) {
		$this->_systemStore = $systemStore;
		parent::__construct($context, $registry, $formFactory, $data);
	}

	protected function _prepareForm() {

		/** @var \HW\Gallery\Model\Gallery\Image $model */
		$model = $this->_coreRegistry->registry('current_model');

		/** @var \HW\Gallery\Model\Gallery $modelGallery */
		$modelGallery = $this->_coreRegistry->registry('gallery_model');

		/** @var \Magento\Framework\Data\Form $form */
		$form = $this->_formFactory->create();


		$fieldset = $form->addFieldset(
			'base_fieldset',
			['legend' => __('General Information'), 'class' => 'fieldset-wide']
		);

		$fieldset->addType('imagegallery' , '\HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Form\Element\Imagegallery');

		if ($model->getId()) {
			$fieldset->addField('image_id', 'hidden', ['name' => 'image_id']);

		}
		$fieldset->addField('gallery_id', 'hidden', ['name' => 'gallery_id']);

		$fieldset->addField(
			'title',
			'text',
			['name' => 'title',
			 'label' => __('Image Title'),
			 'title' => __('Image Title'),
			 'required' => true
			]
		);

		$fieldset->addField(
			'identifier',
			'text',
			['name' => 'identifier',
			 'label' => __('Identifier'),
			 'title' => __('Identifier'),
			 'required' => true
			]
		);

		$fieldset->addField(
			'is_active',
			'select',
			['name' => 'is_active',
			 'label' => __('Status'),
			 'title' => __('Status'),
			 'required' => true,
			 'values' => $model->getAvailableStatuses()
			]
		);

		$fieldset->addField(
			'position',
			'text',
			[
			'label'     => __('Position'),
			'class'     => 'validate-number',
			'required'  => false,
			'name'      => 'position',
			]
		);

		$fieldset->addField(
			'file',
			'imagegallery',
			[
				'label' => __('Image'),
				'name' => 'file',
				'note' => 'Allow image type: jpg, jpeg, gif, png',
			]
		);

		if (!$this->_storeManager->isSingleStoreMode()) {
			$field = $fieldset->addField('stores', 'multiselect',
				[
				'name'      => 'stores[]',
				'label'     => __('Visibility'),
				'required'  => true,
				'values'    => $this->_systemStore->getStoreValuesForForm(false,true),
				]
			);
			$renderer = $this->getLayout()->createBlock(
				\Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element::class
			);
			$field->setRenderer($renderer);
		}
		else {
			$model->setStores($this->_storeManager->getStore()->getId());
			$fieldset->addField('stores', 'hidden', array(
				'name'      => 'stores[]',
				'value'     => $this->_storeManager->getStore()->getId()
			));

		}

		if ($modelGallery->getType() == \HW\Gallery\Model\Gallery::TYPE_BANNERS_GRID){
			$fieldset->addField('url', 'text',
				[
				'label'     => __('Url'),
				'name'      => 'parameters[url]',
				'required'  => true,
				'note'      => __('Use <b>"http://"</b> for external urls.'),
				]
			);

			$fieldset->addField('link_attributes', 'text',
				[
				'label'     => __('Link attributes'),
				'name'      => 'parameters[link_attributes]',
				'note'      => __('You can use <strong>target="_blank"</strong>, <strong>rel="nofollow"</strong> or similar'),
				]
			);

			$fieldset->addField('html', 'textarea',
				[
				'label'     => __('Description'),
				'name'      => 'parameters[html]',
				]
			);
		}
		$form->setValues($model->getData());
		$this->setForm($form);
		return parent::_prepareForm();
	}

	/**
	 * Return Tab label
	 *
	 * @return string
	 * @api
	 */
	public function getTabLabel()
	{
		return __('General');
	}

	/**
	 * Return Tab title
	 *
	 * @return string
	 * @api
	 */
	public function getTabTitle()
	{
		return __('General');
	}

	/**
	 * Can show tab in tabs
	 *
	 * @return boolean
	 * @api
	 */
	public function canShowTab()
	{
		return true;
	}

	/**
	 * Tab is hidden
	 *
	 * @return boolean
	 * @api
	 */
	public function isHidden()
	{
		return false;
	}
}