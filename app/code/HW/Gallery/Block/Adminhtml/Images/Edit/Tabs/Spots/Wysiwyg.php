<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 17.07.2018
 * Time: 12:14
 */
namespace HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Spots;

class Wysiwyg extends \Magento\Backend\Block\Widget\Form\Generic{

	/**
	 * @var \Magento\Cms\Model\Wysiwyg\Config
	 */
	protected $_wysiwygConfig;

	/**
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Magento\Framework\Registry $registry
	 * @param \Magento\Framework\Data\FormFactory $formFactory
	 * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
	 * @param array $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Data\FormFactory $formFactory,
		\Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
		array $data = []
	) {
		$this->_wysiwygConfig = $wysiwygConfig;
		parent::__construct($context, $registry, $formFactory, $data);
	}

	/**
	 * Prepare form.
	 * Adding editor field to render
	 *
	 * @return Form
	 */
	protected function _prepareForm()
	{
		/** @var \Magento\Framework\Data\Form $form */
		$form = $this->_formFactory->create(
			[
				'data' => ['id' => 'wysiwyg_edit_form', 'action' => $this->getData('action'), 'method' => 'post'],
			]
		);

		$config['document_base_url'] = $this->getData('store_media_url');
		$config['store_id'] = $this->getData('store_id');
		$config['add_images'] = false;
		$config['add_variables'] = true;
		$config['add_widgets'] = false;
		$config['add_directives'] = true;
		$config['use_container'] = true;
		$config['container_class'] = 'hor-scroll';

		$form->addField(
			$this->getData('editor_element_id'),
			'editor',
			[
				'name' => 'content',
				'style' => 'width:725px;height:460px',
				'required' => true,
				'force_load' => true,
				'config' => $this->_wysiwygConfig->getConfig($config)
			]
		);
		$this->setForm($form);
		return parent::_prepareForm();
	}
}