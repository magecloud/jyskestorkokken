<?php

namespace HW\Gallery\Block\Adminhtml\Images\Edit\Tabs;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Fauxspots extends Generic implements TabInterface {

	/**
	 * Core system store model
	 *
	 * @var \Magento\Store\Model\System\Store
	 */
	protected $_systemStore;

	/**
	 * @var \HW\Gallery\Model\Gallery\Image\Source\Spot\Type
	 */
	protected $_spotType;

	/**
	 * Fauxspots constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context          $context
	 * @param \Magento\Framework\Registry                      $registry
	 * @param \Magento\Framework\Data\FormFactory              $formFactory
	 * @param \Magento\Store\Model\System\Store                $systemStore
	 * @param \HW\Gallery\Model\Gallery\Image\Source\Spot\Type $spotType
	 * @param array                                            $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Data\FormFactory $formFactory,
		\Magento\Store\Model\System\Store $systemStore,
		\HW\Gallery\Model\Gallery\Image\Source\Spot\Type $spotType,
        array $data = []
	) {
		$this->_systemStore = $systemStore;
		$this->_spotType = $spotType;
		parent::__construct($context, $registry, $formFactory, $data);
	}

	/**
	 * @return $this
	 */
	protected function _prepareForm() {

		/** @var \HW\Gallery\Model\Gallery\Image $model */
		$model = $this->_coreRegistry->registry('current_model');

		/** @var \HW\Gallery\Model\Gallery $modelGallery */
		//$modelGallery = $this->_coreRegistry->registry('gallery_model');

		/** @var \Magento\Framework\Data\Form $form */
		$form = $this->_formFactory->create();

		$countSpots =  $model->getSpotCount();

		for ($i=1;($i<=$countSpots);$i++){

			$fieldset = $form->addFieldset(
				'spot'.$i.'_fieldset',
				['legend' => __('Spot '.$i), 'class' => 'fieldset-wide']
			);


			$fieldset->addField(
				'spot_'.$i.'_kind',
				'select',
				['name' => 'spot['.$i.'][kind]',
				 'label' => __('Kind'),
				 'required' => true,
				 'values'  => $this->_spotType->toOptionArray()
				]
			);
			$fieldset->addField(
				'spot_'.$i.'_label',
				'text',
				['name' => 'spot['.$i.'][label]',
				 'label' => __('Label'),
				]
			);
			$fieldset->addField(
				'spot_'.$i.'_link',
				'text',
				['name' => 'spot['.$i.'][link]',
				 'label' => __('Link'),
				]
			);

			$fieldset->addField(
				'spot_'.$i.'_html',
				'text',
				['name' => 'spot['.$i.'][html]',
				 'label' => __('Html'),
				]
			);

			$fieldset->addField(
				'spot_'.$i.'_top',
				'text',
				['name' => 'spot['.$i.'][top]',
				 'label' => __('Top'),
				]
			);

			$fieldset->addField(
				'spot_'.$i.'_left',
				'text',
				['name' => 'spot['.$i.'][left]',
				 'label' => __('Left'),
				]
			);

			$fieldset->addField(
				'spot_'.$i.'_height',
				'text',
				['name' => 'spot['.$i.'][height]',
				 'label' => __('Height'),
				]
			);

			$fieldset->addField(
				'spot_'.$i.'_width',
				'text',
				['name' => 'spot['.$i.'][width]',
				 'label' => __('Width'),
				]
			);

			$fieldset->addField(
				'spot_'.$i.'_css-class',
				'text',
				['name' => 'spot['.$i.'][css-class]',
				 'label' => __('Css classes'),
				 'note'	=> __('Separate by space')
				]
			);
			$fieldset->addField(
				'spot_'.$i.'_css_predefined_class',
				'hidden',
				['name' => 'spot['.$i.'][css_predefined_class]',
				]
			);
			$fieldset->addField(
				'spot_'.$i.'_remove',
				'checkbox',
				['name' => 'spot['.$i.'][remove]',
				 'label' => __('Remove Spot'),
				]
			);
		}

//		$fieldsetButton = $form->addFieldset(
//			'add_spot_button_fieldset',
//			['legend' => __('Add spot'), 'class' => 'fieldset-wide']
//		);
//
//		$fieldsetButton->addField(
//			'add_spot_button',
//			'button',
//			['name' => 'add_spot',
//			 'label' => __('Add spot'),
//			 'default' => __('Add spot'),
//			 'value' => __('Add spot'),
//			 'note'	=> __('One more spot')
//			]
//		);

//		var_dump($model->getData());


		$form->setValues($model->getData());
		$this->setForm($form);
		return parent::_prepareForm();
	}

	/**
	 * Return Tab label
	 *
	 * @return string
	 * @api
	 */
	public function getTabLabel()
	{
		return __('Spots editor');
	}

	/**
	 * Return Tab title
	 *
	 * @return string
	 * @api
	 */
	public function getTabTitle()
	{
		return __('Spots editor');
	}

	/**
	 * Can show tab in tabs
	 *
	 * @return boolean
	 * @api
	 */
	public function canShowTab()
	{
		return true;
	}

	/**
	 * Tab is hidden
	 *
	 * @return boolean
	 * @api
	 */
	public function isHidden()
	{
		return false;
	}
}