<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 30.03.2018
 * Time: 14:54
 */
namespace HW\Gallery\Block\Adminhtml\Images\Edit;

use \Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic {

	/**
	 * Form constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context  $context
	 * @param \Magento\Framework\Registry              $registry
	 * @param \Magento\Framework\Data\FormFactory      $formFactory
	 * @param array                                    $data
	 */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct() {

        parent::_construct();
        $this->setId('hwgallery_form');
        $this->setTitle(__('Image Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm() {

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data']]
        );

        $form->setHtmlIdPrefix('hwgallery_');

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}