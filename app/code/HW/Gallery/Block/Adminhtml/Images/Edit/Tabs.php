<?php

namespace HW\Gallery\Block\Adminhtml\Images\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs {

	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $_coreRegistry;

	/**
	 * @var \HW\Gallery\Model\Gallery
	 */
	protected $_gallery;

	/**
	 * @var \HW\Gallery\Model\Image
	 */
	protected $_image;

	/**
	 * @var \HW\Gallery\Helper\Data
	 */
	protected $_helper;

	/**
	 * Tabs constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context  $context
	 * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
	 * @param \Magento\Backend\Model\Auth\Session      $authSession
	 * @param \Magento\Framework\Registry              $registry
	 * @param \HW\Gallery\Helper\Data                  $helper
	 * @param array                                    $data
	 */
	public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Registry $registry,
        \HW\Gallery\Helper\Data $helper,
        array $data = []
    ) {
		$this->_coreRegistry = $registry;
		$this->_helper       = $helper;
		parent::__construct($context, $jsonEncoder, $authSession, $data);
    }

	/**
	 * Init
	 *
	 * @return void
	 */
	protected function _construct() {

		parent::_construct();
		$this->setId('hwgallery_edit_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(__('Image Information'));

		$this->_gallery = $this->_coreRegistry->registry('gallery_model');

		$this->_image   = $this->_coreRegistry->registry('current_model');
	}

	/**
	 * @return $this
	 */
	protected function _beforeToHtml() {

		$this->addTab(
			'general_section',
			[
				'label'     => __('General'),
				'title'     => __('General'),
				'content'   => $this->getLayout()->createBlock('HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\General')->toHtml(),
				'active'    => ( $this->getRequest()->getParam('active_tab') == 'general_section' ) ? true : false,
			]
		);

		if($this->_helper->useFauxSpots() && $this->_gallery->getType() == \HW\Gallery\Model\Gallery::TYPE_SLIDER){
			$this->addTab(
				'images_section',
				[
					'label'     => __('Spot Editor'),
					'title'     => __('Spot Editor'),
					'content'   => $this->getLayout()->createBlock('HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Fauxspots')->toHtml(),
//					'content'   => __('Comming soon'),
					'active'    => ( $this->getRequest()->getParam('active_tab') == 'spot_section' ) ? true : false,
				]
			);
		}

		if(!$this->_helper->useFauxSpots() && $this->_gallery->getType() == \HW\Gallery\Model\Gallery::TYPE_SLIDER){

			if($this->_showSpotsTab()) {
				$_content = $this->getLayout()->createBlock('HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Spots')->toHtml();
			} else {
				$_content = __('Upload image first.');
			}

			$this->addTab(
				'images_section_dev',
				[
					'label'     => __('Spot Editor'),
					'title'     => __('Spot Editor'),
//					'content'   => $this->getLayout()->createBlock('HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Spots')->toHtml(),
					'content'   => $_content,
					'active'    => ( $this->getRequest()->getParam('active_tab') == 'spot_section' ) ? true : false,
				]
			);
		}

		return parent::_beforeToHtml();
	}


	/**
	 * @return bool
	 */
	protected function _showSpotsTab(){

		if($this->_image->getId() && $this->_image->getFile()) {
			return true;
		}

		return false;
	}
}
