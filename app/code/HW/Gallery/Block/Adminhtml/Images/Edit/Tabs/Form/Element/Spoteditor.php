<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 12.07.2018
 * Time: 18:08
 */
namespace HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Form\Element;

class Spoteditor extends \Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element implements
	\Magento\Framework\Data\Form\Element\Renderer\RendererInterface {

	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $_coreRegistry;

	/**
	 * @var \HW\Gallery\Model\Gallery\Image\Source\Spot\Type
	 */
	protected $_spotType;

	/**
	 * @var \Magento\Framework\Serialize\Serializer\Serialize
	 */
	protected $_serializer;

	/**
	 * @var \HW\Gallery\Helper\Data
	 */
	protected $_helper;

	/**
	 * Spoteditor constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context           $context
	 * @param \Magento\Framework\Registry                       $registry
	 * @param \HW\Gallery\Model\Gallery\Image\Source\Spot\Type  $spotType
	 * @param \Magento\Framework\Serialize\Serializer\Serialize $serializer
	 * @param \HW\Gallery\Helper\Data                           $helper
	 * @param array                                             $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\HW\Gallery\Model\Gallery\Image\Source\Spot\Type $spotType,
		\Magento\Framework\Serialize\Serializer\Serialize $serializer,
		\HW\Gallery\Helper\Data $helper,
		array $data = [])
	{
		$this->_coreRegistry    = $registry;
		$this->_spotType        = $spotType;
		$this->_serializer      = $serializer;
		$this->_helper          = $helper;
		parent::__construct($context, $data);
	}

	/**
	 * Render element
	 *
	 * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
	 * @return string
	 */
	public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
	{
		/** @var \HW\Gallery\Model\Gallery\Image $model */
		$model = $this->_coreRegistry->registry('current_model');
		$info  = $model->getSpot();
//		var_dump($model->getSpot()); die();
		$i=1;
		$rows='';
		$spots='';

		if(empty($info)) {
			$info = [];
		}

		foreach($info as $item){
			if(!isset($item["css_predefined_class"])) {$item["css_predefined_class"] = '';}
			$rows.='
						<tr spotnum="'.$i.'">
							<td class="a-left ">
								'.$i.'
							</td>												
							<td class="a-left ">								
								<select  title="Gallery Status" name="spot['.$i.'][kind]" id="spot_kind_'.$i.'" onchange="changeKind('.$i.',this)">									
									<option '.($item["kind"]?'selected="selected" ':'').' value="1">'.__('HTML').'</option>
									<option '.(!$item["kind"]?'selected="selected" ':'').' value="0">'.__('Simple').'</option>
								</select>								
							</td>
							<td class="a-left ">
								<input style="margin-bottom:1px;'.($item["kind"]?'display:none;" ':'').'" type="text" class="input-text no-changes " value="'.$item["label"].'" name="spot['.$i.'][label]" onkeyup="changeLabel('.$i.', this)"  id="spot_'.$i.'_label" />
								<input style="'.($item["kind"]?'display:none;" ':'').'" type="text" class="input-text no-changes " value="'.$item["link"].'" name="spot['.$i.'][link]"  id="spot_'.$i.'_link" />
								<textarea style="display:none;" type="hidden"  name="spot['.$i.'][html]" id="spot_'.$i.'_html" spotnum="'.$i.'" >'.$item["html"].'</textarea>
								<button  id="spot_'.$i.'_btn" style="'.(!$item["kind"]?'display:none; ':'').'" onclick="hwGalleryWysiwygEditor.open(\''.$this->getUrl('hwgallery/images/wysiwyg').'\', \'spot_'.$i.'_html\')" class="scalable " type="button" ><span>'.__('WYSIWYG Editor').'</span></button>
							</td>							
							<td class="a-left ">
								<input type="text" class="input-text no-changes top-pos" value="'.$item["top"].'" name="spot['.$i.'][top]" onkeyup="setTop('.$i.', this)" /><br>
								<input type="text" class="input-text no-changes left-pos"  style="margin-top: 1px;" value="'.$item["left"].'" name="spot['.$i.'][left]" onkeyup="setLeft('.$i.', this)" />
							</td>
							<td class="a-left ">
								<button style="text-align:left" onclick="goTop('.$i.',this)" class="scalable " type="button" ><span>'.__('Up').'</span></button><br>
								<button style="text-align:left;margin-top: 1px;" onclick="goLeft('.$i.',this)" class="scalable " type="button" ><span>'.__('Left').'</span></button>
							</td>
							<td class="a-left ">
								<button style="text-align:left" onclick="goBottom('.$i.',this)" class="scalable " type="button" ><span>'.__('Down').'</span></button><br>
								<button style="text-align:left;margin-top: 1px;" onclick="goRight('.$i.',this)" class="scalable " type="button" ><span>'.__('Right').'</span></button>
							</td>
							<td class="a-left ">
								<input type="text" class="input-text no-changes height-pos" value="'.$item["height"].'" name="spot['.$i.'][height]" onkeyup="setHeight('.$i.', this)" /><br>
								<input type="text" class="input-text no-changes width-pos"  style="margin-top: 1px;"  value="'.$item["width"].'" name="spot['.$i.'][width]" onkeyup="setWidth('.$i.', this)" />								
							</td>
							<td class="a-left ">
								<button style="text-align:left" onclick="goHigher('.$i.',this)" class="scalable " type="button" ><span>Height+</span></button><br>
								<button style="text-align:left;margin-top: 1px;" onclick="goWider('.$i.',this)" class="scalable " type="button" ><span>Width+</span></button>								
							</td>
							<td class="a-left ">
								<button style="text-align:left" onclick="goLower('.$i.',this)" class="scalable " type="button" ><span>Height-</span></button><br>
								<button style="text-align:left;margin-top: 1px;" onclick="goNarrow('.$i.',this)" class="scalable " type="button" ><span>Width-</span></button>								
							</td>
							<td class="a-left ">
								<select  title="CSS predefined class" name="spot['.$i.'][css_predefined_class]" id="spot_css_predefined_class_'.$i.'" onchange="changeCSS('.$i.',this)">									
									<option '.($item["css_predefined_class"]=="mb-trans"?'selected="selected" ':'').' value="mb-trans">'.__('Transparent').'</option>
									<option '.($item["css_predefined_class"]=="mb-noarrows"?'selected="selected" ':'').' value="mb-noarrows">'.__('No Arrows').'</option>
									<option '.($item["css_predefined_class"]=="mb-topleft"?'selected="selected" ':'').' value="mb-topleft">'.__('Top Left').'</option>
									<option '.($item["css_predefined_class"]=="mb-topright"?'selected="selected" ':'').' value="mb-topright">'.__('Top Right').'</option>
									<option '.($item["css_predefined_class"]=="mb-bottomleft"?'selected="selected" ':'').' value="mb-bottomleft">'.__('Bottom Left').'</option>
									<option '.($item["css_predefined_class"]=="mb-bottomright"?'selected="selected" ':'').' value="mb-bottomright">'.__('Bottom Right').'</option>
									<option '.($item["css_predefined_class"]=="mb-centerleft"?'selected="selected" ':'').' value="mb-centerleft">'.__('Center Left').'</option>
									<option '.($item["css_predefined_class"]=="b-centerright"?'selected="selected" ':'').' value="mb-centerright">'.__('Center Right').'</option>
								</select>															
								<input type="text" class="input-text no-changes" value="'.$item["css-class"].'" name="spot['.$i.'][css-class]"><br>
							</td>							
							<td class="a-left "><button style="text-align:left" onclick="deleteSpot(this)" class="scalable " type="button" ><span>Delete Spot</span></button></td>
						</tr>	
			';
			$spots.='<div class="spotel '.$item["css_predefined_class"].'" spotnum="'.$i.'" id="spotel-'.$i.'" style="overflow:hidden;cursor:move;height:'.$item["height"].'px;left:'.$item["left"].'px;position:absolute;top:'.$item["top"].'px;width:'.$item["width"].'px;">'
				.'<div  class="spoth"  id="spoth-'.$i.'"  style="position:absolute;top:0px;left:0px;">'.$i.'</div>'
				.'<div id="spotcn-'.$i.'" style="background:none;">'.($item["kind"]?$item["html"]:$item["label"]).'</div>'
				.'<div class="spotrs" spotnum="'.$i.'"  id="spoters-'.$i.'" style="cursor:nw-resize;right:0px;position:absolute;bottom:0px;"></div>'
				.'</div>';
			$i++;
		}

		if(true || $model->getFile()){
			$trUp           =__("Up");
			$trLeft         =__("Left");
			$trDown         =__("Down");
			$trRight        =__("Right");
			$trHeightP      =__("Height+");
			$trWidthP       =__("Width+");
			$trHeightM      =__("Height-");
			$trWidthM       =__("Width-");
			$trDeleteSpot   =__("Delete Spot");
			$trHTML         =__('HTML');
			$trSimple       =__('Simple');
			$trWYSIWYG      =__('WYSIWYG Editor');
			$trTransparent  =__('Transparent');
			$trNoArrows     =__('No Arrows');
			$trTopLeft      =__('Top Left');
			$trTopRight     =__('Top Right');
			$trBottomLeft   =__('Bottom Left');
			$trBottomRight  =__('Bottom Right');
			$trCenterLeft   =__('Center Left');
			$trCenterRight  =__('Center Right');
			$trTitle        =__('Title');
			$trLink         =__('Link or product sku');

			$trUrl          ="#";
			$trUrl          =$this->getUrl('hwgallery/images/wysiwyg');
			$html =<<<EOT
            <script type="text/javascript">        
				function editSpot(el){\$('spotEditor').show();\$(el).up('div').remove();}
				function deleteSpot(el){id=\$(el).up('tr').readAttribute('spotnum');\$(el).up('tr').remove();\$('spotel-'+id).remove();}
				function goTop(id,el){\$('spotel-'+id).setStyle({top:(parseInt(\$('spotel-'+id).getStyle('top'))-1)+'px'});\$(el).up('tr').down('input.top-pos').value=parseInt(\$('spotel-'+id).getStyle('top'));}
				function goBottom(id,el){\$('spotel-'+id).setStyle({top:(parseInt(\$('spotel-'+id).getStyle('top'))+1)+'px'});\$(el).up('tr').down('input.top-pos').value=parseInt(\$('spotel-'+id).getStyle('top'));}
				function goLeft(id,el){\$('spotel-'+id).setStyle({left:(parseInt(\$('spotel-'+id).getStyle('left'))-1)+'px'});\$(el).up('tr').down('input.left-pos').value=parseInt(\$('spotel-'+id).getStyle('left'));}
				function goRight(id,el){\$('spotel-'+id).setStyle({left:(parseInt(\$('spotel-'+id).getStyle('left'))+1)+'px'});\$(el).up('tr').down('input.left-pos').value=parseInt(\$('spotel-'+id).getStyle('left'));}				
				function setTop(id,el){\$('spotel-'+id).setStyle({top:(parseInt(\$(el).value))+'px'});}
				function setLeft(id,el){\$('spotel-'+id).setStyle({left:(parseInt(\$(el).value))+'px'});}				

				function goHigher(id,el){\$('spotel-'+id).setStyle({height:(parseInt(\$('spotel-'+id).getStyle('height'))+1)+'px'});\$(el).up('tr').down('input.height-pos').value=parseInt(\$('spotel-'+id).getStyle('height'));}
				function goLower(id,el){\$('spotel-'+id).setStyle({height:(parseInt(\$('spotel-'+id).getStyle('height'))-1)+'px'});\$(el).up('tr').down('input.height-pos').value=parseInt(\$('spotel-'+id).getStyle('height'));}
				function goWider(id,el){\$('spotel-'+id).setStyle({width:(parseInt(\$('spotel-'+id).getStyle('width'))+1)+'px'});\$(el).up('tr').down('input.width-pos').value=parseInt(\$('spotel-'+id).getStyle('width'));}
				function goNarrow(id,el){\$('spotel-'+id).setStyle({width:(parseInt(\$('spotel-'+id).getStyle('width'))-1)+'px'});\$(el).up('tr').down('input.width-pos').value=parseInt(\$('spotel-'+id).getStyle('width'));}				
				function setHeight(id,el){\$('spotel-'+id).setStyle({height:(parseInt(\$(el).value))+'px'});}
				function setWidth(id,el){\$('spotel-'+id).setStyle({width:(parseInt(\$(el).value))+'px'});}				
				
				function changeKind(id,el){i=parseInt(\$(el).value);if(i){\$('spot_'+id+'_label').hide();\$('spot_'+id+'_link').hide();\$('spot_'+id+'_btn').show();}else{\$('spot_'+id+'_label').show();\$('spot_'+id+'_link').show();\$('spot_'+id+'_btn').hide();}}
				function changeCSS(id,el){\$('spotel-'+id).writeAttribute('class','');\$('spotel-'+id).addClassName('spotel');\$('spotel-'+id).addClassName(\$(el).value);}
				function doMove(el,event){
				    id=\$(el.element).readAttribute('spotnum');
					\$('spot-table').down('input[name="spot['+id+'][top]"]').value=parseInt(\$('spotel-'+id).getStyle('top'));
					\$('spot-table').down('input[name="spot['+id+'][left]"]').value=parseInt(\$('spotel-'+id).getStyle('left'));
				}
				function doResize(el,event){				
				    id=\$(el.element).readAttribute('spotnum');
					w=parseInt(\$('spoters-'+id).getStyle('left'))+10-parseInt(\$('spotel-'+id).getStyle('padding-left'))-parseInt(\$('spotel-'+id).getStyle('padding-right'));
					h=parseInt(\$('spoters-'+id).getStyle('top'))+10;
					if(w<10){w=10}
					if(h<10){h=10}					
					\$('spot-table').down('input[name="spot['+id+'][width]"]').value=w;
					\$('spot-table').down('input[name="spot['+id+'][height]"]').value=h;
				    \$('spotel-'+id).setStyle({height:h+'px'});
					\$('spotel-'+id).setStyle({width:w+'px'});				
					\$('spoters-'+id).setStyle({left:null});
					\$('spoters-'+id).setStyle({top:null});					
				}
				
				function updateSpot(el){
					id=\$(el).readAttribute('spotnum');
					\$('spotcn-'+id).update(\$('spot_'+id+'_html').value);
				}
				
				function changeLabel(id,el){\$('spotcn-'+id).update(\$(el).value);}				

				var currentId=$i;
				var	spotTemplate=
						'<tr spotnum="[[id]]">'+
						'	<td class="a-left ">'+
						'		[[id]]'+
						'	</td>'+

						'	<td class="a-left ">'+								
						'		<select  title="Gallery Status" name="spot[[[id]]][kind]" id="spot_kind_[[id]]" onchange="changeKind([[id]],this)">'+
						'			<option  value="1">$trHTML</option>'+
						'			<option selected="selected" value="0">$trSimple</option>'+
						'		</select>'+
						'	</td>'+
						'	<td class="a-left ">'+						
						'		<input placeholder="$trTitle" style="margin-bottom:1px;" type="text" class="input-text no-changes " value="" name="spot[[[id]]][label]" onkeyup="changeLabel([[id]], this)"  id="spot_[[id]]_label" />'+
						'		<input placeholder="$trLink" type="text" class="input-text no-changes " value="" name="spot[[[id]]][link]"  id="spot_[[id]]_link" />'+
						'		<input type="hidden" value="" name="spot[[[id]]][html]" id="spot_[[id]]_html" spotnum="[[id]]" >'+
						'		<button  id="spot_[[id]]_btn" style="display:none" onclick="hwGalleryWysiwygEditor.open(\'$trUrl\', \'spot_[[id]]_html\')" class="scalable " type="button" ><span>$trWYSIWYG</span></button>'+
						'	</td>'+
						'	<td class="a-left ">'+
						'		<input type="text" class="input-text no-changes top-pos" value="0" name="spot[[[id]]][top]" onkeyup="setTop([[id]], this)" /><br>'+
						'		<input type="text" class="input-text no-changes left-pos" style="margin-top: 1px;" value="0" name="spot[[[id]]][left]" onkeyup="setLeft([[id]], this)" />'+						
						'	</td>'+
						'	<td class="a-left ">'+
						'		<button style="text-align:left" onclick="goTop([[id]],this)" class="scalable " type="button" ><span>$trUp</span></button><br>'+
						'		<button style="text-align:left;margin-top: 1px;" onclick="goLeft([[id]],this)" class="scalable " type="button" ><span>$trLeft</span></button>'+												
						'	</td>'+
						'	<td class="a-left ">'+
						'		<button style="text-align:left" onclick="goBottom([[id]],this)" class="scalable " type="button" ><span>$trDown</span></button><br>'+
						'		<button style="text-align:left;margin-top: 1px;" onclick="goRight([[id]],this)" class="scalable " type="button" ><span>$trRight</span></button>'+						
						'	</td>'+
						'	<td class="a-left ">'+
						'		<input type="text" class="input-text no-changes height-pos" value="20" name="spot[[[id]]][height]" onkeyup="setHeight([[id]], this)" />'+
						'		<input type="text" class="input-text no-changes width-pos"  style="margin-top: 1px;"  value="100" name="spot[[[id]]][width]" onkeyup="setWidth([[id]], this)" />'+						
						'	</td>'+
						'	<td class="a-left ">'+
						'		<button style="text-align:left" onclick="goHigher([[id]],this)" class="scalable " type="button" ><span>$trHeightP</span></button><br>'+
						'		<button style="text-align:left;margin-top: 1px;" onclick="goWider([[id]],this)" class="scalable " type="button" ><span>$trWidthP</span></button>'+						
						'	</td>'+
						'	<td class="a-left ">'+
						'		<button style="text-align:left" onclick="goLower([[id]],this)" class="scalable " type="button" ><span>$trHeightM</span></button>'+
						'		<button style="text-align:left;margin-top: 1px;" onclick="goNarrow([[id]],this)" class="scalable " type="button" ><span>$trWidthM</span></button>'+						
						'	</td>'+						
						'	<td class="a-center ">'+	
						'		<select  title="CSS predefined class" name="spot[[[id]]][css_predefined_class]" id="spot_css_predefined_class_[[id]]" onchange="changeCSS([[id]],this)">'+										
						'			<option selected="selected" value="mb-trans">$trTransparent</option>'+	
						'			<option  value="mb-noarrows">$trNoArrows</option>'+	
						'			<option  value="mb-topleft">$trTopLeft</option>'+	
						'			<option  value="mb-topright">$trTopRight</option>'+	
						'			<option  value="mb-bottomleft">$trBottomLeft</option>'+	
						'			<option  value="mb-bottomright">$trBottomRight</option>'+	
						'			<option  value="mb-centerleft">$trCenterLeft</option>'+	
						'			<option  value="mb-centerright">$trCenterRight</option>'+	
						'		</select>'+							
						'		<input type="text" class="input-text no-changes" value="" name="spot[[[id]]][css-class]"></td>'+
						'	<td class="a-center "><button style="text-align:left" onclick="deleteSpot(this)" class="scalable " type="button" ><span>$trDeleteSpot</span></button></td>'+
						'</tr>';
				
				function addSpot(){						
					var syntax = /(^|.|\\r|\\n)(\\[\\[(\\w+)\\]\\])/;
					var template = new Template(spotTemplate, syntax);							

					
					Element.insert(
						\$('spot-table').down('tbody'),
						{'bottom' : template.evaluate({'id' : currentId})}
					);							
					Element.insert(
						\$('spotWrapper'),
						{top : '<div class="spotel" spotnum="'+currentId+'" id="spotel-'+currentId+'" style="cursor:move;height:20px;left:0px;position:absolute;top:0px;width:100px;"><div class="spoth" style="position:absolute;top:0px;left:0px;">'+currentId+'</div><div id="spotcn-'+currentId+'" style="background:none;"></div><div spotnum="'+currentId+'"  id="spoters-'+currentId+'"  class="spotrs"  style="cursor:nw-resize;height:10px;right:0px;position:absolute;bottom:0px;width:10px;"></div></div>'}
					);
					
							new Draggable('spotel-'+currentId, {onEnd : doMove});
							new Draggable('spoters-'+currentId, {onEnd : doResize});
										
					currentId=currentId+1;
				}				
            </script>
EOT;
			$html='		

			<div class="fieldset" id="spotEditor" > 
				<div class="grid-off"> 
					<div class="hor-scroll"> 
						<div style="position:relative" id="spotWrapper">
							<img src="'. $this->_helper->getImageUrl($model->getFile()) .'" style="max-width: none;" />
							'.$spots.'	
						</div>
					</div>
				</div>
				'.$html.'
				<table cellspacing="0" id="spot-table" class="data grid data-grid" >
					<col width="10">
					<col width="50">
					<col width="200">
					<col width="100">
					<col width="10">
					<col width="10">
					<col width="100">
					<col width="10">
					<col width="10">										
					<col width="50">
					<col width="50">
					<thead>
						<tr class="headings">
							<th class=" no-link last"><span class="nobr">#</span></th>
							<th class=" no-link last"><span class="nobr">'.__('Type').'</span></th>
							<th class=" no-link last"><span class="nobr">'.__('Label/Link(SKU)').'</span></th>
							<th class=" no-link last" colspan="3"><span class="nobr">'.__('Top/Left').'</span></th>
							<th class=" no-link last" colspan="3"><span class="nobr">'.__('Height/Width').'</span></th>							
							<th class=" no-link last"><span class="nobr">'.__('CSS Class').'</span></th>
							<th class=" no-link last"><span class="nobr">'.__('Action').'</span></th>
						</tr>
					</thead>	
					<tbody>
					'.$rows.'
					</tbody>
					<tfoot>
						<tr class="headings">							                      
							<th colspan="11" style="text-align:right"><button  onclick="addSpot()" class="scalable " type="button" ><span>'.__('Add New Spot').'</span></button></th>
						</tr>
					</tfoot>					
				</table>					
			</div>			
	

';
			if($i){
				$html.='
			<script type="text/javascript">
			require(["hw_gallery/prototype","hw_gallery/prototype_effects","hw_gallery/dragdrop"], function($){
			    Event.observe(window, \'load\', function() {
							for(var j=1;j<'.$i.';j++){
								new Draggable(\'spotel-\'+j, {onEnd : doMove });
								new Draggable(\'spoters-\'+j, {onEnd : doResize});
							}
						})
			})
			</script>';
			}
		}else{
			$html="";
		}
		return $html;
	}
}