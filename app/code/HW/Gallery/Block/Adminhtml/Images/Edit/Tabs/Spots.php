<?php

namespace HW\Gallery\Block\Adminhtml\Images\Edit\Tabs;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Spots extends Generic implements TabInterface {

	/**
	 * Core system store model
	 *
	 * @var \Magento\Store\Model\System\Store
	 */
	protected $_systemStore;

	/**
	 * @var \HW\Gallery\Model\Gallery\Image\Source\Spot\Type
	 */
	protected $_spotType;

	/**
	 * Fauxspots constructor.
	 *
	 * @param \Magento\Backend\Block\Template\Context          $context
	 * @param \Magento\Framework\Registry                      $registry
	 * @param \Magento\Framework\Data\FormFactory              $formFactory
	 * @param \Magento\Store\Model\System\Store                $systemStore
	 * @param \HW\Gallery\Model\Gallery\Image\Source\Spot\Type $spotType
	 * @param array                                            $data
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Data\FormFactory $formFactory,
		\Magento\Store\Model\System\Store $systemStore,
		\HW\Gallery\Model\Gallery\Image\Source\Spot\Type $spotType,
        array $data = []
	) {
		$this->_systemStore = $systemStore;
		$this->_spotType = $spotType;
		parent::__construct($context, $registry, $formFactory, $data);
	}

	/**
	 * @return $this
	 */
	protected function _prepareForm() {

		/** @var \HW\Gallery\Model\Gallery\Image $model */
		$model = $this->_coreRegistry->registry('current_model');

		/** @var \HW\Gallery\Model\Gallery $modelGallery */
		//$modelGallery = $this->_coreRegistry->registry('gallery_model');

		/** @var \Magento\Framework\Data\Form $form */
		$form = $this->_formFactory->create();

		$fieldset = $form->addFieldset(
			'spot_fieldset',
			['legend' => __('Spot Editor'), 'class' => 'fieldset-wide']
		);

		$field = $fieldset->addField('editor', 'text',
			array(
				'label' => __('Spot Editor'),
				'name'  => 'editor',
				'class' => 'input-text'
			)
		);

		$renderer = $this->getLayout()->createBlock(
			\HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Form\Element\Spoteditor::class
		);

		$field->setRenderer($renderer);

//		$form->setValues($model->getData());
		$this->setForm($form);
		return parent::_prepareForm();
	}

	/**
	 * Return Tab label
	 *
	 * @return string
	 * @api
	 */
	public function getTabLabel()
	{
		return __('Spots editor');
	}

	/**
	 * Return Tab title
	 *
	 * @return string
	 * @api
	 */
	public function getTabTitle()
	{
		return __('Spots editor');
	}

	/**
	 * Can show tab in tabs
	 *
	 * @return boolean
	 * @api
	 */
	public function canShowTab()
	{
		return true;
	}

	/**
	 * Tab is hidden
	 *
	 * @return boolean
	 * @api
	 */
	public function isHidden()
	{
		return false;
	}
}