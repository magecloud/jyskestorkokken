<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Category form input image element
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace HW\Gallery\Block\Adminhtml\Images\Edit\Tabs\Form\Element;

use Magento\Framework\UrlInterface;

class Imagegallery extends \Magento\Framework\Data\Form\Element\Image
{

	/**
	 * Get image preview url
	 *
	 * @return string
	 */
	protected function _getUrl()
	{
		$url = false;
		if ($this->getValue()) {
			$url = \HW\Gallery\Model\Gallery\Image::PATCH_TO_SOURCE . $this->getValue();
		}
		return $url;
	}

	/**
	 * Return element html code
	 *
	 * @return string
	 */
	public function getElementHtml()
	{
		$html = '';

		if ((string)$this->getValue()) {
			$url = $this->_getUrl();

			if (!preg_match("/^http\:\/\/|https\:\/\//", $url)) {
				$url = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $url;
			}

			$html = '<a href="' .
				$url .
				'"' .
				' onclick="imagePreview(\'' .
				$this->getHtmlId() .
				'_image\'); return false;" ' .
				$this->_getUiId(
					'link'
				) .
				'>' .
				'<img src="' .
				$url .
				'" id="' .
				$this->getHtmlId() .
				'_image" title="' .
				$this->getValue() .
				'"' .
				' alt="' .
				$this->getValue() .
				'" height="auto" width="350" class="small-image-preview v-middle"  ' .
				$this->_getUiId() .
				' />' .
				'</a> ';
		}
		$this->setClass('input-file');
		$html .= \Magento\Framework\Data\Form\Element\AbstractElement::getElementHtml();
		$html .= $this->_getDeleteCheckbox();

		return $html;
	}

}
