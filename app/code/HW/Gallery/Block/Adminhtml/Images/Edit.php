<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 30.03.2018
 * Time: 14:54
 */
namespace HW\Gallery\Block\Adminhtml\Images;

use Magento\Backend\Block\Widget\Form\Container;

class Edit extends Container
{
    /**
    * Core registry
    *
    * @var \Magento\Framework\Registry
    */
    protected $_coreRegistry = null;

	/**
	 * @var \HW\Gallery\Helper\Data
	 */
	protected $_helper;

    /**
    * @param \Magento\Backend\Block\Widget\Context $context
    * @param \Magento\Framework\Registry $registry
    * @param array $data
    */
    public function __construct(
       \Magento\Backend\Block\Widget\Context $context,
       \Magento\Framework\Registry $registry,
       \HW\Gallery\Helper\Data $helper,
       array $data = []
    ) {
       $this->_coreRegistry = $registry;
	   $this->_helper       = $helper;
       parent::__construct($context, $data);
    }

    /**
    *
    * @return void
    */
    protected function _construct()
    {
//       $this->_objectId = 'id';
       $this->_blockGroup = 'HW_Gallery';
       $this->_controller = 'adminhtml_images';

       parent::_construct();

       if ($this->_isAllowedAction('Magento_Backend::content')) {
           $this->buttonList->update('save', 'label', __('Save Image'));
           $this->buttonList->add(
               'saveandcontinue',
               [
                   'label' => __('Save and Continue Edit'),
                   'class' => 'save',
                   'data_attribute' => [
                       'mage-init' => [
                           'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                       ],
                   ]
               ],
               -100
           );

	       if($this->_helper->useFauxSpots()){
		       $this->buttonList->add(
			       'addspot',
			       [
				       'label' => '<strong>'.__('Add Spot').'</strong>',
				       'onclick' => 'setLocation(\'' . $this->getAddSpotUrl() . '\')',
				       'class' => 'save',
			       ],
			       -5
		       );
	       }

	       $this->buttonList->update('delete', 'label', __('Delete'));
       } else {
           $this->buttonList->remove('save');
       }

    }


    /**
    *
    * @return \Magento\Framework\Phrase
    */
    public function getHeaderText()
    {
       if ($this->_coreRegistry->registry('current_model')->getId()) {
           return __("Edit Image '%1'", $this->escapeHtml($this->_coreRegistry->registry('current_model')->getName()));
       } else {
           return __('New Image');
       }
    }

    /**
    * Check permission for passed action
    *
    * @param string $resourceId
    * @return bool
    */
    protected function _isAllowedAction($resourceId)
    {
       return $this->_authorization->isAllowed($resourceId);
    }

    /**
    * Getter of url for "Save and Continue" button
    * tab_id will be replaced by desired by JS later
    *
    * @return string
    */
    protected function _getSaveAndContinueUrl()
    {
       return $this->getUrl('*/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']);
    }

	/**
	 * Get URL for back (reset) button
	 *
	 * @return string
	 */
	public function getBackUrl()
	{
		return $this->getUrl('*/gallery/edit', ['id'=>$this->_coreRegistry->registry('current_model')->getGalleryId(),'active_tab'=>'images_section']);
	}

	/**
	 * @return string
	 */
	public function getAddSpotUrl()
	{
		return $this->getUrl('*/*/edit', ['_current' => true, 'spot_count' => $this->_coreRegistry->registry('current_model')->getSpotCount()+1 , 'active_tab' => 'images_section']);
	}

	/**
	 * @return string
	 */
	public function getDeleteUrl()
	{
		return $this->getUrl('*/*/delete', [$this->_objectId => $this->getRequest()->getParam($this->_objectId),'gallery_id' => $this->_coreRegistry->registry('current_model')->getGalleryId(),'active_tab'=>'images_section']);
	}
}