<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 04.07.2018
 * Time: 15:05
 */

namespace HW\Gallery\Block\Widget;

class Banner extends \HW\Gallery\Block\Widget\AbstractWidget implements \Magento\Widget\Block\BlockInterface {

	/**
	 * banner  template
	 * @var string
	 */
	protected $_template    = 'HW_Gallery::banner/default.phtml';

	protected $_type_code   = \HW\Gallery\Model\Gallery::TYPE_BANNERS_GRID;

	public function getTemplatePath($template){

		return 'HW_Gallery::banner/'.$template.'.phtml';
	}

}