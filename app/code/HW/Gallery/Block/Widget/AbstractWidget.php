<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 12.07.2018
 * Time: 13:01
 */
namespace HW\Gallery\Block\Widget;

abstract class  AbstractWidget extends \HW\Gallery\Block\AbstractBlock implements \Magento\Widget\Block\BlockInterface {

	protected $_useCustomSettings = null;


	public function useCustomSettings() {

		if($this->_useCustomSettings === null) {
			$this->_useCustomSettings = (boolean) $this->getCustomSettings();
		}

		return $this->_useCustomSettings;
	}

	public function matchTemplate() {

		if($this->useCustomSettings()){
			$this->setTemplate($this->getTemplatePath($this->getDesign()));
		} else {
			parent::matchTemplate();
		}

		return $this;
	}

	/**
	 * @return int
	 */
	public function getColsCount(){
		if($this->useCustomSettings() && ($this->getData('grid_cols')!== null)){
			return $this->getData('grid_cols');
		} else {
			return parent::getColsCount();
		}
	}

	/**
	 * @return boolean
	 */
	public function getShowTitle() {
		if($this->useCustomSettings() && ($this->getData('show_title') !== null)){
			return (boolean) $this->getData('show_title');
		} else {
			return parent::getShowTitle();
		}
	}

	/**
	 * @return boolean
	 */
	public function getShowPager(){
		if($this->useCustomSettings() && ($this->getData('show_pager') !== null)){
			return (boolean) $this->getData('show_pager');
		} else {
			return parent::getShowPager();
		}
	}

	/**
	 * @return boolean
	 */
	public function getShowNavigation(){
		if($this->useCustomSettings() && ($this->getData('show_navigation') !== null)){
			return (boolean) $this->getData('show_navigation');
		} else {
			return parent::getShowNavigation();
		}
	}

	/**
	 * @return string
	 */
	public function getCssRules() {
		if($this->useCustomSettings() && ($this->getData('css_rules') !== null)){
			return $this->getData('css_rules');
		} else {
			return parent::getCssRules();
		}
	}

	/**
	 * @return string
	 */
	public function getCssClasses() {
		if($this->useCustomSettings() && ($this->getData('css_class') !== null)){
			return $this->getData('css_class').' '.$this->_model->getData('css_predefined_class');
		} else {
			return parent::getCssClasses();
		}
	}

	/**
	 * @return string
	 */
	public function getEffect(){
		if($this->useCustomSettings() && ($this->getData('effect') !== null)){
			return $this->getData('effect');
		} else {
			return parent::getEffect();
		}
	}

	/**
	 * @return string
	 */
	public function getFrequency(){
		if($this->useCustomSettings() && ($this->getData('frequency') !== null)){
			return $this->getData('frequency');
		} else {
			return parent::getFrequency();
		}
	}

	/**
	 * @return string
	 */
	public function getDuration(){
		if($this->useCustomSettings() && ($this->getData('duration') !== null)){
			return $this->getData('duration');
		} else {
			return parent::getDuration();
		}
	}
}