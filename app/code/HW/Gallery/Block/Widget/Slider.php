<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 04.07.2018
 * Time: 16:47
 */
namespace HW\Gallery\Block\Widget;

class Slider extends \HW\Gallery\Block\Widget\AbstractWidget implements \Magento\Widget\Block\BlockInterface {

	/**
	 * banner  template
	 * @var string
	 */
	protected $_template    = 'HW_Gallery::slider/default.phtml';

	protected $_type_code   = \HW\Gallery\Model\Gallery::TYPE_SLIDER;

	public function getTemplatePath($template){

		return 'HW_Gallery::slider/'.$template.'.phtml';
	}
}