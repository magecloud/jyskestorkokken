<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 03.07.2018
 * Time: 16:59
 */

namespace HW\Gallery\Block;

abstract class AbstractBlock extends \Magento\Framework\View\Element\Template {

	/**
	 * @var \HW\Gallery\Model\Gallery
	 */
	protected $_model;

	/**
	 * @var \HW\Gallery\Model\GalleryFactory
	 */
	protected $_modelFactory;

	/**
	 * @var \HW\Gallery\Model\ResourceModel\Gallery\Image\Collection
	 */
	protected $_images = null;

	/**
	 * @var \HW\Gallery\Helper\Data
	 */
	protected $_helper;

	/**
	 * @var \Magento\Catalog\Model\ProductRepository
	 */
	protected $_productRepository;

	/**
	 * @var \Magento\Catalog\Model\ResourceModel\Product
	 */
	protected $_resourceModelProduct;

	/**
	 * @var string
	 */
    protected $_css_rules = null;

	/**
	 * @var string
	 */
    protected $_instance_number = null;


	/**
	 * AbstractBlock constructor.
	 *
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \HW\Gallery\Model\GalleryFactory                 $modelFactory
	 * @param \HW\Gallery\Helper\Data                          $helper
	 * @param \Magento\Catalog\Model\ProductRepository         $productRepository
	 * @param \Magento\Catalog\Model\ResourceModel\Product     $resourceModelProduct
	 * @param array                                            $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\HW\Gallery\Model\GalleryFactory $modelFactory,
		\HW\Gallery\Helper\Data $helper,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		\Magento\Catalog\Model\ResourceModel\Product $resourceModelProduct,
		array $data = []
	) {

		$this->_modelFactory         = $modelFactory;
		$this->_helper               = $helper;
		$this->_productRepository    = $productRepository;
		$this->_resourceModelProduct = $resourceModelProduct;
		parent::__construct($context, $data);

	}

	/**
	 *
	 */
	public function _construct() {
		parent::_construct();

	}

	protected function _beforeToHtml() {
		parent::_beforeToHtml();
		if(is_integer((integer)$this->getSourceId())){
			$this->_model = $this->_modelFactory->create()->load($this->getSourceId());
		}

		if($this->isGalleryEnabled()
			&& $this->_model->getType() == $this->_type_code
		){
			$this->_images = $this->_model->getImages();

			$this->matchTemplate();
		}
	}

	public function matchTemplate() {
		if($this->getGallery()->getDesign()) {
			$this->setTemplate($this->getTemplatePath($this->getGallery()->getDesign()));
		}
		return $this;
	}

	abstract function getTemplatePath($template);

	/**
	 * @param string $request
	 *
	 * @return string
	 */
	public function getLink($request) {

		$request = trim($request);

		if(strpos($request,'http')===0) { return $request; }

		$productId = $this->_resourceModelProduct->getIdBySku($request);
		if($productId > 0){
			$product = $this->_productRepository->get($request);
			if($product->getVisibility() != \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE
			  && $product->getStatus() != \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED){
				return $product->getProductUrl();
			}
		}

		if(substr_count($request,'/') > 3) {
			return $this->getUrl().$request;
		}

		return $this->getUrl($request);
	}

	/**
	 * @return \HW\Gallery\Model\ResourceModel\Gallery\Image\Collection
	 */
	public function getImages() {

		return $this->_images;
	}

	/**
	 * @param string $file
	 *
	 * @return string
	 */
	public function getImageUrl($file) {

		return $this->_helper->getImageUrl($file);
	}

	/**
	 * @return bool
	 */
	public function isGalleryEnabled() {

		if($this->_model->getId() > 0 && $this->_model->getIsActive() == \HW\Gallery\Model\Gallery::STATUS_ENABLED) {
			return true;
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getCssClasses() {

		return $this->_model->getData('css_class').' '.$this->_model->getData('css_predefined_class');
	}

	/**
	 * @return string
	 */
	public function getCssRules() {

		if($this->_css_rules === null){
			$this->_css_rules = $this->_model->getData('css_rules');
		}
		return $this->_css_rules;
	}

	/**
	 * @return \HW\Gallery\Model\Gallery
	 */
	public function getGallery() {

		return $this->_model;
	}

	/**
	 * @return string
	 */
	public function getInstanceNumber() {

		if($this->_instance_number === null ){
			$this->_instance_number = uniqid($this->getGallery()->getId());
		}

		return $this->_instance_number;

	}


	/**
	 * @return boolean
	 */
	public function getShowTitle() {

		return (boolean) $this->getGallery()->getShowTitle();
	}

	/**
	 * @return int
	 */
	public function getColsCount(){

		return $this->getGallery()->getData('grid_cols');
	}

	/**
	 * @return boolean
	 */
	public function getShowPager(){

		return $this->getGallery()->getData('show_pager');
	}

	/**
	 * @return boolean
	 */
	public function getShowNavigation(){

		return $this->getGallery()->getData('show_navigation');
	}

	/**
	 * @return string
	 */
	public function getEffect(){

		return $this->getGallery()->getData('effect');
	}

	/**
	 * @return string
	 */
	public function getFrequency(){

		return $this->getGallery()->getData('frequency');
	}

	/**
	 * @return string
	 */
	public function getDuration(){

		return $this->getGallery()->getData('duration');
	}

	/**
	 * @param string $html
	 *
	 * @return string
	 */
	public function processedHtml($html){

		return $this->_helper->processedHtml($html);
	}
}