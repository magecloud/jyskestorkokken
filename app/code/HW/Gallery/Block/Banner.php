<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 03.07.2018
 * Time: 10:31
 */
namespace HW\Gallery\Block;

class Banner extends AbstractBlock {

	/**
	 * banner  template
	 * @var string
	 */
	protected $_template    = 'HW_Gallery::banner/default.phtml';
//	protected $_template    = 'HW_Gallery::banner/template_1.phtml';

	protected $_type_code   = \HW\Gallery\Model\Gallery::TYPE_BANNERS_GRID;

	public function getTemplatePath($template){

		return 'HW_Gallery::banner/'.$template.'.phtml';
	}

}