<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 03.10.2018
 * Time: 22:58
 */
namespace HW\CashOnDelivery\Observer;
use Magento\Framework\Event\ObserverInterface;

class PaymentMethodIsActive implements ObserverInterface
{
	/**
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $_logger;

	/**
	 * @var \Magento\Checkout\Model\Session
	 */
	protected $_checkoutSession;

	/**
	 * PaymentMethodIsActive constructor.
	 *
	 * @param \Psr\Log\LoggerInterface $logger
	 */
	public function __construct
	(
		\Psr\Log\LoggerInterface $logger,
		\Magento\Checkout\Model\Session $checkoutSession
	) {
		$this->_logger = $logger;
		$this->_checkoutSession = $checkoutSession;
		return $this;
	}


	/**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {

	    $event = $observer->getEvent();
	    $methodInstance = $event->getMethodInstance();
//	    $this->_logger->debug($methodInstance->getCode());
	    if($methodInstance->getCode() == 'cashondelivery') {

		    if(!($quote = $event->getQuote())) {
			    $quote = $this->_checkoutSession->getQuote();
		    }

		    if ($quote->getId()){
			    $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
//			    $this->_logger->debug($shippingMethod);
			    $result = $event->getResult();
			    if ($shippingMethod != 'freeshipping_freeshipping') {
					$result->setData('is_available', false);
			    } else {
				    $result->setData('is_available', true);
			    }
		    }
	    }
    }

}