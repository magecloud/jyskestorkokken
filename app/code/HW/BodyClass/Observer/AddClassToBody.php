<?php

namespace HW\BodyClass\Observer;

use Magento\Framework\View\Page\Config as PageConfig;
use Magento\Framework\Event\ObserverInterface;

class AddClassToBody implements ObserverInterface
{


	
    
    protected $pageConfig;
    protected $storeManager;

    public function __construct(
        PageConfig $pageConfig, 
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ){
        $this->pageConfig = $pageConfig;
        $this->storeManager = $storeManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $store_code = $this->storeManager->getStore()->getCode();
        $website_code=$this->storeManager->getWebsite()->getCode();
		
        $this->pageConfig->addBodyClass('storeClass-'.$store_code);
		$this->pageConfig->addBodyClass('websiteClass-'.$website_code);
    }
	
}
