var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/create-shipping-address': {
                'HW_CustomerAddressType/js/action/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'HW_CustomerAddressType/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/action/create-billing-address': {
                'HW_CustomerAddressType/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/set-billing-address': {
                'HW_CustomerAddressType/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/place-order': {
                'HW_CustomerAddressType/js/action/set-billing-address-mixin': true
            },
        }
    }
};