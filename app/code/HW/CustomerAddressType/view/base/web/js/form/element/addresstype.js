/**
 * Created by Ilya on 03.08.2018.
 */
define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'Magento_Ui/js/modal/modal'
], function (_, uiRegistry, select, modal) {
    'use strict';

    return select.extend({

        /**
         * Array of field names that depend on the value of
         * this UI component.
         */
        ependentFieldName : null,

        /**
         * Reference storage for dependent fields. We're caching this
         * because we don't want to query the UI registry so often.
         */
        dependentField : null,

        /**
         * Initialize field component, and store a reference to the dependent fields.
         */
        initialize: function() {

            this._super()
                .initObservable()
                .initModules()
                .initStatefull()
                .initLinks()
                .initUnique();

            var currentParentName = this.parentName;

            this.dependentFieldName = ('index = vat_id, parentName = '+currentParentName);

            // We're creating a promise that resolves when we're sure that all our dependent
            // UI components have been loaded. We're also binding our callback because
            // we're making use of `this`
            // uiRegistry.promise('index = vat_id, parentScope = '+currentParentScope).done(_.bind(function() {
            uiRegistry.promise(this.dependentFieldName).done(_.bind(function() {
                // Let's store the arguments (the UI Components we queried for) in our object
                this.dependentField = arguments[0];
                // console.log(this.dependentField);

                // Set the initial visibility of our fields.
                this.processDependentField(this.value());
            }, this));

            // return this;
        },

        processDependentField: function(value) {

            // var currentParentName = this.parentName;

            // var fieldVat = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.vat_id');
            // var fieldVat = uiRegistry.get('customer_form.areas.address.address.address_collection.63.vat_id');
            // var fieldVat = uiRegistry.get('index = vat_id, parentScope = '+currentParentName);
            var fieldVat = this.dependentField;

            // console.log(currentParentName);
            // console.log(this);
            // console.log(fieldVat);

            if(fieldVat == undefined) {
                console.log("may be need to enable show vat on Frontend. See Magento settings.");
                return this._super();
            }

            //todo: need make propper way to set validation. need to save state of validation rules before set new ones.

            var $validation = new Object();

            if (fieldVat.visibleValue == value) {
                $validation['required-entry'] = true;
                fieldVat.validation = $validation;
                fieldVat.required(true);
                // fieldVat.setValidation('required-entry',true);
                fieldVat.show();
            } else {
                fieldVat.hide();
                // $validation['required-entry'] = false;
                fieldVat.validation = $validation;
                fieldVat.required(false);
            }
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {

            this.processDependentField(value);

            return this._super();
        },
    });
});