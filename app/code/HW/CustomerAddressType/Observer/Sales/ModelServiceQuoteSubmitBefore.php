<?php


namespace HW\CustomerAddressType\Observer\Sales;

class ModelServiceQuoteSubmitBefore implements \Magento\Framework\Event\ObserverInterface
{

    protected $_helper;

    protected $_logger;

    protected $_quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Psr\Log\LoggerInterface $logger,
        \HW\CustomerAddressType\Helper\Data $helper
    )
    {
        $this->_quoteRepository = $quoteRepository;
        $this->_logger = $logger;
        $this->_helper = $helper;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {

        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();

        $quote = $this->_quoteRepository->get($order->getQuoteId());

        $this->_helper->transportFieldsFromExtensionAttributesToObject(
            $quote->getBillingAddress(),
            $order->getBillingAddress(),
            'hw_extra_checkout_billing_address_fields'
        );

        if ($order->getShippingAddress()) {
            $this->_helper->transportFieldsFromExtensionAttributesToObject(
                $quote->getShippingAddress(),
                $order->getShippingAddress(),
                'hw_extra_checkout_shipping_address_fields'
            );
        }
    }
}