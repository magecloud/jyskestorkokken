<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 17.07.2018
 * Time: 18:41
 */

namespace HW\CustomerAddressType\Model\Address\Attribute\Source;

class Type extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

	const VALUE_PRIVATE   = 1;
	const VALUE_COMPANY   = 2;


	/**
	 * Retrieve all options array
	 *
	 * @return array
	 */
	public function getAllOptions()
	{
		if (is_null($this->_options))
		{
			$this->_options = array(
				array(
					'label' => __('Private'),
					'value' => self::VALUE_PRIVATE
				),
				array(
					'label' => __('Company'),
					'value' => self::VALUE_COMPANY
				)
			);
		}

		return $this->_options;
	}

	/**
	 * Retrieve option array
	 *
	 * @return array
	 */
	public function getOptionArray()
	{
		$_options = array();
		foreach ($this->getAllOptions() as $option) {
			$_options[$option['value']] = $option['label'];
		}
		return $_options;
	}

	/**
	 * Get a text for option value
	 *
	 * @param string|integer $value
	 * @return string
	 */
	public function getOptionText($value)
	{
		$options = $this->getAllOptions();
		foreach ($options as $option) {
			if ($option['value'] == $value) {
				return $option['label'];
			}
		}
		return false;
	}

}