<?php
namespace HW\CustomerAddressType\Model\Customer\Api\Data\AddressExtensionInterface;

use Magento\Framework\Api\AbstractSimpleObject;

// @todo: Create an interface for this class

/**
 * Class ExtensionAttributes
 *
 * @package HW/CustomerAddressType
 */
class AdditionalAttributes extends AbstractSimpleObject implements \Magento\Customer\Api\Data\AddressExtensionInterface
{
    protected $_attributeCode     = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;


	/**
     * @param string $value
     * @return void
     */
    public function setAddressCustomerType($value)
    {
        $this->setData($this->_attributeCode, $value);
    }

    /**
     * @return mixed|null
     */
    public function getAddressCustomerType()
    {
        return $this->_get($this->_attributeCode);
    }
}