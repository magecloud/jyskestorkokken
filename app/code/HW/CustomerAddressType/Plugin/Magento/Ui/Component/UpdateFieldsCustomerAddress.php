<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 08.08.2018
 * Time: 16:21
 */

namespace HW\CustomerAddressType\Plugin\Magento\Ui\Component;

class UpdateFieldsCustomerAddress
{
	protected $_addressCustomerTypeCode = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;


	/**
	 * @var \HW\CustomerAddressType\Helper\Data
	 */
	protected $_helper;


	/**
	 * DataProviderPlugin constructor.
	 *
	 * @param \HW\CustomerAddressType\Helper\Data $dataHelper
	 */
	public function __construct(
		\HW\CustomerAddressType\Helper\Data $dataHelper

	) {
		$this->_helper = $dataHelper;
	}

	/**
	 * @param \Magento\Ui\Component\AbstractComponent $subject
	 * @param $result
	 * @return array
	 */
	public function afterGetChildComponents(\Magento\Ui\Component\AbstractComponent $subject, $result)
	{

		if($this->_helper->isCustomerEditAdminPage()){
			if($subject->getName() == 'address'){
				$this->processFields($result);
			}
		}


		return $result;
	}

	/**
	 * @param $result
	 *
	 * @internal param $fields
	 */
	protected function processFields(&$result){
		if(is_array($result)){
			if(array_key_exists('vat_id', $result)){
				$temp = $result['vat_id']->getConfig();
				$temp['visibleValue'] = \HW\CustomerAddressType\Model\Address\Attribute\Source\Type::VALUE_COMPANY;;
//				$this->_helper->addLoggerInfo(($temp));

				$result['vat_id']->setConfig($temp);
			}
			if(array_key_exists($this->_addressCustomerTypeCode, $result)){
				$temp = $result[$this->_addressCustomerTypeCode]->getConfig();
				$temp['component'] = "HW_CustomerAddressType/js/form/element/addresstype";
				$temp['caption'] = __("Choose an address type.");
//				$temp['visible'] = false;
//				$this->_helper->addLoggerInfo(($temp));

				$result[$this->_addressCustomerTypeCode]->setConfig($temp);
			}
		}
	}
}