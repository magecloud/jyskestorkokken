<?php
namespace HW\CustomerAddressType\Plugin\Magento\Customer\Block\Address\Edit;

use Magento\Customer\Block\Address\Edit as Subject;
use HW\CustomerAddressType\Block\Address\Type as TypeBlock;

/**
 * Class AddNoteFieldToAddressForm
 *
 * @package Yireo\ExampleAddressFieldNote\Plugin
 */
class AddFieldToAddressForm
{
    /**
     * @param Subject $subject
     * @param string $html
     *
     * @return string
     */
    public function afterToHtml(Subject $subject, $html)
    {
	    /**
	     * Block was create by layout customer_address_form.xml
	     */
    	$fieldBlock = $subject->getLayout()->getBlock('customer_address_edit_type');

        //$fieldBlock = $this->getChildBlock(TypeBlock::class, $subject);
        $fieldBlock->setAddress($subject->getAddress());
        $html = $this->appendBlockBeforeFieldsetEnd($html, $fieldBlock->toHtml());

        return $html;
    }

    /**
     * @param string $html
     * @param string $childHtml
     *
     * @return string
     */
    private function appendBlockBeforeFieldsetEnd($html, $childHtml)
    {
        $pregMatch = '/\<\/fieldset\>/';
        $pregReplace = $childHtml . '\0';
        $html = preg_replace($pregMatch, $pregReplace, $html, 1);

        return $html;
    }

    /**
     * @param $parentBlock
     *
     * @return mixed
     */
    private function getChildBlock($blockClass, $parentBlock)
    {
        return $parentBlock->getLayout()->createBlock($blockClass, basename($blockClass));
    }
}