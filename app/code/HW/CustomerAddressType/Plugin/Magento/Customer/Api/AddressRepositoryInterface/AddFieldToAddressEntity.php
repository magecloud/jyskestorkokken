<?php
namespace HW\CustomerAddressType\Plugin\Magento\Customer\Api\AddressRepositoryInterface;

use Magento\Customer\Api\AddressRepositoryInterface as Subject;
use Magento\Customer\Api\Data\AddressInterface as Entity;

/**
 * Class AddNoteFieldToAddressEntity
 *
 * @package HW\CustomerAddressType\Plugin
 */
class AddFieldToAddressEntity
{
	protected $_attributeCode     = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;

	/**
	 * @var \Magento\Framework\Logger\Monolog
	 */
	protected $_logger;

	/**
	 * @var \Magento\Framework\App\RequestInterface
	 */
	protected $_httpRequest;

	public function __construct(
        \Magento\Framework\App\RequestInterface $httpRequest,
        \Magento\Framework\Logger\Monolog       $logger
    )
    {
        $this->_httpRequest = $httpRequest;
        $this->_logger      = $logger;
    }

    /**
     * @param Subject $subject
     * @param Entity $entity
     *
     * @return Entity
     */
    public function afterGetById(Subject $subject, Entity $entity)
    {
        $extensionAttributes = $entity->getExtensionAttributes();
        if ($extensionAttributes === null) {
            return $entity;
        }

        $value = $this->getAttributeByEntityId($entity);
        $extensionAttributes->setAddressCustomerType($value);
        $entity->setExtensionAttributes($extensionAttributes);

        return $entity;
    }

    /**
     * @param Subject $subject
     * @param Entity $entity
     *
     * @return [Entity]
     */
    public function beforeSave(Subject $subject, Entity $entity)
    {
        $extensionAttributes = $entity->getExtensionAttributes();
        if ($extensionAttributes === null) {
            return [$entity];
        }

        $data = $entity->__toArray();

	    if(isset($data[$this->_attributeCode])){
	    	// saving when crate customer after checkout
	    	$value = $data[$this->_attributeCode];
	    } else {
		    // @todo: Really dirty hack, because Magento\Customer\Controller\Address\FormPost does not support Extension Attributes
		    $value = $this->_httpRequest->getParam($this->_attributeCode);
	    }

	    $this->_logger->info($value);

	    if (!empty($value)) {
		    $entity->setCustomAttribute($this->_attributeCode, $value);
		}

        return [$entity];
    }

    /**
     * @param Entity $entity
     *
     * @return string
     */
    private function getAttributeByEntityId(Entity $entity)
    {
        $attribute = $entity->getCustomAttribute($this->_attributeCode);
        if ($attribute) {
            return $attribute->getValue();
        }

        return '';
    }
}