<?php


namespace HW\CustomerAddressType\Plugin\Magento\Quote\Model;

class ShippingAddressManagement
{
	/**
	 * @var \HW\CustomerAddressType\Helper\Data
	 */
    protected $_helper;

	/**
	 * @var \Psr\Log\LoggerInterface
	 */
    protected $_logger;

	/**
	 * ShippingAddressManagement constructor.
	 *
	 * @param \Psr\Log\LoggerInterface            $logger
	 * @param \HW\CustomerAddressType\Helper\Data $helper
	 */
	public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \HW\CustomerAddressType\Helper\Data $helper
    ) {
        $this->_logger = $logger;
        $this->_helper = $helper;
    }

	/**
	 * @param \Magento\Quote\Model\ShippingAddressManagement $subject
	 * @param                                                $cartId
	 * @param \Magento\Quote\Api\Data\AddressInterface       $address
	 */
	public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {

        $extAttributes = $address->getExtensionAttributes();

        if (!empty($extAttributes)) {
            $this->_helper->transportFieldsFromExtensionAttributesToObject(
                $extAttributes,
                $address,
                'hw_extra_checkout_shipping_address_fields'
            );
        }

    }
}