<?php


namespace HW\CustomerAddressType\Plugin\Magento\Quote\Model;

class BillingAddressManagement
{
	/**
	 * @var \HW\CustomerAddressType\Helper\Data
	 */
    protected $_helper;

	/**
	 * @var \Psr\Log\LoggerInterface
	 */
    protected $_logger;

	/**
	 * BillingAddressManagement constructor.
	 *
	 * @param \Psr\Log\LoggerInterface            $logger
	 * @param \HW\CustomerAddressType\Helper\Data $helper
	 */
	public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \HW\CustomerAddressType\Helper\Data $helper
    ) {
        $this->_logger = $logger;
        $this->_helper = $helper;
    }

	/**
	 * @param \Magento\Quote\Model\BillingAddressManagement $subject
	 * @param                                               $cartId
	 * @param \Magento\Quote\Api\Data\AddressInterface      $address
	 * @param bool                                          $useForShipping
	 */
	public function beforeAssign(
        \Magento\Quote\Model\BillingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address,
        $useForShipping = false
    ) {

        $extAttributes = $address->getExtensionAttributes();
        if (!empty($extAttributes)) {
            $this->_helper->transportFieldsFromExtensionAttributesToObject(
                $extAttributes,
                $address,
                'hw_extra_checkout_billing_address_fields'
            );
        }

    }
}