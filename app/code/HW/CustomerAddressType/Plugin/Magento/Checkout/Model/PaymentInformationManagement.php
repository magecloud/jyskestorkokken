<?php

namespace HW\CustomerAddressType\Plugin\Magento\Checkout\Model;

class PaymentInformationManagement
{
	/**
	 * @var \Experius\ExtraCheckoutAddressFields\Helper\Data
	 */
    protected $_helper;

	/**
	 * @var \Psr\Log\LoggerInterface
	 */
    protected $_logger;

	/**
	 * @param \Psr\Log\LoggerInterface            $logger
	 * @param \HW\CustomerAddressType\Helper\Data $helper
	 */
	public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \HW\CustomerAddressType\Helper\Data $helper
    ) {
        $this->_logger = $logger;
        $this->_helper = $helper;
    }

	/**
	 * @param \Magento\Checkout\Model\PaymentInformationManagement $subject
	 * @param                                                      $cartId
	 * @param \Magento\Quote\Api\Data\PaymentInterface             $paymentMethod
	 * @param \Magento\Quote\Api\Data\AddressInterface             $address
	 */
    public function beforeSavePaymentInformation(
        \Magento\Checkout\Model\PaymentInformationManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {

        $extAttributes = $address->getExtensionAttributes();
        if (!empty($extAttributes)) {
            $this->_helper->transportFieldsFromExtensionAttributesToObject(
                $extAttributes,
                $address,
                'hw_extra_checkout_billing_address_fields'
            );
        }

    }
}