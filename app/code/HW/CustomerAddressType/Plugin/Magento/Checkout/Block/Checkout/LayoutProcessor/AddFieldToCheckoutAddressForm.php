<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 20.07.2018
 * Time: 14:04
 */

namespace HW\CustomerAddressType\Plugin\Magento\Checkout\Block\Checkout\LayoutProcessor;

class AddFieldToCheckoutAddressForm  {

	/**
	 * @var \HW\CustomerAddressType\Model\Address\Attribute\Source\Type
	 */
	protected $_source;

	/**
	 * @var \Magento\Framework\Logger\Monolog
	 */
	protected $_logger;

	/**
	 * @var \Magento\Framework\Stdlib\ArrayManager
	 */
	private $_arrayManager;

	/**
	 * AddFieldToCheckoutAddressForm constructor.
	 *
	 * @param \HW\CustomerAddressType\Model\Address\Attribute\Source\Type $addressTypeSource
	 * @param \Magento\Framework\Logger\Monolog                           $logger
	 * @param \Magento\Framework\Stdlib\ArrayManager                      $arrayManager
	 */
	public function __construct(
		\HW\CustomerAddressType\Model\Address\Attribute\Source\Type $addressTypeSource,
		\Magento\Framework\Logger\Monolog                           $logger,
		\Magento\Framework\Stdlib\ArrayManager                      $arrayManager
	)
	{
		$this->_source = $addressTypeSource;
		$this->_logger = $logger;
		$this->_arrayManager = $arrayManager;
	}

	/**
	 * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
	 * @param array $jsLayout
	 * @return array
	 */
	public function afterProcess(
		\Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
		array  $jsLayout
	) {

		$attributeCode = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;
		$taxVatFieldId = "vat_id";

		$addressCustomerTypeField = [
//			'component'     => 'Magento_Ui/js/form/element/abstract',
			'component'     => 'HW_CustomerAddressType/js/form/element/addresstype',
			'config'        => [
				// customScope is used to group elements within a single form (e.g. they can be validated separately)
				'customScope'   => 'shippingAddress.custom_attributes',
				'customEntry'   => null,
				'template'      => 'ui/form/field',
//				'elementTmpl'   => 'ui/form/element/input',
				'elementTmpl'   => 'ui/form/element/select',
				'tooltip'       => [
					'description' => __("Shipping methods depends on address type."). ' '. __("Use 'Private' if You don't know what is if for."),
				],
				'additionalClasses' => 'hw_field'

			],
			'dataScope'     => 'shippingAddress.custom_attributes' . '.' . $attributeCode,
			'label'         => __('Address Type'),
			'provider'      => 'checkoutProvider',
			'sortOrder'     => 0,
			'validation'    => [
				'required-entry' => true,
			],

//			'imports' => [
//				'disabled' => '${$.provider}:${ $.parentScope }.city:value'
//			],

//			'options'       => [ [ 'value' => '1', 'label' => 'Private' ], [ 'value' => '2', 'label' => 'Company' ] ],
			'options'       => $this->_source->getAllOptions(),
			'default'       => \HW\CustomerAddressType\Model\Address\Attribute\Source\Type::VALUE_PRIVATE,
			'filterBy'      => null,
			'customEntry'   => null,
			'visible'       => true,
			'caption'       => __("Choose an address type.")
		];


		$pathToShippingFieldset         = $this->_arrayManager->findPath('shipping-address-fieldset', $jsLayout).\Magento\Framework\Stdlib\ArrayManager::DEFAULT_PATH_DELIMITER.'children';
		$shippingFieldsetChildrenNode   = $this->_arrayManager->get($pathToShippingFieldset, $jsLayout);

		$shippingFieldsetChildrenNode[$attributeCode] = $addressCustomerTypeField;

		$shippingFieldsetChildrenNode[$taxVatFieldId]['visibleValue'] = \HW\CustomerAddressType\Model\Address\Attribute\Source\Type::VALUE_COMPANY;
		$shippingFieldsetChildrenNode[$taxVatFieldId]['visible'] = false;
//		$shippingFieldsetChildrenNode[$taxVatFieldId]['validation'] = ['required-entry' => true];


		$jsLayout = $this->_arrayManager->set($pathToShippingFieldset, $jsLayout, $shippingFieldsetChildrenNode);

//		$jsLayout['components']['checkout']['children']
//		['steps']['children']['shipping-step']['children']
//		['shippingAddress']['children']
//		['shipping-address-fieldset']['children'][$attributeCode] = $addressCustomerTypeField;



/*
$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
		['shippingAddress']['children']['shipping-address-fieldset']['children']['custom_field1'] = [
			'component' => 'Magento_Ui/js/form/element/abstract',
			'config' => [
				'customScope' => 'shippingAddress.custom_attributes',
				'template' => 'ui/form/field',
				'elementTmpl' => 'ui/form/element/select',
				'options' => [  ],
				'id' => 'custom-field1'
			],
			'dataScope' => 'shippingAddress.custom_attributes.custom_field1',
			'label' => 'Custom Field1',
			'provider' => 'checkoutProvider',
			'visible' => true,
			'validation' => [],
			'sortOrder' => 0,
			'id' => 'custom-field1'
		];
*/
		$this->_logger->info(print_r($jsLayout['components']['checkout']['children']
		['steps']['children']['shipping-step']['children']
		['shippingAddress']['children']
		['shipping-address-fieldset']['children'], true) );
		return $jsLayout;
	}
}