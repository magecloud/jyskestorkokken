<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 10.08.2018
 * Time: 16:00
 */
namespace HW\CustomerAddressType\Plugin\Magento\Framework\Data\Form;

class AddOnchange {

	protected $_addressCustomerTypeCode = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;

	/**
	 * @var \HW\CustomerAddressType\Helper\Data
	 */
	protected $_helper;


	/**
	 * DataProviderPlugin constructor.
	 *
	 * @param \HW\CustomerAddressType\Helper\Data $dataHelper
	 */
	public function __construct(
		\HW\CustomerAddressType\Helper\Data $dataHelper

	) {
		$this->_helper = $dataHelper;
	}

	/**
	 * @param \Magento\Framework\Data\Form $subject
	 * @param                              $result
	 *
	 * @return string
	 */
	public function afterToHtml(\Magento\Framework\Data\Form $subject, $result) {

		if($this->_helper->isOrderCreationAdminPage() && ($result)){

			if($subject->getData('html_id_prefix') == 'order-billing_address_') {
				return $result.$this->_appendBlockBeforeFormEnd('billing');
			}
			if($subject->getData('html_id_prefix') == 'order-shipping_address_') {
				return $result.$this->_appendBlockBeforeFormEnd('shipping');
			}
		}

		return $result;
	}

	/**
	 * @param string $kindOfAddress
	 *
	 * @return string
	 */
	protected function _appendBlockBeforeFormEnd($kindOfAddress = 'billing') {

		$html ="
        <script type=\"text/javascript\">

            require(['jquery'], function($){

                window.updateVatIdfield = function(field,addressKind){
//                    console.log(addressKind);
                    var fieldVat = $('#order-'+addressKind+'_address_vat_id');
//                    console.log(fieldVat);
                    if(fieldVat != undefined) {
                        var field       = $(field);
                        var fieldDiv    = fieldVat.parents('div.admin__field.field-vat-number');
//                        console.log(field);
//                        console.log(fieldDiv);
                        if(field.val() == ". \HW\CustomerAddressType\Model\Address\Attribute\Source\Type::VALUE_COMPANY ."){
                            fieldVat.addClass('required-entry');
                            fieldDiv.addClass('required _required');
                        } else {
                            fieldVat.removeClass('required-entry');
                            fieldDiv.removeClass('required _required');
                            fieldVat.addClass('');
                        }
                    }
                }

                var addressKinds = ['".$kindOfAddress."'];

                $(document).ready(function() {
                    addressKinds.each(function(addressKind,index){
//                        console.log($('#order-'+addressKind+'_address_address_customer_type'));
                        updateVatIdfield('#order-'+addressKind+'_address_address_customer_type',addressKind);

                        $('#order-'+addressKind+'_address_address_customer_type').change(function(){
//                            console.log( $(this).val());
                            updateVatIdfield(this,addressKind);
                        });
                    })
                });
            })
        </script>";

		return $html;
	}

}