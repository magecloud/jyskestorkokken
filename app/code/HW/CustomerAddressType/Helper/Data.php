<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 17.07.2018
 * Time: 16:18
 */
namespace HW\CustomerAddressType\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper{

	const ADDRESS_CUSTOMER_TYPE = 'address_customer_type';

	/**
	 * @var \Magento\Framework\DataObject\Copy\Config
	 */
	protected $_fieldsetConfig;


	/**
	 * Data constructor.
	 *
	 * @param \Magento\Framework\App\Helper\Context     $context
	 * @param \Magento\Framework\DataObject\Copy\Config $fieldsetConfig
	 */
	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\DataObject\Copy\Config $fieldsetConfig
	) {
		parent::__construct($context);
		$this->_fieldsetConfig = $fieldsetConfig;
	}


	/**
	 * @param string $fieldset
	 * @param string $root
	 *
	 * @return array
	 */
	public function getExtraCheckoutAddressFields($fieldset='hw_extra_checkout_shipping_address_fields', $root='global'){

		$fields = $this->_fieldsetConfig->getFieldset($fieldset, $root);

		$extraCheckoutFields = [];

		if(is_array($fields) && !empty($fields)) {
			foreach($fields as $field=>$fieldInfo){
				$extraCheckoutFields[] = $field;
			}
		}

		return $extraCheckoutFields;

	}

	/**
	 * @param        $fromObject
	 * @param        $toObject
	 * @param string $fieldset
	 *
	 * @return mixed
	 */
	public function transportFieldsFromExtensionAttributesToObject(
		$fromObject,
		$toObject,
		$fieldset='hw_extra_checkout_shipping_address_fields'
	)
	{
		$_extraFieldsArray = $this->getExtraCheckoutAddressFields($fieldset);

		if(!is_array($_extraFieldsArray) || empty($_extraFieldsArray)) {
			return $toObject;
		}

		foreach ($_extraFieldsArray as $extraField) {

			$set = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $extraField)));
			$get = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $extraField)));

			$value = $fromObject->$get();
			try {
				$toObject->$set($value);
			} catch (\Exception $e) {
				$this->_logger->critical($e->getMessage());
			}
		}

		return $toObject;
	}

	/**
	 * Check page type
	 * @return bool
	 */
	public function isCustomerEditAdminPage(){
		return $this->_request->getFullActionName() === 'customer_index_edit';
	}

	/**
	 * Check page type
	 * @return bool
	 */
	public function isOrderCreationAdminPage(){
		return $this->_request->getFullActionName() === 'sales_order_create_index'
		|| $this->_request->getFullActionName() === 'sales_order_create_loadBlock';
	}

	/**
	 * check if array path exists
	 * @param $array
	 * @param $path
	 * @param string $separator
	 * @return bool
	 */
	public function array_path_exists($array, $path, $separator = '/'){
		$paths = explode($separator, $path);

		foreach($paths as $sub){
			if(!is_array($array) || !array_key_exists($sub, $array)){
				return false;
			}

			$array = $array[$sub];
		}

		return true;
	}

	/**
	 * @return \Psr\Log\LoggerInterface
	 */
	public function getLogger()
	{
		return $this->_logger;
	}


	/**
	 * @param $data
	 *
	 * @return $this
	 */
	public function addLoggerInfo($data) {

		$this->_logger->info(print_r($data, true));
		return $this;
	}

}