<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 18.07.2018
 * Time: 11:20
 */
namespace HW\CustomerAddressType\Block\Address;

use Braintree\Exception;

class Type extends \Magento\Framework\View\Element\Template {

	/**
	 * @var \HW\CustomerAddressType\Model\Address\Attribute\Source\Type
	 */
	protected $_source;

	/**
	 * Current value
	 * @var int
	 */
	protected $_currentValue;

	/**
	 * @var \Magento\Customer\Api\Data\AddressInterface
	 */
	protected $_address;

	protected $_attributeCode     = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;

	/**
	 * @var string
	 */
	protected $_template = 'address/edit/type.phtml';


	/**
	 * Type constructor.
	 *
	 * @param \Magento\Framework\View\Element\Template\Context                                            $context
	 * @param \HW\CustomerAddressType\Model\Address\Attribute\Source\Type $addressTypeSource
	 * @param array                                                       $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\HW\CustomerAddressType\Model\Address\Attribute\Source\Type $addressTypeSource,
		array $data)
	{
		$this->_source = $addressTypeSource;
		parent::__construct($context, $data);
	}

	/**
	 * Retrieve option array
	 *
	 * @return array
	 */
	public function getOptionArray() {

		return $this->_source->getOptionArray();
	}

	/**
	 * Return the associated address.
	 *
	 * @return \Magento\Customer\Api\Data\AddressInterface
	 */
	public function getAddress()
	{
		return $this->_address;
	}

	/**
	 * Set the associated address.
	 *
	 * @param $address
	 *
	 * @return $this
	 */
	public function setAddress($address)
	{
		$this->_address = $address;

		return $this;
	}

	/**
	 * @return int
	 */

	public function getCurrentValue() {

		/** @var \Magento\Customer\Model\Data\Address $address */
		$address = $this->getAddress();

		$value = $address->getCustomAttribute($this->_attributeCode);

		if ($value == null || !($value instanceof \Magento\Framework\Api\AttributeInterface)) {
			$this->_currentValue = '';
		} else {
			$this->_currentValue = $value->getValue();
		}

		return $this->_currentValue;
	}

}