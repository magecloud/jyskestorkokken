<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 02.08.2018
 * Time: 12:12
 */
namespace HW\CustomerAddressType\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
	/**
	 * @var CustomerSetupFactory
	 */
	protected $_customerSetupFactory;

	/**
	 * @var AttributeSetFactory
	 */
	protected $_attributeSetFactory;

	/**
	 *  @var \Magento\Framework\App\Config\Storage\WriterInterface
	 */
	protected $_configWriter;

	/**
	 * InstallData constructor.
	 *
	 * @param CustomerSetupFactory                                  $customerSetupFactory
	 * @param AttributeSetFactory                                   $attributeSetFactory
	 * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
	 */
	public function __construct(
		CustomerSetupFactory $customerSetupFactory,
		AttributeSetFactory $attributeSetFactory,
		\Magento\Framework\App\Config\Storage\WriterInterface $configWriter
	) {
		$this->_customerSetupFactory = $customerSetupFactory;
		$this->_attributeSetFactory  = $attributeSetFactory;
		$this->_configWriter         = $configWriter;
	}

	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function upgrade(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$setup->startSetup();

		 if (version_compare($context->getVersion(), '0.1.1', '<=')) {

			 $this->_configWriter->save('customer/address/taxvat_show','opt');
			 $this->_configWriter->save('customer/create_account/vat_frontend_visibility',1);
		 }

		$setup->endSetup();
	}

}