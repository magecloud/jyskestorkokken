<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 02.08.2018
 * Time: 12:55
 */
namespace HW\CustomerAddressType\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$setup->startSetup();


//		if (version_compare($context->getVersion(), '0.1.2', '<=')) {
//			$_attributeCodeAddressTaxvat = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TAXVAT;
//			$setup->getConnection()->addColumn(
//				$setup->getTable('quote_address'),
//				$_attributeCodeAddressTaxvat,
//				[
//					'type' => 'text',
//					'length' => 32,
//					'comment' => 'Address Taxvat'
//				]
//			);
//
//			$setup->getConnection()->addColumn(
//				$setup->getTable('sales_order_address'),
//				$_attributeCodeAddressTaxvat,
//				[
//					'type' => 'text',
//					'length' => 32,
//					'comment' => 'Address Taxvat'
//				]
//			);
//		}


		$setup->endSetup();
	}
}