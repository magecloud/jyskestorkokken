<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 02.08.2018
 * Time: 12:36
 */
namespace HW\CustomerAddressType\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
	/**
	 *
	 *
	 * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
	 * @param \Magento\Framework\Setup\ModuleContextInterface $context
	 *
	 * @return void
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();


		$_attributeCodeAddressType = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;
		$setup->getConnection()->addColumn(
			$setup->getTable('quote_address'),
			$_attributeCodeAddressType,
			[
				'type' => 'smallint',
//				'length' => 255,
				'comment' => 'Address Type'
			]
		);

		$setup->getConnection()->addColumn(
			$setup->getTable('sales_order_address'),
			$_attributeCodeAddressType,
			[
				'type' => 'smallint',
//				'length' => 255,
				'comment' => 'Address Type'
			]
		);

		$installer->endSetup();
	}
}