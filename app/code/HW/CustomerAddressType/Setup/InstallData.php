<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 17.07.2018
 * Time: 17:46
 */
namespace HW\CustomerAddressType\Setup;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
	/**
	 * @var CustomerSetupFactory
	 */
	private $_customerSetupFactory;

	/**
	 * @var AttributeSetFactory
	 */
	private $_attributeSetFactory;

	/**
	 * InstallData constructor.
	 *
	 * @param CustomerSetupFactory $customerSetupFactory
	 * @param AttributeSetFactory  $attributeSetFactory
	 */
	public function __construct(
		CustomerSetupFactory $customerSetupFactory,
		AttributeSetFactory $attributeSetFactory
	) {
		$this->_customerSetupFactory = $customerSetupFactory;
		$this->_attributeSetFactory  = $attributeSetFactory;
	}

	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$_attributeCode     = \HW\CustomerAddressType\Helper\Data::ADDRESS_CUSTOMER_TYPE;
		$customerSetup      = $this->_customerSetupFactory->create(['setup' => $setup]);

		$customerEntity = $customerSetup->getEavConfig()->getEntityType(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY);
		$attributeSetId = $customerEntity->getDefaultAttributeSetId();

		/** @var $attributeSet AttributeSet */
		$attributeSet       = $this->_attributeSetFactory->create();
		$attributeGroupId   = $attributeSet->getDefaultGroupId($attributeSetId);

		$customerSetup->addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY, $_attributeCode, [
			'type'              => 'int',
			'label'             => 'Address Type',
			'input'             => 'select',
			'source'            => \HW\CustomerAddressType\Model\Address\Attribute\Source\Type::class,
			'required'          => false,
			'visible'           => true,
			'user_defined'      => true,
			'sort_order'        => 999,
			'position'          => 400,
			'system'            => 0,
			'default'           => \HW\CustomerAddressType\Model\Address\Attribute\Source\Type::VALUE_PRIVATE,
			'note'              => 'Custom Attribute Will Be Used to separate addresses by type'
		]);

		$used_in_forms   = array();
		$used_in_forms[] = "adminhtml_customer_address";
		$used_in_forms[] = "customer_register_address";
		$used_in_forms[] = "customer_address_edit"; //this form code is used in checkout billing/shipping address
		//$used_in_forms[]="adminhtml_customer";
		//$used_in_forms[]="checkout_register";
		//$used_in_forms[]="customer_account_create";

		$attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', $_attributeCode)
			->addData([
				'attribute_set_id'                  => $attributeSetId,
				'attribute_group_id'                => $attributeGroupId,
				'used_in_forms'                     => $used_in_forms,
				'is_used_for_customer_segment'      => true
			]);

		$attribute->save();

	}
}