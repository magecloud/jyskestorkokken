define(['jquery'], function($) {

    sameHeight = function (itemSelector,fixSelector,delta) {
		var elements=new Array();
		var top=null;
		var height=null;
		var maxHeight=0;
		var doResize = false;
		
		function setSameHeight(){
			if(doResize && elements.length > 1){
				$.each(elements,function(index, value){
					var elHeight=$(value).height();
					if(elHeight < maxHeight){
						$(value).find(fixSelector).css( {"margin-top": maxHeight - elHeight + delta});
					}
				});
			}
		}
		
		$(itemSelector+' '+fixSelector).css({'margin-top':""});
		
        $(itemSelector).each(function(){
			var position = $(this).position();
			var elHeight = $(this).height();
			if(top != position.top){
				setSameHeight();
				elements=new Array();
				top = position.top;
				height = elHeight;
				maxHeight = elHeight;
				doResize = false;
			}else{
				doResize=doResize || (elHeight != height);
				maxHeight=Math.max(maxHeight,elHeight);
			}
			elements.push(this);
		});
		setSameHeight();
    }

});