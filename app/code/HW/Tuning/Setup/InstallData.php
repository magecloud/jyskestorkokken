<?php


namespace HW\Tuning\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
	
    public function __construct(
		EavSetupFactory $eavSetupFactory
		)
    {
        $this->eavSetupFactory = $eavSetupFactory;
		
    }
	
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
		
		
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'bottom_description',
			[
				'type' => 'text',
				'label' => 'Bottom Description',
				'input' => 'textarea',
				'required' => false,
				'sort_order' => 4,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
				'wysiwyg_enabled' => true,
				'is_html_allowed_on_front' => true,
				'group' => 'General Information',
			]
		);

    }
	
}
