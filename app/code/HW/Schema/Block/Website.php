<?php

namespace HW\Schema\Block;

class Website extends \Magento\Framework\View\Element\Text
{
    protected $_scopeConfig;
	
	
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,       
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_scopeConfig = $scopeConfig;
    }
	

    protected function _toHtml()
    {
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;		
        if($text=$this->_scopeConfig->getValue('web/schemaJs/website', $storeScope) ) {
            $this->setText($text);
        }
        return parent:: _toHtml() ;
    }
	
	
}