<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 15.08.2018
 * Time: 14:01
 */
namespace HW\Newsletter\Block;

class Subscribe extends \Magento\Newsletter\Block\Subscribe {

	public function getScopeConfig() {

		return $this->_scopeConfig;
	}

	public function getDescription() {

		return trim($this->_scopeConfig->getValue('newsletter/subscription/block_description',\Magento\Store\Model\ScopeInterface::SCOPE_STORES));
	}
}