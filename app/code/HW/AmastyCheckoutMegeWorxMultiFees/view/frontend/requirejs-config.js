/**
 * requirejs-config.js
 *
 * @copyright Copyright © 2018 HeadWay. All rights reserved.
 * @author    ilya.kush@gmaul.com.com
 */

var config = {
    map: {
        '*': {
            'MageWorx_MultiFees/js/view/shipping-fee':'HW_AmastyCheckoutMegeWorxMultiFees/js/view/shipping-fee'
        }
    }
};