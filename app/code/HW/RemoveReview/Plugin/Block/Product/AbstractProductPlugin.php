<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 25.03.2019
 * Time: 13:07
 */
namespace HW\RemoveReview\Plugin\Block\Product;

class AbstractProductPlugin
{


	public function afterGetReviewsSummaryHtml(\Magento\Catalog\Block\Product\AbstractProduct $subject, $result) {

		return '';
	}

	public function beforeGetReviewsSummaryHtml(\Magento\Catalog\Block\Product\AbstractProduct $subject, \Magento\Catalog\Model\Product $product, $templateType = false, $displayIfNoReviews = false) {

	}
}