<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 26.09.2018
 * Time: 22:11
 */
namespace HW\QtyIncrementors\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

	/**
	 * @var \Magento\CatalogInventory\Api\StockRegistryInterface
	 */
	private $_stockRegistry;

	/**
	 * Data constructor.
	 *
	 * @param \Magento\Framework\App\Helper\Context                $context
	 * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
	 */
	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
	) {

		parent::__construct($context);
		$this->_stockRegistry = $stockRegistry;
	}

	/**
	 * @param \Magento\Catalog\Model\Product $product
	 *
	 * @return float|int
	 */
	public function getProductQtyIncrements(\Magento\Catalog\Model\Product $product){

		if($product->getId()) {
			$stockItem = $this->_stockRegistry->getStockItem(
				$product->getId(),
				$product->getStore()->getWebsiteId()
			);
			return (float)$stockItem->getQtyIncrements();
		}

		return 1;
	}
}