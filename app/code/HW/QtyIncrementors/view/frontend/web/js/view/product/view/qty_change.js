define([
    'ko',
    'uiComponent'
], function (ko, Component) {
    'use strict';

    return Component.extend({
        initialize: function () {
            //initialize parent Component
            this._super();
            this.qty = ko.observable(this.defaultQty);
            if(this.stepQty <=0) {
                this.stepQty = 1;
            }
        },

        decreaseQty: function() {
            var newQty = this.qty() - this.stepQty;
            if (newQty < 1) {
                newQty = this.stepQty;
            }
            this.qty(newQty);
        },

        increaseQty: function() {
            var newQty = this.qty() + this.stepQty;
            this.qty(newQty);
        }

    });
});