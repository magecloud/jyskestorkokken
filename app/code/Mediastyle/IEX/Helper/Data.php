<?php

namespace Mediastyle\IEX\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\ObjectManager;

class Data extends AbstractHelper
{
    // Other
    protected $shippingConfig;
    protected $paymentConfig;
    protected $taxRates;
    protected $taxCalculation;
    protected $scopeConfig;
    protected $storeManager;

    // Product
    protected $productCollection;
    protected $productFactory;
    protected $productRepository;
    protected $productConfigurable;

    // Category
    protected $categoryFactory;
    protected $categoryRepository;

    // Order
    protected $orderStates;
    protected $orderCollection;
    protected $orderFactory;

    // Attribute
    protected $attributeRepository;
    protected $tableFactory;
    protected $attributeOptionManagement;
    protected $optionLabelFactory;
    protected $optionFactory;

    public function __construct	(
            \Magento\Shipping\Model\Config $shippingConfig,
            \Magento\Payment\Helper\Data $paymentConfig,
            \Magento\Tax\Model\Calculation\Rate $taxRates,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
            \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $orderStates,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
            \Magento\Catalog\Model\ProductFactory $productFactory,
            \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
            \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollection,
            \Magento\Sales\Model\OrderFactory $orderFactory,
            \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
            \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
            \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
            \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
            \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
            \Magento\Tax\Api\TaxCalculationInterface $taxCalculation,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\ConfigurableProduct\Model\Product\Type\Configurable $productConfigurable,
            \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory,
            \Magento\Catalog\Model\CategoryRepository $categoryRepository
            ) {
        $this->shippingConfig = $shippingConfig;
        $this->paymentConfig = $paymentConfig;
        $this->taxRates = $taxRates;
        $this->scopeConfig = $scopeConfig;
        $this->orderStates = $orderStates;
        $this->productCollection = $productCollection;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->orderCollection = $orderCollection;
        $this->orderFactory = $orderFactory;
        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
        $this->taxCalculation = $taxCalculation;
        $this->storeManager = $storeManager;
        $this->productConfigurable = $productConfigurable;
        $this->categoryFactory = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
    }

    public function GetAllOrders($status, $from_date = NULL, $page = 1, $page_size = 0, $colorAttr = null, $sizeAttr = null)
    {
        $collection = $this->orderCollection->create();
        if ($status) {
            $collection->addFilter('status', $status, 'eq');
        }
        if ($from_date) {
            $collection->addFieldToFilter('updated_at', ['gteq' => $from_date]);
        }
        if ($page_size) 
        {
            $collection->setPageSize($page_size);
            $collection->setCurPage($page);
        }
        $items = $collection->getItems();

        $orders = array();
        foreach ($items as $key => $item)
        {
            $_order = $this->orderFactory->create()->load($key);
            $order_data = $_order->getData();

            // Fix dates
            $order_data['created_at'] = date('Y-m-d\TH:i:s', strtotime($order_data['created_at']));
            $order_data['updated_at'] = date('Y-m-d\TH:i:s', strtotime($order_data['updated_at']));

            // Get billing and shipping address
            $billingAddress = $_order->getBillingAddress()->getData(); 
            $shippingAddress = $billingAddress;

            // If we got a shipping address
            $shipping = $_order->getShippingAddress();
            if ($shipping) {
                $shippingAddress = $_order->getShippingAddress()->getData();
            }

            $billingAddress['fullname'] = $billingAddress['firstname'];
            if ($billingAddress['lastname']) {
                $billingAddress['fullname'] .= ' ' . $billingAddress['lastname'];
            }		

            $shippingAddress['fullname'] = $shippingAddress['firstname'];
            if ($shippingAddress['lastname']) {
                $shippingAddress['fullname'] .= ' ' . $shippingAddress['lastname'];
            }    

            $order_data['billingAddress'] = $billingAddress;
            $order_data['shippingAddress'] = $shippingAddress;

            // Get payment and shipping lines
            $payment = $_order->getPayment()->getData();
            $order_data['paymentInfo'] = $payment;

            // Calculate shipping tax rate
            $order_data['shipping_vatRate'] = 0;
            if ($order_data['shipping_tax_amount'] > 0) 
            {
                $order_data['shipping_vatRate'] = $order_data['shipping_tax_amount'] / ($order_data['shipping_amount'] * 100);
            }

            $order_items = $_order->getAllItems();
            $orderlines = array();

            $linemeta = array();
            foreach ($order_items as $line)
            {
                $linedata = $line->getData();

                if ($linedata['product_type'] == 'configurable')
                {
                    $linemeta[$linedata['item_id']] = $linedata;
                    continue;
                }

                if ($linedata['product_type'] == 'bundle')
                {
                    // TODO Get bundle info
                }

                if ($linedata['parent_item_id'] && isset($linemeta[$linedata['parent_item_id']])) 
                {
                    $parent = $linemeta[$linedata['parent_item_id']];

                    if ($parent['product_type'] == 'bundle') 
                    {
                        $linedata['price'] = 0.00;
                        $linedata['tax_amount'] = 0.00;
                    }	 
                    else
                    {
                        $linedata['price'] = $parent['price'];
                        $linedata['discount_percent'] = $parent['discount_percent'];
                        $linedata['discount_amount'] = $parent['discount_amount'];
                        $linedata['tax_percent'] = $parent['tax_percent'];
                        $linedata['tax_amount'] = $parent['tax_amount'];

                        // Load product
                        $objectManager = ObjectManager::getInstance();
                        $product = $objectManager->create('\Magento\Catalog\Model\Product');
                        $product->load($linedata['product_id']);
                        $product_data = $product->getData();
                        $linedata['current_in_stock'] = $product_data['quantity_and_stock_status']['qty'];

                        if ($colorAttr) {
                            $colorfield = 'get'.ucfirst($colorAttr);
                            $color_id = $product->{$colorfield}();
                            $color_attr = $product->getResource()->getAttribute($colorAttr);

                            if ($color_attr->usesSource() && !empty($color_id)) {
                                $linedata['color'] = $color_attr->getSource()->getOptionText($color_id);
                            }
                        }

                        if ($sizeAttr) {
                            $sizefield = 'get'.ucfirst($sizeAttr);
                            $size_id = $product->{$sizefield}();
                            $size_attr = $product->getResource()->getAttribute($sizeAttr);

                            if ($size_attr->usesSource() && !empty($size_id)) {
                                $linedata['size'] = $size_attr->getSource()->getOptionText($size_id);
                            }
                        }

                        // Load parent product
                        $parentId = $linedata['product_options']['info_buyRequest']['product'];

                        $objectManager = ObjectManager::getInstance();
                        $product = $objectManager->create('\Magento\Catalog\Model\Product');
                        $parent_data = $product->load($parentId);
                        $linedata['parent_sku'] = $parent_data['sku'];
                        $linedata['parent_id'] = $parent_data['entity_id'];
                    }
                }	

                if ($linedata['product_type'] == 'grouped') {
                    $parentId = $linedata['product_options']['super_product_config']['product_id'];

                    if ($parentId) {
                        $objectManager = ObjectManager::getInstance();
                        $product = $objectManager->create('\Magento\Catalog\Model\Product');
                        $parent_data = $product->load($parentId);
                        $linedata['parent_sku'] = $parent_data['sku'];
                        $linedata['parent_id'] = $parent_data['entity_id'];
                    }
                }

                $orderlines[] = $linedata;
            }			

            $order_data['orderlines'] = $orderlines;

            $orders[] = $order_data;
        }

        return $orders;
    }

    public function GetOrderCount($status, $from_date = NULL)
    {
        $collection = $this->orderCollection->create();
        if ($status) {
            $collection->addFilter('status', $status, 'eq');
        }
        if ($from_date) {
            $collection->addFieldToFilter('updated_at', ['gteq' => $from_date]);
        }
        $count = $collection->count();

        return array('count' => $count);
    }

    public function GetOrder($order_id, $colorAttr = null, $sizeAttr = null)
    {
        if (!$order_id) {
            return array('status' => 'error', 'message' => 'No order id provided');
        }

        $_order = $this->orderFactory->create()->load($order_id);
        if (!$_order->getData()) {
            return array('status' => 'error', 'message' => 'Order does not exists');
        }

        $order_data = $_order->getData();

        // Fix dates
        $order_data['created_at'] = date('Y-m-d\TH:i:s', strtotime($order_data['created_at']));
        $order_data['updated_at'] = date('Y-m-d\TH:i:s', strtotime($order_data['updated_at']));

        // Get billing and shipping address
        $billingAddress = $_order->getBillingAddress()->getData(); 
        $shippingAddress = $billingAddress;

        // If we got a shipping address
        $shipping = $_order->getShippingAddress();
        if ($shipping) {
            $shippingAddress = $_order->getShippingAddress()->getData();
        }

        $billingAddress['fullname'] = $billingAddress['firstname'];
        if ($billingAddress['lastname']) {
            $billingAddress['fullname'] .= ' ' . $billingAddress['lastname'];
        }    

        $shippingAddress['fullname'] = $shippingAddress['firstname'];
        if ($shippingAddress['lastname']) {
            $shippingAddress['fullname'] .= ' ' . $shippingAddress['lastname'];
        }    

        $order_data['billingAddress'] = $billingAddress;
        $order_data['shippingAddress'] = $shippingAddress;

        // Get payment and shipping lines
        $payment = $_order->getPayment()->getData();
        $order_data['paymentInfo'] = $payment;

        // Calculate shipping tax rate
        $order_data['shipping_vatRate'] = 0;
        if ($order_data['shipping_tax_amount'] > 0) {
            $order_data['shipping_vatRate'] = $order_data['shipping_tax_amount'] / ($order_data['shipping_amount'] * 100);
        }

        $order_items = $_order->getAllItems();
        $orderlines = array();

        $linemeta = array();
        foreach ($order_items as $line) {
            $linedata = $line->getData();

            if ($linedata['product_type'] == 'configurable') {
                $linemeta[$linedata['item_id']] = $linedata;
                continue;
            }

            if ($linedata['product_type'] == 'bundle') {   
                // TODO Get bundle info
            }

            if ($linedata['parent_item_id'] && isset($linemeta[$linedata['parent_item_id']])) {
                $parent = $linemeta[$linedata['parent_item_id']];

                if ($parent['product_type'] == 'bundle') {
                    $linedata['price'] = 0.00;
                    $linedata['tax_amount'] = 0.00;
                } else {
                    $linedata['price'] = $parent['price'];
                    $linedata['discount_percent'] = $parent['discount_percent'];
                    $linedata['discount_amount'] = $parent['discount_amount'];
                    $linedata['tax_percent'] = $parent['tax_percent'];
                    $linedata['tax_amount'] = $parent['tax_amount'];

                    // Load product
                    $objectManager = ObjectManager::getInstance();
                    $product = $objectManager->create('\Magento\Catalog\Model\Product');
                    $product->load($linedata['product_id']);
                    $product_data = $product->getData();
                    $linedata['current_in_stock'] = $product_data['quantity_and_stock_status']['qty'];

                    if ($colorAttr) {
                        $colorfield = 'get'.ucfirst($colorAttr);
                        $color_id = $product->{$colorfield}();
                        $color_attr = $product->getResource()->getAttribute($colorAttr);

                        if ($color_attr->usesSource() && !empty($color_id)) {
                            $linedata['color'] = $color_attr->getSource()->getOptionText($color_id);
                        }
                    }

                    if ($sizeAttr) {
                        $sizefield = 'get'.ucfirst($sizeAttr);
                        $size_id = $product->{$sizefield}();
                        $size_attr = $product->getResource()->getAttribute($sizeAttr);

                        if ($size_attr->usesSource() && !empty($size_id)) {
                            $linedata['size'] = $size_attr->getSource()->getOptionText($size_id);
                        }
                    }

                    // Load parent product
                    $parentId = $linedata['product_options']['info_buyRequest']['product'];

                    $objectManager = ObjectManager::getInstance();
                    $product = $objectManager->create('\Magento\Catalog\Model\Product');
                    $parent_data = $product->load($parentId);
                    $linedata['parent_sku'] = $parent_data['sku'];
                    $linedata['parent_id'] = $parent_data['entity_id'];
                }
            }

            if ($linedata['product_type'] == 'grouped') {
                $parentId = $linedata['product_options']['super_product_config']['product_id'];

                if ($parentId) {
                    $objectManager = ObjectManager::getInstance();
                    $product = $objectManager->create('\Magento\Catalog\Model\Product');
                    $parent_data = $product->load($parentId);
                    $linedata['parent_sku'] = $parent_data['sku'];
                    $linedata['parent_id'] = $parent_data['entity_id'];
                }
            }

            $orderlines[] = $linedata;
        }

        $order_data['orderlines'] = $orderlines;

        return $order_data;
    }

    public function GetAllProducts($page = 1, $page_size = 0, $from_date = NULL, $colorAttr = null, $sizeAttr = null) {
        $collection = $this->productCollection->create();
        if ($page_size) {
            $collection->setPageSize($page_size);
            $collection->setCurPage($page);
        }
        if ($from_date) {
            $collection->addFieldToFilter('updated_at', ['gteq' => $from_date]);
        }

        $collection->setFlag('has_stock_status_filter', true);
        $items = $collection->getItems();

        $taxRates = $this->taxRates->getCollection()->getData();
        $taxes_included = $this->scopeConfig->getValue('tax/calculation/price_includes_tax',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $default_tax_country = $this->scopeConfig->getValue('tax/defaults/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $default_tax_rate = 25;

        foreach ($taxRates as $rate) {
            if ($rate['tax_country_id'] == $default_tax_country) {
                $default_tax_rate = $rate['rate'];
            }
        }

        $products = array();
        foreach ($items as $key => $item) {
            $_product = $this->productFactory->create()->load($key);
            $productdata = $_product->getData();

            if (!isset($productdata['price'])) {
                $productdata['price'] = 0.00;
            }

            // Check if prices are incl. or excl. tax
            $productdata['taxes_included'] = $taxes_included;
            $productdata['default_tax_calculation'] = $default_tax_country;
            $productdata['price_excl_vat'] = $productdata['price'];
            $productdata['vatRate'] = 0;
            $productdata['taxable'] = 0;
            if (isset($productdata['tax_class_id']) && $productdata['tax_class_id'] != 0) 
            {
                // Get tax rate
                $default_tax_rate = $this->taxCalculation->getCalculatedRate($productdata['tax_class_id']);
                $productdata['vatRate'] = $default_tax_rate;

                if ($default_tax_rate > 0) {
                    $productdata['taxable'] = 1;
                }

                // Get default tax rate	
                if ($productdata['taxes_included']) {
                    // Calculate price excl. vat
                    $productdata['price_excl_vat'] = $productdata['price'] / (1 + ($default_tax_rate / 100));
                } else {
                    // Calculate price incl. vat
                    $productdata['price'] = $productdata['price'] * (1 + ($default_tax_rate / 100));
                }
            }

            // Get parent information if exists
            $productdata['type'] = 'simple';

            $productdata['parentIds'] = $this->productConfigurable->getParentIdsByChild($_product->getId());
            if ($productdata['parentIds']) {
                foreach ($productdata['parentIds'] as $parent) {
                    // Get parent information
                    $parent_product = $this->productFactory->create();
                    $_parent = $parent_product->load($parent);

                    $productdata['parentSku'] = $_parent->getSku();
                    $productdata['parentName'] = $_parent->getName();
                    $productdata['type'] = 'variation';
                }
            }

            // If type configurable set isParent
            if ($productdata['type_id'] == 'configurable') {
                $productdata['isParent'] = true;
            }

            if ($colorAttr) {
                $colorfield = 'get'.ucfirst($colorAttr);
                $color_id = $_product->{$colorfield}();
                $color_attr = $_product->getResource()->getAttribute($colorAttr);

                if ($color_attr->usesSource() && !empty($color_id)) {
                    $productdata['color_label'] = $color_attr->getSource()->getOptionText($color_id);
                }
            }

            if ($sizeAttr) {
                $sizefield = 'get'.ucfirst($sizeAttr);
                $size_id = $_product->{$sizefield}();
                $size_attr = $_product->getResource()->getAttribute($sizeAttr);

                if ($size_attr->usesSource() && !empty($size_id)) {
                    $productdata['size_label'] = $size_attr->getSource()->getOptionText($size_id);
                }
            }

            $products[] = $productdata;
        }

        return $products;
    }

    public function GetProductCount() {
        $collection = $this->productCollection->create();
        $collection->setFlag('has_stock_status_filter', true);
        $count = $collection->count();

        return array('count' => $count);
    }

    public function GetProduct($sku, $colorAttr = null, $sizeAttr = null) {
        $product = $this->productFactory->create();
        $id = $product->getIdBySku($sku);
        if (!$id) {
            return array();
        }

        $_product = $product->load($id);
        if ($_product) 
        {
            $taxRates = $this->taxRates->getCollection()->getData();
            $taxes_included = $this->scopeConfig->getValue('tax/calculation/price_includes_tax',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $default_tax_country = $this->scopeConfig->getValue('tax/defaults/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $default_tax_rate = 25;

            foreach ($taxRates as $rate)
            {
                if ($rate['tax_country_id'] == $default_tax_country) 
                {
                    $default_tax_rate = $rate['rate'];  
                }	   
            }	   

            $productdata = $_product->getData();

            // Check if prices are incl. or excl. tax
            $productdata['taxes_included'] = $taxes_included;
            $productdata['default_tax_calculation'] = $default_tax_country;
            $productdata['price_excl_vat'] = 0;
            if (isset($productdata['price'])) {
                $productdata['price_excl_vat'] = $productdata['price']; 
            }
            $productdata['vatRate'] = 0;
            $productdata['taxable'] = 0;
            if (isset($productdata['tax_class_id']) && $productdata['tax_class_id'] != 0)
            {	
                // Get tax rate
                $default_tax_rate = $this->taxCalculation->getCalculatedRate($productdata['tax_class_id']);
                $productdata['vatRate'] = $default_tax_rate;    

                if ($default_tax_rate > 0) {
                    $productdata['taxable'] = 1;
                }

                // Get default tax rate 
                if ($productdata['taxes_included']) {
                    // Calculate price excl. vat
                    $productdata['price_excl_vat'] = $productdata['price'] / (1 + ($default_tax_rate / 100));
                } else {
                    // Calculate price incl. vat
                    $productdata['price'] = $productdata['price'] * (1 + ($default_tax_rate / 100));
                }
            }

            // Get parent information if exists
            $productdata['type'] = 'simple';

            $productdata['parentIds'] = $this->productConfigurable->getParentIdsByChild($_product->getId());
            if ($productdata['parentIds']) {
                foreach ($productdata['parentIds'] as $parent) {
                    // Get parent information
                    $parent_product = $this->productFactory->create();
                    $_parent = $parent_product->load($parent);
     
                    $productdata['parentSku'] = $_parent->getSku();
                    $productdata['parentName'] = $_parent->getName();
                    $productdata['type'] = 'variation';
                }
            }

            // If type configurable set isParent
            if ($productdata['type_id'] == 'configurable') {
                $productdata['isParent'] = true;
            }

            if ($colorAttr) {
                $colorfield = 'get'.ucfirst($colorAttr);
                $color_id = $_product->{$colorfield}();
                $color_attr = $_product->getResource()->getAttribute($colorAttr);

                if ($color_attr->usesSource() && !empty($color_id)) {
                    $productdata['color_label'] = $color_attr->getSource()->getOptionText($color_id);
                }
            }

            if ($sizeAttr) {
                $sizefield = 'get'.ucfirst($sizeAttr);
                $size_id = $_product->{$sizefield}();
                $size_attr = $_product->getResource()->getAttribute($sizeAttr);

                if ($size_attr->usesSource() && !empty($size_id)) {
                    $productdata['size_label'] = $size_attr->getSource()->getOptionText($size_id);
                }
            }

            return $productdata;
        }			

        return array();
    }	

    public function CreateProduct($postdata)
    {
        // TODO Rewrite products to use productfactory and productRepository
        $result = array();

        // Check if we got a product type 
        if (!$postdata->data->type) 
        {
            return array('status' => 'error', 'message' => 'No product type in data');
        }

        // Skip data if parent product
        if (isset($postdata->data->isParent) && $postdata->data->isParent)  
        {
            return array('status' => 'skipped', 'message' => 'Skipped. Configurable product are to be created by child');
        }

        if ($postdata->data->type == 'variation') 
        {
            // Check if parent product exits
            $objectManager = ObjectManager::getInstance();
            $parentProduct = $objectManager->get('Magento\Catalog\Model\Product');

            $parentId = $parentProduct->getIdBySku($postdata->data->parentSku);

            // If parent do not exists we create it
            if (!$parentId) 
            {
                // Create parent product
                $parent_result = $this->CreateConfigurable($postdata);
                if ($parent_result['status'] == 'error') {
                    return $parent_result;
                }

                $parentId = $parent_result['parentId'];
            }

            // Create variation
            $variation_result = $this->CreateVariation($postdata, $parentId);
            return $variation_result;

        } else {
            // Create simple product
            $result = $this->CreateSimple($postdata);
        }	

        return $result;
    }

    private function CreateSimple($postdata) 
    {
        try 
        {
            // Set store to admin store 0
            $this->storeManager->setCurrentStore(0);

            $stock_status = ($postdata->data->qty >= 1) ? 1 : 0;

            $tax_class = (isset($postdata->data->taxable)) ? $postdata->data->taxable : 2;

            $objectManager = ObjectManager::getInstance();
            $product = $objectManager->create('\Magento\Catalog\Model\Product');
            $product->setSku($postdata->data->sku);		
            $product->setName($postdata->data->name);
            $product->setAttributeSetId($postdata->data->attributeId);
            $product->setStatus(2); // Default on product creation
            $product->setWeight($postdata->data->weight);
            $product->setVisibility(4); // Default is 4
            $product->setTaxClassId($tax_class);
            $product->setTypeId('simple');
            $product->setPrice($postdata->data->price);
            $product->setCategoryIds(array($postdata->data->categoryId));
            $product->setWebsiteIds(array($postdata->data->websiteId));
            $product->setStockData(
                    array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 1,
                        'is_in_stock' => $stock_status,
                        'qty' => $postdata->data->qty
                        )
                    );
            $product->setQuantityAndStockStatus(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);

            // If descriptions and/or short description is received
            if (isset($postdata->data->description))
            {
                $product->setDescription($postdata->data->description);
            }

            if (isset($postdata->data->shortDescription))
            {
                $product->setShortDescription($postdata->data->shortDescription);
            }

            // Generate new URL key to make sure key does not exists
            $url_name = $postdata->data->name.'-'.$postdata->data->sku;
            $url = preg_replace('#[^0-9a-z]+#i', '-', $url_name);
            $product->setUrlKey($url);

            // If custom fields
            if (isset($postdata->data->custom)) {
                if ($postdata->data->custom) {
                    foreach ($postdata->data->custom as $key => $field) {
                        $fieldName = 'set'.$key;
                        $product->$fieldName($field);
                    }
                }
            }

            // Save product
            $save_result = $this->productRepository->save($product);
    
            if (!$save_result->getId()) {
                return array('status' => 'error', 'message' => 'Error creating product');
            }

            $productId = $save_result->getId();
            
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
            $product->setStockData(
                    array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 1,
                        'is_in_stock' => $stock_status,
                        'qty' => $postdata->data->qty
                        )
                    );
            $product->setQuantityAndStockStatus(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);
            $save_result = $this->productRepository->save($product);

            if (!$save_result->getId()) {
                return array('status' => 'error', 'message' => 'Error creating product');
            }

            return array('status' => 'ok', 'message' => 'Created new product with Id: ' . $save_result->getId());
        } 
        catch (Exception $e) 
        {
            return array('status' => 'error', 'message' => $e->getMessage());
        }	
    }

    private function CreateConfigurable($postdata)
    {
        try {
            // Set store to admin store 0
            $this->storeManager->setCurrentStore(0);

            $tax_class = (isset($postdata->data->taxable)) ? $postdata->data->taxable : 2;

            $objectManager = ObjectManager::getInstance();
            $product = $objectManager->create('\Magento\Catalog\Model\Product');
            $product->setSku($postdata->data->parentSku);
            $product->setName($postdata->data->name);
            $product->setAttributeSetId($postdata->data->attributeId);
            $product->setStatus(2); // Default on product creation
            $product->setWeight($postdata->data->weight);
            $product->setVisibility(4); // Default is 4
            $product->setTaxClassId($tax_class);
            $product->setTypeId('configurable');
            $product->setPrice($postdata->data->price);
            $product->setCategoryIds(array($postdata->data->categoryId));
            $product->setWebsiteIds(array($postdata->data->websiteId));
            $product->setStockData(
                    array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 1,
                        'is_in_stock' => 1,
                        )
                    );

            // If descriptions and/or short description is received
            if (isset($postdata->data->description)) {
                $product->setDescription($postdata->data->description);
            }

            if (isset($postdata->data->shortDescription)) {
                $product->setShortDescription($postdata->data->shortDescription);
            }

            // If custom fields
            if (isset($postdata->data->parentCustom)) {
                if ($postdata->data->parentCustom) {
                    foreach ($postdata->data->parentCustom as $key => $field) {
                        $fieldName = 'set'.$key;
                        $product->$fieldName($field);
                    }
                }
            }

            // Generate new URL key to make sure key does not exists
            $url_name = $postdata->data->name.'-'.$postdata->data->parentSku;
            $url = preg_replace('#[^0-9a-z]+#i', '-', $url_name);
            $url = strtolower($url);
            $product->setUrlKey($url);

            // Save product
            $save_result = $this->productRepository->save($product);

            return array('status' => 'ok', 'message' => 'Created new product with id: ' . $save_result->getId(), 'parentId' => $save_result->getId());
        }
        catch (Exception $e) 
        {
            return array('status' => 'error', 'message' => $e->getMessage());
        }
    }

    private function CreateVariation($postdata, $parentId) 
    {
        try
        {
            // Set store to admin store 0
            $this->storeManager->setCurrentStore(0);

            $stock_status = ($postdata->data->qty >= 1) ? 1 : 0;
            $tax_class = (isset($postdata->data->taxable)) ? $postdata->data->taxable : 2;

            $objectManager = ObjectManager::getInstance();
            $product = $objectManager->create('\Magento\Catalog\Model\Product');
            $product->setSku($postdata->data->sku);
            $product->setName($postdata->data->name);
            $product->setAttributeSetId($postdata->data->attributeId);
            $product->setStatus(1); // Default on product creation
            $product->setWeight($postdata->data->weight);
            $product->setVisibility(1); // Default is 1 on variations
            $product->setTaxClassId($tax_class);
            $product->setTypeId('simple');
            $product->setPrice($postdata->data->price);
            $product->setWebsiteIds(array($postdata->data->websiteId));
            $product->setStockData(
                    array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 1,
                        'is_in_stock' => $stock_status,
                        'qty' => $postdata->data->qty
                        )
                    );
            $product->setQuantityAndStockStatus(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);

            // If descriptions and/or short description is received
            if (isset($postdata->data->description)) {
                $product->setDescription($postdata->data->description);
            }

            if (isset($postdata->data->shortDescription)) {
                $product->setShortDescription($postdata->data->shortDescription);
            }

            // If we got a color
            if (isset($postdata->data->color)) {
                if ($postdata->data->color) {
                    $colorId = $this->GetAttributeId($postdata->data->colorAttr, $postdata->data->color);
                    $colorName = 'set'.ucfirst($postdata->data->colorAttr);
                    $product->$colorName($colorId);
                }
            }

            // If we got a size
            if (isset($postdata->data->size)) {
                if ($postdata->data->size) {
                    $sizeId = $this->GetAttributeId($postdata->data->sizeAttr, $postdata->data->size);
                    $sizeName = 'set'.ucfirst($postdata->data->sizeAttr);
                    $product->$sizeName($sizeId);
                }
            }

            // If custom fields
            if (isset($postdata->data->custom)) {
                if ($postdata->data->custom) {
                    foreach ($postdata->data->custom as $key => $field) {
                        $fieldName = 'set'.$key;
                        $product->$fieldName($field);
                    }   
                }
            }

            $save_result = $this->productRepository->save($product);

            if (!$save_result->getId()) {
                return array('status' => 'error', 'message' => 'Error creating product variation');
            }

            $productId = $save_result->getId();

            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
            $product->setStockData(
                    array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 1,
                        'is_in_stock' => $stock_status,
                        'qty' => $postdata->data->qty
                        )
                    );
            $product->setQuantityAndStockStatus(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);
            $save_result = $this->productRepository->save($product);

            if (!$save_result->getId()) {
                return array('status' => 'error', 'message' => 'Error creating product variation');
            }

            // Assign variation to parent product
            $productId = $parentId; // Configurable Product Id
            $childId = $save_result->getId();
            $objectManager = ObjectManager::getInstance();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId); // Load Configurable Product
            $extensions = $product->getExtensionAttributes();
            $associatedProductIds = $extensions->getConfigurableProductLinks();

            $eavConfig = $objectManager->get('Magento\Eav\Model\Config');
            $sizeAttribute = $eavConfig->getAttribute('catalog_product', $postdata->data->sizeAttr);
            $colorAttribute = $eavConfig->getAttribute('catalog_product', $postdata->data->colorAttr);

            $sizeAttributeValues = array();
            $colorAttributeValues = array();		

            // If we got a product size
            if (isset($postdata->data->size)) {
                if ($postdata->data->size) {
                    $sizeAttributeValues[] = array(
                        'label' => $postdata->data->size,
                        'attribute_id' => $sizeAttribute->getId(),
                        'value_index' => $sizeId,
                        );
                }
            }

            if (isset($postdata->data->color)) {
                if ($postdata->data->color) {
                    $colorAttributeValues[] = array(
                        'label' => $postdata->data->color,
                        'attribute_id' => $colorAttribute->getId(),
                        'value_index' => $colorId,
                        );
                }
            }	

            $associatedProductIds[] = $childId;

            $configurableAttributesData = array();

            if (isset($postdata->data->size)) {
                if ($postdata->data->size) {
                    $configurableAttributesData[] =	array(
                        'attribute_id' => $sizeAttribute->getId(),
                        'code' => $sizeAttribute->getAttributeCode(),
                        'label' => $sizeAttribute->getStoreLabel(),
                        'position' => '0',
                        'values' => $sizeAttributeValues,
                        );
                }
            }

            if (isset($postdata->data->color)) {
                if ($postdata->data->color) {
                    $configurableAttributesData[] = array(
                        'attribute_id' => $colorAttribute->getId(),
                        'code' => $colorAttribute->getAttributeCode(),
                        'label' => $colorAttribute->getStoreLabel(),
                        'position' => '0',
                        'values' => $colorAttributeValues,
                        );
                }
            }

            $optionsFactory = $objectManager->create('Magento\ConfigurableProduct\Helper\Product\Options\Factory');
            $configurableOptions = $optionsFactory->create($configurableAttributesData);

            $customAttributes = array();
            if (isset($postdata->data->size)) {
                if ($postdata->data->size) {
                    $customAttributes[] = array(
                        'attribute_code' => $sizeAttribute->getAttributeCode(),
                        'value' => $sizeId,
                        );
                }
            }

            if (isset($postdata->data->color)) {
                if ($postdata->data->color) {
                    $customAttributes[] = array(
                        'attribute_code' => $colorAttribute->getAttributeCode(),
                        'value' => $colorId,
                        );
                }
            }
            $product->setCustomAttributes($customAttributes);
            $extensions = $product->getExtensionAttributes();
            $extensions->setConfigurableProductOptions($configurableOptions);
            $extensions->setConfigurableProductLinks($associatedProductIds);
            $product->setExtensionAttributes($extensions);

            $save_result = $this->productRepository->save($product);

            return array('status' => 'ok', 'message' => 'Created new product with id: ' . $save_result->getId());
        } 
        catch (Exception $e)
        {   
            return array('status' => 'error', 'message' => $e->getMessage());
        }
    }

    public function UpdateProduct($postdata) 
    {
        try {
            if (!$postdata->data->sku) {
                return array('status' => 'error', 'message' => 'No sku provided');
            }

            // Set store to admin store 0
            $this->storeManager->setCurrentStore(0);

            // Update product
            $objectManager = ObjectManager::getInstance();
            $product = $objectManager->create('\Magento\Catalog\Model\Product');
            $product->load($product->getIdBySku($postdata->data->sku));

            // If we need to update name
            if (isset($postdata->data->name)) {
                $product->setName($postdata->data->name);
            }

            // If we need to update name
            if (isset($postdata->data->description)) {
                $product->setDescription($postdata->data->description);
            }

            // If we need to update name
            if (isset($postdata->data->shortDescription)) {
                $product->setShortDescription($postdata->data->shortDescription);
            } 			

            // If we need to update price
            if (isset($postdata->data->price)) {
                $product->setPrice($postdata->data->price);
            }		

            // If custom fields
            if (isset($postdata->data->custom)) {
		if ($postdata->data->custom) {
                    foreach ($postdata->data->custom as $key => $field) {
                        $fieldName = 'set'.$key;
                        $product->$fieldName($field);
                    }
		}
            }

            // If we need to update stock
            if (isset($postdata->data->qty)) {
                $stock_status = ($postdata->data->qty >= 1) ? 1: 0;
                $product->setStockData(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);
                $product->setQuantityAndStockStatus(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);
            }

            $save_result = $this->productRepository->save($product);

            return array('status' => 'ok', 'message' => 'Update product with id: ' . $save_result->getId());
        } catch (Exception $e) {
            return array('status' => 'error', 'message' => $e->getMessage());
        }
    }

    private function GetAttribute($attributeCode)
    {
        return $this->attributeRepository->get($attributeCode);
    }

    private function GetAttributeId($attributeCode, $label)
    {
        // If label if empty
        if (!$label) 
        {
            return array('status' => 'error', 'message' => 'Label cannot be empty');            
        }

        // Check if attribute exists and return id
        $optionId = $this->GetOptionId($attributeCode, $label);

        if (!$optionId) 
        {
            $optionLabel = $this->optionLabelFactory->create();
            $optionLabel->setStoreId(0);
            $optionLabel->setLabel($label);

            $option = $this->optionFactory->create();
            $option->setLabel($optionLabel);
            $option->setStoreLabels([$optionLabel]);
            $option->setSortOrder(0);
            $option->setIsDefault(false);

            $this->attributeOptionManagement->add(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $this->GetAttribute($attributeCode)->getAttributeId(),
                    $option
                    );

            $optionId = $this->GetOptionId($attributeCode, $label, true);
        }

        return $optionId;
    }

    private function GetOptionId($attributeCode, $label, $force = false)
    {
        $attribute = $this->GetAttribute($attributeCode);

        if ($force === true || !isset($this->attributeValues[$attribute->getAttributeId()])) {
            $this->attributeValues[$attribute->getAttributeId()] = array();

            $sourceModel = $this->tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[$attribute->getAttributeId()][$option['label']] = $option['value'];
            }
        }

        if (isset($this->attributeValues[$attribute->getAttributeId()][$label])) {
            return $this->attributeValues[$attribute->getAttributeId()][$label];
        }

        return false;
    }

    public function GetAllCategories($page = 1, $page_size = 0)
    {
        $collection = $this->categoryFactory->create();
        $collection->addAttributeToSelect('*');

        $category_data = [];
        foreach ($collection as $category) {
            $category_data[] = $category->getData();
        }

        return $category_data;
    }

    public function GetCategoryCount()
    {   
        $collection = $this->categoryFactory->create();

        $count = $collection->count();

        return array('count' => $count);
    }   

    public function GetCategory($id)
    {
        if (!$id) {
            return array('status' => 'error', 'message' => 'No category id provided');
        }
 
        $category = $this->categoryRepository->get($id);

        if ($category) {
            return $category->getData();
        } else {
            return array('status' => '', 'message' => 'Could not find category');
        }
    }

    public function SetStock($postdata)
    {
        if (!$postdata->data->sku)
        {
            return array('status' => 'error', 'message' => 'No sku provided');
        }

        // Update product stock
        $objectManager = ObjectManager::getInstance();
        $product = $objectManager->create('\Magento\Catalog\Model\Product');
        $product->load($product->getIdBySku($postdata->data->sku));

        // Check if product exists
        if (!$product->getSku()) {
            return array('status' => 'error', 'message' => 'Product does not exists');
        }

        // If we need to update stock
        if (isset($postdata->data->qty)) 
        {
            $stock_status = ($postdata->data->qty >= 1) ? 1: 0;
            $product->setStockData(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);
            $product->setQuantityAndStockStatus(['qty' => $postdata->data->qty, 'is_in_stock' => $stock_status]);

            $save_result = $this->productRepository->save($product);	

            return array('status' => 'ok', 'message' => 'Stock updated on product with id: ' . $save_result->getId());
        }

        return array('status' => 'error', 'message' => 'Error on updating stock');
    }

    public function ShippingMethods() 
    {
        return $this->shippingConfig->getActiveCarriers();
    }

    public function PaymentMethods() 
    {
        $payment_methods = $this->paymentConfig->getPaymentMethods();
        $methods = array();

        // loop trough the methods
        foreach ($payment_methods as $method) 
        {
            if (isset($method['active'])) 
            {
                if ($method['active']) 
                {
                    $methods[] = $method;
                }
            }	
        }

        return $methods;	
    }

    public function OrderStates() 
    {
        return $this->orderStates->create()->toOptionArray();
    }	

}
