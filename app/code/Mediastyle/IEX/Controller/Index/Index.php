<?php

namespace Mediastyle\IEX\Controller\Index; 

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    protected $scopeconfig;

    protected $helper;

    protected $resultJsonFactory;

    public function __construct(
            Context $context, 
            \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
            \Mediastyle\IEX\Helper\Data $helper, 
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeconfig,
            \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
            ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
        $this->scopeconfig = $scopeconfig;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        // Set json headers
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header('Content-Type: application/json');

        $request = $this->getRequest();
        $type = $request->getParam('type');
        $key = $request->getParam('key');
        $action = $request->getParam('action');
        $sku = $request->getParam('sku');
        $category_id = $request->getParam('category_id');
        $page = $request->getParam('page');	
        $page_size = $request->getParam('page_size');
        $status = $request->getParam('status');
        $order_id = $request->getParam('order_id');
        $from_date = $request->getParam('from_date');
        $colorAttr = $request->getParam('color');
        $sizeAttr = $request->getParam('size');

        $postdata = file_get_contents("php://input");
        $post_data = $postdata;
        $postdata = json_decode($postdata);	

        $data = array();

        if ($key == $this->scopeconfig->getValue('iex_settings/general/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) || ( !empty($this->scopeconfig->getValue('iex_settings/general/api_key2', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) && $key == $this->scopeconfig->getValue('iex_settings/general/api_key2', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) ) 
        {
            switch($type) {
                case 'orders':
                    switch ($action) 
                    {
                        case 'getall':	
                            $data = $this->helper->GetAllOrders($status, $from_date, $page, $page_size, $colorAttr, $sizeAttr);	
                            break;
                        case 'count':	
                            $data = $this->helper->GetOrderCount($status, $from_date);
                            break;
                        case 'find':
                            $data = $this->helper->GetOrder($order_id, $colorAttr, $sizeAttr);
                            break;
                        default:
                            $data = array('status' => 'error', 'message' => 'Action does not exists');
                            break;
                    }
                    break;
                case 'products':
                    switch ($action) 
                    {
                        case 'getall':
                            $data = $this->helper->GetAllProducts($page, $page_size, $from_date, $colorAttr, $sizeAttr);
                            break;
                        case 'count':
                            $data = $this->helper->GetProductCount();
                            break;	
                        case 'find':
                            $data = $this->helper->GetProduct($sku, $colorAttr, $sizeAttr);
                            break;
                        case 'create':
                            $data = $this->helper->CreateProduct($postdata);
                            break;
                        case 'update':
                            $data = $this->helper->UpdateProduct($postdata);
                            break;
                        default:	
                            $data = array('status' => 'error', 'message' => 'Action does not exists');
                            break;
                    }
                    break;
                case 'categories':
                    switch ($action)
                    {
                        case 'getall':
                            $data = $this->helper->GetAllCategories($page, $page_size);
                            break;
                        case 'count':
                            $data = $this->helper->GetCategoryCount();
                            break;
                        case 'find':
                            $data = $this->helper->GetCategory($category_id);
                            break;
                        default:
                            $data = array('status' => 'error', 'message' => 'Action does not exists');
                            break;
                    }
                    break;
                case 'stock':
                    switch ($action) 
                    {
                        case 'update':
                            $data = $this->helper->SetStock($postdata);
                            break;
                        default:	
                            $data = array('status' => 'error', 'message' => 'Action does not exists');
                            break;
                    }
                    break;
                case 'orderstates':
                    $data = $this->helper->OrderStates();
                    break;
                case 'paymentmethods':
                    $data = $this->helper->PaymentMethods();
                    break;
                case 'shippingmethods':	
                    $data = $this->helper->ShippingMethods();
                    break;
            }		
        } 
        else 
        {
            $data = array('status' => 'error', 'message' => 'Access denied');
        }

        $result = $this->resultJsonFactory->create();
        $result->setData($data);
        return $result;
    }

}
