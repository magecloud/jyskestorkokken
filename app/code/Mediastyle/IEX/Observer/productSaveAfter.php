<?php
namespace Mediastyle\IEX\Observer;

use Magento\Framework\Event\ObserverInterface;

define('IEX_API_URL', 'https://api.iex.dk/');
#define('IEX_API_URL', 'http://iex2-dev.local/');

class productSaveAfter implements ObserverInterface
{
    protected $_objectManager;
    protected $scopeConfig;		
    protected $taxRates;
    protected $taxCalculation;

    public function __construct(
            \Magento\Framework\ObjectManagerInterface $objectManager,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
            \Magento\Tax\Model\Calculation\Rate $taxRates,
            \Magento\Tax\Api\TaxCalculationInterface $taxCalculation
            ) {
        $this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
        $this->taxRates = $taxRates;
        $this->taxCalculation = $taxCalculation;
        $this->transfers = array();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // Load observer
        $event = $observer->getEvent();
        $product = $event->getProduct()->getData();

        $iex_enabled = $this->scopeConfig->getValue('iex_settings/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $sync_products = $this->scopeConfig->getValue('iex_settings/products/synchronise_products', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);		

        if (!$iex_enabled) return;

        if (!$sync_products) return;

        $taxRates = $this->taxRates->getCollection()->getData();
        $taxes_included = $this->scopeConfig->getValue('tax/calculation/price_includes_tax',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $default_tax_country = $this->scopeConfig->getValue('tax/defaults/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $default_tax_rate = 25;

        foreach ($taxRates as $rate)
        {
            if ($rate['tax_country_id'] == $default_tax_country)
            {
                $default_tax_rate = $rate['rate'];
            }
        }

        if (!isset($product['price'])) {
            $product['price'] = 0.00;
        }

        // Check if prices are incl. or excl. tax
        $product['taxes_included'] = $taxes_included;
        $product['default_tax_calculation'] = $default_tax_country;
        $product['price_excl_vat'] = $product['price'];
        $product['vatRate'] = 0;
        $product['taxable'] = 0;
        if (isset($productdata['tax_class_id']) && $product['tax_class_id'] != 0) {
            // Get tax rate
            $default_tax_rate = $this->taxCalculation->getCalculatedRate($product['tax_class_id']);
            $product['vatRate'] = $default_tax_rate;

            if ($default_tax_rate > 0) {
                $product['taxable'] = 1;
            }

            // Get default tax rate 
            if ($product['taxes_included']) {
                // Calculate price excl. vat
                $product['price_excl_vat'] = $product['price'] / (1 + ($default_tax_rate / 100));
            } else {
                // Calculate price incl. vat
                $product['price'] = $product['price'] * (1 + ($default_tax_rate / 100));
            }
        }

        $this->addTransfer('product', $product);
        $this->doTransfer(true);

        return;
    }

    private function addTransfer($type, $data)
    {
        $transfer = array(
                'type' => $type,
                'data' => $data
                );

        $this->transfers[] = $transfer;
    }

    private function doTransfer()
    {
        $api_key = $this->scopeConfig->getValue('iex_settings/general/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        // Connect to api 
        $cl = curl_init();
        curl_setopt($cl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cl, CURLOPT_POST, true);

        foreach ($this->transfers as $transfer) {
            $url = IEX_API_URL . $api_key.'/magento2/'.$transfer['type'];

            curl_setopt($cl, CURLOPT_URL, $url);

            $postdata = json_encode($transfer['data']);

            // Add header for encryption, data security purposes
            $request_headers = array();
            $request_headers[] = 'Content-type: application/json';
            $request_headers[] = 'x-iex-api-key:' .$api_key;

            curl_setopt($cl, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($cl, CURLOPT_POSTFIELDS, $postdata);

            $result = curl_exec($cl);
        }

        curl_close($cl);

        return $result;
    }

}
