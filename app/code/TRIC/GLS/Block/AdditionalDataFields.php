<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Block;

use Magento\Framework\View\Element\Template;
use TRIC\GLS\Model\ParcelShop;

class AdditionalDataFields extends Template
{
    /**
     * @var string
     */
    public $shippingMethod;
    /**
     * @var string
     */
    public $postcode;
    /**
     * @var ParcelShop
     */
    public $parcelShop;

    public function __construct(
        Template\Context $context,
        ParcelShop $parcelShop
    ) {
        parent::__construct($context);
        $this->parcelShop = $parcelShop;
    }

    /**
     * @param $postcode
     * @param $countryCode
     *
     * @return mixed
     */
    public function getParcelShops($postcode, $countryCode)
    {
        return $this->parcelShop->getRemoteDroppointResponse($postcode, $countryCode);
    }
}
