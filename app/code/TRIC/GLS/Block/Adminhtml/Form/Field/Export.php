<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Block\Adminhtml\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Escaper;
use Magento\Backend\Model\UrlInterface;

class Export extends AbstractElement
{
    /**
     * @var UrlInterface
     */
    private $backendUrl;

    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        UrlInterface $backendUrl,
        array $data = []
    ) {
        $this->backendUrl = $backendUrl;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
    }

    /**
     * @return string
     */
    public function getElementHtml()
    {
        /** @var \Magento\Backend\Block\Widget\Button $buttonBlock */
        $buttonBlock = $this->getForm()->getParent()->getLayout()
            ->createBlock(\Magento\Backend\Block\Widget\Button::class);
        $params = ['website' => $buttonBlock->getRequest()->getParam('website')];
        $data = [
            'label' => __('Export CSV'),
            'onclick' => 'setLocation("' . $this->backendUrl->getUrl('tric_gls/system_config/export', $params) . '")',
            'class' => ''
        ];

        return $buttonBlock->setData($data)->toHtml();
    }
}
