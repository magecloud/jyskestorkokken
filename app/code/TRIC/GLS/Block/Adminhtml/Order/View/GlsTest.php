<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Block\Adminhtml\Order\View;

use Magento\Sales\Block\Adminhtml\Order\AbstractOrder;

class GlsTest extends AbstractOrder
{
    /**
     * @var string
     */
    public $shippingMethod;

    /**
     *
     * @return \Magento\Framework\DataObject|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getShippingMethod()
    {
        if (!$this->shippingMethod) {
            $this->shippingMethod = $this->getOrder()->getShippingMethod(true);
        }
        return $this->shippingMethod;
    }
    
    public function canShow()
    {
        return true;
    }
}
