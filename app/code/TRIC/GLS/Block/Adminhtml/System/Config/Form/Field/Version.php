<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Block\Adminhtml\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use TRIC\GLS\Helper\Data;

class Version extends Field
{
    const EXTENSION_URL = 'https://store.tric.dk/';
    /**
     * @var Data
     */
    public $helper;

    public function __construct(
        Context $context,
        Data $helper
    ) {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function _getElementHtml(AbstractElement $element)
    {
        $versionLabel = sprintf(
            '<a href="%s" title="%s" target="_blank">%s</a>',
            self::EXTENSION_URL,
            $this->helper->getExtensionName(),
            $this->helper->getExtensionVersion()
        );
        $element->setValue($versionLabel);

        return $element->getValue();
    }
}
