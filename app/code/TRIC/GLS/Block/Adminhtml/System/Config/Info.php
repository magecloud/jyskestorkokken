<?php
namespace TRIC\GLS\Block\Adminhtml\System\Config;


class Info extends \Magento\Backend\Block\AbstractBlock implements \Magento\Framework\Data\Form\Element\Renderer\RendererInterface
{
    protected $scopeConfig;
    protected $configWriter;
    protected $cache;
    protected $glsHelper;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\CacheInterface $cache,
        \TRIC\GLS\Helper\Data $glsHelper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->cache = $cache;
        $this->glsHelper = $glsHelper;
    }
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
	{
		$module = 'TRIC_GLS';
		
		if (!$this->scopeConfig->getValue(strtolower($module), \Magento\Store\Model\ScopeInterface::SCOPE_STORE).'/install/date') {
			$this->configWriter->save(strtolower($module).'/install/date', time());
		}
		
		$moduleName = $this->glsHelper->getExtensionName();
		$moduleVersion = $this->glsHelper->getExtensionVersion();

		$html = '';
		
		try {
			if(true || Mage::app()->getLocale()->getLocaleCode() == 'da_DK') {
				$url = 'https://services.tric.dk/info/extension_dk.html';
			} else {
				$url = 'https://services.tric.dk/info/extension_en.html';
			}
			
			$cacheKey = strtolower($module).'_'.str_replace('.','',$moduleVersion).'_info';
			
			if (!($this->cache->load($cacheKey)) || (time() - $this->cache->load($cacheKey.'_lastcheck')) > 43200) {
				if(extension_loaded('curl')) {
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);

					$html = curl_exec($ch);

					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$errorNo = curl_errno($ch);

					curl_close($ch);

					if($httpCode != 200 || $errorNo > 0) {
						return '';
					}
				}

				if(!$html){
					return '';
				}
				
				$html = str_replace('{{NAME}}', $moduleName, $html);
				$html = str_replace('{{VERSION}}', $moduleVersion, $html);

				$this->cache->save(serialize($html), $cacheKey);
				$this->cache->save(time(), $cacheKey.'_lastcheck');
				return $html;
			}

			$html = unserialize($this->cache->load($cacheKey));
			return $html;
    	
		} catch (Exception $e) {
      		return '';
  		}
    }
}
