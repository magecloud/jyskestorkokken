<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Block\Adminhtml\Rates\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];

        return $data;
    }
}
