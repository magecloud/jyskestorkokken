<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Block\Adminhtml\Carrier\Gls;

use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use TRIC\GLS\Model\ResourceModel\Carrier\Gls\CollectionFactory;
use TRIC\GLS\Model\Carrier\Gls;

class Grid extends Extended
{
    /**
     * @var null
     */
    public $websiteId = null;
    /**
     * @var null
     */
    public $storeId = null;
    /**
     * @var string
     */
    public $conditionName;
    /**
     * @var Gls
     */
    public $gls;
    /**
     * @var CollectionFactory
     */
    public $collectionFactory;

    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $collectionFactory,
        Gls $gls,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->gls = $gls;
        parent::__construct($context, $backendHelper, $data);
    }

    public function _construct()
    {
        parent::_construct();
        $this->setId('shippingGlsGrid');
        $this->_exportPageSize = 10000;
    }

    /**
     * @param $websiteId
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @deprecated
     */
    public function setWebsiteId($websiteId)
    {
        $this->websiteId = $this->_storeManager->getWebsite($websiteId)->getId();

        return $this;
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @deprecated
     */
    public function getWebsiteId()
    {
        if ($this->websiteId === null) {
            $this->websiteId = $this->_storeManager->getWebsite()->getId();
        }

        return $this->websiteId;
    }

    /**
     * @param $storeId
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function setStoreId($storeId)
    {
        $this->storeId = (int)$this->_storeManager->getStore($storeId)->getId();
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStoreId()
    {
        if ($this->storeId === null) {
            $this->storeId = (int)$this->_storeManager->getStore()->getId();
        }

        return $this->storeId;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setConditionName($name)
    {
        $this->conditionName = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getConditionName()
    {
        return $this->conditionName;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _prepareCollection()
    {
        /** @var $collection \TRIC\GLS\Model\ResourceModel\Carrier\Gls\Collection */
        $collection = $this->collectionFactory->create();
        $collection->setStoreFilter($this->getStoreId());
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Exception
     */
    public function _prepareColumns()
    {
        $this->addColumn(
            'dest_country',
            ['header' => __('Country'), 'index' => 'dest_country', 'default' => '*']
        );
        $this->addColumn(
            'dest_zip',
            ['header' => __('Zip/Postal Code'), 'index' => 'dest_zip', 'default' => '*']
        );
        $this->addColumn('price', ['header' => __('Shipping Price'), 'index' => 'price']);

        return parent::_prepareColumns();
    }
}
