<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Block;

use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;

class GoogleMaps extends Template
{
    /**
     * @return mixed
     */
    public function getGoogleMapsKey()
    {
        return $this->_scopeConfig->getValue(
            'gls/additional_settings/google_maps_key',
            ScopeInterface::SCOPE_STORES
        );
    }
}
