<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
declare(strict_types=1);

namespace TRIC\GLS\Helper;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\CsvFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;

class Data extends AbstractHelper
{
    const XML_PATH_ENABLED = 'gls/general/enabled';
    const XML_PATH_DEBUG = 'gls/general/debug';
    const XML_PATH_SEND_TRACKING_EMAIL = 'gls/additional_settings/send_tracking_email';
    const XML_PATH_GLS_ACCOUNT = 'gls/general/account';
    /**
     * @var \Psr\Log\LoggerInterface
     */
    public $logger;
    /**
     * @var ModuleListInterface
     */
    public $moduleList;
    /**
     * @var ManagerInterface
     */
    public $messageManager;
    /**
     * @var DirectoryList
     */
    public $directoryList;
    /**
     * @var CsvFactory
     */
    public $csvFactory;
    /**
     * @var File
     */
    public $ioFile;
    /**
     * @var StoreManagerInterface
     */
    public $storeManager;
    /**
     * @var Json
     */
    public $json;
    /**
     * @var null|int
     */
    public $storeId = null;

    public function __construct(
        Context $context,
        ModuleListInterface $moduleList,
        ManagerInterface $messageManager,
        DirectoryList $directoryList,
        CsvFactory $csvFactory,
        File $ioFile,
        StoreManagerInterface $storeManager,
        Json $json
    ) {
        $this->logger = $context->getLogger();
        $this->moduleList = $moduleList;
        $this->messageManager = $messageManager;
        $this->directoryList = $directoryList;
        $this->csvFactory = $csvFactory;
        $this->ioFile = $ioFile;
        $this->storeManager = $storeManager;
        $this->json = $json;
        parent::__construct($context);
    }

    /**
     * @param string $configPath
     * @param int    $storeId
     *
     * @return mixed
     */
    public function getConfig(string $configPath, $storeId = 0)
    {
        return $this->scopeConfig->getValue($configPath, ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Fetch current store id, defaults back to, if none can be retrieved
     * @return int
     */
    private function getStoreId(): int
    {
        if ($this->storeId === null) {
            try {
                $this->storeId = $this->storeManager->getStore()->getId();
            } catch (NoSuchEntityException $exception) {
                $this->logger->critical($exception->getMessage(), ['exception' => $exception]);
            }
        }

        return $this->storeId ?: 0;
    }

    /**
     * Is module enabled, returns true or false
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->getConfig(self::XML_PATH_ENABLED, $this->getStoreId()) === '1' ? true : false;
    }

    /**
     * Whenever or not to to send tracking emails from the module
     * @return bool
     */
    public function sendTrackingEmail(): bool
    {
        return $this->getConfig(self::XML_PATH_SEND_TRACKING_EMAIL, $this->getStoreId()) === '1' ? true : false;
    }

    /**
     * Returns the debug status
     * @return bool
     */
    public function getDebugStatus(): bool
    {
        return $this->getConfig(self::XML_PATH_DEBUG, $this->getStoreId()) === '1' ? true : false;
    }

    /**
     * @param string $message
     * @param bool   $useSeparator
     */
    public function log($message, $useSeparator = false): void
    {
        if ($this->getDebugStatus()) {
            if ($useSeparator) {
                $this->logger->addDebug(str_repeat('=', 100));
            }
            $this->logger->addDebug($message);
        }
    }

    /**
     * @param string|array $data
     *
     * @return array
     */
    public function formatAdditionalData($data): array
    {
        $formatted = [];
        if (!is_array($data)) {
            $data = $this->json->unserialize($data);
        }
        if (isset($data['pickup_id'])) {
            $formatted[] = $data['pickup_company'];
            $formatted[] = $data['pickup_street'];
            $formatted[] = $data['pickup_postcode'] . ' ' . $data['pickup_city'];
            $formatted[] = __('Will be picked up by:') . ' ' . $data['pickup_name'];
        }
        if (isset($data['notification'])) {
            $formatted[] = __('SMS notifications:') . ' ' . $data['notification'];
        }
        if (isset($data['comment'])) {
            $formatted[] = __('Placement for the package:') . ' ' . $data['comment'];
        }
        if ($format = 'html') {
            $formatted = implode('<br/>', $formatted);
        } elseif ($format = 'pdf') {
            $formatted = implode('|', $formatted);
        }

        return $formatted;
    }

    /**
     * @param      $order
     * @param bool $returnLabel
     * @param bool $filename
     */
    public function createCsvFileForOrder($order, $returnLabel = false, $filename = false): void
    {
        $orderId = $order->getRealOrderId();
        $storeId = $order->getStoreId();
        $weight = $order->getWeight() ?: 1;
        $shippingMethod = $order->getData('shipping_method');
        $shippingAddress = $order->getShippingAddress();
        $additionalData = $this->json->unserialize($order->getShippingGlsAdditionalData() ?? '{}');
        $company = $shippingAddress->getCompany() ?: '';
        $street = $shippingAddress->getStreet();
        $street1 = $street[0];
        $street2 = (count($street) > 1) ? $street[1] : '';
        $region = $shippingAddress->getRegion();
        $postcode = $shippingAddress->getPostcode();
        $city = $shippingAddress->getCity();
        $countryCode = $this->getGlsCountryCode($shippingAddress->getCountryId());
        $email = $order->getCustomerEmail();
        $telephone = trim($shippingAddress->getTelephone());
        $comment = '';
        $services = '';
        $name = $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();
        try {
            $csvData = [];
            $parcelType = 'A';
            $shipmentType = 'A';
            if (strpos($shippingMethod, 'gls_business') !== false) {
                $services = ltrim(str_replace('gls_business', '', $shippingMethod), '_');
                if ($countryCode != '8') {
                    $shipmentType = 'U';
                }
                if ($returnLabel) {
                    $shipmentType = 'B';
                    if ($returnLabel == 'email') {
                        $shipmentType = '2';
                    }
                }
                $comment = $additionalData['comment'] ?? '';
            } elseif (strpos($shippingMethod, 'gls_international') !== false) {
                $services = ltrim(str_replace('gls_international', '', $shippingMethod), '_');
                if ($countryCode != '8') {
                    $shipmentType = 'U';
                }
                if ($returnLabel) {
                    $shipmentType = 'B';
                    if ($returnLabel == 'email') {
                        $shipmentType = '2';
                    }
                }
                $comment = $additionalData['comment'] ?? '';
            } elseif (strpos($shippingMethod, 'gls_parcelshop') !== false) {
                $services = ltrim(str_replace('gls_parcelshop', '', $shippingMethod), '_');
                $shipmentType = 'Z';
                if ($returnLabel) {
                    $shipmentType = 'B';
                    if ($returnLabel == 'email') {
                        $shipmentType = '2';
                    }
                }
                $pickupId = $order->getShippingGlsPickupId() ?: '';
                $pickupName = $additionalData['pickup_name'] ?? $name;
                $telephone = $additionalData['notification'] ?? $telephone;
            } elseif (strpos($shippingMethod, 'gls_private') !== false) {
                $services = ltrim(str_replace('gls_private', '', $shippingMethod), '_');
                $parcelType = 'M';
                if ($returnLabel) {
                    $parcelType = 'A';
                    $shipmentType = 'B';
                    if ($returnLabel == 'email') {
                        $shipmentType = '2';
                    }
                }
                $telephone = $additionalData['notification'] ?? $telephone;
                $comment = $additionalData['comment'] ?? '';
            }
            $csvData[1] = $orderId;
            $csvData[2] = $company ?: $name;
            $csvData[3] = $street1 . ' ' . $street2;
            $csvData[4] = $region;
            $csvData[5] = $postcode;
            $csvData[6] = $city;
            $csvData[7] = $countryCode;
            $csvData[8] = date('d-m-y');
            $csvData[9] = str_replace('.', ',', $weight);
            $csvData[10] = 1;
            $csvData[11] = '';
            $csvData[12] = '';
            $csvData[13] = $parcelType;
            $csvData[14] = $shipmentType;
            $csvData[15] = $pickupName ?? '';
            $csvData[16] = $comment;
            $csvData[17] = $this->getConfig(self::XML_PATH_GLS_ACCOUNT, $storeId);
            $csvData[18] = $email;
            $csvData[19] = $telephone;
            $csvData[20] = strtoupper($services);
            $csvData[21] = '';
            $csvData[22] = $pickupId ?? '';
            foreach ($csvData as $key => $value) {
                $value = is_string($value) ? trim(utf8_decode($value)) : $value;
                $csvData[$key] = $value ?? '';
            }
            if (!$filename) {
                $filename = $orderId . '.csv';
            }
            $glsDirName = $this->directoryList->getPath('var') . '/GLS';
            $this->ioFile->checkAndCreateFolder($glsDirName);
            $csvFile = $this->csvFactory->create();
            $csvFile->saveData($glsDirName . '/' . $filename, [$csvData]);
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
            $this->messageManager->addError(__('An error has occurred when creating CSV file for ordrer %s', $orderId));
        }
    }

    private function getGlsCountryCode($countryCode): string
    {
        $globalCountryCodes = [
            'AL' => '70',
            'DZ' => '208',
            'AD' => '43',
            'AO' => '330',
            'AI' => '446',
            'AG' => '459',
            'AR' => '528',
            'AM' => '77',
            'AW' => '474',
            'AU' => '800',
            'AT' => '38',
            'AZ' => '78',
            'BS' => '453',
            'BH' => '640',
            'BD' => '666',
            'BB' => '469',
            'BY' => '73',
            'BE' => '2',
            'BZ' => '421',
            'BJ' => '284',
            'BM' => '413',
            'BT' => '675',
            'BO' => '516',
            'BA' => '93',
            'BW' => '391',
            'BR' => '508',
            'BN' => '703',
            'BG' => '100',
            'BF' => '236',
            'BI' => '328',
            'KH' => '696',
            'CM' => '302',
            'CA' => '404',
            'CV' => '247',
            'KY' => '463',
            'CF' => '306',
            'TD' => '244',
            'CL' => '512',
            'CN' => '720',
            'CO' => '480',
            'CG' => '318',
            'CD' => '322',
            'CI' => '272',
            'HR' => '92',
            'CY' => '196',
            'CZ' => '61',
            'DK' => '8',
            'DJ' => '338',
            'DM' => '460',
            'DO' => '456',
            'EC' => '500',
            'EG' => '220',
            'GQ' => '310',
            'ER' => '336',
            'EE' => '233',
            'ET' => '334',
            'FO' => '234',
            'FJ' => '815',
            'FI' => '32',
            'FR' => '1',
            'PF' => '822',
            'GA' => '314',
            'GM' => '252',
            'GE' => '76',
            'DE' => '4',
            'GH' => '276',
            'GI' => '292',
            'GR' => '9',
            'GL' => '304',
            'GD' => '473',
            'GT' => '416',
            'GN' => '260',
            'GW' => '257',
            'GY' => '488',
            'HT' => '452',
            'HN' => '424',
            'HK' => '740',
            'HU' => '64',
            'IS' => '352',
            'IN' => '664',
            'ID' => '700',
            'IR' => '616',
            'IQ' => '612',
            'IE' => '7',
            'IL' => '624',
            'IT' => '5',
            'JM' => '464',
            'JP' => '732',
            'JO' => '628',
            'KZ' => '79',
            'KE' => '346',
            'KW' => '636',
            'KG' => '83',
            'LA' => '684',
            'LV' => '428',
            'LB' => '604',
            'LS' => '395',
            'LR' => '268',
            'LY' => '216',
            'LT' => '440',
            'LU' => '19',
            'MO' => '743',
            'MK' => '96',
            'MG' => '370',
            'MW' => '386',
            'MY' => '701',
            'MV' => '667',
            'ML' => '232',
            'MT' => '46',
            'MA' => '204',
            'MH' => '824',
            'MR' => '228',
            'MU' => '373',
            'MX' => '412',
            'FM' => '823',
            'MD' => '74',
            'MC' => '492',
            'MN' => '716',
            'MS' => '470',
            'MZ' => '366',
            'MM' => '676',
            'NA' => '389',
            'NP' => '672',
            'AN' => '478',
            'NL' => '3',
            'NC' => '809',
            'NZ' => '804',
            'NI' => '432',
            'NE' => '240',
            'NG' => '288',
            'NO' => '28',
            'MP' => '820',
            'OM' => '649',
            'PK' => '662',
            'PW' => '825',
            'PA' => '442',
            'PG' => '801',
            'PY' => '520',
            'PH' => '708',
            'PL' => '60',
            'PT' => '10',
            'QA' => '644',
            'RE' => '638',
            'RO' => '66',
            'RU' => '75',
            'RW' => '324',
            'SM' => '674',
            'SA' => '632',
            'SN' => '248',
            'CS' => '98',
            'SC' => '355',
            'SL' => '264',
            'SG' => '706',
            'SK' => '63',
            'SI' => '91',
            'SO' => '342',
            'ZA' => '388',
            'ES' => '11',
            'LK' => '669',
            'KN' => '449',
            'LC' => '465',
            'VC' => '467',
            'SD' => '224',
            'SZ' => '393',
            'SE' => '30',
            'CH' => '39',
            'SY' => '608',
            'TW' => '736',
            'TJ' => '82',
            'TH' => '680',
            'TG' => '280',
            'TT' => '472',
            'TN' => '212',
            'TR' => '52',
            'TM' => '80',
            'UG' => '350',
            'UA' => '72',
            'AE' => '647',
            'GB' => '6',
            'US' => '400',
            'UY' => '524',
            'UZ' => '81',
            'VU' => '816',
            'VA' => '45',
            'VE' => '484',
            'VN' => '690',
            'VG' => '468',
            'VI' => '457',
            'WF' => '811',
            'YE' => '653',
            'ZM' => '378',
            'ZW' => '382'
        ];

        return $globalCountryCodes[$countryCode];
    }
}
