<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Model\Rates\Source;

use Magento\Framework\Data\OptionSourceInterface;
use TRIC\GLS\Model\Rates;

class IsActive implements OptionSourceInterface
{
    /**
     * @var Rates
     */
    public $glsRates;

    public function __construct(
        Rates $glsRates
    ) {
        $this->glsRates = $glsRates;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->glsRates->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        return $options;
    }
}
