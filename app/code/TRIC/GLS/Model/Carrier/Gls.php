<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Model\Carrier;

use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use TRIC\GLS\Model\ResourceModel\Carrier\GlsFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Address\RateRequest;

class Gls extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    public $_code = 'gls'; // @codingStandardsIgnoreLine
    /**
     * @var bool
     */
    public $_isFixed = true; // @codingStandardsIgnoreLine
    /**
     * @var array
     */
    public $conditions = ['package_value', 'package_weight', 'package_qty'];
    /**
     * @var array
     */
    public $conditionNames = [];
    /**
     * @var ResultFactory
     */
    public $rateResultFactory;
    /**
     * @var MethodFactory
     */
    public $resultMethodFactory;
    /**
     * @var GlsFactory
     */
    public $glsFactory;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $resultMethodFactory,
        GlsFactory $glsFactory,
        array $data = []
    ) {
        $this->_logger = $logger;
        $this->rateResultFactory = $rateResultFactory;
        $this->resultMethodFactory = $resultMethodFactory;
        $this->glsFactory = $glsFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        foreach ($this->getCode('condition_name') as $k => $v) {
            $this->conditionNames[] = $k;
        }
    }

    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual()) {
                            $request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual()) {
                    $request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
                }
            }
        }
        $freeQty = 0;
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
            }
            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }
        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();
        $request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);
        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();
        $rates = $this->getRate($request);
        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);
        if (!empty($rates)) {
            foreach ($rates as $rate) {
                if (!empty($rate) && $rate['price'] >= 0) {
                    /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                    $method = $this->resultMethodFactory->create();
                    $method->setCarrier('gls');
                    $method->setCarrierTitle($this->getConfigData('title'));
                    if ($rate['services']) {
                        $rate['method'] .= '_' . strtolower(str_replace([',', ' '], '', $rate['services']));
                    }
                    $method->setMethod($rate['method']);
                    $method->setMethodTitle($rate['title']);
                    if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
                        $shippingPrice = 0;
                    } else {
                        $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                    }
                    $method->setPrice($shippingPrice);
                    $method->setCost($rate['cost']);
                    $result->append($method);
                }
            }
        } else {
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
            $error = $this->_rateErrorFactory->create(
                [
                    'data' => [
                        'carrier' => $this->_code,
                        'carrier_title' => $this->getConfigData('title'),
                        'error_message' => $this->getConfigData('specificerrmsg'),
                    ],
                ]
            );
            $result->append($error);
        }

        return $result;
    }

    /**
     * @param RateRequest $request
     *
     * @return mixed
     */
    public function getRate(RateRequest $request)
    {
        return $this->glsFactory->create()->getRate($request);
    }

    /**
     * @param        $type
     * @param string $code
     *
     * @return mixed
     * @throws LocalizedException
     */
    public function getCode($type, $code = '')
    {
        $codes = [
            'condition_name' => [
                'package_weight' => __('Weight'),
                'package_value' => __('Price'),
                'package_qty' => __('# of Items'),
            ],
            'condition_name_short' => [
                'package_weight' => __('Weight (and above)'),
                'package_value' => __('Order Subtotal (and above)'),
                'package_qty' => __('# of Items (and above)'),
            ],
        ];
        if (!isset($codes[$type])) {
            throw new LocalizedException(__('Please correct Gls Rate code type: %1.', $type));
        }
        if ('' === $code) {
            return $codes[$type];
        }
        if (!isset($codes[$type][$code])) {
            throw new LocalizedException(__('Please correct Gls Rate code for type %1: %2.', $type, $code));
        }

        return $codes[$type][$code];
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [
            'parcelshop' => __('ParcelShop'),
            'private' => __('Private'),
            'business' => __('Business'),
            'international' => __('International')
        ];
    }

    /**
     * @return array
     */
    public function getConditions()
    {
        return $this->conditions;
    }
}
