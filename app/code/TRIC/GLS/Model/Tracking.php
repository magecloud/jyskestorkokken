<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
declare(strict_types=1);

namespace TRIC\GLS\Model;

use Exception;
use Magento\Sales\Model\Order\Shipment\Track;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Sales\Model\ShipOrder;
use Magento\Sales\Api\Data\ShipmentCommentCreationInterface;
use Magento\Sales\Model\Convert\Order as OrderConvert;
use Psr\Log\LoggerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory as ShipmentCollection;
use TRIC\GLS\Helper\Data as GLSHelper;

class Tracking
{
    const STATUS_FOLDER = "GLS-STATUS";
    /**
     * @var File
     */
    private $filesystem;
    /**
     * @var DirectoryList
     */
    private $directoryList;
    /**
     * @var OrderInterface
     */
    private $orderInterface;
    /**
     * @var TrackFactory
     */
    private $trackFactory;
    /**
     * @var ShipOrder
     */
    private $shipOrder;
    /**
     * @var ShipmentCommentCreationInterface
     */
    private $shipmentCommentCreation;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var OrderConvert
     */
    private $orderConvert;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var ShipmentCollection
     */
    private $shipmentCollection;
    /**
     * @var GLSHelper
     */
    private $glsHelper;

    public function __construct(
        File $filesystem,
        DirectoryList $directoryList,
        OrderInterface $orderInterface,
        TrackFactory $trackFactory,
        ShipOrder $shipOrder,
        ShipmentCommentCreationInterface $shipmentCommentCreation,
        OrderConvert $orderConvert,
        LoggerInterface $logger,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderRepositoryInterface $orderRepository,
        ShipmentCollection $shipmentCollection,
        GLSHelper $glsHelper
    ) {
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
        $this->orderInterface = $orderInterface;
        $this->trackFactory = $trackFactory;
        $this->shipOrder = $shipOrder;
        $this->shipmentCommentCreation = $shipmentCommentCreation;
        $this->orderConvert = $orderConvert;
        $this->logger = $logger;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderRepository = $orderRepository;
        $this->shipmentCollection = $shipmentCollection;
        $this->glsHelper = $glsHelper;
    }

    /**
     * Cronjob to import tracking on GLS orders
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function importTracking()
    {
        if (!$this->glsHelper->isEnabled()) {
            exit(1);
        }
        $statusFolder = $this->directoryList->getPath(DirectoryList::VAR_DIR) . "/" . self::STATUS_FOLDER;
        $this->filesystem->checkAndCreateFolder($statusFolder);
        $this->filesystem->open(['path' => $statusFolder]);
        $files = $this->filesystem->ls(File::GREP_FILES);
        if (!empty($files)) {
            foreach ($files as $fileinfo) {
                if ($fileinfo['filetype'] === 'txt') {
                    $filename = $fileinfo['text'];
                    $data = explode(" ", $this->filesystem->read($filename));
                    $incrementId = null;
                    $trackAndTrace = trim($data[0]);
                    $dataCount = count($data);
                    if (isset($data[$dataCount - 1])) {
                        $incrementId = trim($data[$dataCount - 1]);
                    }
                    $trackAndTrace = !is_array($trackAndTrace) ? [$trackAndTrace] : $trackAndTrace;
                    if ($incrementId && !empty($trackAndTrace)) {
                        $searchCriteria = $this->searchCriteriaBuilder
                            ->addFilter('increment_id', $incrementId, 'eq')->create();
                        $order = $this->orderRepository->getList($searchCriteria)->getFirstItem();
                        if ($order && $orderId = $order->getId()) {
                            $trackingTitle = $order->getShippingDescription();
                            if ($order->canShip()) {
                                try {
                                    $tracks = [];
                                    foreach ($trackAndTrace as $trackingNumber) {
                                        $tracks[] = $this->setTrackingData($trackingNumber, $trackingTitle);
                                    }
                                    $this->shipOrder->execute(
                                        $orderId,
                                        $this->createShipmentItems($order),
                                        $this->glsHelper->sendTrackingEmail(), //notify
                                        false, //append comment
                                        $this->setShipmentComment(''),
                                        $tracks
                                    );
                                } catch (Exception $exception) {
                                    $this->logger->critical($exception->getMessage(), ['exception' => $exception]);
                                }
                            } else {
                                $existingTrackNumbers = [];
                                $tracksCollection = $order->getTracksCollection();
                                foreach ($tracksCollection->getItems() as $track) {
                                    $existingTrackNumbers[] = $track->getTrackNumber();
                                }
                                foreach ($trackAndTrace as $trackingNumber) {
                                    if (!in_array($trackingNumber, $existingTrackNumbers)) {
                                        try {
                                            $shipments = $this->shipmentCollection->create()
                                                ->addFieldToFilter('order_id', $orderId)
                                                ->setOrder('entity_id', 'DESC')
                                                ->setPageSize(1)
                                                ->setCurPage(1);
                                            if (!empty($shipments)) {
                                                foreach ($shipments as $shipment) {
                                                    $shipment->addTrack($this->setTrackingData(
                                                        $trackingNumber,
                                                        $trackingTitle
                                                    ));
                                                    if ($this->glsHelper->sendTrackingEmail()) {
                                                        $shipment->setSendEmail(1);
                                                    }
                                                    $shipment->save();
                                                }
                                            }
                                        } catch (Exception $exception) {
                                            $this->logger->critical(
                                                $exception->getMessage(),
                                                ['exception' => $exception]
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $this->filesystem->rm($filename);
                }
            }
        }
    }

    /**
     * @param string $trackingNumber
     * @param string $trackingTitle
     *
     * @return Track
     */
    private function setTrackingData(string $trackingNumber, string $trackingTitle): Track
    {
        $track = $this->trackFactory->create();
        $track->setTrackNumber($trackingNumber);
        $track->setCarrierCode('gls');
        $track->setTitle($trackingTitle);

        return $track;
    }

    /**
     * @param       $order
     * @param array $items
     *
     * @return array
     * @throws LocalizedException
     */
    private function createShipmentItems($order, array $items = []): array
    {
        $shipmentItem = [];
        foreach ($order->getAllItems() as $orderItem) {
            if (empty($items) || array_key_exists($orderItem->getId(), $items)) {
                $qty = empty($items) ? $orderItem->getQtyOrdered() - $orderItem->getQtyShipped() :
                    $items[$orderItem->getId()];
                $shipmentItem[] = $this->orderConvert->itemToShipmentItem($orderItem)->setQty($qty);
            }
        }

        return $shipmentItem;
    }

    /**
     * @param $comment
     *
     * @return ShipmentCommentCreationInterface|null
     */
    private function setShipmentComment(string $comment): ?ShipmentCommentCreationInterface
    {
        if ($comment) {
            return $this->shipmentCommentCreation->setComment($comment);
        } else {
            return null;
        }
    }
}
