<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Model\ResourceModel\Rates;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    public $_idFieldName = 'entity_id'; // @codingStandardsIgnoreLine

    public function _construct()
    {
        $this->_init('TRIC\GLS\Model\Rates', 'TRIC\GLS\Model\ResourceModel\Rates');
    }
}
