<?php
/**
 * Magento Extension by TRIC Solutions
 * @copyright  Copyright (c) 2017 TRIC Solutions (https://www.tric.dk)
 * @license    https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       https://store.tric.dk
 */

namespace TRIC\GLS\Model\ResourceModel\Carrier\Gls;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public $countryTable;
    public $regionTable;

    public function _construct()
    {
        $this->_init(
            'TRIC\GLS\Model\Carrier\Gls',
            'TRIC\GLS\Model\ResourceModel\Carrier\Gls'
        );
        $this->countryTable = $this->getTable('directory_country');
        $this->regionTable = $this->getTable('directory_country_region');
    }

    /**
     * @return AbstractCollection|void
     */
    public function _initSelect()
    {
        parent::_initSelect();
        $this->_select->joinLeft(
            ['country_table' => $this->countryTable],
            'country_table.country_id = main_table.dest_country_id',
            ['dest_country' => 'iso3_code']
        )->joinLeft(
            ['region_table' => $this->regionTable],
            'region_table.region_id = main_table.dest_region_id',
            ['dest_region' => 'code']
        );
    }

    /**
     * @param $storeId
     *
     * @return Collection
     */
    public function setStoreFilter($storeId)
    {
        return $this->addFieldToFilter('store_id', $storeId);
    }

    /**
     * @param $conditionName
     *
     * @return Collection
     */
    public function setConditionFilter($conditionName)
    {
        return $this->addFieldToFilter('condition_name', $conditionName);
    }

    /**
     * @param $countryId
     *
     * @return Collection
     */
    public function setCountryFilter($countryId)
    {
        return $this->addFieldToFilter('dest_country_id', $countryId);
    }
}
