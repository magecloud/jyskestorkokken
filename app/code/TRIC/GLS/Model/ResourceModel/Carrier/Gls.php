<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Model\ResourceModel\Carrier;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use TRIC\GLS\Model\Carrier\Gls as CarrierGls;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryCollection;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionCollection;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Framework\DataObject;
use Magento\Framework\Filesystem;
use Magento\Framework\Exception\LocalizedException;

class Gls extends AbstractDb
{
    /**
     * Import Gls rates store ID
     * @var int
     */
    public $importStoreId = 0;
    /**
     * Errors in import process
     * @var array
     */
    public $importErrors = [];
    /**
     * Count of imported Gls rates
     * @var int
     */
    public $importedRows = 0;
    /**
     * Array of unique gls rate keys to protect from duplicates
     * @var array
     */
    public $importUniqueHash = [];
    /**
     * Array of countries keyed by iso2 code
     * @var array
     */
    public $importIso2Countries;
    /**
     * Array of countries keyed by iso3 code
     * @var array
     */
    public $importIso3Countries;
    /**
     * Associative array of countries and regions
     * [country_id][region_code] = region_id
     * @var array
     */
    public $importRegions;
    /**
     * Import Gls Rate condition name
     * @var string
     */
    public $importConditionName;
    /**
     * Array of condition full names
     * @var array
     */
    public $conditionFullNames = [];
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $coreConfig;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    public $logger;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;
    /**
     * @var \TRIC\GLS\Model\Carrier\Gls
     */
    public $carrierGls;
    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    public $countryCollectionFactory;
    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    public $regionCollectionFactory;
    /**
     * Filesystem instance
     * @var \Magento\Framework\Filesystem
     */
    public $filesystem;

    public function __construct(
        Context $context,
        LoggerInterface $logger,
        ScopeConfigInterface $coreConfig,
        StoreManagerInterface $storeManager,
        CarrierGls $carrierGls,
        CountryCollection $countryCollectionFactory,
        RegionCollection $regionCollectionFactory,
        Filesystem $filesystem,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->coreConfig = $coreConfig;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->carrierGls = $carrierGls;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->filesystem = $filesystem;
    }

    /**
     * Define main Gls and id field name
     * @return void
     */
    public function _construct()
    {
        $this->_init('tric_gls_rates', 'entity_id');
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return array
     * @throws LocalizedException
     */
    public function getRate(RateRequest $request)
    {
        $connection = $this->getConnection();
        $bind = [
            ':store_id' => (int)$request->getStoreId(),
            ':country_id' => $request->getDestCountryId(),
            ':region_id' => $request->getDestRegionId(),
            ':postcode' => $request->getDestPostcode(),
        ];
        $select = $connection->select()->from(
            $this->getMainTable()
        )->where(
            '(store_id = 0 OR FIND_IN_SET(:store_id,store_id))'
        )->where(
            'is_active = 1'
        )->order(
            ['sort_order ASC', 'dest_country_id DESC', 'dest_region_id DESC', 'dest_zip DESC']
        );
        $orWhere = '(' . implode(') OR (', [
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = :region_id AND dest_zip = :postcode",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = :region_id AND dest_zip = ''",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = :region_id AND dest_zip = '*'",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = '' AND dest_zip = :postcode",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = '*' AND dest_zip = :postcode",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = '' AND dest_zip = ''",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = '*' AND dest_zip = ''",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = '' AND dest_zip = '*'",
                "FIND_IN_SET(:country_id,dest_country_id) AND dest_region_id = '*' AND dest_zip = '*'",
            ]) . ')';
        $select->where($orWhere);
        $orWhere = [];
        $i = 0;
        foreach ($this->carrierGls->getConditions() as $conditionName) {
            $nameKey = sprintf(':condition_name_%d', $i);
            $valueKey = sprintf(':condition_value_%d', $i);
            $orWhere[] =
                "(condition_name = {$nameKey} AND condition_from <= {$valueKey} AND condition_to > {$valueKey})";
            $bind[$nameKey] = $conditionName;
            $bind[$valueKey] = abs($request->getData($conditionName));
            $i++;
        }
        if ($orWhere) {
            $select->where(implode(' OR ', $orWhere));
        }
        $results = $connection->fetchAll($select, $bind); // @codingStandardsIgnoreLine
        if ($results) {
            foreach ($results as $key => $result) {
                if ($result['dest_zip'] == '*') {
                    $results[$key]['dest_zip'] = '';
                }
            }
        }

        return $results;
    }

    /**
     * @return $this
     * @throws LocalizedException
     */
    public function uploadAndImport()
    {
        if (empty($_FILES['groups']['tmp_name']['gls']['fields']['import']['value'])) { // @codingStandardsIgnoreLine
            return $this;
        }
        $columns = [
            'entity_id',
            'store_id',
            'title',
            'method',
            'dest_country_id',
            'dest_region_id',
            'dest_zip',
            'condition_name',
            'condition_from',
            'condition_to',
            'price',
            'cost',
            'sort_order',
            'is_active',
        ];
        $csvFile = $_FILES['groups']['tmp_name']['gls']['fields']['import']['value']; // @codingStandardsIgnoreLine
        $this->importUniqueHash = [];
        $this->importErrors = [];
        $this->importedRows = 0;
        $this->importStoreId = 0;
        $tmpDirectory = $this->filesystem->getDirectoryRead(Filesystem\DirectoryList::SYS_TMP);
        $path = $tmpDirectory->getRelativePath($csvFile);
        $stream = $tmpDirectory->openFile($path);
        $headers = $stream->readCsv();
        if ($headers === false || count($headers) < 11) {
            $stream->close();
            throw new LocalizedException(__('Please correct Gls Rates File Format.'));
        }
        $connection = $this->getConnection();
        $connection->beginTransaction();
        $this->_loadDirectoryCountries();
        $this->_loadDirectoryRegions();
        try {
            $rowNumber = 1;
            $importData = [];
            while (false !== ($csvLine = $stream->readCsv())) {
                $rowNumber++;
                if (empty($csvLine)) {
                    continue;
                }
                $row = $this->_getImportRow($csvLine, $rowNumber);
                if ($row !== false) {
                    $importData[] = $row;
                }
                $this->getConnection()->insertOnDuplicate($this->getMainTable(), array_combine($columns, $row));
            }
            $stream->close();
        } catch (LocalizedException $e) {
            $connection->rollback();
            $stream->close();
            throw new LocalizedException(__($e->getMessage()));
        } catch (\Exception $e) {
            $connection->rollback();
            $stream->close();
            throw new LocalizedException(__('Something went wrong while importing gls rates.'));
        }
        $connection->commit();
        if ($this->importErrors) {
            $error = __(
                'We couldn\'t import this file because of these errors: %1',
                implode(" \n", $this->importErrors)
            );
            throw new LocalizedException($error);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function _loadDirectoryCountries()
    {
        if ($this->importIso2Countries !== null && $this->importIso3Countries !== null) {
            return $this;
        }
        $this->importIso2Countries = [];
        $this->importIso3Countries = [];
        /** @var $collection \Magento\Directory\Model\ResourceModel\Country\Collection */
        $collection = $this->countryCollectionFactory->create();
        foreach ($collection->getData() as $row) {
            $this->importIso2Countries[$row['iso2_code']] = $row['country_id'];
            $this->importIso3Countries[$row['iso3_code']] = $row['country_id'];
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function _loadDirectoryRegions()
    {
        if ($this->importRegions !== null) {
            return $this;
        }
        $this->importRegions = [];
        /** @var $collection \Magento\Directory\Model\ResourceModel\Region\Collection */
        $collection = $this->regionCollectionFactory->create();
        foreach ($collection->getData() as $row) {
            $this->importRegions[$row['country_id']][$row['code']] = (int)$row['region_id'];
        }

        return $this;
    }

    /**
     * @param $conditionName
     *
     * @return mixed
     * @throws LocalizedException
     */
    public function _getConditionFullName($conditionName)
    {
        if (!isset($this->conditionFullNames[$conditionName])) {
            $name = $this->carrierGls->getCode('condition_name_short', $conditionName);
            $this->conditionFullNames[$conditionName] = $name;
        }

        return $this->conditionFullNames[$conditionName];
    }

    /**
     * @param     $row
     * @param int $rowNumber
     *
     * @return array|bool
     * @throws LocalizedException
     */
    public function _getImportRow($row, $rowNumber = 0)
    {
        foreach ($row as $key => $value) {
            $row[$key] = trim($value);
        }
        if (isset($this->importIso2Countries[$row[4]])) {
            $row[4] = $this->importIso2Countries[$row[4]];
        } elseif (isset($this->importIso3Countries[$row[4]])) {
            $row[4] = $this->importIso3Countries[$row[4]];
        } elseif ($row[4] == '*' || $row[4] == '') {
            $row[4] = '0';
        } else {
            $this->importErrors[] = __('Please correct Country "%1" in the Row #%2.', $row[4], $rowNumber);

            return false;
        }
        if ($row[4] != '0' && isset($this->importRegions[$row[4]][$row[5]])) {
            $row[5] = $this->importRegions[$row[4]][$row[5]];
        } elseif ($row[5] == '*' || $row[5] = '' || !isset($this->importRegions[$row[4]][$row[5]])) {
            $row[5] = '';
        } else {
            $this->importErrors[] = __('Please correct Region/State "%1" in the Row #%2.', $row[5], $rowNumber);

            return false;
        }
        $conditionName = $this->_getConditionFullName($row[7]);
        if ($conditionName === false) {
            $this->importErrors[] = __(
                'Please correct %1 "%2" in the Row #%3.',
                $this->_getConditionFullName($row[7]),
                $row[7],
                $rowNumber
            );

            return false;
        }
        $row[8] = $this->_parseDecimalValue($row[8]);
        if ($row[8] === false) {
            $this->importErrors[] = __('Please correct Condition From "%1" in the Row #%2.', $row[8], $rowNumber);

            return false;
        }
        $row[9] = $this->_parseDecimalValue($row[9]);
        if ($row[9] === false) {
            $this->importErrors[] = __('Please correct Condition To "%1" in the Row #%2.', $row[9], $rowNumber);

            return false;
        }
        $row[10] = $this->_parseDecimalValue($row[10]);
        if ($row[10] === false) {
            $this->importErrors[] = __('Please correct Price "%1" in the Row #%2.', $row[10], $rowNumber);

            return false;
        }
        $row[11] = $this->_parseDecimalValue($row[11]);
        if ($row[11] === false) {
            $this->importErrors[] = __('Please correct Cost "%1" in the Row #%2.', $row[11], $rowNumber);

            return false;
        }

        return $row;
    }

    /**
     * @param $value
     *
     * @return bool|float
     */
    public function _parseDecimalValue($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
        $value = (double)sprintf('%.4F', $value);
        if ($value < 0.0000) {
            return false;
        }

        return $value;
    }
}
