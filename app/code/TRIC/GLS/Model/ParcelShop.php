<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Model;

use Magento\Framework\App\CacheInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;

class ParcelShop
{
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var Curl
     */
    public $curl;
    /**
     * @var Json
     */
    private $json;

    public function __construct(
        CacheInterface $cache,
        Curl $curl,
        Json $json
    ) {
        $this->cache = $cache;
        $this->curl = $curl;
        $this->json = $json;
    }

    /**
     * @param $postcode
     * @param $countryCode
     *
     * @return bool|false|string
     */
    public function getRemoteDroppointResponse($postcode, $countryCode)
    {
        $cacheKey = 'gls_parcelshop_' . $countryCode . '_' . $postcode;
        if ($data = $this->getResultFromCache($cacheKey)) {
            return $data;
        }
        $url = 'https://services.tric.dk/gls/api.php?postalCode=' . $postcode . '&countryCode=' . $countryCode;
        $curl = $this->curl;
        $curl->get($url);
        $data = utf8_encode($curl->getBody());
        $data = $this->handleResponse($data);
        if ($data) {
            $this->storeResultInCache($data, $cacheKey);
        }

        return $data;
    }

    /**
     * @param $data
     *
     * @return bool|false|string
     */
    private function handleResponse($data)
    {
        $parcelShops = [];
        if ($data = $this->json->unserialize($data)) {
            if (isset($data['servicePointInformationResponse']['servicePoints'])) {
                foreach ($data['servicePointInformationResponse']['servicePoints'] as $servicePoint) {
                    $parcelShop = [
                        'id' => $servicePoint['servicePointId'],
                        'company' => $servicePoint['name'],
                        'street' => $servicePoint['visitingAddress']['streetName'],
                        'postcode' => $servicePoint['visitingAddress']['postalCode'],
                        'city' => $servicePoint['visitingAddress']['city']
                    ];
                    if (isset($servicePoint['coordinate']) && isset($servicePoint['coordinate']['northing'])
                        && $servicePoint['coordinate']['northing']) {
                        $parcelShop['coordinates'] = [
                            $servicePoint['coordinate']['northing'],
                            $servicePoint['coordinate']['easting']
                        ];
                    }
                    if (isset($servicePoint['openingHours']) && $servicePoint['openingHours']) {
                        $parcelShop['opening_hours'] = $servicePoint['openingHours'];
                    }
                    $parcelShop['label'] = implode(', ', [
                        $parcelShop['company'],
                        $parcelShop['street'],
                        $parcelShop['postcode'] . ' ' . $parcelShop['city']
                    ]);
                    $parcelShops[] = $parcelShop;
                }
            }
        }

        return $this->json->serialize($data);
    }

    /**
     * @param string $data
     * @param string $cacheKey
     * @param array  $tags
     * @param int    $lifeTime
     *
     * @return bool
     */
    private function storeResultInCache(string $data, string $cacheKey, $tags = ['block_html'], $lifeTime = 86400): bool
    {
        return $this->cache->save($data, $cacheKey, $tags, $lifeTime);
    }

    /**
     * @param string $cacheKey
     *
     * @return string
     */
    private function getResultFromCache(string $cacheKey): string
    {
        return $this->cache->load($cacheKey);
    }
}
