<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Rates extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'tric_gls_rates';
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**
     * @var string
     */
    public $_cacheTag = 'tric_gls_rates'; // @codingStandardsIgnoreLine
    /**
     * @var string
     */
    public $_eventPrefix = 'tric_gls_rates'; // @codingStandardsIgnoreLine

    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('TRIC\GLS\Model\ResourceModel\Rates');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param array $data
     *
     * @return $this
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function saveCollection(array $data)
    {
        if (isset($data[$this->getId()])) {
            $this->addData($data[$this->getId()]);
            $this->getResource()->save($this);
        }

        return $this;
    }

    public function getAvailableStatuses()
    {
        return [
            self::STATUS_ENABLED => __('Enabled'),
            self::STATUS_DISABLED => __('Disabled')
        ];
    }
}
