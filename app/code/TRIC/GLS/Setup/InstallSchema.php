<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) //@codingStandardsIgnoreLine
    {
        $setup->startSetup();
        $table = $setup->getConnection()->newTable(
            $setup->getTable('tric_gls_rates')
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'store_id',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => false, 'default' => 0],
            'Store ID'
        )->addColumn(
            'dest_country_id',
            Table::TYPE_TEXT,
            128,
            ['nullable' => false],
            'Country ID'
        )->addColumn(
            'dest_region_id',
            Table::TYPE_TEXT,
            128,
            ['nullable' => false, 'default' => ''],
            'Region ID'
        )->addColumn(
            'dest_zip',
            Table::TYPE_TEXT,
            20,
            ['nullable' => false, 'default' => ''],
            'Post Code (Zip)'
        )->addColumn(
            'condition_name',
            Table::TYPE_TEXT,
            20,
            ['nullable' => false],
            'Condition Name'
        )->addColumn(
            'condition_from',
            Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => false],
            'Condition From Value'
        )->addColumn(
            'condition_to',
            Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => false],
            'Condition To Value'
        )->addColumn(
            'method',
            Table::TYPE_TEXT,
            20,
            ['nullable' => false],
            'Method'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            128,
            ['nullable' => false],
            'Title'
        )->addColumn(
            'services',
            Table::TYPE_TEXT,
            128,
            ['nullable' => false, 'default' => ''],
            'Services'
        )->addColumn(
            'cost',
            Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => false, 'default' => 0],
            'Cost Price'
        )->addColumn(
            'price',
            Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => false, 'default' => 0],
            'Price'
        )->addColumn(
            'sort_order',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => false, 'default' => 0],
            'Sort Order'
        )->addColumn(
            'is_active',
            Table::TYPE_INTEGER,
            1,
            ['nullable' => false, 'default' => 1],
            'Active Status'
        )
            ->setComment(
                'GLS Shipping Rates Table'
            );
        $setup->getConnection()->createTable($table);
        foreach (['sales_order', 'quote'] as $tableAlias) {
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($tableAlias),
                    'shipping_gls_pickup_id',
                    [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => 32,
                        'default' => null,
                        'comment' => 'GLS Pickup ID'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($tableAlias),
                    'shipping_gls_additional_data',
                    [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'comment' => 'GLS Additional Data'
                    ]
                );
        }
        $setup->endSetup();
    }
}
