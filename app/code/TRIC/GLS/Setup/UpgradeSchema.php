<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $setup->getConnection()->modifyColumn(
                $setup->getTable('tric_gls_rates'),
                'store_id',
                ['type' => Table::TYPE_TEXT, 'length' => 50, 'nullable' => false]
            );
        }
        if (version_compare($context->getVersion(), '1.2.2', '<')) {
            $setup->getConnection()->modifyColumn(
                $setup->getTable('tric_gls_rates'),
                'dest_country_id',
                ['type' => Table::TYPE_TEXT, 'length' => Table::DEFAULT_TEXT_SIZE, 'nullable' => false]
            );
        }
        $setup->endSetup();
    }
}
