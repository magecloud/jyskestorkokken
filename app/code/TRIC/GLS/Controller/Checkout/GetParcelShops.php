<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use TRIC\GLS\Model\ParcelShop;

class GetParcelShops extends Action
{
    /**
     * @var ParcelShop
     */
    public $parcelShop;

    public function __construct(
        Context $context,
        ParcelShop $parcelShop
    ) {
        parent::__construct($context);
        $this->parcelShop = $parcelShop;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $request = $this->getRequest();
        if ($request->isAjax()) {
            $postcode = $request->getParam('postcode', false);
            if ($countryCode = $request->getParam('country', false)) {
                return $result->setJsonData(
                    $this->parcelShop->getRemoteDroppointResponse($postcode, $countryCode)
                );
            }
        }

        return $result->setJsonData('false');
    }
}
