<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Checkout;

use Magento\Framework\App\Action;
use Magento\Checkout\Model\Session;

class SaveData extends Action\Action
{
    /**
     * @var Session
     */
    private $checkoutSession;

    public function __construct(
        Action\Context $context,
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $quote = $this->checkoutSession->getQuote();
        $isActive = $this->getRequest()->getParam('is_active');
        $shippingGlsPickupId = $this->getRequest()->getParam('shipping_gls_pickup_id');
        $shippingGlsAdditionalData = $this->getRequest()->getParam('shipping_gls_additional_data');
        if ($isActive) {
            $quote->setShippingGlsPickupId($shippingGlsPickupId)
                ->setShippingGlsAdditionalData($shippingGlsAdditionalData);
        } else {
            $quote->setShippingGlsPickupId(null)
                ->setShippingGlsAdditionalData(null);
        }
        $quote->save();
    }
}
