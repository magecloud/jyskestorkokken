<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Registry;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class GetAdditionalData extends Action
{
    /**
     * @var Session
     */
    public $backendAuthSession;
    /**
     * @var Registry
     */
    public $registry;
    /**
     * @var ManagerInterface
     */
    public $eventManager;
    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    public function __construct(
        Session $backendAuthSession,
        Registry $registry,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager
    ) {
        $this->backendAuthSession = $backendAuthSession;
        $this->registry = $registry;
        $this->eventManager = $eventManager;
        $this->storeManager = $storeManager;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|string
     */
    public function execute()
    {
        return '';
    }
}
