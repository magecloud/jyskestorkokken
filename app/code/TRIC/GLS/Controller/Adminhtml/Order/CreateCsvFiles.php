<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Adminhtml\Order;

use Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\OrderManagementInterface;
use TRIC\GLS\Helper\Data;

class CreateCsvFiles extends AbstractMassAction
{
    /**
     * @var OrderManagementInterface
     */
    public $orderManagement;
    /**
     * @var Data
     */
    public $tricHelper;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OrderManagementInterface $orderManagement,
        Data $data
    ) {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->orderManagement = $orderManagement;
        $this->tricHelper = $data;
    }

    /**
     * Create GLS CSV files for selected orders
     *
     * @param AbstractCollection $collection
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection) // @codingStandardsIgnoreLine
    {
        $countCsvFilesCreated = 0;
        foreach ($collection->getItems() as $order) {
            if (!$order->getEntityId()) {
                continue;
            }
            $this->tricHelper->createCsvFileForOrder($order);
            $countCsvFilesCreated++;
        }
        if ($countCsvFilesCreated) {
            $this->messageManager->addSuccess(__(
                'You have created GLS CSV files for %1 order(s). Find them in the folder "var/GLS".',
                $countCsvFilesCreated
            ));
        } else {
            $this->messageManager->addError(__('No GLS CSV files were created'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->getComponentRefererUrl());

        return $resultRedirect;
    }
}
