<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Adminhtml\Rates;

use Magento\Backend\App\Action;
use TRIC\GLS\Model\RatesFactory;

class Delete extends Action
{
    /**
     * @var RatesFactory
     */
    public $ratesFactory;

    public function __construct(
        Action\Context $context,
        RatesFactory $ratesFactory
    ) {
        $this->ratesFactory = $ratesFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->_authorization->isAllowed('TRIC_GLS::rates')) {
            $id = $this->getRequest()->getParam('entity_id', null);
            try {
                $glsRate = $this->ratesFactory->create()->load($id);
                if ($glsRate->getId()) {
                    $glsRate->delete();
                    $this->messageManager->addSuccessMessage(__('You deleted the record.'));
                } else {
                    $this->messageManager->addErrorMessage(__('Record does not exist.'));
                }
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }

        return $resultRedirect->setPath('*/*');
    }
}
