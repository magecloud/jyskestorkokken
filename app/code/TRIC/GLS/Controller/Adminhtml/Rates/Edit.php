<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Adminhtml\Rates;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use TRIC\GLS\Model\RatesFactory;

class Edit extends Action
{
    /**
     * @var Registry|null
     */
    public $coreRegistry = null;
    /**
     * @var PageFactory
     */
    public $resultPageFactory;
    /**
     * @var RatesFactory
     */
    public $ratesFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        RatesFactory $ratesFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->ratesFactory = $ratesFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->_authorization->isAllowed('TRIC_GLS::rates')) {
            $id = $this->getRequest()->getParam('entity_id');
            $resultPage = $this->resultPageFactory->create();
            $rateFactory = $this->ratesFactory->create();
            if ($id) {
                $resultPage->getConfig()->getTitle()->prepend(__('Edit rate'));
                $rateFactory->load($id);
                if (!$rateFactory->getId()) {
                    $this->messageManager->addErrorMessage(__('This rate no longer exists.'));

                    /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                    return $resultRedirect->setPath('*/*/');
                }
            } else {
                $resultPage->getConfig()->getTitle()->prepend(__('New Edit'));
            }
            $data = $this->_session->getFormData(true);
            if (!empty($data)) {
                $rateFactory->addData($data);
            }
            $this->coreRegistry->register('entity_id', $id);
            /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
            $resultPage->setActiveMenu('TRIC_GLS::rates');

            return $resultPage;
        }

        return $resultRedirect->setPath('*/*/');
    }
}
