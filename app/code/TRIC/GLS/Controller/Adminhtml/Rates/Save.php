<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Adminhtml\Rates;

use Magento\Backend\App\Action;
use TRIC\GLS\Model\RatesFactory;

class Save extends Action
{
    /** @var RatesFactory $objectFactory */
    public $objectFactory;

    public function __construct(
        Action\Context $context,
        RatesFactory $ratesFactory
    ) {
        $this->objectFactory = $ratesFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->_authorization->isAllowed('TRIC_GLS::rates')) {
            $data = $this->getRequest()->getParams();
            if ($data) {
                $params = [];
                $objectInstance = $this->objectFactory->create();
                $idField = $objectInstance->getIdFieldName();
                if (empty($data[$idField])) {
                    $data[$idField] = null;
                } else {
                    $objectInstance->load($data[$idField]);
                    $params[$idField] = $data[$idField];
                }
                foreach ($data as $key => $value) {
                    if (is_array($value)) {
                        $data[$key] = implode(',', $value);
                    }
                }
                $objectInstance->addData($data);
                $this->_eventManager->dispatch(
                    'tric_gls_rates_prepare_save',
                    ['object' => $this->objectFactory, 'request' => $this->getRequest()]
                );
                try {
                    $objectInstance->save();
                    $this->messageManager->addSuccessMessage(__('You saved this record.'));
                    $this->_getSession()->setFormData(false);
                    if ($this->getRequest()->getParam('back')) {
                        $params = [$idField => $objectInstance->getId(), '_current' => true];

                        return $resultRedirect->setPath('*/*/edit', $params);
                    }

                    return $resultRedirect->setPath('*/*/');
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the record.'));
                }
                $this->_getSession()->setFormData($this->getRequest()->getPostValue());

                return $resultRedirect->setPath('*/*/edit', $params);
            }
        }

        return $resultRedirect->setPath('*/*/');
    }
}
