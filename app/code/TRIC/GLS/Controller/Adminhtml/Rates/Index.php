<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
namespace TRIC\GLS\Controller\Adminhtml\Rates;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    public $resultPageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        if ($this->_authorization->isAllowed('TRIC_GLS::rates')) {
            /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
            $resultPage = $this->resultPageFactory->create();
            $resultPage->setActiveMenu('TRIC_GLS::rates');
            $resultPage->getConfig()->getTitle()->prepend(__('GLS Shipping Rates'));

            return $resultPage;
        }

        return $this->resultRedirectFactory->create()->setPath('*');
    }
}
