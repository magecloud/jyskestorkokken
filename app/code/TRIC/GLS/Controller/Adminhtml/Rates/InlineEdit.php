<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Adminhtml\Rates;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use TRIC\GLS\Model\ResourceModel\Rates\Collection;

class InlineEdit extends Action
{
    /** @var JsonFactory $jsonFactory */
    public $jsonFactory;
    /**
     * @var Collection
     */
    public $ratesCollection;

    public function __construct(
        Action\Context $context,
        Collection $collection,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->ratesCollection = $collection;
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && !empty($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }
        try {
            $this->ratesCollection
                ->addFieldToFilter('entity_id', ['in' => array_keys($postItems)])
                ->walk('saveCollection', [$postItems]);
        } catch (\Exception $exception) {
            $messages[] = __('There was an error saving the data: ') . $exception->getMessage();
            $error = true;
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
