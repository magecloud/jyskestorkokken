<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Controller\Adminhtml\Rates;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\DataObject;

class Validate extends Action
{
    /** @var JsonFactory $jsonFactory */
    public $jsonFactory;
    /** @var DataObject $dataObject */
    private $dataObject;

    public function __construct(
        Action\Context $context,
        JsonFactory $jsonFactory,
        DataObject $dataObject
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->dataObject = $dataObject;
    }

    public function execute()
    {
        $response = $this->dataObject;
        $response->setError(false);
        $requiredFields = [
            'title' => __('Title'),
            'price' => __('Price'),
            'condifition_from' => __('Condition from'),
            'condifition_to' => __('Condition to'),
            'condition_name' => __('Condition name')
        ];
        foreach ($this->getRequest()->getParams() as $field => $value) {
            if (in_array($field, array_keys($requiredFields)) && $value == '') {
                if (!is_array($response->getMessages())) {
                    $response->setMessages([]);
                }
                $messages = $response->getMessages();
                $messages[] = __('To save, you should fill in required "%1" field', $requiredFields[$field]);
                $response->setMessages($messages);
                $response->setError(true);
            }
        }
        $resultJson = $this->jsonFactory->create()->setData($response);

        return $resultJson;
    }
}
