##### TRIC Solutions
# GLS og PakkeShop integration til Magento 2
### Installation
##### Manuel installation
> **Note:** Vær sikker på, at TRIC Info modulet er installeret og opdateret til den nyeste version. Dette gælder kun ved manuel installation.

Udpak filerne til:

```app/code/TRIC/GLS ```

##### Composer installation (anbefaldet)
For at installeret med composer, skal du være oprettet kunde hos TRIC Solutions. Du skal bruge et brugernavn og password, for at kunne installere med composer.

Navigér til Magento's rod mappe, og skriv følgende kommandoer i terminalen:

```sh 
$ composer config repositories.tric composer https://repo.tric.dk/{DIT_UNIKKE_KUNDENAVN}/
$ composer require tric/module-gls
```

Husk at erstatte {DIT_UNIKKE_KUNDENAVN}, med det unikke kundenavn du har fået udleveret af TRIC Solutions.

#### Opsætning
****
Opsætningen af modulet kræver, at man har SSH adgang til shoppen.
Tjek hvad for en tilstand Magento shoppen kører i, da der er to forskellige måder at opsætte modulet på:

```sh
$ php bin/magento deploy:mode:show
```

Hvis shoppen kører i "developer" tilstand, så skal modulet sættes op på følgende måde (Kopiér hele linjen og indsæt i terminalen):

```sh
$ php bin/magento module:enable TRIC_Info TRIC_GLS \
&& php bin/magento setup:upgrade \
&& php bin/magento setup:di:compile \
&& php bin/magento cache:flush
```

Hvis shoppen kører i "production" tilstand, brug følgende kommando (Kopiér hele linjen og indsæt i terminalen):

```sh
$ php bin/magento maintenance:enable \
&& php bin/magento module:enable TRIC_Info TRIC_GLS \
&& php bin/magento setup:upgrade \
&& php bin/magento deploy:mode:set production \
&& php bin/magento cache:flush \
&& php bin/magento maintenance:disable
```

License
----

Proprietær


Versionering
----

Vi bruger [SemVer](http://semver.org/) for versionering.
