/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
let config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'TRIC_GLS/js/action/set-shipping-information-mixin': true
            }
        }
    },
    map: {
        '*': {
            'gls': 'TRIC_GLS/js/model/gls'
        }
    },
};

