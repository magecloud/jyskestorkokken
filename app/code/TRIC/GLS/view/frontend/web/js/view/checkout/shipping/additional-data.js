define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'mage/translate',
    'mage/url'
], function ($, ko, Component, quote, $t, urlBuilder) {
    'use strict';
    function GlsParcelShop(data) {
        let self = this;
        if (data.hasOwnProperty('deliveryAddress')) {
            self.id = data.servicePointId;
            self.name = data.name;
            self.label = data.name + ' - ' + data.deliveryAddress.streetName;
            self.company = data.name;
            self.street = data.deliveryAddress.streetName;
            self.postcode = data.deliveryAddress.postalCode;
            self.city = data.deliveryAddress.city;
            self.coordinates = data.coordinate;
        } else {
            self.id = 0;
            self.name = '';
            self.label = data.label;
            self.company = '';
            self.street = '';
            self.postcode = '';
            self.city = '';
            self.coordinates = '';
        }
        return self;
    }
    return Component.extend({
        defaults: {
            template: 'TRIC_GLS/checkout/shipping/additional_data'
        },
        glsParcelShopData: {
            infowindow: false,
            shippingMethod: ko.observable(),
            postcode: ko.observable(),
            pickupname: ko.observable(),
            notification: ko.observable(),
            moved: ko.observable(false),
            availableParcelShops: ko.observableArray([]),
            selectedParcelShop: ko.observable(),
            updateParcelShops: function (parcelShops) {
                this.availableParcelShops([]);
                for (let i in parcelShops) {
                    let parcelShop = new GlsParcelShop(parcelShops[i]);
                    this.availableParcelShops.push(parcelShop);
                }
            },
            showOnMap: function () {
                let self = this;
                require(
                    [
                        'Magento_Ui/js/modal/modal'
                    ],
                    function (modal) {
                        let options = {
                            autoOpen: true,
                            type: 'popup',
                            responsive: true,
                            innerScroll: true,
                            title: 'Vælg PakkeShop',
                            buttons: [{
                                text: $t('Continue'),
                                class: '',
                                click: function () {
                                    this.closeModal();
                                }
                            }]
                        };
                        modal(options, $('#gls-popup-map'));
                        $('#gls-map').css('height', '400px');
                    }
                );
                let lt = self.availableParcelShops()[0].coordinates.northing;
                let ln = self.availableParcelShops()[0].coordinates.easting;
                let mapOptions = {
                    zoom: 13,
                    center: new google.maps.LatLng(lt, ln),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    draggable: true,
                    zoomControl: true,
                    scrollwheel: true,
                    disableDoubleClickZoom: true,
                    keyboardShortcuts: false,
                    streetViewControl: false
                };
                let map = new google.maps.Map(document.getElementById('gls-map'), mapOptions);
                let geocoder = new google.maps.Geocoder();
                if (self.availableParcelShops().length > 1) {
                    let markers = [];
                    for (let i in self.availableParcelShops()) {
                        let shop = self.availableParcelShops()[i];
                        if (shop.coordinates) {
                            let marker = self.placeMarkers(shop.id, shop.coordinates.northing, shop.coordinates.easting, shop.company, shop.street, map, markers);
                            markers.push(marker);
                        } else {
                            self.reverseGeocode(shop.id, shop.street + ',' + shop.postcode + ',' + shop.city, shop.company, geocoder, map, markers);
                        }
                    }
                }
            },
            placeMarkers: function (id, lat, lng, shopname, address, resultsMap, markers) {
                let self = this;
                let marker = new google.maps.Marker({
                    map: resultsMap,
                    position: new google.maps.LatLng(lat, lng)
                });
                let contentString = '<div id="content"><strong>' + shopname + '</strong><br/>' + address + '</div>';
                let infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                marker.infowindow = infowindow;
                marker.addListener('click', function () {
                    infowindow.open(resultsMap, marker);
                    self.toggleMarkerBounce(markers, this, resultsMap);
                    self.showActiveMarkerInfo(shopname, address);
                    self.changeSelect(id);
                });
                return marker;
            },
            reverseGeocode: function (id, address, shopname, geocoder, resultsMap, markers) {
                let self = this;
                geocoder.geocode({
                    'address': address
                }, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resultsMap.setCenter(results[0].geometry.location);
                        let marker = new google.maps.Marker({
                            map: resultsMap,
                            position: results[0].geometry.location
                        });
                        markers.push(marker);
                        let contentString = '<div id="content"><strong>' + shopname + '</strong><br/>' + address + '</div>';
                        let infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });
                        marker.addListener('click', function () {
                            infowindow.open(resultsMap, marker);
                            self.toggleMarkerBounce(markers, this, resultsMap);
                            self.showActiveMarkerInfo(shopname, address);
                            self.changeSelect(id);
                        });
                    }
                });
            },
            mapUpdateZoom: function (resultsMap, markers) {
                let bounds = new google.maps.LatLngBounds();
                for (let i = 0; i < markers.length; i++) {
                    bounds.extend(markers[i].getPosition());
                }
                resultsMap.fitBounds(bounds);
            },
            toggleMarkerBounce: function (markers, marker, map) {
                let x = 0;
                while (x < markers.length) {
                    markers[x].setAnimation(null);
                    markers[x].infowindow.close();
                    x++;
                }
                marker.setAnimation(google.maps.Animation.BOUNCE);
                marker.infowindow.open(map, marker);
            },
            showActiveMarkerInfo: function (name, address) {
                let modal_footer = $('.modal-popup.modal-slide .modal-footer');
                if (!modal_footer.find('.activeinfo').length) {
                    modal_footer.prepend('<div class="activeinfo"></div>');
                }
                let activeinfo = $('.modal-popup.modal-slide .modal-footer .activeinfo');
                activeinfo.empty();
                activeinfo.append('<span class="headline">Valgt PakkeShop:</span><span class="name">' + name + '</span><span class="street">' + address + '</span>');
            },
            changeSelect: function (id) {
                let select = $('#gls-parcelshop-pickup-id');
                $(select).val(id);
            },
            no_parcelshop_label: $t('No ParcelShops available, please try again'),
        },
        glsPrivateData: {
            privateNotification: ko.observable(),
            privateComment: ko.observable(),
            isDeposit: ko.computed(function () {
                let shippingMethod = quote.shippingMethod();
                return !!(shippingMethod && shippingMethod.method_code.indexOf("c") > 0);
            }, this)
        },
        glsBusinessData: {
            businessComment: ko.observable(),
        },
        initObservable: function () {
            this._super();
            this.selectedMethod = ko.computed(function () {
                let self = this;
                let method = quote.shippingMethod();
                let selectedMethod = method != null ? method.carrier_code + '_' + method.method_code : null;
                self.glsParcelShopData.shippingMethod(selectedMethod);
                if (selectedMethod && selectedMethod.indexOf('gls_parcelshop') === 0) {
                    let pickupId = $('#gls-parcelshop-pickup-id').val();
                    if (quote.shippingAddress() && quote.shippingAddress().postcode && (!pickupId || parseInt(pickupId) <= 0)) {
                        let postcode = quote.shippingAddress().postcode;
                        self.glsParcelShopData.postcode(postcode);
                    }
                }
                return selectedMethod;
            }, this);
            this.selectedMethodIsAvailable = ko.computed(function () {
                return true;
            }, this);
            this.glsParcelShopData.postcode.subscribe(function (val) {
                if (val.length >= 4) {
                    let country = quote.shippingAddress().countryId;
                    this.searchParcelShopsGls(val, country);
                }
            }, this);
            return this;
        },
        initialize: function () {
            this._super();
        },
        searchParcelShopsGls: function (postcode, country) {
            let self = this;
            let parcelShopSearch = [
                {label: $t('Searching ParcelShops...')}
            ];
            self.glsParcelShopData.updateParcelShops(parcelShopSearch);
            $('button.showmap').hide();
            let params = {'postcode': postcode, 'country': country};
            $.ajax({
                type: 'post',
                url: urlBuilder.build('gls/checkout/getParcelShops'),
                data: params
            }).done(function (result) {
                if (result) {
                    self.glsParcelShopData.updateParcelShops(result.servicePointInformationResponse.servicePoints);
                    if (Object.keys(require.s.contexts['_'].config.paths).indexOf('googlemaps') >= 0) {
                        require(['googlemaps']);
                        $('button.showmap').css('display', 'block');
                    }
                } else {
                    let parcelShopSearch = [
                        {label: $t('No ParcelShops found')}
                    ];
                    self.glsParcelShopData.updateParcelShops(parcelShopSearch);
                }
            });
        }
    });
});
