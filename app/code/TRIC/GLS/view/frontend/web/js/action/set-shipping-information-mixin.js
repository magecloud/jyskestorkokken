/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'uiRegistry',
    'mage/translate',
    'gls',
    'TRIC_GLS/js/model/gls-validator',
], function ($, wrapper, quote, registry, $t, gls, glsValidator) {
    'use strict';
    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            if (!glsValidator.validate()) {
                return false;
            } else {
                return originalAction();
            }
        });
    };
});
