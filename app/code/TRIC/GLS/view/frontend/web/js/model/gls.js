/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
define([
    'jquery',
    'mage/url'
], function ($, urlBuilder) {
    'use strict';
    return {
        getParcelShops: function (postcode, country, shippingMethod) {
            let params = {'postcode': postcode, 'country': country};
            $.ajax({
                type: 'post',
                url: url.build('gls/checkout/getParcelShops'),
                data: params
            }).done(function (result) {
                let select = $('#' + shippingMethod + '_pickup_id').empty();
                if (result.length) {
                    $(result).each(function (k, v) {
                        $(select).append(
                            $('<option>', {value: v.id})
                                .data('company', v.company)
                                .data('street', v.street)
                                .data('postcode', v.postcode)
                                .data('city', v.city)
                                .text(v.label)
                        );
                    })
                } else {
                    $(select).append(
                        $('<option>', {value: ''}).text($(select).attr('data-no-parcelshops'))
                    );
                }
            });
        },
        update: function (data) {
            let shippingGlsAdditionalData = JSON.stringify(data);
            this._sendRequest({
                'is_active': true,
                'shipping_gls_pickup_id': data.pickup_id,
                'shipping_gls_additional_data': shippingGlsAdditionalData
            });
        },
        clear: function () {
            this._sendRequest({'is_active': false});
        },
        _sendRequest: function (data) {
            $.ajax({
                method: 'post',
                url: urlBuilder.build('gls/checkout/saveData'),
                data: data,
                showLoader: true
            });
        },
    };
});
