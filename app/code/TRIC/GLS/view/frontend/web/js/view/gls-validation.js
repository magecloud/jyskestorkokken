/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'TRIC_GLS/js/model/gls-validator'
    ],
    function (Component, additionalValidators, glsValidator) {
        'use strict';
        additionalValidators.registerValidator(glsValidator);
        return Component.extend({});
    }
);
