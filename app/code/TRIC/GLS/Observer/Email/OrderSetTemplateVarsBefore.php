<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Observer\Email;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Serialize\Serializer\Json;

class OrderSetTemplateVarsBefore implements ObserverInterface
{
    /**
     * @var Json
     */
    private $json;

    public function __construct(Json $json)
    {
        $this->json = $json;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $transport = $observer->getEvent()->getTransport();
        $order = $transport->getOrder();
        if ($order->getIsVirtual()) {
            return;
        }
        $shippingMsg = $transport->getShippingMsg() ? $transport->getShippingMsg() . '<br/>' : '';
        if ($glsAdditionalData = $order->getShippingGlsAdditionalData()) {
            $data = $this->json->unserialize($glsAdditionalData);
            if (isset($data['pickup_id']) &&
                $data['pickup_id'] &&
                isset($data['pickup_company']) &&
                $data['pickup_company']) {
                $shippingMsg .= $data['pickup_company'] . ' (ID: ' . $data['pickup_id'] . ')';
            } elseif (isset($data['comment']) && $data['comment']) {
                $shippingMsg .= '' . __('Placement for the package') . ': ' . $data['comment'];
            }
        }
        $transport->setShippingMsg(trim($shippingMsg));
    }
}
