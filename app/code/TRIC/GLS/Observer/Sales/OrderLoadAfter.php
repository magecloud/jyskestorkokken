<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Api\Data\OrderExtension;

class OrderLoadAfter implements ObserverInterface
{
    public $orderExtension;

    public function __construct(
        OrderExtension $orderExtension
    ) {
        $this->orderExtension = $orderExtension;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getOrder();
        $extensionAttributes = $order->getExtensionAttributes();
        if ($extensionAttributes === null) {
            $extensionAttributes = $this->orderExtension;
        }
        $attr = $order->getData('shipping_gls_pickup_id');
        $extensionAttributes->setShippingGlsPickupId($attr);
        $attr = $order->getData('shipping_gls_additional_data');
        $extensionAttributes->setShippingGlsAdditionalData($attr);
        $order->setExtensionAttributes($extensionAttributes);
    }
}
