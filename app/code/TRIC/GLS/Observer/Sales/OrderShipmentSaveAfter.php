<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use TRIC\GLS\Helper\Data;
use Magento\Framework\Event\Observer;

class OrderShipmentSaveAfter implements ObserverInterface
{
    public $helper;

    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->helper->getConfig('gls/general/auto_generate_csv_on_shipment_create')) {
            $shipment = $observer->getEvent()->getShipment();
            $order = $shipment->getOrder();
            $filename = $order->getRealOrderId() . '-' . $shipment->getIncrementId() . '.csv';
            $this->helper->createCsvFileForOrder($order, false, $filename);
        }
    }
}
