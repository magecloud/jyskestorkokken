<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\GLS\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Serialize\Serializer\Json;

class QuoteSubmitBefore implements ObserverInterface
{
    /**
     * @var Json
     */
    private $json;

    public function __construct(Json $json)
    {
        $this->json = $json;
    }

    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        $order = $event->getOrder();
        $shippingAddress = $order->getShippingAddress();
        $quote = $event->getQuote();
        $glsPickupId = $quote->getData('shipping_gls_pickup_id');
        $glsAdditionalData = $quote->getData('shipping_gls_additional_data');
        $order->addData([
            'shipping_gls_pickup_id' => $glsPickupId,
            'shipping_gls_additional_data' => $glsAdditionalData
        ]);
        if ($glsPickupId && $glsAdditionalData) {
            $data = $this->json->unserialize($glsAdditionalData);
            $name = $data['pickup_name'] ?? $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();
            $pickupName = explode(' ', $name);
            $firstname = array_shift($pickupName);
            $lastname = !empty($pickupName) ? implode(' ', $pickupName) : '-';
            $glsPickupAddressData = [
                'firstname' => trim($firstname),
                'lastname' => trim($lastname),
                'company' => $data['pickup_company'],
                'street' => $data['pickup_street'],
                'postcode' => $data['pickup_postcode'],
                'city' => $data['pickup_city'],
                'region' => '',
                'country_id' => $data['pickup_country'] ?? $shippingAddress->getCountryId(),
                'telephone' => $data['notification'] ?? $shippingAddress->getTelephone(),
                'fax' => '',
                'save_in_address_book' => 0
            ];
            $shippingAddress->addData($glsPickupAddressData);
        } elseif ($glsAdditionalData) {
            $data = $this->json->unserialize($glsAdditionalData);
            if (isset($data['notification']) && $data['notification']) {
                $shippingAddress->setTelephone($data['notification']);
            }
        }
    }
}
