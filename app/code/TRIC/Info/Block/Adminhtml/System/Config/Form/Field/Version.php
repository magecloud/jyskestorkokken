<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\Info\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use TRIC\Info\Helper\Data;
use Magento\Framework\App\Request\Http;

class Version extends Field
{
    const EXTENSION_URL = 'https://store.tric.dk/';
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Http
     */
    private $request;

    public function __construct(
        Context $context,
        Data $helper,
        Http $http
    ) {
        $this->helper = $helper;
        $this->request = $http;
        parent::__construct($context);
    }

    public function _getElementHtml(AbstractElement $element)
    {
        $moduleCode = $this->request->getParam('section');
        $versionLabel = sprintf(
            '<a href="%s" title="%s" target="_blank">%s</a>',
            self::EXTENSION_URL,
            $this->helper->getExtensionName($moduleCode),
            $this->helper->getExtensionVersion($moduleCode)
        );
        $element->setValue($versionLabel);

        return $element->getValue();
    }
}
