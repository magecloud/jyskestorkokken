<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\Info\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use TRIC\Info\Helper\Data;
use Magento\Framework\App\Request\Http;

class Extensions extends Field
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Http
     */
    private $request;

    public function __construct(
        Context $context,
        Data $helper,
        Http $http
    ) {
        $this->helper = $helper;
        $this->request = $http;
        parent::__construct($context);
    }

    public function render(AbstractElement $element)
    {
        $html = "";
        foreach ($this->helper->getAllTricModules() as $tricModule) {
            $moduleHtml = "<td>" . $tricModule['name'] . "</td>";
            $moduleHtml .= "<td>" . $tricModule['setup_version'] . "</td>";
            $html .= $this->_decorateRowHtml($element, $moduleHtml);
        }

        return $html;
    }
}
