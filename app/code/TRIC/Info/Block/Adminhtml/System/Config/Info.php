<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\Info\Block\Adminhtml\System\Config;

use Magento\Backend\Block\AbstractBlock;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\CacheInterface;
use TRIC\Info\Helper\Data as InfoHelper;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Request\Http;

class Info extends AbstractBlock implements RendererInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var WriterInterface
     */
    private $configWriter;
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var InfoHelper
     */
    private $infoHelper;
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var Http
     */
    private $request;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        WriterInterface $configWriter,
        CacheInterface $cache,
        InfoHelper $infoHelper,
        Curl $curl,
        Http $request
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->cache = $cache;
        $this->infoHelper = $infoHelper;
        $this->curl = $curl;
        $this->request = $request;
    }

    public function render(AbstractElement $element)
    {
        $html = $element->getBeforeElementHtml();
        $moduleCode = $this->request->getParam('section');
        $moduleName = $this->infoHelper->getExtensionName($moduleCode);
        $configPath = strtolower($moduleName) . '/install/date';
        if (!$this->scopeConfig->getValue($configPath)) {
            $this->configWriter->save($configPath, time());
        }
        $moduleVersion = $this->infoHelper->getExtensionVersion($moduleCode);
        try {
            if ($moduleName === "TRIC_Info") {
                $url = 'https://services.tric.dk/info/about_dk.html';
            } else {
                $url = 'https://services.tric.dk/info/extension_dk.html';
            }
            $cacheKey = strtolower($moduleName) . '_' . str_replace('.', '', $moduleVersion) . '_info';
            if (!($this->cache->load($cacheKey)) || (time() - $this->cache->load($cacheKey . '_lastcheck')) > 3600) {
                $curl = $this->curl;
                $curl->get($url);
                $html .= $curl->getBody();
                if (!$html) {
                    return '';
                }
                if ($moduleName != "TRIC_Info") {
                    $html = str_replace(['{{NAME}}', '{{VERSION}}'], [$moduleName, $moduleVersion], $html);
                }
                $this->cache->save($html, $cacheKey);
                $this->cache->save(time(), $cacheKey . '_lastcheck');

                return $html;
            }
            $html = $this->cache->load($cacheKey);

            return $html;
        } catch (\Exception $e) {
            return '';
        }
    }
}
