<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

namespace TRIC\Info\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Payment\Gateway\Http\Client\Zend;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    private $logger;
    private $moduleList;
    private $messageManager;

    public function __construct(
        Context $context,
        ModuleListInterface $moduleList,
        ManagerInterface $messageManager
    ) {
        $this->logger = $context->getLogger();
        $this->moduleList = $moduleList;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * @param string $configPath
     * @param int    $storeId
     *
     * @return mixed
     */
    public function getConfig($configPath, $storeId = 0)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param $moduleCode
     *
     * @return mixed
     */
    public function getExtensionName($moduleCode)
    {
        $moduleInfo = $this->moduleList->getOne($this->getModuleName($moduleCode));

        return $moduleInfo['name'];
    }

    /**
     * @param $moduleCode
     *
     * @return mixed
     */
    public function getExtensionVersion($moduleCode)
    {
        $moduleInfo = $this->moduleList->getOne($this->getModuleName($moduleCode));

        return $moduleInfo['setup_version'];
    }

    /**
     * @return array
     */
    public function getAllTricModules()
    {
        $modules = $this->moduleList->getAll();
        foreach ($modules as $key => $module) {
            if (substr($module['name'], 0, 5) !== "TRIC_") {
                unset($modules[$key]);
                continue;
            }
        }

        return $modules;
    }

    /**
     * @param $moduleCode
     *
     * @return mixed|string
     */
    public function getModuleName($moduleCode)
    {
        $moduleList = [
            'postnord' => 'TRIC_PostNord',
            'gls' => 'TRIC_GLS',
            'economic' => 'TRIC_Economic'
        ];

        return array_key_exists($moduleCode, $moduleList) ? $moduleList[$moduleCode] : "TRIC_Info";
    }
}
