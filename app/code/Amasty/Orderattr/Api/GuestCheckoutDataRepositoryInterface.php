<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Orderattr
 */


namespace Amasty\Orderattr\Api;

/**
 * @api
 */
interface GuestCheckoutDataRepositoryInterface
{
    /**
     * Save Data from Frontend Checkout
     *
     * @param string|int $amastyCartId
     * @param string $checkoutFormCode
     * @param string $shippingMethod
     * @param \Amasty\Orderattr\Api\Data\EntityDataInterface $entityData
     * @throws \Magento\Framework\Exception\InputException
     *
     * @return \Amasty\Orderattr\Api\Data\EntityDataInterface
     */
    public function save(
        $amastyCartId,
        $checkoutFormCode,
        $shippingMethod,
        \Amasty\Orderattr\Api\Data\EntityDataInterface $entityData
    );
}
