define([
    'jquery',
    'mage/utils/wrapper',
    'Amasty_Orderattr/js/model/validate-and-save',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, validateAndSave, quote) {
    'use strict';

    return function (placeOrderAction) {
        return wrapper.wrap(placeOrderAction, function (originalAction, messageContainer) {
            if (typeof window.orderAttributesPreSend !== "undefined" && window.orderAttributesPreSend) {
                var result = $.Deferred(),
                    attributesTypes = [
                        'amastyShippingAttributes',
                        'amastyPaymentAttributes',
                        'amastySummaryAttributes',
                        'amastyShippingMethodAttributes',
                        'before-place-order.amastyPaymentMethodAttributes'
                    ],
                    formCode = 'amasty_checkout';

                if (quote.isVirtual()) {
                    attributesTypes = [
                        'amastyPaymentAttributes',
                        'before-place-order.amastyPaymentMethodAttributes',
                        'amastySummaryAttributes'
                    ];
                    formCode = 'amasty_checkout_virtual';
                }

                validateAndSave(attributesTypes, formCode).done(function(valid){
                    if (valid) {
                        result.resolve(originalAction(messageContainer));
                    } else {
                        result.reject();
                    }
                });

                return result.promise();
            }
            return originalAction(messageContainer);
        });
    };
});
