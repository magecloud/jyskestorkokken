define([
    'jquery',
    'mage/utils/wrapper',
    'Amasty_Orderattr/js/model/validate-and-save'
], function ($, wrapper, validateAndSave) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var result = $.Deferred(),
                attributesTypes = [
                    'amastyShippingAttributes',
                    'amastyShippingMethodAttributes'
                ];

            validateAndSave(attributesTypes, 'amasty_checkout_shipping').done(function(valid){
                if (valid) {
                    result.resolve(originalAction());
                } else {
                    result.reject();
                }
            });

            return result.promise();
        });
    };
});
