<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Model;

class Schedule extends \Magento\Framework\Model\AbstractModel
{

    protected $_scheduleCollections = array();
    protected $_customerGroup = array();
    protected $_rules = array();

    protected $_dateTime;
    protected $_date;

    protected $_basicFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    protected $_customerLog = array();
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ResourceModel\History\CollectionFactory
     */
    protected $historyCollectionFactory;

    /**
     * @var History
     */
    protected $history;

    /**
     * Schedule constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\Schedule $resource
     * @param ResourceModel\Schedule\Collection $resourceCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param Event\BasicFactory $basicFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param ResourceModel\History\CollectionFactory $historyCollectionFactory
     * @param History $history
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Amasty\Followup\Model\ResourceModel\Schedule $resource,
        \Amasty\Followup\Model\ResourceModel\Schedule\Collection $resourceCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Amasty\Followup\Model\Event\BasicFactory $basicFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Amasty\Followup\Model\ResourceModel\History\CollectionFactory $historyCollectionFactory,
        \Amasty\Followup\Model\HistoryFactory  $historyFactory,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_dateTime = $dateTime;
        $this->_date = $date;
        $this->_basicFactory = $basicFactory;
        $this->history = $historyFactory;

        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );

        $this->objectManager = $objectManager;
        $this->historyCollectionFactory = $historyCollectionFactory;
    }

    public function _construct()
    {
        $this->_init('Amasty\Followup\Model\ResourceModel\Schedule');
    }

    public function getConfig()
    {
        $config = $this->getData();

        unset($config['rule_id']);

        $config['days'] = $this->getDays();
        $config['hours'] = $this->getHours();
        $config['minutes'] = $this->getMinutes();
        $config['discount_amount'] = $config['discount_amount'] * 1;
        $config['discount_qty'] = $config['discount_qty'] * 1;
        
        return $config;
    }

    public function getDeliveryTime()
    {
        return ($this->getDays() * 24 * 60 * 60) +
            ($this->getHours() * 60 * 60) +
            ($this->getMinutes() * 60);
    }

    protected function _getScheduleCollection($rule)
    {
        if (!isset($this->_scheduleCollections[$rule->getId()])){
            $this->_scheduleCollections[$rule->getId()] = $this
                ->getCollection()
                ->addRule($rule);
        }

        return $this->_scheduleCollections[$rule->getId()];
    }

    public function _getCustomerEmailVars($customer, $history)
    {
        $logCustomer = $this->_loadCustomerLog($customer);
        $customerGroup = $this->_loadCustomerGroup($customer->getGroupId());

        return array(
            Formatmanager::TYPE_CUSTOMER => $customer,
            Formatmanager::TYPE_CUSTOMER_GROUP => $customerGroup,
            Formatmanager::TYPE_CUSTOMER_LOG => $logCustomer,
            Formatmanager::TYPE_HISTORY => $history,
        );

    }

    protected function _loadCustomerLog($customer)
    {
        $customerId = $customer->getId();

        if (!isset($this->_customerLog[$customerId])){

            $this->_customerLog[$customerId] = $this->objectManager
                ->create('Magento\Customer\Model\Logger')
                ->get($customerId);
            //$this->_customerLog[$customerId]->setInactiveDays(floor((time() - strtotime($this->_customerLog[$customerId]->getLastLoginAt())) / 60 / 60 / 24));

        }
        return $this->_customerLog[$customerId];
    }

    protected function _loadCustomerGroup($id)
    {
        if (!isset($this->_customerGroup[$id])){
            $this->_customerGroup[$id] = $this->objectManager
                ->create('Magento\Customer\Model\Group')
                ->load($id);
        }

        return $this->_customerGroup[$id];
    }


    public function createCustomerHistory($rule, $event, $customer, $product = null)
    {
        $ret = array();
        $scheduleCollection = $this->_getScheduleCollection($rule);
        $scheduledAt = null;

        foreach($scheduleCollection as $schedule){
            $this->_storeManager->setCurrentStore($customer->getStoreId());

            $history = $this->history->create();

            $history->initCustomerItem($customer);

            if ($product instanceof \Magento\Catalog\Model\Product) {
                $scheduledAt = $product->getSpecialFromDate();
            }

            $history->createItem($schedule, $customer->getTargetCreatedAt(), $scheduledAt);

            if (is_null($product)) {
                $email = $event->getEmail($schedule, $history, $this->_getCustomerEmailVars($customer, $history));
                $history->saveEmail($email);
            }

            $ret[] = $history;
        }
        return $ret;
    }

    /**
     * @param $rule
     * @param $event
     * @param $order
     * @param $quote
     * @param $customer
     * @return array
     */
    public function createOrderHistory($rule, $event, $order, $quote, $customer)
    {
        $ret = [];
        $scheduleCollection = $this->_getScheduleCollection($rule);

        if (!$customer->getId()) {
            $this->_initCustomer($customer, $order);
        }

        foreach ($scheduleCollection as $schedule) {
            $this->_storeManager->setCurrentStore($quote->getStoreId());

            $history = $this->objectManager
                ->create('Amasty\Followup\Model\History');

            if ($this->isAlreadyScheduledOrder($order, $schedule, $schedule->getRuleId(), $quote->getStoreId())) {
                continue;
            }

            $history->initOrderItem($order, $quote);

            $history->createItem($schedule, $quote->getTargetCreatedAt());

            $email = $event->getEmail(
                $schedule,
                $history,
                $this->_getOrderEmailVars($order, $quote, $customer, $history)
            );

            if (!$email) {
                if ($history->getStatus() != \Amasty\Followup\Model\History::STATUS_NO_CROSSEL_PRODUCT) {
                    $history->setStatus(\Amasty\Followup\Model\History::STATUS_NO_PRODUCT);
                    $history->save();
                }
            } else {
                $history->saveEmail($email);
                $ret[] = $history;
            }
        }

        return $ret;
    }

    /**
     * @param $order
     * @param $schedule
     * @param $ruleId
     * @param $storeId
     * @return bool
     */
    public function isAlreadyScheduledOrder($order, $schedule, $ruleId, $storeId)
    {
        $historyCollection = $this->historyCollectionFactory->create();
        $historyCollection->getOrderFilter($order->getId(), $schedule->getScheduleId(), $ruleId, $storeId);

        return $historyCollection->getSize() ? true : false;
    }

    public function _initCustomer(&$customer, $order)
    {

        $data = $order->getBillingAddress()->getData();
        $customer->setData($data);
    }

    protected function _getOrderEmailVars($order, $quote, $customer, $history)
    {
        $vars = $this->_getCustomerEmailVars($customer, $history);
        $vars[Formatmanager::TYPE_ORDER] = $order;
        $vars[Formatmanager::TYPE_QUOTE] = $quote;
        return $vars;
    }

    public function getDays()
    {
        return $this->getDelayedStart() > 0 && floor($this->getDelayedStart() / 24 / 60 / 60) ?
            floor($this->getDelayedStart() / 24 / 60 / 60) :
            NULL;
    }

    public function getHours()
    {
        $days = $this->getDays();
        $time = $this->getDelayedStart() - ($days * 24 * 60 * 60);

        return $time > 0 ?
            floor($time / 60 / 60) :
            NULL;
    }

    public function getMinutes()
    {
        $days = $this->getDays();
        $hours = $this->getHours();
        $time = $this->getDelayedStart() - ($days * 24 * 60 * 60) - ($hours * 60 * 60);

        return $time > 0 ?
            floor($time / 60) :
            NULL;
    }

    public function checkCustomerRules($customer, $types = array(), $product = null)
    {

        $ruleCollection = $this->_getRuleCollection($types);
        foreach($ruleCollection as $rule){

            $event = $rule->getStartEvent();

            if ($event->validate($customer)){
                $this->createCustomerHistory($rule, $event, $customer, $product);
            }
        }
    }

    public function _getRuleCollection($types = array())
    {

        $ruleCollection = $this->objectManager
            ->create('Amasty\Followup\Model\ResourceModel\Rule\Collection')
            ->addStartFilter($types);

        return $ruleCollection;
    }


    public function checkSubscribtionRules($subscriber, $customer, $types = array())
    {
        $ruleCollection = $this->_getRuleCollection($types);

        foreach($ruleCollection as $rule){

            $event = $rule->getStartEvent();

            if ($event->validateSubscription($subscriber, $customer)){
                $this->createCustomerHistory($rule, $event, $customer);
            }
        }
    }

    public function _process()
    {
        $historyCollection = $this->_getHistoryCollection()
            ->addReadyFilter($this->_dateTime->formatDate($this->_basicFactory->create()->getCurrentExecution()));

        foreach($historyCollection as $history){

            $rule = $this->_loadRule($history->getRuleId());

            if ($history->validateBeforeSent($rule)) {
                $history->processItem($rule, $history->getEmail());
            } else {
                $history->cancelItem();
            }
        }
    }

    protected function _getHistoryCollection()
    {
        $historyCollection = $this->objectManager
            ->create('Amasty\Followup\Model\ResourceModel\History\Collection')
            ->addOrderData();

        return $historyCollection;
    }

    protected function _loadRule($ruleId)
    {

        if (!isset($this->_rules[$ruleId])){
            $this->_rules[$ruleId] = $this->objectManager
                ->create('Amasty\Followup\Model\Rule')
                ->load($ruleId);
        }

        return $this->_rules[$ruleId];
    }

}
