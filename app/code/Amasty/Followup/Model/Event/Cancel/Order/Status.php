<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Model\Event\Cancel\Order;

use \Magento\Sales\Model\Order as SalesOrder;

class Status extends \Amasty\Followup\Model\Event\Basic
{
    public function validate($history)
    {
        $collection = $this->_objectManager
            ->create('Magento\Sales\Model\ResourceModel\Order\Collection');

        $status = $this->_status->getStatus();
        $historyCreatedAt = $history->getCreatedAt();

        switch ($status){
            case SalesOrder::STATE_PROCESSING:
                $collection = $this->addFilterForProcessing($collection, $historyCreatedAt);
                break;
            case SalesOrder::STATE_COMPLETE:
                $collection = $this->addFilterForInvoice($collection, $historyCreatedAt);
                $collection = $this->addFilterForShip($collection, $historyCreatedAt);
                break;
            case SalesOrder::STATE_CANCELED:
                $collection = $this->addFilterForCanceled($collection, $historyCreatedAt);
                break;
            case SalesOrder::STATE_CLOSED:
                $collection = $this->addFilterForClosed($collection, $historyCreatedAt);
                break;

        }

        return $collection->getSize() > 0;
    }

    protected function addFilterForProcessing($collection, $historyCreatedAt)
    {
        $collection->getSelect()->joinLeft(
            array('invoice' => $collection->getTable('sales_invoice')),
            'main_table.entity_id = invoice.order_id',
            array()
        );

        $collection->getSelect()->joinLeft(
            array('shipment' => $collection->getTable('sales_shipment')),
            'main_table.entity_id = shipment.order_id',
            array()
        );

        $collection->addFieldToFilter(
            array('invoice.created_at', 'shipment.order_id'),
            array(
                array('gteq' => $historyCreatedAt),
                array('gteq' => $historyCreatedAt)
            )
        );

        return $collection;
    }

    protected function addFilterForInvoice($collection, $historyCreatedAt)
    {
        $collection->getSelect()->joinInner(
            array('invoice' => $collection->getTable('sales_invoice')),
            'main_table.entity_id = invoice.order_id',
            array()
        );

        $collection->addFieldToFilter('invoice.created_at', array(
            'gteq' => $historyCreatedAt
        ));

        return $collection;
    }

    protected function addFilterForShip($collection, $historyCreatedAt)
    {
        $collection->getSelect()->joinInner(
            array('shipment' => $collection->getTable('sales_shipment')),
            'main_table.entity_id = shipment.order_id',
            array()
        );

        $collection->addFieldToFilter('shipment.created_at', array(
            'gteq' => $historyCreatedAt
        ));

        return $collection;
    }


    protected function addFilterForCanceled($collection, $historyCreatedAt)
    {
        $collection->addFieldToFilter('main_table.status', array(
            'eq' => 'canceled'
        ));

        $collection->addFieldToFilter('main_table.updated_at', array(
            'gteq' => $historyCreatedAt
        ));

        return $collection;
    }

    protected function addFilterForClosed($collection, $historyCreatedAt)
    {
        $collection->getSelect()->joinInner(
            array('creditmemo' => $collection->getTable('sales_creditmemo')),
            'main_table.entity_id = creditmemo.order_id',
            array()
        );

        $collection->addFieldToFilter('creditmemo.created_at', array(
            'gteq' => $historyCreatedAt
        ));

        return $collection;
    }
}
