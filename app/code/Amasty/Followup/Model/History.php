<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

namespace Amasty\Followup\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class History extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_PENDING = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_SENT = 'sent';
    const STATUS_CANCEL = 'cancel';
    const STATUS_NO_PRODUCT = 'no_product';
    const STATUS_NO_CROSSEL_PRODUCT = 'no_crossel_products';

    const REASON_BLACKLIST = 'blacklist';
    const REASON_EVENT = 'event';
    const REASON_ADMIN = 'admin';
    const REASON_NOT_SUBSCRIBED = 'not_subsribed';
    
    const NAME_XML_PATH = 'amfollowup/templates/name';
    const EMAIL_XML_PATH = 'amfollowup/templates/email';
    const CC_XML_PATH = 'amfollowup/templates/cc';
    
    protected $_cancelEventValidation = array();
    protected $_cancelNotSubscribedValidation = array();
    protected $_cancelBlacklistValidation = array();

    protected $_dateTime;
    protected $_date;
    protected $_storeManager;
    protected $_store;

    protected $_transportBuilder;
    protected $_templateFactory;
    protected $_message;
    protected $_scopeConfig;

    protected $_groupRepository;
    protected $_searchCriteriaBuilder;

    protected $_basicFactory;
    /**
     * @var \Magento\Framework\Mail\MessageFactory
     */
    protected $messageFactory;
    /**
     * @var \Magento\Framework\Mail\TransportInterfaceFactory
     */
    protected $mailTransportFactory;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * @var \Amasty\Base\Model\Serializer
     */
    private $serializer;

    /**
     * History constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Mail\TransportInterfaceFactory $mailTransportFactory
     * @param \Magento\Framework\Mail\Template\FactoryInterface $templateFactory
     * @param \Magento\Framework\Mail\MessageFactory $messageFactory
     * @param Event\Basic $basicFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param ResourceModel\History|null $resource
     * @param ResourceModel\History\Collection|null $resourceCollection
     * @param GroupRepositoryInterface $groupRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Amasty\Base\Model\Serializer $serializer
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\TransportInterfaceFactory $mailTransportFactory,
        \Magento\Framework\Mail\Template\FactoryInterface $templateFactory,
        \Magento\Framework\Mail\MessageFactory $messageFactory,
        \Amasty\Followup\Model\Event\Basic $basicFactory,
        ScopeConfigInterface $scopeConfig,
        \Amasty\Followup\Model\ResourceModel\History $resource = null,
        \Amasty\Followup\Model\ResourceModel\History\Collection $resourceCollection = null,
        GroupRepositoryInterface $groupRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Amasty\Base\Model\Serializer $serializer,
        array $data = []
    ){

        $this->_dateTime = $dateTime;
        $this->_date = $date;
        $this->_storeManager = $storeManager;
        $this->_templateFactory = $templateFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_groupRepository = $groupRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_basicFactory = $basicFactory;

        parent::__construct($context, $registry, $resource, $resourceCollection);
        $this->messageFactory = $messageFactory;
        $this->mailTransportFactory = $mailTransportFactory;
        $this->objectManager = $objectManager;
        $this->serializer = $serializer;
    }

    public function _construct()
    {
        parent::_construct();
        $this->_init('Amasty\Followup\Model\ResourceModel\History');
    }

    public function processItem($rule, $email = null, $testMode = false)
    {
        $this->setExecutedAt($this->_dateTime->formatDate($this->_basicFactory->getCurrentExecution()));
        $this->setStatus(self::STATUS_PROCESSING);
        $this->save();

        if ($this->_sendEmail($rule, $email, $testMode)){
            $this->setFinishedAt($this->_dateTime->formatDate($this->_basicFactory->getCurrentExecution()));
            $this->setStatus(self::STATUS_SENT);
            $this->save();
        }
    }

    protected function _sendEmail($rule, $email = null, $testMode = false)
    {
        $storeId = $this->_storeManager->getStore()->getId();

        $recipient = $this->_scopeConfig->getValue("amfollowup/test/recipient");
        $safeMode = $this->_scopeConfig->getValue("amfollowup/test/safe_mode");

        $to = $email;

        if (intval($safeMode) == 1 || $testMode) {
            $to = $recipient;
        }

        $name = $this->getCustomerName();

        $senderName = $rule->getSenderName()
            ? $rule->getSenderName()
            : $this->_scopeConfig->getValue(self::NAME_XML_PATH, ScopeInterface::SCOPE_STORE, $storeId);
        $senderEmail = $rule->getSenderEmail()
            ? $rule->getSenderEmail()
            : $this->_scopeConfig->getValue(self::EMAIL_XML_PATH, ScopeInterface::SCOPE_STORE, $storeId);
        $cc = $rule->getSenderCc()
            ? $rule->getSenderCc()
            : $this->_scopeConfig->getValue(self::CC_XML_PATH, ScopeInterface::SCOPE_STORE, $this->getStoreId());

        $message = $this->messageFactory->create();

        if (is_null($this->getBody())) {
            $event = $rule->getStartEvent();
            $schedule = $this->getSchedule();
            $customer = $this->objectManager
                ->create('Magento\Customer\Model\Customer')
                ->load($this->getCustomerId());
            $email = $event->getEmail($schedule, $this, $schedule->_getCustomerEmailVars($customer, $this));
            $this->saveEmail($email);
        }

        $message
            ->addTo($to, $name)
            ->setFrom($senderEmail, $senderName)
            ->setMessageType(\Magento\Framework\Mail\MessageInterface::TYPE_HTML)
            ->setBody($this->getBody())
            ->setSubject($this->getSubject());

        if (!empty($cc) && !$safeMode && !$testMode) {
            $message->addBcc(explode(',', $cc));
        }

        $mailTransport = $this->mailTransportFactory->create(['message' => clone $message]);

        $mailTransport->sendMessage();

        return true;
    }

    public function getSchedule()
    {
        return $this->objectManager->create('Amasty\Followup\Model\Schedule')->load($this->getScheduleId());
    }
    
    public function initOrderItem($order, $quote)
    {
        $this->addData(
            [
                'order_id'  => $order->getId(),
                'increment_id' => $order->getIncrementId(),
                'store_id'  => $quote->getStoreId(),
                'email'  => $quote->getCustomerEmail(),
                'customer_id' => $quote->getCustomerId(),
                'customer_name' => $quote->getCustomerFirstname(). ' ' .$quote->getCustomerLastname()
            ]
        );
        
        return $this;
    }
    
    public function initCustomerItem($customer)
    {
        $this->addData(
            [
                'store_id'  => $customer->getStoreId(),
                'email'  => $customer->getEmail(),
                'customer_id' => $customer->getId(),
                'customer_name' => $customer->getFirstname(). ' ' .$customer->getLastname()
            ]
        );
        
        return $this;
    }
    
    protected function _getCoupon($rule, $schedule)
    {
        $coupon = array(
            'code' => null,
            'id' => null
        );

        if ($schedule->getUseRule()){
            $salesCoupon = $this->_generateCouponPool($rule);
            $coupon['id'] = $salesCoupon->getId();
            $coupon['code'] = $salesCoupon->getCode();

        } else if ($rule) {
            $coupon['code'] = $rule->getCouponCode();
        }

        return $coupon;
    }

    protected function _generateCouponPool(\Magento\SalesRule\Model\Rule $rule)
    {
        $salesCoupon = null;

        $generator = $rule->getCouponCodeGenerator();

        $generator = $this->objectManager
            ->create('Magento\SalesRule\Model\Coupon\Massgenerator');

        $generator->setData(array(
            'rule_id' => $rule->getId(),
            'qty' => 1,
            'length' => 12,
            'format' => 'alphanum',
            'prefix' => '',
            'suffix' => '',
            'dash' => '0',
            'uses_per_coupon' => '0',
            'uses_per_customer' => '0',
            'to_date' => '',
        ));

        $generator->generatePool();
        $generated = $generator->getGeneratedCount();

        $resourceCoupon = $this->objectManager
            ->create('Magento\SalesRule\Model\ResourceModel\Coupon\Collection');

        $resourceCoupon
            ->addFieldToFilter('main_table.rule_id', $rule->getId())
            ->getSelect()
            ->joinLeft(
                array('h' => $resourceCoupon->getTable('amasty_amfollowup_history')),
                'main_table.coupon_id = h.coupon_id',
                array()
            )->where('h.history_id is null')
            ->order('main_table.coupon_id desc')
            ->limit(1);

        $items = $resourceCoupon->getItems();

        if (count($items) > 0){
            $salesCoupon = end($items);
        }

        return $salesCoupon;
    }
    
    public function createItem($schedule, $createdAt = null, $scheduledAt = null)
    {
        
        $rule = $this->_getRule($schedule);
        $coupon = $this->_getCoupon($rule, $schedule);
        
        $createdAt =  $createdAt
            ? $createdAt
            : $this->_dateTime->formatDate($this->_basicFactory->getCurrentExecution());

        $scheduledAt = $scheduledAt
            ? strtotime($scheduledAt) + $schedule->getDelayedStart()
            : strtotime($createdAt) + $schedule->getDelayedStart();

        $this->addData(array(
           'public_key' => uniqid(),
           'schedule_id' => $schedule->getId(),
           'rule_id' => $schedule->getRuleId(),
           'created_at' => $createdAt,
           'scheduled_at' => $this->_dateTime->formatDate($scheduledAt),
           'status' => self::STATUS_PENDING,
           'sales_rule_id' => $rule ? $rule->getId() : null,
           'coupon_code' => $coupon['code'],
           'coupon_id' => $coupon['id'],
           'coupon_to_date' => $rule ? $rule->getToDate() : null,
           
        ));

        $this->save();
        
        return $this;
    }
    
    public function saveEmail($email = array())
    {
        $this->addData(array(
            'subject' => $email['subject'],
            'body' => $email['body'],
        ));
        $this->save();
        
        return $this;
    }

    protected function _getRule($schedule)
    {
        
        $rule = null;
        if ($schedule->getUseRule()) {
            $rule = $this->objectManager
                ->create('Magento\SalesRule\Model\Rule')
                ->load($schedule->getSalesRuleId());

        } else if ($schedule->getCouponType()) {
            $store = $this->_storeManager->getStore($this->getStoreId());
            $rule = $this->objectManager
                ->create('\Amasty\Followup\Model\Rule')
                ->load($schedule->getRuleId());

            $rule = $this->_createCoupon(
                    $store, 
                    $schedule,
                    $rule
            );
        }
        
        return $rule;
        
    }

    protected function _getCouponToDate($days, $delayedStart){
        return $this->_dateTime->formatDate(
            $this->_date->gmtTimestamp()
            + $days * 24 * 3600
            + $delayedStart
        );
    }

    protected function _createCoupon($store, $schedule, $rule){
        $salesRule = $this->objectManager
            ->create('Magento\SalesRule\Model\Rule');

        $salesRule->setData(array(
            'name' => 'Amasty: Followup Coupon #' . $this->getEmail(),
            'is_active' => '1',
            'website_ids' => array(0 => $store->getWebsiteId()),
            'customer_group_ids' => $this->_getGroupsIds($rule),
            'coupon_code' => strtoupper(uniqid()),
            'uses_per_coupon' => 1,
            'coupon_type' => 2,
            'from_date' => '',
            'to_date' => $this->_getCouponToDate($schedule->getExpiredInDays(), $schedule->getDeliveryTime()),
            'uses_per_customer' => 1,
            'simple_action' => $schedule->getCouponType(),
            'discount_amount' => $schedule->getDiscountAmount(),
            'stop_rules_processing' => '0',
            'from_date' => '',
        ));

        if ($schedule->getDiscountQty() > 0){
            $salesRule->setDiscountQty($schedule->getDiscountQty());
        }

        if ($schedule->getDiscountStep() > 0){
            $salesRule->setDiscountStep($schedule->getDiscountStep());
        }

        $salesRule->setConditionsSerialized($this->serializer->serialize($this->_getConditions($rule)));

        $salesRule->save();

        return $salesRule;
    }

    protected function _getConditions(\Amasty\Followup\Model\Rule $rule)
    {
        $salesRuleConditions = [];
        $conditions = $rule->getSalesRule()->getConditions()->asArray();

        if (isset($conditions['conditions'])){
            foreach($conditions['conditions'] as $idx => $condition)
            {
                $salesRuleConditions[] = $condition;
            }
        }

        return array(
            'type'       => 'Magento\SalesRule\Model\Rule\Condition\Combine',
            'attribute' => '',
            'operator' => '',
            'value'      => '1',
            'is_value_processed' => '',
            'aggregator' => 'all',
            'conditions' => $salesRuleConditions
        );
    }

    protected function _getGroupsIds(\Amasty\Followup\Model\Rule $rule)
    {
        $groupsIds = [];
        $strGroupIds = $rule->getCustGroups();

        if (!empty($strGroupIds)){
            $groupsIds = explode(',', $strGroupIds);
        } else {
            foreach($this->_groupRepository->getList($this->_searchCriteriaBuilder->create())
                        ->getItems() as $group){
                $groupsIds[] = $group->getId();
            }
        }

        return $groupsIds;

    }
    
    public function massCancel($ids){
        $collection = $this->getCollection()
            ->addFieldToFilter('history_id', array('in' => $ids));
        foreach($collection as $history){
            $history->reason = Amasty_Followup_Model_History::REASON_ADMIN;
            $history->status = Amasty_Followup_Model_History::STATUS_CANCEL;
            $history->save();
        }
    }

    protected function validateBlacklist($history){
        
        if (!isset($this->_cancelBlacklistValidation[$history->getEmail()])){
            $blist = $this->objectManager
                ->create('Amasty\Followup\Model\Blacklist')
                ->load($history->getEmail(), 'email');

            $this->_cancelBlacklistValidation[$history->getEmail()] = $blist->getId() === null;
        }
        
        return $this->_cancelBlacklistValidation[$history->getEmail()];
    }
    
    protected function validateNotSubscribed($rule, $history){
        if (!isset($this->_cancelNotSubscribedValidation[$rule->getId()]))
            $this->_cancelNotSubscribedValidation[$rule->getId()] = array();
        
        if (!isset($this->_cancelNotSubscribedValidation[$rule->getId()][$history->getCustomerId()])){
            $subscriber = $this->objectManager
                ->create('Magento\Newsletter\Model\Subscriber')
                ->load($history->getCustomerId(), 'customer_id');

            $this->_cancelNotSubscribedValidation[$rule->getId()][$history->getCustomerId()] =
                    $subscriber->getSubscriberStatus() != \Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED;
        }
        
        return $this->_cancelNotSubscribedValidation[$rule->getId()][$history->getCustomerId()];
    }

    protected function validateCancelEvent($rule, $history){
        if (!isset($this->_cancelEventValidation[$rule->getId()]))
            $this->_cancelEventValidation[$rule->getId()] = array();
        
        if (!isset($this->_cancelEventValidation[$rule->getId()][$history->getEmail()])){
            $cancelEvents = $rule->getCancelEvents();
            if ($cancelEvents) {
                foreach($cancelEvents as $event){
                    if ($event->validate($history)) {
                        $this->_cancelEventValidation[$rule->getId()][$history->getEmail()] = true;
                        break;
                    } else {
                        $this->_cancelEventValidation[$rule->getId()][$history->getEmail()] = false;
                    }
                }
            }
        }
        
        if (!isset($this->_cancelEventValidation[$rule->getId()][$history->getEmail()]))
            $this->_cancelEventValidation[$rule->getId()][$history->getEmail()] = false;
        
        return $this->_cancelEventValidation[$rule->getId()][$history->getEmail()];
    }
    
    public function validateBeforeSent($rule){
        if (!$this->validateBlacklist($this)){
            $this->setReason(self::REASON_BLACKLIST);
        } else if ($rule->getToSubscribers() && $this->validateNotSubscribed($rule, $this)){
            $this->setReason(self::REASON_NOT_SUBSCRIBED);
        } else if ($this->validateCancelEvent($rule, $this)){
            $this->setReason(self::REASON_EVENT);
        }
        
        return !$this->getReason();
    }
    
    public function cancelItem(){
        $this->setStatus(self::STATUS_CANCEL);
        $this->save();
    }

    public function getStore($storeId = null)
    {
        if (!$storeId){
            $storeId = $this->getStoreId();
        }

        if (!$this->_store) {
            $this->_store = $this->_storeManager->getStore($storeId);
        }

        return $this->_store;
    }
}
