<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Model;

class Indexer extends \Magento\Framework\DataObject
{
    protected $_scheduleFactory;

    protected $_basicFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    public function __construct(
        \Amasty\Followup\Model\ScheduleFactory $scheduleFactory,
        \Amasty\Followup\Model\Event\BasicFactory $basicFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->_scheduleFactory = $scheduleFactory;
        $this->_basicFactory = $basicFactory;
        $this->objectManager = $objectManager;
        parent::__construct($data);
    }

    function run($cronExecution = false)
    {
        $basic = $this->_basicFactory->create();
        $basic->clear();

        $this->_prepareOrderRules();
        $this->_prepareCustomerRules();

        $this->_scheduleFactory->create()->_process();
        $basic->getFlag()->save();
    }

    public function _prepareOrderRules()
    {
        $schedule = $this->_scheduleFactory->create();

        $ruleCollection = $schedule->_getRuleCollection(
            [
                Rule::TYPE_ORDER_NEW,
                Rule::TYPE_ORDER_SHIP,
                Rule::TYPE_ORDER_INVOICE,
                Rule::TYPE_ORDER_COMPLETE,
                Rule::TYPE_ORDER_CANCEL
            ]
        );

        foreach ($ruleCollection as $rule) {
            $event = $rule->getStartEvent();

            $quoteCollection = $event->getCollection();
            $quotes = $quoteCollection->getItems();

            foreach ($quotes as $quote) {
                if ($event->validate($quote)) {
                    $order = $this->objectManager
                        ->create('Magento\Sales\Model\Order')->load($quote->getOrderId());

                    $customer = $this->objectManager
                        ->create('Magento\Customer\Model\Customer')->load($quote->getCustomerId());

                    $schedule->createOrderHistory($rule, $event, $order, $quote, $customer);
                }
            }
        }
    }

    protected function _prepareCustomerRules()
    {
        $schedule = $this->_scheduleFactory->create();

        $ruleCollection = $schedule->_getRuleCollection(
            [
                Rule::TYPE_CUSTOMER_ACTIVITY,
                Rule::TYPE_CUSTOMER_BIRTHDAY,
                Rule::TYPE_CUSTOMER_DATE,
                Rule::TYPE_CUSTOMER_WISHLIST,
            ]
        );

        foreach ($ruleCollection as $rule) {

            $event = $rule->getStartEvent();

            $customerCollection = $event->getCollection();

            foreach ($customerCollection as $customer) {

                if ($event->validate($customer)) {
                    $schedule->createCustomerHistory($rule, $event, $customer);
                }
            }
        }
    }
}
