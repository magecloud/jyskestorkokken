<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

use Magento\Framework\App\Config\ScopeConfigInterface;


class SalesruleValidatorProcess implements ObserverInterface
{
    protected $_historyFactory;
    protected $_scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Amasty\Followup\Model\HistoryFactory $historyFactory
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_historyFactory = $historyFactory;
    }

    public function execute(EventObserver $observer)
    {
        if ($this->_scopeConfig->getValue('amfollowup/general/customer_coupon'))
        {
            $salesRule = $observer->getEvent()->getRule();

            $history = $this->_historyFactory->create()->load($salesRule->getId(), 'sales_rule_id');

            if ($history->getId()){
                $customerEmail = $observer->getEvent()->getQuote()->getCustomer()->getEmail() ?
                    $observer->getEvent()->getQuote()->getCustomer()->getEmail() :
                    $observer->getEvent()->getQuote()->getBillingAddress()->getEmail();

                if ($customerEmail != $history->getEmail())
                {
                    $observer->getEvent()->getQuote()->setCouponCode("");
                }
            }
        }
    }
}