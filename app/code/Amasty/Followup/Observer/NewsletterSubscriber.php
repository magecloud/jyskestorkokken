<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Amasty\Followup\Model\Rule as Rule;

class NewsletterSubscriber implements ObserverInterface
{
    protected $_scheduleFactory;
    protected $_customerFactory;
    protected static $_onNewsletterSubscriberSaveAfterChecked = false;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    public function __construct(
        \Amasty\Followup\Model\ScheduleFactory $scheduleFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->_scheduleFactory = $scheduleFactory;
        $this->_customerFactory = $customerFactory;
        $this->date = $date;
    }

    public function execute(EventObserver $observer)
    {
        $subscriber = $observer->getSubscriber();
        if ($subscriber->getChangeStatusAt()
            && $subscriber->getSubscriberStatus() == \Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED
        ) {
            self::$_onNewsletterSubscriberSaveAfterChecked = true;
            return;
        }
        if (!self::$_onNewsletterSubscriberSaveAfterChecked) {
            $customer = $this->_customerFactory->create();
            if (!$subscriber->getCustomerId()){
                $customer->addData(array(
                    "email" => $subscriber->getSubscriberEmail(),
                    "store_id" => $subscriber->getStoreId(),
                ));

            } else {
                $customer = $customer->load($subscriber->getCustomerId());
            }

            if($subscriber->getChangeStatusAt() &&
                $subscriber->getSubscriberStatus() == \Magento\Newsletter\Model\Subscriber::STATUS_UNSUBSCRIBED
            ) {
                $subscriber->setSubscriberStatus(\Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED);
            }

            $this->_scheduleFactory->create()->checkSubscribtionRules($subscriber, $customer, array(
                Rule::TYPE_CUSTOMER_SUBSCRIPTION
            ));

            self::$_onNewsletterSubscriberSaveAfterChecked = true;
            $subscriber->setChangeStatusAt($this->date->date("Y-m-d H:i:s"));
            $subscriber->save();

        }
    }
}