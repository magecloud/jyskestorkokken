<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Amasty\Followup\Model\Rule as Rule;

class CustomerSaveBefore implements ObserverInterface
{
    protected $_scheduleFactory;
    protected $_onCustomerChecked = false;

    public function __construct(
        \Amasty\Followup\Model\ScheduleFactory $scheduleFactory
    ) {
        $this->_scheduleFactory = $scheduleFactory;
    }

    public function execute(EventObserver $observer)
    {
        $customer = $observer->getCustomer();

        if (!$this->_onCustomerChecked) {
            $customer->setData('target_ceated_at', $customer->getCreatedAt());

            $this->_scheduleFactory->create()->checkCustomerRules($customer, array(
                Rule::TYPE_CUSTOMER_GROUP,
                Rule::TYPE_CUSTOMER_NEW
            ));
            $this->_onCustomerChecked = true;
        }
    }
}