<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Helper;

use Amasty\Followup\Model\Rule as Rule;
use Magento\Email\Model\ResourceModel\Template;
use \Magento\Sales\Model\Order as SalesOrder;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CONFIG_PATH_GENERAL_BIRTHDAY_OFFSET = 'amfollowup/general/birthday_offset';
    const AMASTY_SEGMENT_MODULE_DEPEND_NAMESPACE = 'Amasty_Segments';

    protected $coreRegistry;
    protected $_objectManager;
    protected $_scopeConfig;

    /**
     * @var Template
     */
    private $templateResourceModel;

    /**
     * @var Template\CollectionFactory
     */
    private $templateCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Email\Model\ResourceModel\Template $templateResourceModel,
        \Magento\Framework\Registry $registry,
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $templateCollectionFactory
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context);
        $this->_objectManager = $objectManager;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->templateResourceModel = $templateResourceModel;
        $this->templateCollectionFactory = $templateCollectionFactory;
    }

    public function getEventTypes()
    {
        return
            array(
                array(
                    'label' => __('Order'),
                    'value' => array
                    (
                        array
                        (
                            'label' => 'Created',
                            'value' => Rule::TYPE_ORDER_NEW
                        ),
                        array
                        (
                            'label' => 'Shipped',
                            'value' => Rule::TYPE_ORDER_SHIP
                        ),
                        array
                        (
                            'label' => 'Invoiced',
                            'value' => Rule::TYPE_ORDER_INVOICE
                        ),
                        array
                        (
                            'label' => 'Completed',
                            'value' => Rule::TYPE_ORDER_COMPLETE
                        ),
                        array
                        (
                            'label' => 'Cancelled',
                            'value' => Rule::TYPE_ORDER_CANCEL
                        ),
                    )
                ),
                array(
                    'label' => __('Customer'),
                    'value' => array
                    (
                        array
                        (
                            'label' => 'No Activity',
                            'value' => Rule::TYPE_CUSTOMER_ACTIVITY
                        ),
                        array
                        (
                            'label' => 'Changed Group',
                            'value' => Rule::TYPE_CUSTOMER_GROUP
                        ),
                        array
                        (
                            'label' => 'Subscribed  to Newsletter',
                            'value' => Rule::TYPE_CUSTOMER_SUBSCRIPTION
                        ),
                        array
                        (
                            'label' => 'Birthday',
                            'value' => Rule::TYPE_CUSTOMER_BIRTHDAY
                        ),
                        array
                        (
                            'label' => 'Registration',
                            'value' => Rule::TYPE_CUSTOMER_NEW
                        ),
                    )
                ),
                array(
                    'label' => __('Wishlist'),
                    'value' => array
                    (
                        array
                        (
                            'label' => 'Product Added',
                            'value' => Rule::TYPE_CUSTOMER_WISHLIST
                        ),
                        array
                        (
                            'label' => 'Shared',
                            'value' => Rule::TYPE_CUSTOMER_WISHLIST_SHARED
                        ),
                        array
                        (
                            'label' => 'Wishlist on sale',
                            'value' => Rule::TYPE_CUSTOMER_WISHLIST_SALE
                        ),
                        array
                        (
                            'label' => 'Wishlist back in stock',
                            'value' => Rule::TYPE_CUSTOMER_WISHLIST_BACK_INSTOCK
                        ),
                    )
                ),
                array(
                    'label' => __('Date'),
                    'value' => array
                    (
                        array
                        (
                            'label' => 'Date',
                            'value' => Rule::TYPE_CUSTOMER_DATE
                        )
                    )
                )
            );
    }

    public function getCancelTypes($useOrderEvents = FALSE){
        $otherEvents = array();

        if ($useOrderEvents)
            $otherEvents = array_merge ($this->getOrderCancelEvents(), $otherEvents);

        return array_merge(array(

            Rule::TYPE_CANCEL_CUSTOMER_LOGGEDIN => __('Customer logged in'),
            Rule::TYPE_CANCEL_ORDER_COMPLETE => __('New Order Placed'),
            Rule::TYPE_CANCEL_CUSTOMER_CLICKLINK => __('Customer clicked on a link in the email '),
            Rule::TYPE_CANCEL_CUSTOMER_WISHLIST_SHARED => __('Customer wishlist shared'),
        ), $otherEvents);
    }

    public function getOrderCancelEvents(){
        $ret = array();
        $state = [
            SalesOrder::STATE_PROCESSING,
            SalesOrder::STATE_COMPLETE,
            SalesOrder::STATE_CLOSED,
            SalesOrder::STATE_CANCELED
        ];
        $orderStatusCollection = $this->_objectManager
            ->create('Magento\Sales\Model\ResourceModel\Order\Status\Collection')
            ->joinStates()
            ->addFieldToFilter('state_table.state', array('in' => $state))
            ->addFieldToFilter('state_table.is_default', array('eq' => 1));

        foreach($orderStatusCollection as $status){
            $ret[$this->getOrderCancelEventKey($status)] = __('Order Becomes: %1', $status->getLabel());
        }

        return $ret;
    }

    public function getOrderCancelEventKey($status){
        return Rule::TYPE_CANCEL_ORDER_STATUS . $status->getStatus();
    }

    public function getScopeValue($path, $scoreCode = null){
        return $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $scoreCode);
    }

    public function createTemplate($templateCode, $templateLabel) {

            $template = $this->_objectManager
                ->create('Magento\Email\Model\Template');

            $template->setForcedArea($templateCode);
            $template->loadDefault($templateCode);
            $template->setData('orig_template_code', $templateCode);
            $template->setData('template_variables', \Zend_Json::encode($template->getVariablesOptionArray(true)));
            $template->setData('template_code', $templateLabel);
            $template->setTemplateType(\Magento\Email\Model\Template::TYPE_HTML);
            $template->setId(NULL);
        if (!$this->templateResourceModel->checkCodeUsage($template)) {
            $template->save();
        }
    }

    /**
     * @param $type
     * @return $this
     */
    public function getEmailTemplatesCollection($type){
        $collection = $this->templateCollectionFactory->create()
            ->addFieldToFilter(
                "orig_template_code",
                [
                    'like' => "%amfollowup_" . $type . "%"
                ]
            );

        if ($type == "customer_wishlist") {
            $collection->addFieldToFilter(
                "orig_template_code",
                ['nlike' => "%amfollowup_" . $type . "_shared%"]
            )->addFieldToFilter(
                "orig_template_code",
                ['nlike' => "%amfollowup_" . $type . "_sale%"]
            )->addFieldToFilter(
                "orig_template_code",
                ['nlike' => "%amfollowup_" . $type . "_back_instock%"]
            );
        }

        $collection->load();

        return $collection;
    }

    public function getDays($timestamp){
        return $timestamp > 0 && floor($timestamp / 24 / 60 / 60) ?
            floor($timestamp / 24 / 60 / 60) :
            NULL;
    }

    public function getHours($timestamp){
        $days = $this->getDays($timestamp);
        $time = $timestamp - ($days * 24 * 60 * 60);

        return $time > 0 ?
            floor($time / 60 / 60) :
            NULL;
    }

    public function getMinutes($timestamp){
        $days = $this->getDays($timestamp);
        $hours = $this->getHours($timestamp);
        $time = $timestamp - ($days * 24 * 60 * 60) - ($hours * 60 * 60);

        return $time > 0 ?
            floor($time / 60) :
            NULL;
    }

}
