<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Controller\Adminhtml\Rule;

class TestEmail extends \Amasty\Followup\Controller\Adminhtml\Rule
{

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $ruleId = $this->getRequest()->getParam('rule_id');

        $rule = $this->_objectManager->create('Amasty\Followup\Model\Rule')->load($ruleId);

        if ($rule->isOrderRelated()){
            $this->testOrderRule($rule);
        } else {
            $this->testCustomerRule($rule);
        }

    }

    protected function testCustomerRule($rule) {
        $customerId = $this->getRequest()->getParam('id');
        $customer = $this->_objectManager->create('Magento\Customer\Model\Customer')->load($customerId);

        if ($rule->getId() && $customer->getId()){
            $schedule = $this->_objectManager->create('Amasty\Followup\Model\Schedule');

            $event = $rule->getStartEvent();

            $historyItems = $schedule->createCustomerHistory($rule, $event, $customer);

            foreach ($historyItems as $history)
                $history->processItem($rule, null, true);
        }
    }

    protected function testOrderRule($rule) {
        $orderId = $this->getRequest()->getParam('id');
        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($orderId);

        $quote = $this->_objectManager->create('Magento\Quote\Model\Quote')->loadByIdWithoutStore($order->getQuoteId());
        $customer = $this->_objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());

        if ($rule->getId() && $order->getId() && $quote->getId()){
            $schedule = $this->_objectManager->create('Amasty\Followup\Model\Schedule');

            $event = $rule->getStartEvent();

            $historyItems = $schedule->createOrderHistory($rule, $event, $order, $quote, $customer);
            
            foreach ($historyItems as $history)
                $history->processItem($rule, null, true);
        }
    }
}
