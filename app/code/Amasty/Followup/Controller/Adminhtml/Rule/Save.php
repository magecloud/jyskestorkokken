<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

namespace Amasty\Followup\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;
use Psr\Log\LoggerInterface;

class Save extends \Amasty\Followup\Controller\Adminhtml\Rule
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Amasty\Base\Model\Serializer
     */
    private $serializer;
    /**
     * @var \Amasty\Followup\Helper\Data
     */
    private $helper;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param LoggerInterface $logger
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Amasty\Base\Model\Serializer $serializer
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Amasty\Base\Model\Serializer $serializer,
        \Amasty\Followup\Helper\Data $helper
    ) {
        $this->date = $date;
        $this->serializer = $serializer;
        $this->helper = $helper;
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $resultForwardFactory,
            $logger
        );
    }

    /**
     * execute
     */
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            $data = $this->getRequest()->getPostValue();

            try {
                $model = $this->_objectManager->create('Amasty\Followup\Model\Rule');
                $id = $this->getRequest()->getParam('rule_id');

                if ($id) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        throw new \Magento\Framework\Exception\LocalizedException(__('The wrong rule is specified.'));
                    }
                } else {
                    $data['schedule'] = [$this->getDefaultScheduleValue($data)];
                }

                if (isset($data['rule']) && isset($data['rule']['conditions'])) {
                    $data['conditions'] = $data['rule']['conditions'];

                    unset($data['rule']);

                    $salesRule = $this->_objectManager->create('Amasty\Followup\Model\SalesRule');
                    $salesRule->loadPost($data);

                    $data['conditions_serialized'] = $this->serializer->serialize(
                        $salesRule->getConditions()->asArray()
                    );

                    unset($data['conditions']);
                }

                if (isset($data['store_ids']) && is_array($data['store_ids'])) {
                    $data['stores'] = implode(',', $data['store_ids']);
                } else {
                    $data['stores'] = '';
                }

                if (isset($data['segments_ids']) && is_array($data['segments_ids'])) {
                    $data['segments'] = implode(',', $data['segments_ids']);
                } else {
                    $data['segments'] = '';
                }

                if (isset($data['customer_group_ids']) && is_array($data['customer_group_ids'])) {
                    $data['cust_groups'] = implode(',', $data['customer_group_ids']);
                } else {
                    $data['cust_groups'] = '';
                }

                if (isset($data['customer_date_event'])) {
                    $data['customer_date_event'] = $this->date->date("Y-m-d H:i:s", $data['customer_date_event']);
                }

                $model->setData($data);
                $this->prepareForSave($model);
                $this->_session->setPageData($model->getData());
                $model->save();
                $id = $model->getRuleId();

                if (isset($data['schedule'])) {
                    $model->setSchedule($data['schedule']);
                }

                if ($model->getSchedule()) {
                    $model->saveSchedule();
                } elseif ($id) {
                    $this->messageManager->addWarningMessage(
                        __('Please set Schedule.')
                    );

                    $this->_session->setPageData($data);
                    return $this->_redirect('amasty_followup/*/edit', ['id' => $model->getId()]);
                }

                $this->messageManager->addSuccess(__('You saved the rule.'));
                $this->_session->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('amasty_followup/*/edit', ['id' => $model->getId()]);
                    return;
                }
                $this->_redirect('amasty_followup/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
                $id = (int)$this->getRequest()->getParam('rule_id');
                if (!empty($id)) {
                    $this->_redirect('amasty_followup/*/edit', ['id' => $id]);
                } else {
                    $this->_redirect('amasty_followup/*/new');
                }
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('Something went wrong while saving the rule data. Please review the error log.')
                );
                $this->logger->critical($e);
                $this->_session->setPageData($data);
                $this->_redirect('amasty_followup/*/edit', ['id' => $this->getRequest()->getParam('rule_id')]);
                return;
            }
        }
        $this->_redirect('amasty_followup/*/');
    }

    public function prepareForSave($model)
    {
        $fields = array('methods', 'cancel_event_type');
        foreach ($fields as $f){
            // convert data from array to string
            $val = $model->getData($f);
            $model->setData($f, '');

            if (is_array($val)){
                // need commas to simplify sql query
                $model->setData($f, ',' . implode(',', $val) . ',');
            }
        }

        return true;
    }

    /**
     * @param array $data
     * @return array
     */
    private function getDefaultScheduleValue($data)
    {
        $emailTemplateId = isset($data['start_event_type'])
            ? $this->helper->getEmailTemplatesCollection($data['start_event_type'])->getFirstItem()->getId() : '';

        return [
            'schedule_id' => '',
            'email_template_id' => $emailTemplateId,
            'delivery_time' => [
                'days' => '',
                'hours' => '',
                'minutes' => '5'
            ],
            'coupon' => [
            ]
        ];
    }
}
