<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


namespace Amasty\Followup\Controller\Adminhtml\Rule;

class Grid extends \Amasty\Followup\Controller\Adminhtml\Rule
{

    public function execute()
    {
        $ruleId = (int)$this->getRequest()->getParam('id');

        $rule = $this->_objectManager->create('Amasty\Followup\Model\Rule');
        $isExistingRule = (bool)$ruleId;

        if ($isExistingRule) {
            $rule = $rule->load($ruleId);
            if (!$rule->getId()) {
                $this->messageManager->addError(__('Something went wrong while editing the rule.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('amasty_followup/*/index');
                return $resultRedirect;
            }
        }

        $this->initCurrentRule($rule);

        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
