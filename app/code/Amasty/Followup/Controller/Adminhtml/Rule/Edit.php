<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

namespace Amasty\Followup\Controller\Adminhtml\Rule;

use Magento\Framework\Exception\NoSuchEntityException;

class Edit extends \Amasty\Followup\Controller\Adminhtml\Rule
{

    public function execute()
    {
        $ruleId = (int)$this->getRequest()->getParam('id');

        $ruleData = [];

        $rule = $this->_objectManager->create('Amasty\Followup\Model\Rule');
        $isExistingRule = (bool)$ruleId;

        if ($isExistingRule) {
            $rule = $rule->load($ruleId);

            if (!$rule->getId()) {
                $this->messageManager->addError(__('Something went wrong while editing the rule.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('amasty_followup/*/index');
                return $resultRedirect;
            }
        }
        
        $this->initCurrentRule($rule);

        /*$rule->getSalesRule()
            ->getConditions()->setJsFormObject('rule_conditions_fieldset');*/

        $ruleData['rule_id'] = $ruleId;

        $this->_getSession()->setRuleData($ruleData);

        $resultPage = $this->resultPageFactory->create();
        $this->prepareDefaultCustomerTitle($resultPage);
        if ($isExistingRule) {
            $resultPage->getConfig()->getTitle()->prepend($rule->getName());
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Rule'));
        }

        return $resultPage;
    }
}
