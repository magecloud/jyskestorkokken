<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

namespace Amasty\Followup\Block\Adminhtml\Rule\Edit\Tab\Test;

use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Order extends \Magento\Reports\Block\Adminhtml\Grid\Shopcart
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ){
        parent::__construct($context, $backendHelper);

        $this->setId('amasty_followup_rule_test');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);

        $this->objectManager = $objectManager;
    }

    protected function _prepareCollection()
    {
        $collection = $this->objectManager
            ->create('Magento\Sales\Model\ResourceModel\Order\Grid\Collection');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _addColumns()
    {
        $this->addColumn('run', array(
            'header'    => '',
            'index'     =>'customer_id',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'Amasty\Followup\Block\Adminhtml\Rule\Edit\Tab\Test\Renderer\Run',
            'align'     => 'center',
        ));

        $this->addColumn('real_order_id', array(
            'header'=> __('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        if (!$this->_storeManager->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => __('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }

        $this->addColumn('created_at', array(
            'header' => __('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('billing_name', array(
            'header' => __('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => __('Ship to Name'),
            'index' => 'shipping_name',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => __('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));

        $this->addColumn('grand_total', array(
            'header' => __('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));

        $statuses = $this->objectManager
            ->create('Magento\Sales\Model\Order\Config');

        $this->addColumn('status', array(
            'header' => __('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => $statuses->getStatuses(),
        ));
    }

    protected function _prepareColumns()
    {
        $this->_addColumns();

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }
}