<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ProductAttachment
 */


namespace Amasty\ProductAttachment\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Amasty\ProductAttachment\Model\Modifier;
use Magento\Downloadable\Helper\File;
use Amasty\ProductAttachment\Model\File as ModelFile;
use Magento\Framework\Escaper;

class AttachmentsPanel extends AbstractModifier
{
    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var Modifier
     */
    private $modifier;

    /**
     * @var File
     */
    public $downloadableFile;

    /**
     * @var ModelFile
     */
    public $fileModel;

    /**
     * @var Escaper
     */
    public $escaper;

    public function __construct(
        LocatorInterface $locator,
        Modifier $modifier,
        ModelFile $fileModel,
        File $downloadableFile,
        Escaper $escaper
    ) {
        $this->locator = $locator;
        $this->modifier = $modifier;
        $this->fileModel = $fileModel;
        $this->downloadableFile = $downloadableFile;
        $this->escaper = $escaper;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        return $this->modifier->modifyMeta($meta);
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        $model = $this->locator->getProduct();

        $data[$model->getId()]['amasty_product_attachments']['attachments'] = $this->getLinkData();

        return $data;
    }

    /**
     * @return array
     */
    public function getLinkData()
    {
        $linkArr = [];

        $product = $this->locator->getProduct();

        $productId = $product->getId();
        $storeId = $product->getStoreId();

        /**
         * @var \Amasty\ProductAttachment\Model\ResourceModel\File\Collection $fileCollection
         */
        $files = $this->fileModel->getCollection();
        $files->getFilesAdminByProductId($productId,$storeId);
        $fileHelper = $this->downloadableFile;
        /**
         * TODO: Magento bug, customer group id 0 don't selected in form by default
         */
        foreach ($files as $item) {
            /**
             * @var \Amasty\ProductAttachment\Model\File $item
             */
            $tmpLinkItem = [
                'id'                          => $item->getFileId(),
                'label'                       => $this->escaper->escapeHtml(
                    $item->getLabel()
                ),
                'use_default_label'           => $item->getLabelIsDefault() ? '1' : '0',
                'file_name'                   => $this->escaper->escapeHtml(
                    $item->getFileName()
                ),
                'customer_group'              => $item->getCustomerGroups(),
                'use_default_customer_group'  => $item->getCustomerGroupIsDefault() ? '1' : '0',
                'file_type'                   => $item->getFileType(),
                'file_url'                    => $item->getFileUrl(),
                'show_for_ordered'            => $item->getShowForOrdered(),
                'use_default_show_for_ordered'=> $item->getShowForOrderedIsDefault() ? '1' : '0',
                'is_visible'                  => $item->getIsVisible(),
                'use_default_is_visible'      => $item->getIsVisibleIsDefault() ? '1' : '0',
                'position'                    => $item->getPosition(),
            ];

            $linkFile = $item->getFilePath();
            if ($linkFile) {
                $file = $fileHelper->getFilePath($this->fileModel->getBasePath(), $linkFile);

                $fileExist = $fileHelper->ensureFileInFilesystem($file);

                if ($fileExist) {
                    $tmpLinkItem['file'] = [
                        [
                            'file' => $linkFile,
                            'name' => $item->getFileName(),
                            'size' => $fileHelper->getFileSize($file),
                            'status' => 'old',
                            'url'  => $this->modifier->getDownloadUrl($item->getFileId())
                        ],
                    ];
                }
            }

            $linkArr[] = $tmpLinkItem;
        }

        return $linkArr;
    }
}
