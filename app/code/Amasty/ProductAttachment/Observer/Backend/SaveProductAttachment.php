<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ProductAttachment
 */

namespace Amasty\ProductAttachment\Observer\Backend;

use Magento\Framework\Event\ObserverInterface;

class SaveProductAttachment implements ObserverInterface
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Amasty\ProductAttachment\Model\ResourceModel\File\Collection
     */
    private $fileCollection;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Message\Manager $messageManager
     * @param \Amasty\ProductAttachment\Model\ResourceModel\File\Collection $fileCollection
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Message\Manager $messageManager,
        \Amasty\ProductAttachment\Model\ResourceModel\File\Collection $fileCollection
    ) {
        $this->objectManager = $objectManager;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
        $this->fileCollection = $fileCollection;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controller = $observer->getController();

        if ($this->isProductController($controller)) {
            $productId = $observer->getProduct()->getId();
            $this->saveFilesData($controller->getRequest(), $productId);
        }

        if (!$controller && $observer->getCategory()) {
            $categoryId = $observer->getCategory()->getId();
            $this->saveFilesData($observer->getRequest(), $categoryId, true);
        }
    }

    private function isProductController($controller)
    {
        return $controller instanceof \Magento\Catalog\Controller\Adminhtml\Product\Save;
    }

    /**
     * @param \Magento\Framework\App\Request\Http $request
     * @param string $elementId
     * @param bool $isCategory
     */
    private function saveFilesData($request, $elementId, $isCategory = false)
    {
        $attachDataFromForm = $request->getParam(
            'amasty_product_attachments', []
        );
        $storeId = $request->getParam('store', 0);
        if (!$isCategory) {
            $files = $this->fileCollection->getFilesByProductAndStore($elementId, $storeId);
            $id = 'product_id';
        } else {
            $files = $this->fileCollection->getFilesByCategoryAndStore($elementId, $storeId);
            $id = 'category_id';
        }
        $filesFromForm = [];
        if (isset($attachDataFromForm['attachments'])) {
            foreach ($attachDataFromForm['attachments'] as $fileData) {
                $fileModel = $this->createFileModel();
                try {
                    $fileData[$id] = $elementId;
                    $file = $fileModel->saveProductAttachment($fileData, $storeId);
                    $filesFromForm[] = $file->getId();
                } catch (\Exception $e) {
                    $this->addErrors($e);
                    $this->rollbackCreateFile($fileModel);
                }
            }
        }
        /** @var \Amasty\ProductAttachment\Model\File $file */
        foreach ($files as $file) {
            if (!in_array($file->getId(), $filesFromForm)) {
                try {
                    $file->delete();
                } catch (\Exception $e) {
                    $this->addErrors($e);
                }
            }
        }
    }

    /**
     * @param \Amasty\ProductAttachment\Model\File $fileModel
     */
    private function rollbackCreateFile($fileModel)
    {
        if ($fileModel->isObjectNew()) {
            $fileModel->delete();
        }
    }

    private function addErrors($errors)
    {
        foreach ($errors as $error) {
            $this->messageManager->addError($error);
        }
    }

    /**
     * @return \Amasty\ProductAttachment\Model\File
     */
    private function createFileModel()
    {
        return $this->objectManager->create('Amasty\ProductAttachment\Model\File');
    }
}
