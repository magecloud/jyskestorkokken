<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ProductAttachment
 */


namespace Amasty\ProductAttachment\Model;

use Magento\Downloadable\Model\Source\TypeUpload;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form;
use Magento\Ui\Component\Container;
use Magento\Framework\UrlInterface;
use Amasty\ProductAttachment\Model\Config\Source\CustomerGroup;
use Magento\Config\Model\Config\Source\Yesno;
use Amasty\ProductAttachment\Model\File as ModelFile;
use Magento\Downloadable\Helper\File;
use Magento\Framework\Escaper;
use Amasty\ProductAttachment\Helper\Config;

class Modifier
{
    /**
     * @var ArrayManager
     */
    public $arrayManager;

    /**
     * @var Config
     */
    public $attachConfig;

    /**
     * @var CustomerGroup
     */
    public $groupSource;

    /**
     * @var Yesno
     */
    public $yesnoSource;

    /**
     * @var TypeUpload
     */
    public $typeUpload;

    /**
     * @var UrlInterface
     */
    public $urlBuilder;

    /**
     * @var ModelFile
     */
    public $fileModel;

    /**
     * @var File
     */
    public $downloadableFile;

    /**
     * @var Escaper
     */
    public $escaper;

    public function __construct(
        ArrayManager $arrayManager,
        CustomerGroup $groupSource,
        Yesno $yesnoSource,
        TypeUpload $typeUpload,
        UrlInterface $urlBuilder,
        ModelFile $fileModel,
        File $downloadableFile,
        Escaper $escaper,
        Config $attachConfig
    ) {
        $this->arrayManager = $arrayManager;
        $this->attachConfig = $attachConfig;
        $this->groupSource = $groupSource;
        $this->yesnoSource = $yesnoSource;
        $this->typeUpload = $typeUpload;
        $this->urlBuilder = $urlBuilder;
        $this->fileModel = $fileModel;
        $this->downloadableFile = $downloadableFile;
        $this->escaper = $escaper;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $panelConfig['arguments']['data']['config'] = [
            'componentType' => Form\Fieldset::NAME,
            'label' => $this->attachConfig->getBlockLabel() ? $this->attachConfig->getBlockLabel() : __('Product Attachments'),
            'additionalClasses' => 'admin__fieldset-section',
            'collapsible' => true,
            'opened' => false,
            'dataScope' => 'data',
        ];

        // @codingStandardsIgnoreStart
        $information['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/html',
            'additionalClasses' => 'admin__fieldset-note',
            'content' => __('Alphanumeric, dash and underscore characters are recommended for filenames. Improper characters are replaced with \'_\'.'),
        ];
        // @codingStandardsIgnoreEnd

        $panelConfig = $this->arrayManager->set(
            'children',
            $panelConfig,
            [
                'attachments' => $this->getDynamicRows(),
                'information_links' => $information,
            ]
        );

        return $this->arrayManager->set('amasty_product_attachments', $meta, $panelConfig);
    }

    /**
     * @return array
     */
    public function getDynamicRows()
    {
        $dynamicRows['arguments']['data']['config'] = [
            'addButtonLabel' => __('Add New File'),
            'componentType' => DynamicRows::NAME,
            'itemTemplate' => 'record',
            'renderDefaultRecord' => false,
            'columnsHeader' => true,
            'additionalClasses' => 'admin__field-wide',
            'dataScope' => 'amasty_product_attachments',
            'deleteProperty' => 'is_delete',
            'deleteValue' => '1',
        ];

        return $this->arrayManager->set('children/record', $dynamicRows, $this->getRecord());
    }

    /**
     * @return array
     */
    public function getRecord()
    {
        $record['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'isTemplate' => true,
            'is_collection' => true,
            'component' => 'Magento_Ui/js/dynamic-rows/record',
            'dataScope' => '',
        ];
        $recordPosition['arguments']['data']['config'] = [
            'componentType' => Form\Field::NAME,
            'formElement' => Form\Element\Input::NAME,
            'dataType' => Form\Element\DataType\Number::NAME,
            'dataScope' => 'position',
            'visible' => false,
        ];
        $recordActionDelete['arguments']['data']['config'] = [
            'label' => null,
            'componentType' => 'actionDelete',
            'fit' => true,
        ];

        return $this->arrayManager->set(
            'children',
            $record,
            [
                'container_label' => $this->getLabelColumn(),
                'container_file_name' => $this->getFileNameColumn(),
                'container_file' => $this->getFileColumn(),
                'container_customer_group' => $this->getCustomerGroupColumn(),
                'container_show_for_ordered' => $this->getShowOrderedColumn(),
                'container_is_visible' => $this->getIsVisibleColumn(),
                'position' => $recordPosition,
                'action_delete' => $recordActionDelete,
            ]
        );
    }

    /**
     * @return array
     */
    public function getLabelColumn()
    {
        $labelContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('Label'),
            'dataScope' => '',
        ];
        $labelField['arguments']['data']['config'] = [
            'formElement' => Form\Element\Input::NAME,
            'componentType' => Form\Field::NAME,
            'dataType' => Form\Element\DataType\Text::NAME,
            'dataScope' => 'label',
            'validation' => [
                'required-entry' => true,
            ],
        ];

        return $this->arrayManager->set('children/label', $labelContainer, $labelField);
    }

    /**
     * @return array
     */
    public function getFileNameColumn()
    {
        $fileNameContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('File Name'),
            'dataScope' => '',
        ];
        $fileNameField['arguments']['data']['config'] = [
            'formElement' => Form\Element\Input::NAME,
            'componentType' => Form\Field::NAME,
            'dataType' => Form\Element\DataType\Text::NAME,
            'dataScope' => 'file_name',
            'validation' => [
                'required-entry' => true,
            ],
        ];

        return $this->arrayManager->set('children/file_name', $fileNameContainer, $fileNameField);
    }

    /**
     * @return array
     */
    public function getFileColumn()
    {
        $fileContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('Attach File or Enter Link'),
            'dataScope' => '',
        ];
        $fileTypeField['arguments']['data']['config'] = [
            'formElement' => Form\Element\Select::NAME,
            'componentType' => Form\Field::NAME,
            'component' => 'Magento_Downloadable/js/components/upload-type-handler',
            'dataType' => Form\Element\DataType\Text::NAME,
            'dataScope' => 'file_type',
            'options' => $this->typeUpload->toOptionArray(),
            'typeFile' => 'file_upload',
            'typeUrl' => 'file_url',
        ];
        $fileLinkUrl['arguments']['data']['config'] = [
            'formElement' => Form\Element\Input::NAME,
            'componentType' => Form\Field::NAME,
            'dataType' => Form\Element\DataType\Text::NAME,
            'dataScope' => 'file_url',
            'placeholder' => 'URL',
            'validation' => [
                'required-entry' => true,
                'validate-url' => true,
            ],
        ];
        $fileUploader['arguments']['data']['config'] = [
            'formElement' => 'fileUploader',
            'componentType' => 'fileUploader',
            'component' => 'Magento_Downloadable/js/components/file-uploader',
            'elementTmpl' => 'Magento_Downloadable/components/file-uploader',
            'fileInputName' => 'links',
            'uploaderConfig' => [
                'url' => $this->urlBuilder->getUrl(
                    'amfile/file/upload',
                    ['type' => 'links', '_secure' => true]
                ),
            ],
            'dataScope' => 'file',
            'validation' => [
                'required-entry' => true,
            ],
        ];

        return $this->arrayManager->set(
            'children',
            $fileContainer,
            [
                'file_type' => $fileTypeField,
                'file_url' => $fileLinkUrl,
                'file_upload' => $fileUploader
            ]
        );
    }

    /**
     * @return array
     */
    public function getCustomerGroupColumn()
    {
        $labelContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('Customer Group'),
            'dataScope' => '',
        ];
        $labelField['arguments']['data']['config'] = [
            'formElement' => Form\Element\MultiSelect::NAME,
            'componentType' => Form\Field::NAME,
            'dataType' => Form\Element\DataType\Number::NAME,
            'dataScope' => 'customer_group',
            'options' => $this->groupSource->toOptionArray()
        ];

        return $this->arrayManager->set('children/customer_group', $labelContainer, $labelField);
    }

    /**
     * @return array
     */
    public function getShowOrderedColumn()
    {
        $labelContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('Show only if a Product has been Ordered'),
            'dataScope' => '',
        ];
        $labelField['arguments']['data']['config'] = [
            'formElement' => Form\Element\Select::NAME,
            'componentType' => Form\Field::NAME,
            'dataType' => Form\Element\DataType\Text::NAME,
            'dataScope' => 'show_for_ordered',
            'options' => $this->yesnoSource->toOptionArray()
        ];

        return $this->arrayManager->set('children/show_for_ordered', $labelContainer, $labelField);
    }

    /**
     * @return array
     */
    public function getIsVisibleColumn()
    {
        $labelContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('Visible'),
            'dataScope' => '',
        ];
        $labelField['arguments']['data']['config'] = [
            'formElement' => Form\Element\Select::NAME,
            'componentType' => Form\Field::NAME,
            'dataType' => Form\Element\DataType\Text::NAME,
            'dataScope' => 'is_visible',
            'options' => $this->yesnoSource->toOptionArray()
        ];

        return $this->arrayManager->set('children/is_visible', $labelContainer, $labelField);
    }

    /**
     * @param $fileId
     * @return string
     */
    public function getDownloadUrl($fileId)
    {
        return $this->fileModel->getDownloadUrlBackend($fileId);
    }
}
