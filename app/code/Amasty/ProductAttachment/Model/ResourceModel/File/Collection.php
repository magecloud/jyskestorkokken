<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ProductAttachment
 */

namespace Amasty\ProductAttachment\Model\ResourceModel\File;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct(
            $entityFactory, $logger, $fetchStrategy, $eventManager, $connection,
            $resource
        );
        $this->collectionFactory = $collectionFactory;
        $this->_setIdFieldName('id');
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Amasty\ProductAttachment\Model\File',
            'Amasty\ProductAttachment\Model\ResourceModel\File'
        );
    }

    /**
     * @param int $productId
     * @param int $storeId
     * @return $this
     */
    public function getFilesAdminByProductId($productId, $storeId)
    {
        $this->getFilesAdmin($storeId)->getSelect()
            ->where('main_table.product_id = ?', $productId);

        return $this;
    }

    /**
     * @param int $categoryId
     * @param int $storeId
     * @return $this
     */
    public function getFilesAdminByCategoryId($categoryId, $storeId)
    {
        $this->getFilesAdmin($storeId)->getSelect()
            ->where('main_table.category_id = ?', $categoryId);

        return $this;
    }

    /**
     * @param array $productIds
     * @param int $storeId
     * @return $this
     */
    public function getFilesAdminByProductIds($productIds, $storeId)
    {
        $this->getFilesAdmin($storeId)->getSelect()
            ->where('main_table.product_id IN (?)', $productIds);

        return $this;
    }

    /**
     * @param array $categoryIds
     * @param int $storeId
     * @return $this
     */
    public function getFilesAdminByCategoryIds($categoryIds, $storeId)
    {
        $this->getFilesAdmin($storeId)->getSelect()
            ->where('main_table.category_id IN (?)', $categoryIds);

        return $this;
    }

    /**
     * @param int $storeId
     *
     * @return $this
     */
    protected function getFilesAdmin($storeId)
    {
        $adapter = $this->getConnection();
        $joinCondition = $adapter->quoteInto("s.file_id = main_table.id AND s.store_id = ?", $storeId);
        $select = $this->getSelect()
            ->join(
                ['d' => $this->getTable('amasty_file_store')],
                'main_table.id = d.file_id AND d.store_id = 0',
                ['file_id' => 'main_table.id', '*']
            )
            ->joinLeft(
                ['s' => $this->getTable('amasty_file_store')],
                $joinCondition,
                [
                    'label' => $this->getCheckSql(
                        "s.label = '' OR s.label IS NUll", 'd.label',
                        's.label'
                    ),
                    'is_visible' => $this->getCheckSql(
                        "s.is_visible = '-1' OR s.is_visible IS NUll",
                        'd.is_visible', 's.is_visible'
                    ),
                    'show_for_ordered' => $this->getCheckSql(
                        "s.show_for_ordered = '-1' OR s.show_for_ordered IS NUll",
                        'd.show_for_ordered', 's.show_for_ordered'
                    ),
                    'position' => $this->getCheckSql(
                        "s.position = '' OR s.position IS NUll",
                        'd.position',
                        's.position'
                    ),
                    'label_is_default' => $this->getCheckSql(
                        "s.label = '' OR s.label IS NUll", '1', '0'
                    ),
                    'is_visible_is_default' => $this->getCheckSql(
                        "s.is_visible = '-1' OR s.is_visible IS NUll",
                        '1', '0'
                    ),
                    'show_for_ordered_is_default' => $this->getCheckSql(
                        "s.show_for_ordered = '-1' OR s.show_for_ordered IS NUll",
                        '1', '0'
                    ),
                    'customer_group_is_default' => $this->getCheckSql(
                        "s.customer_group_is_default IS NULL",
                        '1', 's.customer_group_is_default'
                    ),
                ]
            )
            ->joinLeft(
                ['cg' => $this->getTable(
                    'amasty_file_customer_group'
                )],
                'cg.file_id = main_table.id
                AND (
                    (
                        (s.store_id IS NULL OR s.customer_group_is_default = 1)
                        AND d.store_id = cg.store_id
                    )
                    OR s.store_id = cg.store_id
                )',
                ['customer_groups' => new \Zend_Db_Expr(
                    'GROUP_CONCAT(`cg`.`customer_group_id`)'
                )]
            )
            ->group('main_table.id')
            ->order('s.position ASC');

        return $this;
    }

    /**
     * @param int $productId
     * @param array $categoryIds
     * @param int $storeId
     * @param int $customerId
     * @param int $customerGroupId
     *
     * @return $this
     */
    public function getFilesFrontend($productId, $categoryIds, $storeId, $customerId, $customerGroupId)
    {
        $adapter = $this->getConnection();
        $categoryWhere = $adapter->quoteInto("main_table.category_id IN (?)", $categoryIds);
        $productWhere = $adapter->quoteInto("main_table.product_id  = ?", $productId);
        $joinCondition = $adapter->quoteInto("s.file_id = main_table.id AND s.store_id = ?", $storeId);
        $select = $this->getSelect()
            ->join(
                ['d' => $this->getTable('amasty_file_store')],
                "main_table.id = d.file_id AND d.store_id = 0"
            )
            ->joinLeft(
                ['s' => $this->getTable('amasty_file_store')], $joinCondition,
                [
                    'label' => $this->getCheckSql(
                        "(s.label IS NULL OR s.label = '')", 'd.label',
                        's.label'
                    ),
                    'is_visible' => $this->getCheckSql(
                        "(s.is_visible IS NULL OR s.is_visible = '-1')",
                        'd.is_visible', 's.is_visible'
                    ),
                    'position' => $this->getCheckSql(
                        "(s.position IS NULL OR s.position = '')", 'd.position',
                        's.position'
                    ),
                    'show_for_ordered' => $this->getCheckSql(
                        "(s.show_for_ordered IS NULL OR s.show_for_ordered = '-1')",
                        'd.show_for_ordered', 's.show_for_ordered'
                    )
                ]
            )
            ->joinLeft(
                ['cg' => $this->getTable('amasty_file_customer_group')],
                'cg.file_id = main_table.id
                AND (
                    (
                        (s.store_id IS NULL OR s.customer_group_is_default = 1)
                        AND d.store_id = cg.store_id
                    )
                    OR s.store_id = cg.store_id
                )', ''
            )
            ->where(
                "(cg.id IS NULL OR cg.customer_group_id = ?)", $customerGroupId
            )
            ->where($categoryWhere . ' OR ' . $productWhere)
            ->where("((s.is_visible IS NULL OR s.is_visible = '-1') AND d.is_visible = 1) OR (s.is_visible = '1')")
            ->order("position ASC")
            ->group('main_table.id');

        $productRowId = implode(',', $this->getCustomerOrderedProduct($productId, $customerId));
        $categoryRowId = implode(',', $this->getCustomerOrderedCategories($productId, $categoryIds, $customerId));
        $having = '(';
        if ($categoryRowId) {
            $having .= sprintf('( show_for_ordered = 1 AND main_table.category_id IN (%s)) OR ', $categoryRowId);
        }
        if ($productRowId) {
            $having .= sprintf('( show_for_ordered = 1 AND main_table.product_id IN (%s)) OR ', $productRowId);
        }
        $having .= 'show_for_ordered = 0)';
        $having = new \Zend_Db_Expr($having);
        $select->having($having);

        return $this;
    }

    public function getFileFrontend($productId, $categoryId, $storeId, $customerId, $customerGroupId, $fileId)
    {
        $this->getFilesFrontend($productId, $categoryId, $storeId, $customerId, $customerGroupId);
        $this->getSelect()->where('main_table.id = ?', $fileId);

        return $this;
    }

    public function loadByIdAndCustomerGroupIdAndOrdered($productId, $categoryId, $storeId, $customerId, $customerGroupId, $fileId)
    {
        $this->getFileFrontend($productId, $categoryId, $storeId, $customerId, $customerGroupId, $fileId);

        return $this->getFirstItem();
    }

    public function getCustomerOrderedProduct($productId, $customerId)
    {
        $salesCollection = $this->getSalesCollection($productId, $customerId);

        return $salesCollection->getColumnValues('product_id');
    }

    public function getCustomerOrderedCategories($productId, $categoryIds, $customerId)
    {
        $salesCollection = $this->getSalesCollection($productId, $customerId);
        $count = is_countable($categoryIds) ? count($categoryIds) : 0;

        if ($count > 0) {
            $salesCollection->join(
                ['cp' => $this->getTable('catalog_category_product')],
                'oi.product_id = cp.product_id',
                'category_id'
            )->addFieldToFilter('cp.category_id', $categoryIds);
        }

        return $salesCollection->getColumnValues('category_id');
    }

    public function getSalesCollection($productId, $customerId)
    {
        $salesCollection = $this->collectionFactory->create();
        $salesCollection->getSelect()->reset(\Zend_Db_Select::COLUMNS);
        $joinCond = 'main_table.entity_id = oi.order_id';
        if ($productId) {
            $joinCond .= sprintf(' AND product_id = %d', $productId);
        }
        $salesCollection
            ->join(
                ['oi' => $this->getTable('sales_order_item')],
                $joinCond,
                'product_id'
            )
            ->addFieldToFilter('main_table.customer_id', $customerId)
            ->addFieldToFilter('main_table.status', \Magento\Sales\Model\Order::STATE_COMPLETE);
        $salesCollection->distinct(true);

        return $salesCollection;
    }

    public function getCheckSql($expression, $true, $false)
    {
        if ($expression instanceof \Zend_Db_Expr || $expression instanceof Zend_Db_Select) {
            $expression = sprintf("IF((%s), %s, %s)", $expression, $true, $false);
        } else {
            $expression = sprintf("IF(%s, %s, %s)", $expression, $true, $false);
        }

        return new \Zend_Db_Expr($expression);
    }

    /**
     * Join catalog_product_entity table
     *
     * @return $this
     */
    public function addProducts()
    {
        $alias = 'product';
        $fromPart = $this->getSelect()->getPart(\Zend_Db_Select::FROM);
        if (isset($fromPart[$alias])) {
            // avoid double join
            return $this;
        }

        $this->getSelect()->joinLeft(
            [$alias => $this->getTable('catalog_product_entity')],
            'main_table.product_id = ' . $alias . '.entity_id'
        );

        return $this;
    }

    /**
     * @param int $productId
     * @param int $storeId
     *
     * @return $this
     */
    public function getFilesByProductAndStore($productId, $storeId)
    {
        $this->addFieldToFilter('product_id', $productId)
            ->join(
                ['store' => $this->getTable('amasty_file_store')],
                'main_table.id = store.file_id AND store.store_id = ' . $storeId,
                ['store_id']
            );

        return $this;
    }

    /**
     * @param int $categoryId
     * @param int $storeId
     * @return $this
     */
    public function getFilesByCategoryAndStore($categoryId, $storeId)
    {
        $this->addFieldToFilter('category_id', $categoryId)
            ->join(
                ['store' => $this->getTable('amasty_file_store')],
                'main_table.id = store.file_id AND store.store_id = ' . $storeId,
                ['store_id']
            );

        return $this;
    }
}
