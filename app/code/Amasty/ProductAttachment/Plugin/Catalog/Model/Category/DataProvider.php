<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ProductAttachment
 */


namespace Amasty\ProductAttachment\Plugin\Catalog\Model\Category;

use Amasty\ProductAttachment\Model\Modifier;
use Magento\Downloadable\Helper\File;
use Amasty\ProductAttachment\Model\File as ModelFile;
use Magento\Framework\Escaper;

class DataProvider
{
    /**
     * @var Modifier
     */
    private $modifier;

    /**
     * @var File
     */
    public $downloadableFile;

    /**
     * @var ModelFile
     */
    public $fileModel;

    /**
     * @var Escaper
     */
    public $escaper;
    
    public function __construct(
        Modifier $modifier,
        ModelFile $fileModel,
        File $downloadableFile,
        Escaper $escaper
    ) {
        $this->modifier = $modifier;
        $this->fileModel = $fileModel;
        $this->downloadableFile = $downloadableFile;
        $this->escaper = $escaper;
    }

    /**
     * @param \Magento\Catalog\Model\Category\DataProvider $subject
     * @param array $meta
     * @return array
     */
    public function afterPrepareMeta($subject, $meta)
    {
        $meta = $this->modifier->modifyMeta($meta);
        $meta['amasty_product_attachments']['arguments']['data']['config']['dataScope'] = null;

        return $meta;
    }

    /**
     * @param \Magento\Catalog\Model\Category\DataProvider $subject
     * @param $data
     * @return mixed
     */
    public function afterGetData($subject, $data)
    {
        $categoryId = $subject->getCurrentCategory()->getId();
        $data[$categoryId]['amasty_product_attachments']['attachments'] = $this->getLinkData($subject);

        return $data;
    }

    /**
     * @param \Magento\Catalog\Model\Category\DataProvider $subject
     * @return mixed
     */
    public function getLinkData($subject)
    {
        $linkArr = [];

        $category = $subject->getCurrentCategory();

        $categoryId = $category->getId();
        $storeId = $category->getStoreId();

        /**
         * @var \Amasty\ProductAttachment\Model\ResourceModel\File\Collection $fileCollection
         */
        $files = $this->fileModel->getCollection();
        $files->getFilesAdminByCategoryId($categoryId,$storeId);
        $fileHelper = $this->downloadableFile;
        /**
         * TODO: Magento bug, customer group id 0 don't selected in form by default
         */
        foreach ($files as $item) {
            /**
             * @var \Amasty\ProductAttachment\Model\File $item
             */
            $tmpLinkItem = [
                'id'                          => $item->getFileId(),
                'label'                       => $this->escaper->escapeHtml(
                    $item->getLabel()
                ),
                'use_default_label'           => $item->getLabelIsDefault() ? '1' : '0',
                'file_name'                   => $this->escaper->escapeHtml(
                    $item->getFileName()
                ),
                'customer_group'              => $item->getCustomerGroups(),
                'use_default_customer_group'  => $item->getCustomerGroupIsDefault() ? '1' : '0',
                'file_type'                   => $item->getFileType(),
                'file_url'                    => $item->getFileUrl(),
                'show_for_ordered'            => $item->getShowForOrdered(),
                'use_default_show_for_ordered'=> $item->getShowForOrderedIsDefault() ? '1' : '0',
                'is_visible'                  => $item->getIsVisible(),
                'use_default_is_visible'      => $item->getIsVisibleIsDefault() ? '1' : '0',
                'position'                    => $item->getPosition(),
            ];

            $linkFile = $item->getFilePath();
            if ($linkFile) {
                $file = $fileHelper->getFilePath($this->fileModel->getBasePath(), $linkFile);

                $fileExist = $fileHelper->ensureFileInFilesystem($file);

                if ($fileExist) {
                    $tmpLinkItem['file'] = [
                        [
                            'file' => $linkFile,
                            'name' => $item->getFileName(),
                            'size' => $fileHelper->getFileSize($file),
                            'status' => 'old',
                            'url'  => $this->modifier->getDownloadUrl($item->getFileId())
                        ],
                    ];
                }
            }

            $linkArr[] = $tmpLinkItem;
        }

        return $linkArr;
    }
}
