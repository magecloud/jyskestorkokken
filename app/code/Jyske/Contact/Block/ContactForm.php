<?php
/**
 * ContactForn
 *
 * @copyright Copyright © 2019 HeadWay. All rights reserved.
 * @author    ilya.kush@gmail.com
 */

namespace Jyske\Contact\Block;


class ContactForm extends \Magento\Contact\Block\ContactForm {

	/**
	 * @return $this
	 */
	public function _prepareLayout() {

		$this->pageConfig->getTitle()->set($this->pageConfig->getTitle()->get().' '.$this->_scopeConfig->getValue('general/store_information/name'),\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

		return parent::_prepareLayout();
	}

	/**
	 * @param $field string
	 *
	 * @return string
	 */
	public function getStoreInformation($field){

		return $this->_scopeConfig->getValue('general/store_information/'.$field,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	/**
	 * @return mixed
	 */
	public function getCustomerSupportEmail(){

		return $this->_scopeConfig->getValue('trans_email/ident_support/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
}