<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 08.11.2018
 * Time: 13:00
 */
namespace Jyske\Theme\Helper;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Newlabel extends \Magento\Framework\Url\Helper\Data
{

	/**
	 * @var TimezoneInterface
	 */
	protected $_localeDate;

	/**
	 * Newlabel constructor.
	 *
	 * @param TimezoneInterface $localeDate
	 */
	public function __construct(
		TimezoneInterface $localeDate
	) {
		$this->_localeDate = $localeDate;
	}

	public function isProductNew($product)
	{
		$newsFromDate = $product->getNewsFromDate();
		$newsToDate = $product->getNewsToDate();
		if (!$newsFromDate && !$newsToDate) {
			return false;
		}

		return $this->_localeDate->isScopeDateInInterval(
			$product->getStore(),
			$newsFromDate,
			$newsToDate
		);
	}
}