<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 18.09.2018
 * Time: 11:23
 */
namespace Jyske\Theme\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
//	use Magento\Framework\App\Config\Storage\WriterInterface;
//	use Magento\Framework\Event\ManagerInterface;
//	use Magento\Framework\Setup\InstallDataInterface;
//	use Magento\Framework\Setup\ModuleContextInterface;
//	use Magento\Framework\Setup\ModuleDataSetupInterface;
//	use Magento\Store\Model\GroupFactory;
//	use Magento\Store\Model\ResourceModel\Group;
//	use Magento\Store\Model\ResourceModel\Store;
//	use Magento\Store\Model\ResourceModel\Website;
//	use Magento\Store\Model\StoreFactory;
//	use Magento\Store\Model\WebsiteFactory;

	/**
	 *  @var \Magento\Framework\App\Config\Storage\WriterInterface
	 */
	protected $_configWriter;

	/**
	 * @var WebsiteFactory|\Magento\Store\Model\WebsiteFactory
	 */
	protected $_websiteFactory;
	/**
	 * @var Website|\Magento\Store\Model\ResourceModel\Website
	 */
	protected $_websiteResourceModel;
	/**
	 * @var StoreFactory|\Magento\Store\Model\StoreFactory
	 */
	protected $_storeFactory;
	/**
	 * @var \Magento\Store\Model\GroupFactory
	 */
	protected $_groupFactory;
	/**
	 * @var Group|\Magento\Store\Model\ResourceModel\Group
	 */
	protected $_groupResourceModel;
	/**
	 * @var Store|\Magento\Store\Model\ResourceModel\Store
	 */
	protected $_storeResourceModel;
	/**
	 * @var ManagerInterface|\Magento\Framework\Event\ManagerInterface
	 */
	protected $_eventManager;


	/**
	 * InstallData constructor.
	 *
	 * @param WriterInterface|\Magento\Framework\App\Config\Storage\WriterInterface $configWriter
	 * @param WebsiteFactory|\Magento\Store\Model\WebsiteFactory                    $websiteFactory
	 * @param Website|\Magento\Store\Model\ResourceModel\Website                    $websiteResourceModel
	 * @param Store|\Magento\Store\Model\ResourceModel\Store                        $storeResourceModel
	 * @param Group|\Magento\Store\Model\ResourceModel\Group                        $groupResourceModel
	 * @param StoreFactory|\Magento\Store\Model\StoreFactory                        $storeFactory
	 * @param \Magento\Store\Model\GroupFactory                                     $groupFactory
	 * @param ManagerInterface|\Magento\Framework\Event\ManagerInterface            $eventManager
	 *
	 * @internal param $GroupFactory $ groupFactory
	 */
	public function __construct(
		\Magento\Framework\App\Config\Storage\WriterInterface    $configWriter,
        \Magento\Store\Model\WebsiteFactory                      $websiteFactory,
        \Magento\Store\Model\ResourceModel\Website $websiteResourceModel,
        \Magento\Store\Model\ResourceModel\Store $storeResourceModel,
        \Magento\Store\Model\ResourceModel\Group $groupResourceModel,
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Magento\Store\Model\GroupFactory $groupFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager
	) {

		$this->_configWriter         = $configWriter;
		$this->_websiteFactory       = $websiteFactory;
		$this->_websiteResourceModel = $websiteResourceModel;
		$this->_storeFactory         = $storeFactory;
		$this->_groupFactory         = $groupFactory;
		$this->_groupResourceModel   = $groupResourceModel;
		$this->_storeResourceModel   = $storeResourceModel;
		$this->_eventManager         = $eventManager;
	}

	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function install(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$setup->startSetup();

		$_businnesWebsiteCode   = 'businnes';
		$_privateWebsiteCode    = 'private';

		/** @var \Magento\Store\Model\Website $website */
		$website = $this->_websiteFactory->create();
		$website->load('base','code');
		if($website->getId()){
			$website->setCode($_businnesWebsiteCode);
			$website->setName('Business website');
			$website->setSortOrder(1);
			$this->_websiteResourceModel->save($website);
		}

		/** @var \Magento\Store\Model\Group $group */
		$group = $this->_groupFactory->create();
		$group->load('main_website_store','code');
		if($group->getId()){
			$group->setName('Jyske Business Store');
			$group->setCode($_businnesWebsiteCode.'_store');
			$this->_groupResourceModel->save($group);
		}

		/** @var  \Magento\Store\Model\Store $store */
		$store = $this->_storeFactory->create();
		$store->load('default');
		if($store->getId()){
			$store->setCode($_businnesWebsiteCode.'_dk');
			$store->setName('Dansk');
			$this->_storeResourceModel->save($store);
		}

		$website = $this->_websiteFactory->create();
		$website->load($_privateWebsiteCode);
		if(!$website->getId()){
			$website->setCode($_privateWebsiteCode);
			$website->setName('Private website');
			$website->setSortOrder(2);
			$website->setDefaultGroupId(2);
			$this->_websiteResourceModel->save($website);
		}


		if($website->getId()){
			/** @var \Magento\Store\Model\Group $group */
			$group = $this->_groupFactory->create();
			$group->load($_privateWebsiteCode.'_store','code');
			if(!$group->getId()) {
				$group->setWebsiteId($website->getWebsiteId());
				$group->setCode($_privateWebsiteCode.'_store');
				$group->setName('Jyske Private Store');
				$group->setRootCategoryId(2);
				$group->setDefaultStoreId(2);
				$this->_groupResourceModel->save($group);
			}
		}

		/** @var  \Magento\Store\Model\Store $store */
		$store = $this->_storeFactory->create();
		$store->load($_privateWebsiteCode.'_dk');
		if(!$store->getId()){
			$group = $this->_groupFactory->create();
			$group->load($_privateWebsiteCode.'_store', 'code');
			$store->setCode($_privateWebsiteCode.'_dk');
			$store->setName('Dansk');
			$store->setWebsite($website);
			$store->setGroupId($group->getId());
			$store->setData('is_active','1');
			$this->_storeResourceModel->save($store);
			// Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
			$this->_eventManager->dispatch('store_add', ['store' => $store]);
		}


		$setup->endSetup();
	}
}