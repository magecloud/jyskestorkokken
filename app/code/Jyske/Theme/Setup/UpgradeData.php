<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 18.09.2018
 * Time: 14:02
 */
namespace Jyske\Theme\Setup;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
	/**
	 *  @var \Magento\Framework\App\Config\Storage\WriterInterface
	 */
	protected $_configWriter;

	/**
	 * @var \Magento\Cms\Model\Block
	 */
	protected $_modelCmsBlock;

	/**
	 * UpgradeData constructor.
	 *
	 * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
	 * @param \Magento\Cms\Model\Block                              $modelCmsBlock
	 */
	public function __construct(
		\Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
		\Magento\Cms\Model\Block $modelCmsBlock) {

		$this->_configWriter    = $configWriter;
		$this->_modelCmsBlock   = $modelCmsBlock;
	}

	/**
	 *
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	public function upgrade(\Magento\Framework\Setup\ModuleDataSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$setup->startSetup();

		 if (version_compare($context->getVersion(), '0.1.1', '<=')) {

			 $this->_configWriter->save('currency/options/customsymbol','{"DKK":"DKK"}');
			 $this->_configWriter->save('admin/security/admin_account_sharing','1');
			 $this->_configWriter->save('sendfriend/email/enabled','0');
			 $this->_configWriter->save('cataloginventory/item_options/backorders','1'	);
			 $this->_configWriter->save('cataloginventory/options/show_out_of_stock','1'	);


			 $this->_configWriter->save('shariff/icon/services_list','{"twitter":{"sort_number":"2","active":"1","value":"twitter","label":"Twitter"},"facebook":{"sort_number":"1","active":"1","value":"facebook","label":"Facebook"},"googleplus":{"sort_number":"3","active":"1","value":"googleplus","label":"Google Plus"},"linkedin":{"sort_number":"4","active":"1","value":"linkedin","label":"Linkedin"},"pinterest":{"sort_number":"5","active":"1","value":"pinterest","label":"Pinterest"},"xing":{"sort_number":"1","active":"0","value":"xing","label":"Xing"},"whatsapp":{"sort_number":"1","active":"0","value":"whatsapp","label":"Whatsapp"},"mail":{"sort_number":"6","active":"1","value":"mail","label":"Mail"},"info":{"sort_number":"1","active":"0","value":"info","label":"Info"},"addthis":{"sort_number":"1","active":"0","value":"addthis","label":"Add This"},"tumblr":{"sort_number":"1","active":"0","value":"tumblr","label":"Tumblr"},"flattr":{"sort_number":"1","active":"0","value":"flattr","label":"Flattr"},"diaspora":{"sort_number":"1","active":"0","value":"diaspora","label":"Diaspora"},"reddit":{"sort_number":"1","active":"0","value":"reddit","label":"Reddit"},"stumbleupon":{"sort_number":"1","active":"0","value":"stumbleupon","label":"StumbleUpon"},"threema":{"sort_number":"1","active":"0","value":"threema","label":"Threema"},"weibo":{"sort_number":"1","active":"0","value":"weibo","label":"Weibo"},"tencent-weibo":{"sort_number":"1","active":"0","value":"tencent-weibo","label":"Tencent Weibo"},"qzone":{"sort_number":"1","active":"0","value":"qzone","label":"Qzone"},"print":{"sort_number":"1","active":"0","value":"print","label":"Print"},"telegram":{"sort_number":"1","active":"0","value":"telegram","label":"Telegram"},"vk":{"sort_number":"1","active":"0","value":"vk","label":"Vk"},"flipboard":{"sort_number":"1","active":"0","value":"flipboard","label":"Flipboard"}}');
			 $this->_configWriter->save('shariff/icon/info_display','blank');
			 $this->_configWriter->save('shariff/icon/lang','da');
			 $this->_configWriter->save('shariff/icon/limit',7);
			 $this->_configWriter->save('shariff/icon/theme','standard');
			 $this->_configWriter->save('shariff/icon/button_style','icon');
			 $this->_configWriter->save('shariff/position/product_page','below');
			 $this->_configWriter->save('shariff/position/category_page',0);
			 $this->_configWriter->save('shariff/position/cms_page',0);
			 $this->_configWriter->save('shariff/position/homepage',0);
			 $this->_configWriter->save('shariff/setting/js_config',1);
			 $this->_configWriter->save('shariff/setting/css_config',2);


			 $this->_configWriter->save('amfile/additional/detect_mime',1);
			 $this->_configWriter->save('amfile/import/ftp_dir','amasty/amfile/import/ftp');
			 $this->_configWriter->save('amfile/block/position','after'	);
			 $this->_configWriter->save('amfile/block/sibling_tab','product.attributes');
			 $this->_configWriter->save('amfile/block/block_location','product.info.details');
			 $this->_configWriter->save('amfile/block/block_label','Dokumenter'	);
			 $this->_configWriter->save('amfile/block/show_ordered','0'	);
			 $this->_configWriter->save('amfile/block/customer_group','0,1,2,3,4,5'	);
			 $this->_configWriter->save('amfile/block/display_block','1'	);


			 $this->_configWriter->save('socialshare/pinitsharing/display_pinit_count','0'	);
			 $this->_configWriter->save('socialshare/pinitsharing/enable_pinit','1'	);
			 $this->_configWriter->save('socialshare/twitter/enable_twitter','1'	);
			 $this->_configWriter->save('socialshare/googleplus/display_google_count','0'	);
			 $this->_configWriter->save('socialshare/googleplus/enable_google_plus','1'	);
			 $this->_configWriter->save('socialshare/facebook/display_facebook_count','0'	);
			 $this->_configWriter->save('socialshare/facebook/display_onlylike','0'	);
			 $this->_configWriter->save('socialshare/facebook/enable_facebook','1'	);

			 $this->_configWriter->save('amshopby_brand/product_page/height','30'	);
			 $this->_configWriter->save('amshopby_brand/product_page/width','30'	);
			 $this->_configWriter->save('amshopby_brand/slider/image_height',NULL);
			 $this->_configWriter->save('amshopby_brand/slider/slider_width',NULL);
			 $this->_configWriter->save('amshopby_brand/product_page/height','30'	);
				$this->_configWriter->save('amshopby_brand/product_page/width','30'	);
				$this->_configWriter->save('amshopby_brand/product_page/display_description','0'	);
				$this->_configWriter->save('amshopby_brand/product_page/display_brand_image','0'	);
				$this->_configWriter->save('amshopby_brand/more_from_brand/count','7'	);
				$this->_configWriter->save('amshopby_brand/more_from_brand/title','More from {brand_name}'	);
				$this->_configWriter->save('amshopby_brand/more_from_brand/enable','0'	);
				$this->_configWriter->save('amshopby_brand/slider/autoplay','0'	);
				$this->_configWriter->save('amshopby_brand/slider/pagination_show','0'	);
				$this->_configWriter->save('amshopby_brand/slider/simulate_touch','1'	);
				$this->_configWriter->save('amshopby_brand/slider/infinity_loop','1'	);
				$this->_configWriter->save('amshopby_brand/slider/buttons_show','1'	);
				$this->_configWriter->save('amshopby_brand/slider/show_label','1'	);
				$this->_configWriter->save('amshopby_brand/slider/image_width','130'	);
				$this->_configWriter->save('amshopby_brand/slider/slider_title','Featured Brands'	);
				$this->_configWriter->save('amshopby_brand/slider/slider_title_color','#000'	);
				$this->_configWriter->save('amshopby_brand/slider/slider_header_color','transparent'	);
				$this->_configWriter->save('amshopby_brand/slider/sort_by','position'	);
				$this->_configWriter->save('amshopby_brand/slider/items_number','8'	);
				$this->_configWriter->save('amshopby_brand/slider/notice','{{widget type="Amasty\ShopbyBrand\Block\Widget\BrandSlider" template="widget/brand_list/slider.phtml"}}'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/display_zero','1'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/show_count','1'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/filter_display_all','1'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/show_filter','1'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/show_search','1'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/image_width','100'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/show_images','0'	);
				$this->_configWriter->save('amshopby_brand/brands_landing/notice','{{widget type="Amasty\ShopbyBrand\Block\Widget\BrandList" columns="3" template="widget/brand_list/index.phtml"}}'	);
				$this->_configWriter->save('amshopby_brand/general/show_on_listing','0'	);
				$this->_configWriter->save('amshopby_brand/general/top_links','0'	);
				$this->_configWriter->save('amshopby_brand/general/topmenu_enabled','0'	);
				$this->_configWriter->save('amshopby_brand/general/attribute_code','manufacturer'	);
				$this->_configWriter->save('amasty_methods/shipping_method/restrict_method','0'	);
				$this->_configWriter->save('amasty_methods/payment_method/restrict_method','0'	);

		 }

		if (version_compare($context->getVersion(), '0.1.2', '<=')) {


			$this->_configWriter->save('amasty_checkout/general/title',null);
			$this->_configWriter->save('amasty_checkout/general/description',null);
			$this->_configWriter->save('amasty_checkout/general/reload_shipping',1);
			$this->_configWriter->save('amasty_checkout/general/reload_payments',1);

			$this->_configWriter->save('amasty_checkout/default_values/address_country_id','DK');
			$this->_configWriter->save('amasty_checkout/default_values/shipping_method','flatrate_flatrate');

			$this->_configWriter->save('amasty_checkout/design/layout','3columns');
			$this->_configWriter->save('amasty_checkout/design/header_footer',0);

			$this->_configWriter->save('amasty_checkout/additional_options/comment',1);
			$this->_configWriter->save('amasty_checkout/additional_options/discount',1);
			$this->_configWriter->save('amasty_checkout/additional_options/create_account',1);
			$this->_configWriter->save('amasty_checkout/additional_options/display_agreements','order_totals');

		}

		if (version_compare($context->getVersion(), '0.1.3', '<=')) {
			$this->_configWriter->save('amasty_checkout/general/allow_edit_options',1);
		}

		$setup->endSetup();
	}
}