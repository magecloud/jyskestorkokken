<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 02.10.2018
 * Time: 16:17
 */
namespace Jyske\Theme\Model\OfflinePayments;

use Magento\Payment\Helper\Data as PaymentHelper;

class InstructionsConfigProvider extends \Magento\OfflinePayments\Model\InstructionsConfigProvider {

	/**
	 * @var \Magento\Cms\Model\Template\FilterProvider
	 */
	protected $_filterProvider;

	/**
	 * InstructionsConfigProvider constructor.
	 *
	 * @param PaymentHelper                              $paymentHelper
	 * @param \Magento\Framework\Escaper                 $escaper
	 * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
	 */
	public function __construct(
		PaymentHelper $paymentHelper,
		\Magento\Framework\Escaper $escaper,
		\Magento\Cms\Model\Template\FilterProvider $filterProvider
	){

		$this->_filterProvider = $filterProvider;
		parent::__construct($paymentHelper,$escaper);
	}

	/**
	 * Get instructions text from config
	 *
	 * @param string $code
	 * @return string
	 */
	protected function getInstructions($code)
	{
		$_html = nl2br($this->escaper->escapeHtml($this->methods[$code]->getInstructions(),array('a','span')));
		return $this->_filterProvider->getBlockFilter()->filter($_html);
	}

}