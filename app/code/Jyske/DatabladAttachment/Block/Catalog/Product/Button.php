<?php

/**
 * Button
 *
 * @copyright Copyright © 2019 HeadWay. All rights reserved.
 * @author    ilya.kush@gmail.com
 */
namespace Jyske\DatabladAttachment\Block\Catalog\Product;

class Button extends \Amasty\ProductAttachment\Block\Catalog\Product\Attachment {

	/**
	 *
	 */
	protected function _prepareLayout() {

		\Magento\Framework\View\Element\Template::_prepareLayout();
	}

	/**
	 *
	 */
	protected function _construct() {
		parent::_construct();

		$this->setTemplate('Jyske_DatabladAttachment::product/data-sheet.phtml');
	}

	public function getCodeWord(){

		return $this->_scopeConfig->getValue('amfile/datasheet/code_word', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

}