<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 14.09.2018
 * Time: 14:39
 */
namespace Jyske\Review\Block\Product\View;

class ListView extends \Magento\Review\Block\Product\View\ListView
{

	/**
	 * Prepare product review list toolbar
	 *
	 * @return $this
	 */
	protected function _prepareLayout()
	{
		\Magento\Review\Block\Product\View::_prepareLayout();

		$toolbar = $this->getLayout()->getBlock('product_review_list.toolbar');
		if ($toolbar) {
			$toolbar->setAvailableLimit(array(4 => 4, 8 => 8, 16 => 16, 32 => 32));
			$toolbar->setLimit(8);
			$toolbar->setCollection($this->getReviewsCollection());

			$this->setChild('toolbar', $toolbar);
		}

		return $this;
	}

}