<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Jyske\CatalogInventory\Block\Listing;

/**
 * Product stock qty default block
 *
 * @api
 * @since 100.0.2
 */
class DefaultStockqty extends \Magento\CatalogInventory\Block\Stockqty\DefaultStockqty implements \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        return \Magento\Framework\View\Element\Template::_toHtml();
    }



	/**
	 * Retrieve product object
	 *
	 * @return \Magento\Catalog\Model\Product
	 */
    public function getProduct() {

    	if($this->getData('product') !== null ) {
    		return $this->getData('product');
	    }

	    return parent::getProduct();
    }

	/**
	 * Retrieve visibility of stock qty message
	 *
	 * @return bool
	 */
	public function isMsgVisible($showNegativeQty = false)
	{
		if($showNegativeQty){
			return $this->getStockQtyLeft() <= $this->getThresholdQty();
		} else {
			return parent::isMsgVisible();
		}
	}

	/**
	 * @return bool
	 */
	public function canConfigureProduct() {

		return $this->getProduct()->canConfigure();
	}
}
