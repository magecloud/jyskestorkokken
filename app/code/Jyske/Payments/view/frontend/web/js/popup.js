/**
 * popup
 *
 * @copyright Copyright © 2018 HeadWay. All rights reserved.
 * @author    ilya.kush@gmaul.com.com
 */

define([
    'ko',
    'uiComponent',
    'jquery',
    'Magento_Ui/js/modal/modal'
], function (ko, Component, $, modal) {
    'use strict';

    return Component.extend({
        initialize: function () {
            //initialize parent Component
            this._super();

            var options = {
                type: 'popup',
                modalClass: this.modalClass,
                responsive: true,
                innerScroll: true,
                title: this.title,
                modalLeftMargin: 45,
                // buttons: []
            };
            // console.log('1235');
            var popup = modal(options, $(this.elementId));

            // this.openPopup();

        },

        openPopup: function() {
            // console.log('123');
            $(this.elementId).modal("openModal");
        },
        
        openWindow: function () {
            window.open('https://pbs-erhverv.dk/LS/?id=0&pbs=27503360bbeffa171f95f5374d576982&dbnr=&cvr=&knmin=&knmax=','LSTilmeld','resizable=no,toolbar=no,scrollbars=no,menubar=no,location=no,status=yes,width=375,height=500');
        } 

    });
});