/**
 * requirejs-config
 *
 * @copyright Copyright © 2018 HeadWay. All rights reserved.
 * @author    ilya.kush@gmaul.com.com
 */

var config = {
    map: {
        '*': {
            'Magento_OfflinePayments/js/view/payment/method-renderer/banktransfer-method': 'Jyske_Payments/js/view/payment/method-renderer/banktransfer-method',
        }
    }
}