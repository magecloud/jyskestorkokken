<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 03.12.2018
 * Time: 14:47
 */
namespace Jyske\OfflinePayments\Block\Info;

class Checkmo extends \Magento\OfflinePayments\Block\Info\Checkmo
{

	/**
	 * @var string
	 */
	protected $_template = 'Jyske_OfflinePayments::info/checkmo.phtml';


	/**
	 * @return string
	 */
	public function toPdf()
	{
		$this->setTemplate('Jyske_OfflinePayments::info/pdf/checkmo.phtml');
		return $this->toHtml();
	}
}