module.exports = {
    jyske_default: {
        area: 'frontend',
        name: 'jyske/default',
        locale: 'da_DK',
        files: [
            'css/styles-m',
            'css/styles-l'
        ],
        dsl: 'less'
    }
};