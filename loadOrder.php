<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '5G');
error_reporting(E_ALL);

use Magento\Framework\App\Bootstrap;
require 'app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$id = 22;
$order = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface')->get($id);
$fieldsetConfig = $objectManager->create('\Magento\Framework\DataObject\Copy\Config');
$objectCopyService = $objectManager->create('\Magento\Framework\DataObject\Copy');
$addressFactory = $objectManager->create('\Magento\Customer\Api\Data\AddressInterfaceFactory');
$regionFactory = $objectManager->create('\Magento\Customer\Api\Data\RegionInterfaceFactory');
$customerFactory = $objectManager->create('\Magento\Customer\Api\Data\CustomerInterfaceFactory');

$delegateService = $objectManager->create('\Magento\Sales\Model\Order\OrderCustomerDelegate');

//$objectManager->create('\Magento\Customer\Model\Data\Address\Interceptor');
echo "<code><pre>";

foreach ($order->getAddresses() as $address){
//	var_dump($address->getData());
}




$fields = $fieldsetConfig->getFieldset('order_address', 'global');
//var_dump("--------------------------------------------------");
//var_dump($fields);




//Prepare customer data from order data if customer doesn't exist yet.
$customerData = $objectCopyService->copyFieldsetToTarget(
	'order_address',
	'to_customer',
	$order->getBillingAddress(),
	[]
);
$addresses = $order->getAddresses();
foreach ($addresses as $address) {
	$addressData = $objectCopyService->copyFieldsetToTarget(
		'order_address',
		'to_customer_address',
		$address,
		[]
	);

//	var_dump($addressData);

	/** @var AddressInterface $customerAddress */
//	var_dump($addressData);
	$customerAddress = $addressFactory->create(['data' => $addressData]);
//	var_dump(get_class_methods($customerAddress));
//	var_dump(($customerAddress->__toArray()));
//	var_dump(($customerAddress->getExtensionAttributes()));
//	var_dump(($customerAddress->getCustomAttributes()));
	switch ($address->getAddressType()) {
		case \Magento\Quote\Model\Quote\Address::ADDRESS_TYPE_BILLING:
			$customerAddress->setIsDefaultBilling(true);
			break;
		case \Magento\Quote\Model\Quote\Address::ADDRESS_TYPE_SHIPPING:
			$customerAddress->setIsDefaultShipping(true);
			break;
	}

	if (is_string($address->getRegion())) {
		/** @var RegionInterface $region */
		$region = $regionFactory->create();
		$region->setRegion($address->getRegion());
		$region->setRegionCode($address->getRegionCode());
		$region->setRegionId($address->getRegionId());
		$customerAddress->setRegion($region);
	}
	$customerData['addresses'][] = $customerAddress;
//	var_dump($customerAddress->getAddressCustomerType());
}

$customer = $customerFactory->create(['data' => $customerData]);

//$customer->save()
//var_dump(get_class($customer ));
var_dump(($customer->__toArray() ));

//var_dump($customer->getCustomAttributesCodes());
//var_dump($customerData['addresses']);
//var_dump($customerData);

//$_customerAddresses = $customer->getAddresses();
//
//foreach ($_customerAddresses as $_customerAddress) {
//	var_dump($_customerAddress->debug());
//}

var_dump($delegateService->delegateNew(22)) ;

