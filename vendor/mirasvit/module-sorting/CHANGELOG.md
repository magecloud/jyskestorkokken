## 1.1.1
*(2021-01-27)*

#### Improvements
* Default sort direction

#### Fixed
* Issue with selecting image attribute (non-default entity type id)

---

## 1.1.0
*(2020-12-30)*

#### Improvements
* Reindex sorting indexes after product change

#### Fixed
* Fixed the issue with the error: Deprecated Functionality: Array and string offset access syntax with curly braces is deprecated


---

## 1.0.58
*(2020-12-22)*

#### Fixed
* Issue with REAS API (Magento 2.3)

---

## 1.0.57
*(2020-12-07)*

#### Improvements
* Set direction for widgets

---

## 1.0.56
*(2020-12-04)*

#### Fixed
* Issue with applying sorting on custom collections
* Sorting for REST API

---

## 1.0.54
*(2020-11-27)*

#### Improvements
* Developer Mode flag

#### Fixed
* Issue with types

---

## 1.0.53
*(2020-11-24)*

#### Fixed
* Compatibility with Olegnax_LayeredNavigation
* Minor fixes

---

## 1.0.51
*(2020-11-23)*

#### Improvements
* Removed compatibility with Magento 2.1, 2.2 (PHP 7.1+)
* Improved module performance 

---

## 1.0.50
*(2020-10-12)*

#### Improvements
* Quantity in the sorting preview

---

## 1.0.49
*(2020-10-08)*

#### Features
* New ranking factor "Stock Quantity"

---

## 1.0.48
*(2020-09-23)*

#### Features
* Pre-ready sorting criteria

#### Fixed
* Score on preview page

---

## 1.0.47
*(2020-09-09)*

#### Improvements
* Ranking Factor Precision

---

## 1.0.46
*(2020-08-19)*
 
#### Refactor
* Improved module structure

---

## 1.0.45
*(2020-08-11)*

#### Fixed
* Compatibility issue with ElasticSearch

---

## 1.0.44
*(2020-07-29)*

#### Improvements
* Support of Magento 2.4

---

## 1.0.43
*(2020-06-30)*

#### Fixed
* Issue with default sorting not applied on brand pages (Mirasvit_Brand module).

---

## 1.0.42
*(2020-05-15)*

#### Fixed
* Issue with sorting by position attribute when Use Flat Catalog Product enabled
* Issue with sorting collection at the Advanced Product Feed module

#### Features
* NO_SORT flag for product collections in custom blocks.
* Ability to disable sorting for custom blocks in the configurations.

---


## 1.0.41
*(2020-04-14)*

#### Fixed
* Issue with sorting by attribute with the type boolean/dropdown ("You cannot define a correlation name '...' more than once").
* Issue with inactive criteria used for sorting in custom blocks

---


## 1.0.40
*(2020-03-27)*

#### Fixed
* Issue with widgets with random products order (Cannot use object of type Zend_Db_Expr as array. Affects only 1.0.39)

---

## 1.0.39
*(2020-03-26)*

#### Fixed
* Issue with sorting in widgets when elasticsearch is used

---

## 1.0.38
*(2020-03-23)*

#### Fixed
* Issue with default Magento order for attributes (duplicated fields in ORDER BY clause)

---


## 1.0.37
*(2020-03-16)*

#### Features
* New ranking factor "New products" based on "Set product as new from ... to ..." attribute

---


## 1.0.36
*(2020-02-28)*

#### Fixed
* Error in some cases. Cannot use object of type Zend_Db_Expr as array. Affects only 1.0.35

---

## 1.0.35
*(2020-02-27)*

#### Improvements
* Discount ranking factor calculation for configurable/bundle/grouped products

#### Fixed
* Sorting by name (only M2.3.4)
* subconditions for sort by attribute

---


## 1.0.34
*(2020-02-21)*

#### Fixed
* Apply sorting only on frontend

---

### 1.0.33
*(2020-02-12)* 

* Improve debug widget
* Added custom sorting fix
* Fixed calculation of price factor (for complex product types)

----

## 1.0.32
*(2020-02-06)*

#### Fixed
* Default sorting field
* Missing positionOrderApplied variable when global sorting applied
* Wrong product position when position sort order applied
* Bestseller score factor indexing issue
* Unable to apply sort by name

---

## 1.0.29
*(2019-12-30)*

#### Improvements
* Code refactoring

#### Fixed
*  Sort by name issue

---

## 1.0.28
*(2019-11-25)*

#### Fixed
* Elasticsearch compatibility

---

## 1.0.27
*(2019-11-21)*

#### Improvements
* Admin styles

---

## 1.0.26
*(2019-11-07)*

#### Fixed
* Disable sorting of products collection in CLI mode (prevent possible indexation issue)
* Issue during indexation Discount Ranking Factor
* Unexpected indexer dependency

---

## 1.0.25
*(2019-10-10)*

#### Fixed
* Compatibility with native ES

---

## 1.0.24
*(2019-09-02)*

#### Fixed
* Compatibility with EE Elasticsearch

---

## 1.0.23
*(2019-07-25)*

#### Improvements
* Debug mode

#### Fixed
* Possible issue with second join (temporary table)
* Popularity factor

---

## 1.0.22
*(2019-05-20)*

#### Fixed
* Issue with sorting if criteria/factor is empty
* Issue with applying custom sorting for category

---

## 1.0.21
*(2019-05-16)*

#### Improvements
* integration with Autocomplete
* Rating Factor. Use also number of ratings

---

## 1.0.20
*(2019-04-17)*

#### Improvements
* Conditions column in criteria listing
* Enabled Inline Editor
* Search results sorting

---

## 1.0.19
*(2019-04-10)*

#### Improvements
* Inline debug interface (&debug=sorting)

---

## 1.0.18
*(2019-04-08)*

#### Fixed
* Compatibility issue with Magento Enterprise

---

## 1.0.17
*(2019-04-01)*

#### Improvements
* Ability to use native "Position" as sorting attribute

---

## 1.0.16
*(2019-03-27)*

#### Improvements
* Ability to defined limits

#### Fixed
* Error during indexation of sorting index if an attribute is not set in a rating factor
* By default sort direction is set to the direction of a default criterion, even when custom direction used

---

## 1.0.15
*(2019-03-19)*

#### Fixed
* Compatibility with Magento 2.1.x

---

## 1.0.14
*(2019-03-18)*

#### Fixed
* Attribute sort direction does not change
* Saving sorting code

---

## 1.0.13
*(2019-03-11)*

#### Improvements
* Popularity Factor
* Discount Factor
* Preview interface

---

## 1.0.12
*(2019-03-07)*

#### Improvements
* Ability to use sorting for catalog widget "Catalog Products List"

#### Fixed
* Sort direction does not change

---

## 1.0.11
*(2019-03-04)*

#### Improvements
* Changed sorting interface

---

## 1.0.10
*(2019-02-22)*

#### Improvements
* Add module translation file mirasvit/module-navigation[#79]()

---

## 1.0.9
*(2019-02-11)*

#### Fixed
* System settings page not loaded after adding a new attribute to sorting

---

## 1.0.8
*(2019-01-10)*

#### Fixed
* Conflict with Mirasvit Search module: searching products fails with error (since 1.0.7)

---

## 1.0.7
*(2019-01-09)*

#### Features
* Push 'out of stock' products to the end of a list

#### Documentation
* Info about new settings

---

## 1.0.7
*(2019-01-08)*

#### Features
* Push 'out of stock' products to the end of a list 

---

## 1.0.6
*(2018-12-27)*

#### Fixed
* Compatibility with Magento 2.1

---

## 1.0.5
*(2018-12-11)*

#### Fixed
* Error during reindex mst_sorting by cron

---

## 1.0.4
*(2018-12-07)*

#### Features
* Show configurable products at top of the list #3

---

## 1.0.3
*(2018-12-06)*

#### Fixed
* Error during reindexing due to discount criterion

---

## 1.0.2
*(2018-12-06)*

#### Fixed
* Error during reindex sorting index

---

## 1.0.1
*(2018-12-05)*

#### Improvements
* Compatibility with M2.1

#### Documentation
* Added module docs
