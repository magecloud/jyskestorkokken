<?php
namespace Swissup\CustomerFieldManager\Controller\Adminhtml\Index;

class Index extends \Swissup\FieldManager\Controller\Adminhtml\Index\Index
{
    const ADMIN_RESOURCE = 'Swissup_CustomerFieldManager::index';
    const TITLE = 'Customer Fields Manager';
    const KEY_WEBSITE = 'swissup_customer_field_manager_website';
}
