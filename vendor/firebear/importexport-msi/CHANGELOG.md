1.1.0
=============
* Bugfixes:
    * The export stock source issue was fixed when the mapping feature is used.
    * Export stock source filter issue was fixed.
    * processedEntitiesCount calculate issue was fixed.
    * sku validation was added.
    * stock source qty export for existing products.
