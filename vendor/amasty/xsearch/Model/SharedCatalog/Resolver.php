<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Xsearch
 */


namespace Amasty\Xsearch\Model\SharedCatalog;

use Amasty\Xsearch\Model\ResourceModel\SharedCatalog;
use Amasty\Xsearch\Model\Di\Wrapper as SharedCatalogDiProxy;
use Magento\Company\Model\CompanyContext;
use Magento\SharedCatalog\Model\CustomerGroupManagement;
use Magento\SharedCatalog\Model\Config;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Resolver
{
    /**
     * @var CustomerGroupManagement
     */
    private $customerGroupManagement;

    /**
     * @var Config
     */
    private $sharedConfig;

    /**
     * @var CompanyContext
     */
    private $companyContext;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var SharedCatalog
     */
    private $sharedCatalog;

    public function __construct(
        StoreManagerInterface $storeManager,
        SharedCatalogDiProxy $customerGroupManagement,
        SharedCatalogDiProxy $sharedConfig,
        SharedCatalogDiProxy $companyContext,
        SharedCatalog $sharedCatalog
    ) {
        $this->storeManager = $storeManager;
        $this->sharedCatalog = $sharedCatalog;
        $this->customerGroupManagement = $customerGroupManagement;
        $this->sharedConfig = $sharedConfig;
        $this->companyContext = $companyContext;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isEnabled()
    {
        $customerGroupId = $this->companyContext->getCustomerGroupId();
        $website = $this->storeManager->getWebsite()->getId();

        return $this->sharedConfig->isActive(ScopeInterface::SCOPE_WEBSITE, $website)
            && !$this->customerGroupManagement->isMasterCatalogAvailable($customerGroupId);
    }

    /**
     * @param array $searchResponse
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resolve($searchResponse = [])
    {
        $customerGroupId = $this->companyContext->getCustomerGroupId();
        $correctIds = $this->sharedCatalog->getCatalogItems($customerGroupId);

        $searchResponse['products'] = array_intersect_key(
            $searchResponse['products'],
            array_flip($correctIds)
        );
        $searchResponse['hits'] = count($searchResponse['products']);

        return $searchResponse;
    }
}
