<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_LabelGraphQl
 */


declare(strict_types=1);

namespace Amasty\LabelGraphQl\Model\Resolver;

use Magento\Customer\Model\Session;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\UrlInterface;

class LabelProvider implements ResolverInterface
{
    /**
     * @var \Amasty\Label\Model\LabelViewer
     */
    private $labelViewer;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var Session
     */
    private $session;

    public function __construct(
        \Amasty\Label\Model\LabelViewer $labelViewer,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Psr\Log\LoggerInterface $logger,
        Session $session
    ) {
        $this->labelViewer = $labelViewer;
        $this->logger = $logger;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->session = $session;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws \Exception
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['productIds']) || !is_array($args['productIds']) || !isset($args['mode'])) {
            throw new GraphQlNoSuchEntityException(__('Wrong parameter provided.'));
        }

        try {
            $result = [];
            $storeId = (int)$context->getExtensionAttributes()->getStore()->getId();
            $this->session->setCustomerId($context->getUserId());
            foreach ($this->getProducts($args['productIds'], $storeId) as $product) {
                $labels = [];
                foreach ($this->labelViewer->getAppliedLabels($product, $args['mode']) as $label) {
                    /** @var \Amasty\Label\Model\Labels  $label */
                    $data = $label->getData();
                    $data['product_id'] = $label->getAppliedProductId();
                    $data['size'] = $label->getValue('image_size');
                    $data['txt'] = $label->getText();
                    $data['image'] = $this->getRelativePath($label->getImageSrc(), $context);
                    $data['position'] = $label->getCssClass();
                    $data['style'] = $label->getStyle();

                    $labels[] = $data;
                }

                $result[$product->getId()]['items'] = $labels;
            }

            return $result;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new GraphQlNoSuchEntityException(__('Something went wrong.'));
        }
    }

    /**
     * @param string $src
     * @param $context
     *
     * @return string|string[]
     */
    protected function getRelativePath(string $src, $context)
    {
        $baseUrl = $context->getExtensionAttributes()->getStore()->getBaseUrl();

        return str_replace($baseUrl, '', $src);
    }

    /**
     * @param array $productIds
     * @param int $storeId
     *
     * @return \Magento\Framework\DataObject[]
     */
    protected function getProducts(array $productIds, int $storeId)
    {
        $collection = $this->productCollectionFactory->create()
            ->addStoreFilter($storeId)
            ->addPriceData()
            ->addIdFilter($productIds);

        return $collection->getItems();
    }
}
