<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_LabelGraphQl
 */


declare(strict_types=1);

namespace Amasty\LabelGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;

class Setting implements ResolverInterface
{
    /**
     * @var \Amasty\Label\Helper\Config
     */
    private $settings;

    public function __construct(
        \Amasty\Label\Helper\Config $settings
    ) {
        $this->settings = $settings;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws \Exception
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        try {
            return [
                'product_container' => $this->settings->getProductContainerPath(),
                'category_container' => $this->settings->getCategoryContainerPath(),
                'max_labels' => $this->settings->getMaxLabels(),
                'show_several_on_place' => (int)$this->settings->isShowSeveralOnPlace(),
                'labels_alignment' => $this->settings->getLabelAlignment(),
                'margin_between' => $this->settings->getMarginBetween()
            ];
        } catch (\Exception $e) {
            throw new GraphQlNoSuchEntityException(__('Something went wrong during getting settings data'));
        }
    }
}
